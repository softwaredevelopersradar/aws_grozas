﻿using Bearing;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using OEM;
using OxyPlot;
using RadarZorkiRControl;
using Rodnik;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using WPFControlConnection;
using ZorkiR;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        System.Timers.Timer tmrZorkiRCheck = new System.Timers.Timer();

        System.Timers.Timer tmrZorkiRAngCheck = new System.Timers.Timer();

        private void ZorkiRControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (udpZorkiR != null && udpZorkiR.IsConnected)
                {
                    DisconnectZorkiR();

                    if (udpZorkiRAng != null && udpZorkiRAng.IsConnected)
                        DisconnectZorkiRAng();
                }
                    
                else
                {
                    ConnectZorkiR();
                    ConnectZorkiRAng();
                }
                    
            }

            catch
            { }
        }
        
        
        private void ConnectPortZorkiR(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionZorkiR = ConnectionStates.Connected;
            InitializeTimerZorkiRCheck();
        }

        private void DisconnectPortZorkiR(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionZorkiR = ConnectionStates.Disconnected;
            DestructorTimerZorkiRCheck();
        }

        private void UdpZorkiR_OnReceiveMarkPOI(object sender, MarkPOIEventArgs e)
        {
            try
            {
                //ZorkiRMark mark = DecodeMarkZorkiR(e);

                //if (mark != null)
                //{
                //    //if (mark.Flag == FlagRodnik.Reset)
                //    //    DeleteRodnikTarget(mark.ID);
                //    //else
                //        if (zorkiRTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                //        zorkiRTargets.Add(new ZorkiRTarget { ID = mark.ID });

                //    zorkiRTargets.Where(x => x.ID == mark.ID).FirstOrDefault().zorkiRMarks.Add(mark);

                //    UpdateZorkiRTargetsMap(zorkiRTargets);


                //    UpdateTableZorkiR();
                //}


            }
            catch
            { }
        }


       

        private void UdpZorkiR_OnReceiveMarkVOI(object sender, MarkVOIEventArgs e)
        {
            try
            {               
                ZorkiRMark mark = DecodeMarkZorkiR(e);

                if (mark != null)
                {
                    if (mark.State == StateZorkiR.Reset)
                    {
                        ZorkiRTarget del = zorkiRTargets.Where(x => x.ID == mark.ID).FirstOrDefault();
                        zorkiRTargets.Remove(del);                        
                    }
                    else
                    {
                        if (zorkiRTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                            zorkiRTargets.Add(new ZorkiRTarget { ID = mark.ID });

                        zorkiRTargets.Where(x => x.ID == mark.ID).FirstOrDefault().zorkiRMarks.Add(mark);
                    }

                    //CheckVoiceAlarm();
                }
            }
            catch
            {
                
            }

        }
        

        private ZorkiRMark DecodeMarkZorkiR(MarkVOIEventArgs e)
        {
            ZorkiRMark zorkiR = null;

            try
            {
                zorkiR = new ZorkiRMark();


                zorkiR.StartPosition = new GrozaSModelsDBLib.Coord()
                {
                    Latitude = mainWindowViewModel.LocalPropertiesVM.Lemt.Latitude,
                    Longitude = mainWindowViewModel.LocalPropertiesVM.Lemt.Longitude
                };


                zorkiR.ID = (short)e.TrackNumber;
                zorkiR.Azimuth = (float)e.Azimuth;
                zorkiR.Range = (float)e.Range;
                zorkiR.Speed = (float)e.GroundSpeed;
                zorkiR.RadialSpeed = (float)e.RadialSpeed;
                zorkiR.Aspect = (float)e.Aspect;
               

                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                zorkiR.Loctime = start.AddMilliseconds(e.RegTime).ToLocalTime();


                zorkiR.State = (StateZorkiR)e.State;
                zorkiR.Class = (ClassZorkiR)e.TargetType;


            }
            catch { }

            return zorkiR;
        }

        private void DeleteZorkiRTarget(short ID)
        {
            try
            {
                zorkiRTargets.Remove(zorkiRTargets.Where(x => x.ID == ID).FirstOrDefault());
            }
            catch { }
        }

        private void ClearZorkiRTarget()
        {
            try
            {
                zorkiRTargets.Clear();
            }
            catch { }
        }

        private void UpdateTableZorkiR()
        {
            List<ZorkiRModel> listZorkiRModels = new List<ZorkiRModel>();

            foreach (var zor in zorkiRTargets)
            {
                listZorkiRModels.Add(new ZorkiRModel()
                {
                    Id = zor.ID,
                    Azimuth = zor.zorkiRMarks.Last().Azimuth,
                    Range = zor.zorkiRMarks.Last().Range,
                    Aspect = zor.zorkiRMarks.Last().Aspect,
                    TimeReg = zor.zorkiRMarks.Last().Loctime,
                    TargetType = Enum.GetName(typeof(ClassZorkiR), zor.zorkiRMarks.Last().Class),
                    Speed = zor.zorkiRMarks.Last().Speed,
                    RadialSpeed = zor.zorkiRMarks.Last().RadialSpeed,
                    State = Enum.GetName(typeof(StateZorkiR), zor.zorkiRMarks.Last().State),

                });
            }

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucZorkiR.ListZorkiRModel = listZorkiRModels;
                  
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void DeleteRecZorkiR(short Id)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucZorkiR.DeleteRecZorkiR = Id;
                }), DispatcherPriority.Background);
                
            }
            catch
            { }
        }

        private void InitializeTimerZorkiRCheck()
        {
            tmrZorkiRCheck.Elapsed += TickTimerZorkiRCheck;
            tmrZorkiRCheck.Interval = 3000;

            tmrZorkiRCheck.AutoReset = true;
            tmrZorkiRCheck.Enabled = true;
        }

        private void InitializeTimerZorkiRAngCheck()
        {
            tmrZorkiRAngCheck.Elapsed += TickTimerZorkiRAngCheck;
            tmrZorkiRAngCheck.Interval = 5000;

            tmrZorkiRAngCheck.AutoReset = true;
            tmrZorkiRAngCheck.Enabled = true;
        }

        private void DestructorTimerZorkiRCheck()
        {
            tmrZorkiRCheck.Elapsed -= TickTimerZorkiRCheck;

            tmrZorkiRCheck.Enabled = false;
        }

        private void DestructorTimerZorkiRAngCheck()
        {
            tmrZorkiRAngCheck.Elapsed -= TickTimerZorkiRAngCheck;

            tmrZorkiRAngCheck.Enabled = false;
        }

        void TickTimerZorkiRCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (zorkiRTargets != null)
                {

                    
                    //foreach (var item in zorkiRTargets.Where(x => x.zorkiRMarks.Last().Loctime.AddSeconds(mainWindowViewModel.LocalPropertiesVM.Lemt.LifeTime) > DateTime.Now).ToList())
                    //    zorkiRTargets.Remove(item);

                    //UpdateTableZorkiR();
                    //UpdateZorkiRTargetsMap(zorkiRTargets);
                    //.


                    //List<ZorkiRTarget> zorkiRTemp = new List<ZorkiRTarget>();

                    TimeSpan tt = new TimeSpan(0, 0, mainWindowViewModel.LocalPropertiesVM.Lemt.LifeTime * (-1));
                    var zorkiRTemp = zorkiRTargets.Where(x => x.zorkiRMarks.Last().Loctime - DateTime.Now < tt).ToList();

                    foreach (var t in zorkiRTemp)
                        DeleteZorkiRTarget(t.ID);

                    UpdateTableZorkiR();
                    UpdateZorkiRTargetsMap(zorkiRTargets);

                    if (zorkiRTargets!= null && zorkiRTargets.Count > 0)
                    {
                        alert?.PlaySound2(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileRadar, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
                    }
                }
            }
            catch 
            { }

        }

        void TickTimerZorkiRAngCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (udpZorkiRAng != null)
            //{
            //    var jammer = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);

            //    if (jammer != null)
            //        udpZorkiRAng.SetAngle(jammer.Coordinates.Latitude, jammer.Coordinates.Longitude, 361);

            //    //if (jammer != null)
            //    //    udpZorkiRAng.SetCoordinateCourseAngle(jammer.Coordinates.Latitude, jammer.Coordinates.Longitude, (short)(mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle+ 
            //    //                                                                                     mainWindowViewModel.LocalPropertiesVM.EOM.ErrorDeg));

            //}

            SendZorkiRCoord();

        }

        private void SendZorkiRCoord(short angle = 361)
        {
            if (udpZorkiRAng != null)
            {
                var jammer = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);

                if (jammer != null)
                    udpZorkiRAng.SetAngle(jammer.Coordinates.Latitude, jammer.Coordinates.Longitude, angle);

                //if (jammer != null)
                //    udpZorkiRAng.SetCoordinateCourseAngle(jammer.Coordinates.Latitude, jammer.Coordinates.Longitude, (short)(mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle+ 
                //  
            }
        }


        private void ucZorkiR_OnDeleteRecord(object sender, RadarZorkiRControl.ZorkiRModel e)
        {
            DeleteZorkiRTarget(e.Id);
           
            UpdateTableZorkiR();

            UpdateZorkiRTargetsMap(zorkiRTargets);
        }

        private void ucZorkiR_OnClearRecords(object sender, EventArgs e)
        {
            ClearZorkiRTarget();

            UpdateTableZorkiR();

            UpdateZorkiRTargetsMap(zorkiRTargets);
        }

        private void ucZorkiR_OnCentering(object sender, RadarZorkiRControl.ZorkiRModel e)
        {
        
            if (zorkiRTargets.Where(x => x.ID == e.Id).FirstOrDefault() != null)
                mainWindowViewModel.LocationCenter = zorkiRTargets.Where(x => x.ID == e.Id).FirstOrDefault().zorkiRMarks.Last().Coordinate;
        }


        private void UdpZorkiRAng_OnConnectPort(object sender, EventArgs e)
        {
            InitializeTimerZorkiRAngCheck();
        }

        private void UdpZorkiRAng_OnDisconnectPort(object sender, EventArgs e)
        {
            DestructorTimerZorkiRAngCheck();
        }
    }
}
