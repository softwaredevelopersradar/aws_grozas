﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;

using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using ZorkiR;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public UDPZorkiR udpZorkiR;

        public UDPZorkiRAng udpZorkiRAng;

        ObservableCollection<ZorkiRTarget> zorkiRTargets = new ObservableCollection<ZorkiRTarget>();

        private void ConnectZorkiR()
        {
            if (udpZorkiR != null)
                DisconnectZorkiR();

            udpZorkiR = new UDPZorkiR();

            udpZorkiR.OnConnectPort += ConnectPortZorkiR;
            udpZorkiR.OnDisconnectPort += DisconnectPortZorkiR;

            udpZorkiR.OnReceiveMarkPOI += UdpZorkiR_OnReceiveMarkPOI;
            udpZorkiR.OnReceiveMarkVOI += UdpZorkiR_OnReceiveMarkVOI;
            udpZorkiR.OnReceiveCoord += UdpZorkiR_OnReceiveCoord;

            udpZorkiR.Connect(mainWindowViewModel.LocalPropertiesVM.Lemt.IpAddressLocal, mainWindowViewModel.LocalPropertiesVM.Lemt.PortLocal,
                                   mainWindowViewModel.LocalPropertiesVM.Lemt.IpAddressRemoute, mainWindowViewModel.LocalPropertiesVM.Lemt.PortRemoute);



            ClearZorkiTR();

        }

        

        private void DisconnectZorkiR()
        {

            if (udpZorkiR != null)
            {
                udpZorkiR.Disconnect();

                udpZorkiR.OnConnectPort -= ConnectPortZorkiR;
                udpZorkiR.OnDisconnectPort -= DisconnectPortZorkiR;

                udpZorkiR.OnReceiveMarkPOI -= UdpZorkiR_OnReceiveMarkPOI;
                udpZorkiR.OnReceiveMarkVOI -= UdpZorkiR_OnReceiveMarkVOI;
                udpZorkiR.OnReceiveCoord -= UdpZorkiR_OnReceiveCoord;


                udpZorkiR = null;

                ClearZorkiTR();
            }

        }


        private void ConnectZorkiRAng()
        {
            if (udpZorkiRAng != null)
                DisconnectZorkiRAng();

            udpZorkiRAng = new UDPZorkiRAng();

            udpZorkiRAng.OnConnectPort += UdpZorkiRAng_OnConnectPort;
            udpZorkiRAng.OnDisconnectPort += UdpZorkiRAng_OnDisconnectPort;

            


            udpZorkiRAng.Connect(mainWindowViewModel.LocalPropertiesVM.Lemt.IpAddressLocal, mainWindowViewModel.LocalPropertiesVM.Lemt.PortLocal,
                                   mainWindowViewModel.LocalPropertiesVM.Lemt.AddRemoteIPAddress, mainWindowViewModel.LocalPropertiesVM.Lemt.AddRemotePort);

        }

        private void UdpZorkiR_OnReceiveCoord(object sender, ZorkiR.Events.CoordEventArgs e)
        {                        
            UpdateZorkiRPosition(e.Latitude, e.Longitude);
        }

        private void DisconnectZorkiRAng()
        {

            if (udpZorkiRAng != null)
            {
                udpZorkiRAng.Disconnect();
                udpZorkiRAng.OnConnectPort -= UdpZorkiRAng_OnConnectPort;

                udpZorkiRAng = null;

            }

        }

        

        private void ClearZorkiTR()
        {
            try 
            {
                zorkiRTargets.Clear();
                //UpdateZorkiRTargetsMap(zorkiRTargets);
            }
            catch { }
        }

    }
}

