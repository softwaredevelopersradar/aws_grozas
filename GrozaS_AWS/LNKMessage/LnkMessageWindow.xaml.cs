﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UINotificationWindow;

namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for LnkMessageWindow.xaml
    /// </summary>
    public partial class LnkMessageWindow : Window
    {
        public LnkMessageWindow(TypeMessage type, bool isInAuto)
        {
            InitializeComponent();
            NotificationWind.MessageType = type;
            NotificationWind.IsInAuto = isInAuto;
            NotificationWind.OnMyAnswer += NotificationWind_OnMyAnswer;
        }

        

        public event EventHandler OnClosingWindow;
        public event EventHandler<AnswerResult> OnMyAnswer;

        #region DP
        public LinkedMessageModel Request
        {
            get { return (LinkedMessageModel)GetValue(RequestProperty); }
            set { SetValue(RequestProperty, value); }
        }

        public static readonly DependencyProperty RequestProperty =
            DependencyProperty.Register("Request", typeof(LinkedMessageModel),
            typeof(LnkMessageWindow));


        public LinkedMessageModel Answer
        {
            get { return (LinkedMessageModel)GetValue(AnswerProperty); }
            set { SetValue(AnswerProperty, value); }
        }

        public static readonly DependencyProperty AnswerProperty =
            DependencyProperty.Register("Answer", typeof(LinkedMessageModel),
            typeof(LnkMessageWindow));


        public bool IsInAuto
        {
            get { return (bool)GetValue(IsInAutoProperty); }
            set { SetValue(IsInAutoProperty, value); }
        }

        public static readonly DependencyProperty IsInAutoProperty =
            DependencyProperty.Register("IsInAuto", typeof(bool),
            typeof(LnkMessageWindow));

        #endregion

        private void LnkMessWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = true;
            OnClosingWindow?.Invoke(this, null);
        }

        private void NotificationWind_OnMyAnswer(object sender, AnswerResult e)
        {
            OnMyAnswer?.Invoke(this, e);
        }


        public void SetAnswerStatus(StatusMessage status)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                NotificationWind.SetAnswerStatus(status);

            });
            
        }


        public void ConfirmRequest()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                NotificationWind.ConfirmRequest();

            });
            
        }
    }
}
