﻿using GrozaSModelsDBLib;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using UINotificationWindow;
using UISource;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private LnkMessageWindow NotificationIRequestWindow;
        private LnkMessageWindow NotificationIAnswerWindow;

        Uri requestUri = new Uri("pack://application:,,,/Images/time.png", UriKind.RelativeOrAbsolute);
        Uri answerUri = new Uri("pack://application:,,,/Images/bell.png", UriKind.RelativeOrAbsolute);

        private void InitNotificationIRequestWindow()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIRequestWindow != null && NotificationIRequestWindow.IsVisible)
                {
                    NotificationIRequestWindow.Close();
                }

                NotificationIRequestWindow = new LnkMessageWindow(TypeMessage.IRequest, false);
                NotificationIRequestWindow.Title = "REQUEST";
                NotificationIRequestWindow.Icon = BitmapFrame.Create(requestUri);  
                SetBinding(NotificationIRequestWindow, TypeMessage.IRequest);

                NotificationIRequestWindow.OnClosingWindow += NotificationIRequestWindow_OnClosingWindow;
                NotificationIRequestWindow.Topmost = false;
                NotificationIRequestWindow.ShowActivated = false;
                NotificationIRequestWindow.Show();
            });
        }


        private void NotificationIRequestWindow_OnClosingWindow(object sender, EventArgs e)
        {
            BindingOperations.ClearAllBindings(NotificationIRequestWindow);
            NotificationIRequestWindow = null;
        }


        private void InitNotificationIAnswerWindow(bool isInAuto)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIAnswerWindow != null && NotificationIAnswerWindow.IsVisible)
                {
                    NotificationIAnswerWindow.Close();
                }

                NotificationIAnswerWindow = new LnkMessageWindow(TypeMessage.IAnswer, isInAuto);
                NotificationIAnswerWindow.Title = "ANSWER";
                NotificationIAnswerWindow.Icon = BitmapFrame.Create(answerUri);
                SetBinding(NotificationIAnswerWindow, TypeMessage.IAnswer);

                NotificationIAnswerWindow.OnClosingWindow += NotificationIAnswerWindow_OnClosingWindow;
                NotificationIAnswerWindow.OnMyAnswer += NotificationIAnswerWindow_OnMyAnswer;
                NotificationIAnswerWindow.Topmost = false;
                NotificationIAnswerWindow.ShowActivated = false;
                NotificationIAnswerWindow.Show();

                NotificationIAnswerWindow.ConfirmRequest();
            });
        }


        private void NotificationIAnswerWindow_OnClosingWindow(object sender, EventArgs e)
        {
            BindingOperations.ClearAllBindings(NotificationIAnswerWindow);
            NotificationIAnswerWindow = null;
        }


        private void NotificationIAnswerWindow_OnMyAnswer(object sender, AnswerResult e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (e.Result)
                {
                    SendResultExecutiveBearing(mainWindowViewModel.ExBearingSendAnswer, TypeQuery.ANSWERYES);
                    NotificationIAnswerWindow.SetAnswerStatus(StatusMessage.Accept);
                }
                else
                {
                    SendResultExecutiveBearing(mainWindowViewModel.ExBearingSendAnswer, TypeQuery.ANSWERNO);
                    NotificationIAnswerWindow.SetAnswerStatus(StatusMessage.Decline);
                }
            });
        }


        private void StatusAutoAnswer(StatusMessage status)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIAnswerWindow == null)
                    return;
                NotificationIAnswerWindow.SetAnswerStatus(status);
            });
        }


        private void ConfirmRequest()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIRequestWindow == null)
                    return;
                NotificationIRequestWindow.ConfirmRequest();
            });
        }


        private void AcceptAnswer()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIRequestWindow == null)
                    return;
                NotificationIRequestWindow.SetAnswerStatus(StatusMessage.Accept);
            });
        }

        private void DeclineAnswer()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (NotificationIRequestWindow == null)
                    return;
                NotificationIRequestWindow.SetAnswerStatus(StatusMessage.Decline);
            });

        }


        private void SuggestNewTarget()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {

                if (NotificationIRequestWindow == null)
                    return;
                NotificationIRequestWindow.SetAnswerStatus(StatusMessage.Wrong);
            });

        }


        private void ShowSentExecutingBearingRequest(Models.SourceDF source, TypeQuery typeQuery)
        {
            try
            {
                var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own);
                var linkedStation = lJammerStation.Find(x => x.Role == StationRole.Linked);
                mainWindowViewModel.ExBearingSendRequest = new Models.ExBearing() { StationNumber = (byte)ownStation.Id, Frequency = source.Track.Last().Frequency, Bearing = source.Track.Last().Bearings.Where(x => x.Jammer == ownStation.Id).FirstOrDefault().Bearing, TimeAsk = DateTime.Now, ID = source.ID, Time = source.Track.Last().Time };
                mainWindowViewModel.ExBearingReceiveAnswer = new Models.ExBearing() { StationNumber = (byte)linkedStation.Id };
                InitNotificationIRequestWindow();
            }
            
            catch
            { }
        }


        private void ShowSentExecutingBearingRequest(RecDroneModel source, TypeQuery typeQuery)
        {
            try
            {
                var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own);
                var linkedStation = lJammerStation.Find(x => x.Role == StationRole.Linked);
                mainWindowViewModel.ExBearingSendRequest = new Models.ExBearing() { StationNumber = (byte)ownStation.Id, Frequency = source.Frequency, Bearing = source.BearingOwn, TimeAsk = DateTime.Now, ID = source.ID, Time = source.TimeUpdate };
                mainWindowViewModel.ExBearingReceiveAnswer = new Models.ExBearing() { StationNumber = (byte)linkedStation.Id };
                InitNotificationIRequestWindow();
            }
            

            catch { }
        }



        private void SetBinding(LnkMessageWindow sender, TypeMessage type)
        {
            var myConverter = new ExBearingToMessageConverter();

            Binding bindAnswer = new Binding();
            bindAnswer.Source = mainWindowViewModel;
            bindAnswer.Mode = BindingMode.TwoWay;
            bindAnswer.Converter = myConverter;

            Binding bindRequest = new Binding();
            bindRequest.Source = mainWindowViewModel;
            bindRequest.Mode = BindingMode.TwoWay;
            bindRequest.Converter = myConverter;

            switch (type)
            {
                case TypeMessage.IAnswer:
                    bindAnswer.Path = new PropertyPath("ExBearingSendAnswer");
                    bindRequest.Path = new PropertyPath("ExBearingReceiveRequest");
                    break;
                case TypeMessage.IRequest:
                    bindAnswer.Path = new PropertyPath("ExBearingReceiveAnswer");
                    bindRequest.Path = new PropertyPath("ExBearingSendRequest");
                    break;
            }

            Binding bindIsInAuto = new Binding();
            bindIsInAuto.Source = mainWindowViewModel;
            bindIsInAuto.Mode = BindingMode.OneWay;
            bindIsInAuto.Path = new PropertyPath("AutoUpdateSource");

            sender.SetBinding(LnkMessageWindow.AnswerProperty, bindAnswer);
            sender.SetBinding(LnkMessageWindow.RequestProperty, bindRequest);
            sender.SetBinding(LnkMessageWindow.IsInAutoProperty, bindIsInAuto);
        }

    }
}
