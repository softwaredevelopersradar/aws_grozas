﻿using GrozaS_AWS.Models;
using System.Windows;
using UsrpLib;

namespace GrozaS_AWS
{
    

    public partial class MainWindow : Window
    {
        private SpoofGenerator ClientSpoofing;

        TrackSpoofing trackSpoofingDrone = new TrackSpoofing();

        private void ConnectSpoofingSerever()
        {
            try
            {
                ClientSpoofing = new SpoofGenerator(mainWindowViewModel.LocalPropertiesVM.SS.Port,
                                                    mainWindowViewModel.LocalPropertiesVM.SS.IpAddress,
                                                    0,3);

                ClientSpoofing.ConnectNet += ClientSpoofing_ConnectNet;
                ClientSpoofing.DisconnectNet += ClientSpoofing_DisconnectNet;
                ClientSpoofing.ConnectionFailed += ClientSpoofing_ConnectionFailed;
                ClientSpoofing.LostGenerator += ClientSpoofing_LostGenerator;
                ClientSpoofing.SendCmd += ClientSpoofing_SendCmd;
                ClientSpoofing.ReplyCommand += ClientSpoofing_ReplyCommand;
                ClientSpoofing.ReplyTrueCoords += ClientSpoofing_ReplyTrueCoords;
                ClientSpoofing.ReplyFakeCoords += ClientSpoofing_ReplyFakeCoords;               
                ClientSpoofing.ReplySpoofModeDetailed += ClientSpoofing_ReplySpoofModeDetailed;
                ClientSpoofing.ReplySimulatedSats += ClientSpoofing_ReplySimulatedSats;
                ClientSpoofing.ReplySpoofMode += ClientSpoofing_ReplySpoofMode;

                ClientSpoofing.Connect(mainWindowViewModel.LocalPropertiesVM.SS.Port,
                                                    mainWindowViewModel.LocalPropertiesVM.SS.IpAddress);

            }

            catch { }

        }

        

        private void DisconnectSpoofingSerever()
        {
            try
            {
                ClientSpoofing.Disconnect();

                ClientSpoofing.ConnectNet -= ClientSpoofing_ConnectNet;
                ClientSpoofing.DisconnectNet -= ClientSpoofing_DisconnectNet;
                ClientSpoofing.ConnectionFailed -= ClientSpoofing_ConnectionFailed;
                ClientSpoofing.LostGenerator -= ClientSpoofing_LostGenerator;
                ClientSpoofing.SendCmd -= ClientSpoofing_SendCmd;
                ClientSpoofing.ReplyCommand -= ClientSpoofing_ReplyCommand;
                ClientSpoofing.ReplyTrueCoords -= ClientSpoofing_ReplyTrueCoords;
                ClientSpoofing.ReplyFakeCoords -= ClientSpoofing_ReplyFakeCoords;
                ClientSpoofing.ReplySpoofModeDetailed -= ClientSpoofing_ReplySpoofModeDetailed;
                ClientSpoofing.ReplySimulatedSats -= ClientSpoofing_ReplySimulatedSats;
                ClientSpoofing.ReplySpoofMode -= ClientSpoofing_ReplySpoofMode;

                ClientSpoofing = null;
            }

            catch { }
        }


    }
}