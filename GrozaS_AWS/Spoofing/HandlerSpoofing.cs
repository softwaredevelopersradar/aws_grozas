﻿
using AwsToConsoleTransferLib;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using UISpoof.Classes;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        System.Timers.Timer tmrSpoofing = new System.Timers.Timer();
       
        private void ClientSpoofing_ReplySpoofModeDetailed(object sender, AwsToConsoleTransferLib.DetailedStateReplyEventArgs e)
        {
            spoofingWindow.Mode = (EStatus)e.State;
            

            string sat = GetStringSatelites(e.Satellites);
            sat = sat.Replace('T', 'G');

            spoofingWindow.SatelitesGPS = "";
            spoofingWindow.SatelitesGPS = (spoofingWindow.Mode== EStatus.Stop)?"":sat;


            AnalizeSpoofMode();

            
        }


        private void ClientSpoofing_ReplySpoofMode(object sender, AwsToConsoleTransferLib.StateReplyEventArgs e)
        {
            spoofingWindow.Mode = (EStatus)e.State;
            AnalizeSpoofMode();

           
        }


        private void ClientSpoofing_ReplySimulatedSats(object sender, AwsToConsoleTransferLib.SimulatedSatsEventArgs e)
        {
            string sat = GetStringSatelites(e.Satellites);

            switch (e.Type)
            {
                case CodesGnssTypes.GPS:
                    sat = sat.Replace('T', 'G');
                    spoofingWindow.SatelitesGPS = "";
                    spoofingWindow.SatelitesGPS = sat;
                    break;

                case CodesGnssTypes.GLONASS:
                    sat = sat.Replace('T', 'R');
                    spoofingWindow.SatelitesGLONASS = "";
                    spoofingWindow.SatelitesGLONASS = sat;
                    break;

                //3=beidou
                case CodesGnssTypes.GPS_GLONASS:
                    sat = sat.Replace('T', 'C');
                    spoofingWindow.SatellitesBeidou = "";
                    spoofingWindow.SatellitesBeidou = sat;
                    break;
                //4=galileo
                case CodesGnssTypes.GPS_GLONASS_2:
                    sat = sat.Replace('T', 'E');
                    spoofingWindow.SatellitesGalileo = "";
                    spoofingWindow.SatellitesGalileo = sat;
                    break;

                default:
                    break;
            }


            
        }


        private string GetStringSatelites(List<byte> Satellites)
        {
            string sat = "";

            if (Satellites != null && Satellites.Count > 0)
            {
                foreach (var s in Satellites)
                    sat += "T" + s.ToString() + "  ";
            }

            return sat;

        }

        private void StopGenerationClear()
        {
            if (spoofingWindow.Mode == EStatus.Stop)
            {
                trackSpoofingDrone.Coordinate.Clear();

                trackSpoofingDrone.Angle = -1;
                UpdateSpoofingTrack(trackSpoofingDrone);
            }
        }


        private void AnalizeSpoofMode()
        {
            mainWindowViewModel.ActiveSpoofing = Convert.ToBoolean(spoofingWindow.Mode);

            //AnalizeMode((EStatus)e.State);

            AnalizeMode(spoofingWindow.Mode);

            StopGenerationClear();

            Log.SetModeSpoofing(mainWindowViewModel.ActiveSpoofing, spoofingWindow.SatelitesGPS + spoofingWindow.SatelitesGLONASS + spoofingWindow.SatellitesBeidou + spoofingWindow.SatellitesGalileo, mainWindowViewModel.StartSpoofingTime);

        }

        private void ClientSpoofing_ReplyFakeCoords(object sender, AwsToConsoleTransferLib.ParamsFakeEventArgs e)
        {
            if (spoofingWindow.Mode != EStatus.Stop)
            {
                SimulationParam simulationParam = ConvertToSimulatationParam((float)e.Coords.LatitudeDeg, (float)e.Coords.LongitudeDeg,
                                                                             e.Coords.HeadingDeg, e.Coords.Speed,
                                                                             e.Coords.Elevation, 0);

                

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    spoofingWindow.SimulationParamOutput = simulationParam;
                });

                trackSpoofingDrone.Coordinate.Add(new Coord()
                {
                    Latitude = simulationParam.Coordinate.Latitude,
                    Longitude = simulationParam.Coordinate.Longitude

                });

                trackSpoofingDrone.Angle = simulationParam.Angle;
                UpdateSpoofingTrack(trackSpoofingDrone);
            }

        }

        private void ClientSpoofing_ReplyTrueCoords(object sender, AwsToConsoleTransferLib.ParamsRealEventArgs e)
        {
            SimulationParam simulationParam = ConvertToSimulatationParam((float)e.Coords.LatitudeDeg, (float)e.Coords.LongitudeDeg,
                                                                         e.Coords.HeadingDeg, e.Coords.Speed,
                                                                         e.Coords.Elevation, CodeRangeBack(e.Coords.RangeGate));


            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                spoofingWindow.SimulationParamInput = simulationParam;
            });

            
        }



        private void ClientSpoofing_ReplyCommand(object sender, AwsToConsoleTransferLib.BaseReplyEventArgs e)
        {
           // throw new System.NotImplementedException();
        }

        private void ClientSpoofing_SendCmd(object sender, byte e)
        {
            //throw new System.NotImplementedException();
        }



        private void ClientSpoofing_LostGenerator(object sender, System.EventArgs e)
        {
           // throw new System.NotImplementedException();
        }

        private void ClientSpoofing_ConnectionFailed(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionSpoofing = WPFControlConnection.ConnectionStates.Disconnected;
        }

        private void ClientSpoofing_DisconnectNet(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionSpoofing = WPFControlConnection.ConnectionStates.Disconnected;
        }

        private void ClientSpoofing_ConnectNet(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionSpoofing = WPFControlConnection.ConnectionStates.Connected;

            //Thread.Sleep(1500);

            //SendGetDetailModeSpoofing();

            Thread.Sleep(200);

            SendGetParamSpoofing();
        }

        private void SpoofingConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ClientSpoofing != null)
                    DisconnectSpoofingSerever();
                else
                    ConnectSpoofingSerever();
            }

            catch { }
        }


        private SimulationParam ConvertToSimulatationParam(float latitude, float longitude, 
                                                           short angle, float speed, 
                                                           short elevation, byte range)
        {
            SimulationParam simulationParam = new SimulationParam();
            try
            {
                simulationParam.Coordinate.Latitude = latitude;
                simulationParam.Coordinate.Longitude = longitude;
                
                simulationParam.Angle = angle;
                simulationParam.Speed = speed;
                simulationParam.Elevation = elevation;

                simulationParam.Range = range;
            }
            catch { }
            
            return simulationParam;
        }



        private bool SendSetModeSpoofing(bool active)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                if (active)
                    ClientSpoofing.SpoofingOn();
                else
                    ClientSpoofing.SpoofingOff();


                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool SendSetModeSpoofing(bool active, ESpoofSystem eSpoof)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                if (active && (eSpoof != ESpoofSystem.NONE))
                    ClientSpoofing.SpoofingOn((CodesGnssTypes)(byte)eSpoof);
                else
                    ClientSpoofing.SpoofingOff();


                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool SendSetModeSpoofing(bool active, byte NumberAmplifier)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                if (active )
                    ClientSpoofing.SpoofingOn(CodesGnssTypes.GPS, NumberAmplifier);
                else
                    ClientSpoofing.SpoofingOff();


                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool SendSetModeSpoofing(bool active, Systems systems)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                if (active && (systems.Gps || systems.Glonass || systems.Beidou || systems.Galileo))
                {
                    SendStartCoordsSpoofing();
                    ClientSpoofing.SpoofingOn(systems.Gps, systems.Glonass, systems.Beidou, systems.Galileo);
                } 
                else
                    ClientSpoofing.SpoofingOff();


                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool SendGetModeSpoofing()
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.GetGeneratorState();
            }
            catch
            {
                return false;
            }
        }

        private bool SendGetDetailModeSpoofing()
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.GetDetailedGeneratorState();
            }
            catch
            {
                return false;
            }
        }

        private bool SendGetParamSpoofing()
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.GetParams();
            }
            catch
            {
                return false;
            }
        }

        private bool SendGetFakeCoordSpoofing()
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.GetFakeCoords();
            }
            catch
            {
                return false;
            }
        }

        private bool SendSetParamSpoofing(AwsToConsoleTransferLib.ControlParameters param)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.SetControlParams(param);
            }
            catch
            {
                return false;
            }
        }


        private bool SendSetStartCoordinates(AwsToConsoleTransferLib.CoordinatesParameters param)
        {
            if (ClientSpoofing == null)
                return false;

            try
            {
                return ClientSpoofing.SetStartCoord(param);
            }
            catch
            {
                return false;
            }
        }


        private void AnalizeMode(EStatus mode)
        {
            switch (mode)
            {
                case EStatus.Stop:
                    DestructorTimerSpoofing();
                    BlockGnssAntenna(false);
                    break;

                case EStatus.Synchronization:
                    break;


                case EStatus.Generation:
                    InitializeTimerSpoofing();
                    BlockGnssAntenna(true);

                    break;

                default:
                    break;

            }
        }

        private void InitializeTimerSpoofing()
        {
            tmrSpoofing.Elapsed += TickTimerSpoofing;
            tmrSpoofing.Interval = 500;

            tmrSpoofing.AutoReset = true;
            tmrSpoofing.Enabled = true;
        }

        private void DestructorTimerSpoofing()
        {
            tmrSpoofing.Elapsed -= TickTimerSpoofing;

            tmrSpoofing.Enabled = false;
        }

        void TickTimerSpoofing(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                SendGetFakeCoordSpoofing();
            }
            catch 
            { }

        }
    }
}
