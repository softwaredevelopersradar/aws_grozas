﻿
using CompassHttpLib;
using CompassHttpLib.Models;
using DLL_Compass;
using GrozaR_UHF_DLL;

using System.Windows;
using TDF;
using WPFControlConnection;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        ComGrozaR comGrozaR = new ComGrozaR();


        HttpHubEventBased httpHub;

        private void ConnectComGrozaR()
        {
            try
            {
                comGrozaR = new ComGrozaR();
                comGrozaR.OnOpenPort += GrozaR_OnOpenPort;
                comGrozaR.OnClosePort += GrozaR_OnClosePort;                
                comGrozaR.OnPosition += GrozaR_OnPosition;


                comGrozaR.OpenPort(mainWindowViewModel.LocalPropertiesVM.GrozaR.ComPort, 115200);
            }

            catch { }
        }

        private void DisconnectComGrozaR()
        {
            try
            {
                if (comGrozaR == null)
                    return;

                comGrozaR.ClosePort();

                comGrozaR.OnOpenPort -= GrozaR_OnOpenPort;
                comGrozaR.OnClosePort -= GrozaR_OnClosePort;               
                comGrozaR.OnPosition -= GrozaR_OnPosition;

                comGrozaR = null;
            }

            catch { }
        }


        private async void ConnectURLGrozaR()
        {
            try
            {
                httpHub = new HttpHubEventBased(new CompassHttpLib.Models.HttpHubConfiguration() { OperatorDtoRequestIntervalMilliseconds = 300 },
                                                mainWindowViewModel.LocalPropertiesVM.GrozaR.HostURL);// "https://178.163.196.23:443/");
                

                User user = new User()
                {
                    UserName = "Master+b5de1856",
                    Password = "63306d79-a93c-4af3-b6df-c7ec6dcbb54c",                    
                };

                httpHub.OnReceiveOperatorDtosCollection += HttpHub_OnReceiveOperatorDtosCollection;

                mainWindowViewModel.StateConnectionGrozaR_URL = await httpHub.TryAuthorizeAsync(user) ? ConnectionStates.Connected : ConnectionStates.Disconnected;
              
            }

            catch
            {
                
            }
        }

        

        private async void DisconnectURLGrozaR()
        {
            try
            {
                httpHub.Dispose();
                httpHub.OnReceiveOperatorDtosCollection -= HttpHub_OnReceiveOperatorDtosCollection;
                mainWindowViewModel.StateConnectionGrozaR_URL = ConnectionStates.Disconnected;

                httpHub = null;
            }

            catch { }
        }

    }
}
