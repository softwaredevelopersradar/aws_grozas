﻿using GrozaR_UHF_DLL.Events;
using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPFControlConnection;
using WpfMapControl;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    using CompassHttpLib.DTOs;
    using CompassHttpLib.Models;
    using GrozaSModelsDBLib;
    using System.Threading;
    using TableEvents;

    public partial class MainWindow : Window
    {

        private void GrozaRConnectionURL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (httpHub != null)
                    DisconnectURLGrozaR();
                else
                    ConnectURLGrozaR();

            }
            catch
            { }
        }

        private void GrozaRConnectionUHF_Click(object sender, RoutedEventArgs e)
        {
            

            try
            {
                if (comGrozaR != null)
                    DisconnectComGrozaR();
                else
                    ConnectComGrozaR();

            }
            catch
            { }
        }


        private void GrozaR_OnOpenPort(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionGrozaR_UHF = ConnectionStates.Connected;

        }

        private void GrozaR_OnClosePort(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionGrozaR_UHF = ConnectionStates.Disconnected;
        }

        private void GrozaR_OnPosition(object sender, PositionEventArgs e)
        {

            // Example
            //this.ucOperatorGun.UpdateCoordinates(1, new Coord(-34.88787879, 34.6577896678, -1), 250);

            UpdatePositionGrozaR(new List<PositionEventArgs>() { new PositionEventArgs(e.Id, e.Latitude, e.Longitude, e.Azimuth, e.State)});
        }

        private void UpdatePositionGrozaR(List<PositionEventArgs> position)
        {           
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                try
                {

                    foreach (var p in position)
                    {
                        lOperatorGrozaR.FindAll(x => x.ID == p.Id).ForEach(x =>
                        {
                            x.Coordinate.Latitude = p.Latitude;
                            x.Coordinate.Longitude = p.Longitude;
                            x.Azimuth = p.Azimuth;
                            x.Mode = (ModeGrozaR)p.State;
                        });

                        this.ucOperatorGun.UpdateCoordinates(p.Id, new Coord() { Latitude = p.Latitude, Longitude = p.Longitude }, p.Azimuth);
                    }


                    
                    UpdateMapGrozaR(lOperatorGrozaR);
                }
                catch { }               
            });
            
        }


        private void HttpHub_OnReceiveOperatorDtosCollection(System.Collections.Generic.IEnumerable<OperatorDto> operatorDtos)
        {
            if (operatorDtos == null)
                return;

            List<PositionEventArgs> listGrozaR = new List<PositionEventArgs>();
            foreach(var od in operatorDtos)
            {
                listGrozaR.Add(new PositionEventArgs(Convert.ToByte(od.SerialNumber),
                                                       od.OwnCoordinates.Latitude,
                                                       od.OwnCoordinates.Longitude,
                                                       od.Azimuth,
                                                       (byte)(od.GunEmissionStatus)));
            }

            UpdatePositionGrozaR(listGrozaR);
        }

        private async void SendTargetPosition(Coord coordTarget)
        {

            var lOperatorGun = await mainWindowViewModel.clientDB.Tables[NameTable.TableOperatorGun].LoadAsync<TableOperatorGun>();

            foreach (var grozaR in lOperatorGun)
            {
                switch (grozaR.TypeConnection)
                {
                    case TypeConnectGun.Modem3G:
                        try
                        {
                            await httpHub.PostTargetInformationAsync(new StationDto()
                            {
                                TargetCoordinates = new Coordinates()
                                { Latitude = coordTarget.Latitude, Longitude = coordTarget.Longitude },
                                ConcreteOperator = grozaR.Id.ToString()
                            });
                        }
                        catch
                        { }
                        break;


                    case TypeConnectGun.ModemUHF:
                        try
                        {
                            try
                            {
                                comGrozaR.SendTargetPosition((byte)grozaR.Id, coordTarget.Latitude, coordTarget.Longitude);
                            }
                            catch
                            { }
                        }
                        catch
                        { }
                        break;

                }
            }

            
            
            
        }

        private void RastrMap_OnOnSetTargetForGrozaR(object sender, Location e)
        {
            SendTargetPosition(new Coord(e.Latitude, e.Longitude, (float)e.Altitude));
        }
    }
}
