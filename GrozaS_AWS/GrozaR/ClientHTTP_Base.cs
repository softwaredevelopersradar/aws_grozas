﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.GrozaR
{
    public class ClientHTTP_Base
    {
        private  HttpClient _httpClient;

        //private readonly TokenService _tokenService;
        //private readonly PositionService _positionService;

        public string HostURL { get; private set; }

        public async Task ConnectServer(string host)
        {
            HostURL = host;

            var httpClientHandler = new HttpClientHandler();
            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };

            _httpClient = new HttpClient(httpClientHandler, true);

            var result = await _httpClient.GetAsync(new Uri(host));

           //var result = await _httpClient.GetAsync(new Uri(host));


            //_httpClient.ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) =>
            //{
            //    return true;
            //};



            //this._tokenService = new TokenService(httpClient, connectionString);
            //this._positionService = new PositionService(httpClient, connectionString);

        }

    }
}
