﻿public enum Antenna : byte
{
    JAMMING = 1,
    JAMMING_SECOND = 2,
    CONNECT_LINK = 3,
    CONNECT_PC = 4

}