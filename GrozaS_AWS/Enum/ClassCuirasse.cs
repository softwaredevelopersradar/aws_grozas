﻿public enum ClassCuirasse : byte
{
    Unknown = 0,
    Mavic = 1,
    Mavic2 ,
    MavicAir ,
    MavicAir2 ,
    DJIPhantom3 ,
    DJIPhantom4
}