﻿namespace GrozaS_AWS
{ 
    public enum ModeSpectrum
    {
        Instantaneous = 0,
        AverageLast,
        AverageAccumulate
    }
}