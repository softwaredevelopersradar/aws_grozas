﻿public enum ClassZorkiR : byte
{
   

    //Human = 0,
    //UAV = 1,
    //Car = 2,
    //Group = 3,
    //HumanM,
    //GroupM,
    //CarM,
    //IFV,
    //Tank,
    //Helicopter,
    //UAVm,
    //Unknown




    Human = 0,
    Car = 1,
    Helicopter = 2,
    Group = 3,
    UAV = 4,
    Tank = 5,
    IFV = 6,
    Unknown = 7,
    Human_manual = 8,
    Group_manual = 9,
    Car_manual = 10,
    MilitaryCar_manual = 11,
    Tank_manual = 12,
    Helicopter_manual = 13,
    UAV_manual = 14,
    Unknown_manual = 15

    //Undefined = -1, // неопределено
    //Human,          // человек
    //Car,            // машина
    //Helicopter,     // вертолет
    //Group,          // группа
    //Uav,            // БПЛА
    //Tank,           // танк
    //MilitaryCar,    // БМП(БТР)
    //Unrecognized,   // неопознанный
    //Human_manual,       // человек (ручное)
    //Group_manual,       // группа (ручное)
    //Car_manual,         // автомобиль (ручное)
    //MilitaryCar_manual, // БМП(БТР) (ручное)
    //Tank_manual,        // танк (ручное)
    //Helicopter_manual,  // вертолет (ручное)
    //Uav_manual,         // БПЛА (ручное)
    //Unrecognized_manual	// неопознанный (ручное)
}