﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaS_AWS.AmplifierUpd
{
    internal class WrapSHS
    {
        private bool _waitAnswerAmplifier = false;

        private Queue<ObjectCmd> _queueSHS = new Queue<ObjectCmd>();

        private Thread _queueThread;

        private ComSHS _comAmp;

        public WrapSHS(ref ComSHS ComSHS)
        {
            _comAmp = ComSHS;
        }

        public void Init()
        {
            _queueThread = new Thread(new ThreadStart(StartControlQueue));
            _queueThread.IsBackground = true;
            _queueThread.Start();
        }


        private void StartControlQueue()
        {
            try
            {
                if (_queueSHS.Count != 0 && !_waitAnswerAmplifier)
                {
                    ObjectCmd objectCmd = _queueSHS.Dequeue();

                    switch (objectCmd.Code)
                    {
                        case ECmdSHS.STATUS:
                            _waitAnswerAmplifier = _comAmp.SendStatusAntennaState(0);
                            break;

                        case ECmdSHS.LETTER:
                            _waitAnswerAmplifier = _comAmp.SendSetGNSS((TGpsGlonass)objectCmd.Param, (TBeidouGalileo)objectCmd.Param);
                            break;

                        case ECmdSHS.GNSS:
                           
                            _waitAnswerAmplifier = _comAmp.SendSetGNSS((TGpsGlonass)objectCmd.Param, (TBeidouGalileo)objectCmd.Param);
                            break;

                        case ECmdSHS.SPOOF:                            
                            _waitAnswerAmplifier = _comAmp.SendSetSPOOF((byte)objectCmd.Param);
                            break;

                        case ECmdSHS.ANTENNA:
                            _waitAnswerAmplifier = _comAmp.SendRelaySwitching((byte)objectCmd.Param);
                            break;

                        case ECmdSHS.RADIAT_OFF:

                            _waitAnswerAmplifier = _comAmp.SendRadiatOff((byte)objectCmd.Param);
                            break;

                        default:
                            break;
                    }


                    Thread.Sleep(100);
                }
            }
            catch
            { }
        }


    }
}
