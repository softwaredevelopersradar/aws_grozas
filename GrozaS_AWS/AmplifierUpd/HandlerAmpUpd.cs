﻿



using GrozaSModelsDBLib;
using RecognitionSystemProtocol;
using SHS_DLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using UISuppressSource;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private bool _waitAnswerAmp1 = false;
        private bool _waitAnswerAmp2 = false;

        List<State> StateAmp1 = new List<State>();
        List<State> StateAmp2 = new List<State>();

        NavigationJamming NavigationJammingAmp1 = new NavigationJamming();
        NavigationJamming NavigationJammingAmp2 = new NavigationJamming();

        //System.Diagnostics.Process ProcessAmpUpd1;
        //System.Diagnostics.Process ProcessAmpUpd2;


        private void AmpUpdConnection1_Click(object sender, RoutedEventArgs e)
        {
            if (comAmp1 != null)
                DisconnectAmp1();
            else
                ConnectAmp1();
        }

        private void AmpUpdConnection2_Click(object sender, RoutedEventArgs e)
        {
            if (comAmp2 != null)
                DisconnectAmp2();
            else
                ConnectAmp2();
        }

        private void ComAmp_OnConfirmStatus(object sender, ConfirmStatusEventArgs confirmStatusEventArgs)
        {
            List<State> StateAmp = new List<State>();
            if (confirmStatusEventArgs.ParamAmp != null)
            {
                if (confirmStatusEventArgs.ParamAmp.Length == 5)
                {
                   
                    for (int i = 0; i < 5; i++)
                    {

                        State stateLetter = new State();

                        // power
                        switch (confirmStatusEventArgs.ParamAmp[i].Power)
                        {
                            case 0:
                                stateLetter.Power = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Power = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Power = StateMode.Unknown;
                                break;
                        }

                        // synthezator
                        switch (confirmStatusEventArgs.ParamAmp[i].Snt)
                        {
                            case 0:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Synthesizer = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;
                        }

                      
                        // radiation
                        switch (confirmStatusEventArgs.ParamAmp[i].Rad)
                        {
                            case 0:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;

                            case 1:
                                
                                stateLetter.Radiation = StateMode.Ok;
                                stateLetter.Temperature = confirmStatusEventArgs.ParamAmp[i].Temp;
                                stateLetter.Current = (float)((double)confirmStatusEventArgs.ParamAmp[i].Current / 10.0);

                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        stateLetter.Error = StateMode.Unknown;
                                        break;

                                    case 1:
                                        stateLetter.Error = StateMode.Ok;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            default:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;
                        }

                        StateAmp.Add(stateLetter);

                    }

                    

                    if ((ComSHS)sender == comAmp1)
                    {
                        StateAmp1 = StateAmp;
                        ResetFlag(1);
                        JammingDrone_UpdateAllStates(_jamManager1, StateAmp);
                    }
                        

                    if ((ComSHS)sender == comAmp2)
                    {
                        StateAmp2 = StateAmp;
                        ResetFlag(2);
                        JammingDrone_UpdateAllStates(_jamManager2, StateAmp2);
                    }
                        

                    //if (tParamAmp[0].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprLetter1Amp1, strSuppressL1, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprLetter1Amp1, strSuppressL1, Color.Red);


                    //if (tParamAmp1[1].bRad == 0)
                    //{
                    //    if (!BeidouAmp1 && !BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Black);
                    //    if (BeidouAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Black);
                    //    if (BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Black);
                    //}
                    //else
                    //{
                    //    FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Black);
                    //    FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Black);
                    //    FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Black);

                    //    if (!BeidouAmp1 && !BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Red);
                    //    if (BeidouAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Red);
                    //    if (BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Red);
                    //}





                    //if (tParamAmp1[2].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprLetter3Amp1, strSuppressL3, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprLetter3Amp1, strSuppressL3, Color.Red);

                    //if (tParamAmp1[3].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprGNSS_Amp1, strSuppressGNSS, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprGNSS_Amp1, strSuppressGNSS, Color.Red);

                    //if (tParamAmp1[4].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Black);
                    //else
                    //{
                    //    if (bSetRadiatAmp1[4] == 1)
                    //        FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Red);
                    //    else
                    //        FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Black);
                    //}

                }

            }
        }



        private void ComAmp_OnConfirmStatusAntenna(object sender, ConfirmStatusEventArgs confirmStatusEventArgs)
        {
            List<State> StateAmp = new List<State>();
            if (confirmStatusEventArgs.ParamAmp != null)
            {
                if (confirmStatusEventArgs.ParamAmp.Length == 5)
                {

                    for (int i = 0; i < 5; i++)
                    {

                        State stateLetter = new State();

                        // power
                        switch (confirmStatusEventArgs.ParamAmp[i].Power)
                        {
                            case 0:
                                stateLetter.Power = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Power = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Power = StateMode.Unknown;
                                break;
                        }

                        // synthezator
                        switch (confirmStatusEventArgs.ParamAmp[i].Snt)
                        {
                            case 0:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;

                            case 1:
                                stateLetter.Synthesizer = StateMode.Ok;
                                break;

                            default:
                                stateLetter.Synthesizer = StateMode.Unknown;
                                break;
                        }


                        // radiation
                        switch (confirmStatusEventArgs.ParamAmp[i].Rad)
                        {
                            case 0:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;

                            case 1:

                                stateLetter.Radiation = StateMode.Ok;
                                stateLetter.Temperature = confirmStatusEventArgs.ParamAmp[i].Temp;
                                stateLetter.Current = (float)((double)confirmStatusEventArgs.ParamAmp[i].Current / 10.0);

                                switch (confirmStatusEventArgs.ParamAmp[i].Error)
                                {
                                    case 0:
                                        stateLetter.Error = StateMode.Unknown;
                                        break;

                                    case 1:
                                        stateLetter.Error = StateMode.Ok;
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            default:
                                stateLetter.Error = StateMode.Unknown;
                                stateLetter.Radiation = StateMode.Unknown;

                                stateLetter.Temperature = 0;
                                stateLetter.Current = 0;
                                break;
                        }

                        StateAmp.Add(stateLetter);

                    }



                    if ((ComSHS)sender == comAmp1)
                    {
                        StateAmp1 = StateAmp;
                        ResetFlag(1);
                        JammingDrone_UpdateAllStates(_jamManager1, StateAmp);

                        if(confirmStatusEventArgs.ParamAmp.Length>3)
                            JammingDrone_UpdateAntennaState (_jamManager1, (AntennaMode)(confirmStatusEventArgs.ParamAmp[3].Antenna+1));
                    }


                    if ((ComSHS)sender == comAmp2)
                    {
                        StateAmp2 = StateAmp;
                        ResetFlag(2);
                        JammingDrone_UpdateAllStates(_jamManager2, StateAmp2);

                        if (confirmStatusEventArgs.ParamAmp.Length > 3)
                            JammingDrone_UpdateAntennaState(_jamManager2, (AntennaMode)(confirmStatusEventArgs.ParamAmp[3].Antenna+1));
                    }


                    //if (tParamAmp[0].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprLetter1Amp1, strSuppressL1, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprLetter1Amp1, strSuppressL1, Color.Red);


                    //if (tParamAmp1[1].bRad == 0)
                    //{
                    //    if (!BeidouAmp1 && !BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Black);
                    //    if (BeidouAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Black);
                    //    if (BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Black);
                    //}
                    //else
                    //{
                    //    FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Black);
                    //    FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Black);
                    //    FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Black);

                    //    if (!BeidouAmp1 && !BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bSupprLetter2Amp1, strSuppressL2, Color.Red);
                    //    if (BeidouAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouAmp1, strSuppressBeidou, Color.Red);
                    //    if (BeidouGalileoAmp1)
                    //        FuncShow.ShowInfoButton(bBeidouGalileoAmp1, strSuppressBeidouGalileo, Color.Red);
                    //}





                    //if (tParamAmp1[2].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprLetter3Amp1, strSuppressL3, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprLetter3Amp1, strSuppressL3, Color.Red);

                    //if (tParamAmp1[3].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprGNSS_Amp1, strSuppressGNSS, Color.Black);
                    //else
                    //    FuncShow.ShowInfoButton(bSupprGNSS_Amp1, strSuppressGNSS, Color.Red);

                    //if (tParamAmp1[4].bRad == 0)
                    //    FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Black);
                    //else
                    //{
                    //    if (bSetRadiatAmp1[4] == 1)
                    //        FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Red);
                    //    else
                    //        FuncShow.ShowInfoButton(bSupprSpoofAmp1, strSuppressSpoof, Color.Black);
                    //}

                }

            }
        }



        private void ComAmp_OnConfirmGetParam(object sender, ConfirmGetParamEventArgs[] confirmGetParamEventArgs)
        {
            if ((ComSHS)sender == comAmp1)
            {
                ResetFlag(1);
                SendGetStatusAmp(1);

            }

            if ((ComSHS)sender == comAmp2)
            {
                ResetFlag(2);
                SendGetStatusAmp(2);
            }
                
        }

        private void ComAmp_OnConfirmSet(object sender, ConfirmSetEventArgs confirmSetEventArgs )
        {
            if ((ComSHS)sender == comAmp1)
            {
                ResetFlag(1);
                SendGetStatusAmp(1);
            }

            if ((ComSHS)sender == comAmp2)
            {
                ResetFlag(2);
                SendGetStatusAmp(2);
            }
                
        }

        private void ComAmp_OnConnect(object sender, bool value)
        {
            if ((ComSHS)sender == comAmp1)
            {
                mainWindowViewModel.StateConnectionAmplifier1 = ConnectionStates.Connected;
                SendGetStatusAmp(1);
            }


            if ((ComSHS)sender == comAmp2)
            {
                mainWindowViewModel.StateConnectionAmplifier2 = ConnectionStates.Connected;
                SendGetStatusAmp(2);
            }

            InitializeTimerGetStatusAmplifier();

            tmrGetStatusAmplifier.Enabled = true;


        }

        private void ComAmp_OnDisconnect(object sender, bool value)
        {
            if ((ComSHS)sender == comAmp1)
                mainWindowViewModel.StateConnectionAmplifier1 = ConnectionStates.Disconnected;


            if ((ComSHS)sender == comAmp2)
                mainWindowViewModel.StateConnectionAmplifier2 = ConnectionStates.Disconnected;

            ComAmp_OnConfirmStatus((ComSHS)sender, new ConfirmStatusEventArgs(0, new TParamAmp[5]));

        }



        private void SendGetStatusAmp(int Amplifier)
        {
            switch (Amplifier)
            {
                case 1:
                    try
                    {


                        //_waitAnswerAmp1 = comAmp1.SendStatus(0);
                        _waitAnswerAmp1 = comAmp1.SendStatusAntennaState(0);
                        
                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        //_waitAnswerAmp2 = comAmp2.SendStatus(0);
                        _waitAnswerAmp2 = comAmp2.SendStatusAntennaState(0);
                        
                    }
                    catch
                    { }
                 
                    break;

                default:
                    break;
            }
            
        }

        private void SendRadiatOffAmp(int Amplifier, byte Letter)
        {

            
            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        _waitAnswerAmp1 = comAmp1.SendRadiatOff(Letter);
                        
                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        _waitAnswerAmp2 = comAmp2.SendRadiatOff(Letter);
                        
                    }
                    catch
                    { }
                    break;

                default:
                    break;
            }

            
        }

       
        private void SendSetGNSSAmp(int Amplifier, NavigationJamming navigationJamming)
        {
            if (mainWindowViewModel.TempMode != Mode.JAMMING)
            {
                return;
            }

            TBeidouGalileo beidouGalileo = new TBeidouGalileo();
            beidouGalileo.beidouL1 = navigationJamming.GNSSL1;
            beidouGalileo.beidouL2 = navigationJamming.GNSSL2;
            beidouGalileo.galileoL1 = navigationJamming.GNSSL1;
            beidouGalileo.galileoL2 = navigationJamming.GNSSL2;


            TGpsGlonass gnss = new TGpsGlonass();
            gnss.gpsL1 = navigationJamming.GpsL1;
            gnss.gpsL2 = navigationJamming.GpsL2;

            gnss.glnssL1 = navigationJamming.GlonassL1;
            gnss.glnssL2 = navigationJamming.GlonassL2;



            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        
                            _waitAnswerAmp1 = comAmp1.SendSetGNSS(gnss, beidouGalileo);
                       
                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {                        
                            _waitAnswerAmp2 = comAmp2.SendSetGNSS(gnss, beidouGalileo);
                        
                    }
                    catch
                    { }

                    break;

                default:
                    break;
            }
        }

        private void SendSetLetterAmp(int Amplifier, int Duration, TableSuppressSource source)
        {

            if (mainWindowViewModel.TempMode != Mode.JAMMING)
            {
                return;
            }
            TParamFWS[] param = new TParamFWS[1];
            param[0].Freq = (uint)source.FrequencyMHz*1000;
            param[0].Modulation = (byte)(source.Modulation);

            switch (source.Modulation)
            {
                case Modulation.None:
                    param[0].Deviation = 0;
                    param[0].Manipulation = 0;
                    param[0].Duration = 0;
                    break;

                case Modulation.KFM:
                    param[0].Deviation = 0;
                    param[0].Manipulation = source.Manipulation;
                    param[0].Duration = 0;
                    break;

                case Modulation.LCM:

                    //if (source.Deviation  >= 1000)
                    //{
                    //    param[0].Deviation = (byte)(source.Deviation / 1000);
                    //    param[0].Deviation = (byte)(param[0].Deviation | 128);
                    //}

                    //else

                    //    param[0].Deviation = (byte)(source.Deviation);

                    param[0].Deviation = source.Deviation;
                    param[0].Manipulation = Convert.ToByte(source.ScanSpeed);
                    param[0].Duration = 0;
                    break;


                

                default:
                    break;
            }

            

            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        _waitAnswerAmp1 = comAmp1.SendSetParamFWS(Duration, param);
                        
                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        _waitAnswerAmp2 = comAmp2.SendSetParamFWS(Duration, param);
                        
                    }
                    catch
                    { }

                    break;

                default:
                    break;
            }
        }

        private void SendSetLetterAmp(int Amplifier, int Duration, List<TableSuppressSource> source)
        {

            if (mainWindowViewModel.TempMode != Mode.JAMMING)
            {
                return;
            }
            TParamFWS[] param = new TParamFWS[source.Count];

            int i = 0;
            foreach (var s in source)
            {
                param[i].Freq = (uint)s.FrequencyMHz * 1000;
                param[i].Modulation = (byte)(s.Modulation);

                switch (s.Modulation)
                {
                    case Modulation.None:
                        param[i].Deviation = 0;
                        param[i].Manipulation = 0;
                        param[i].Duration = 0;
                        break;

                    case Modulation.KFM:
                        param[i].Deviation = 0;
                        param[i].Manipulation = s.Manipulation;
                        param[i].Duration = 0;
                        break;

                    case Modulation.LCM:

                        //if (source.Deviation  >= 1000)
                        //{
                        //    param[0].Deviation = (byte)(source.Deviation / 1000);
                        //    param[0].Deviation = (byte)(param[0].Deviation | 128);
                        //}

                        //else

                        //    param[0].Deviation = (byte)(source.Deviation);

                        param[i].Deviation = s.Deviation;
                        param[i].Manipulation = Convert.ToByte(s.ScanSpeed);
                        param[i].Duration = 0;
                        break;


                    default:
                        break;
                }
                i++;
            }
            



            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        _waitAnswerAmp1 = comAmp1.SendSetParamFWS(Duration, param);

                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        _waitAnswerAmp2 = comAmp2.SendSetParamFWS(Duration, param);

                    }
                    catch
                    { }

                    break;

                default:
                    break;
            }
        }


        private void SendSetSpoofAmp(int Amplifier)
        {
            if (mainWindowViewModel.TempMode != Mode.JAMMING)
            {
                return;
            }
            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        _waitAnswerAmp1 = comAmp1.SendSetSPOOF();
                        
                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        _waitAnswerAmp2 = comAmp2.SendSetSPOOF();
                        
                    }
                    catch
                    { }
                    break;

                default:
                    break;
            }
            
        }
        

        private void SendTypeAntenna(int Amplifier, TypeAntenna antenna)
        {
            switch (Amplifier)
            {
                case 1:
                    try
                    {
                        _waitAnswerAmp1 = comAmp1.SendRelaySwitching((byte)(antenna - 1));

                    }
                    catch
                    { }
                    break;

                case 2:
                    try
                    {
                        _waitAnswerAmp2 = comAmp2.SendRelaySwitching((byte)(antenna - 1));

                    }
                    catch
                    { }
                    break;

                default:
                    break;
            }
        }





        private void UpdateParamGNSSAmp(List<TableSuppressGnss> listGNSS)
        {

            List<TableSuppressGnss> listAmp1GNSS = listGNSS.Where(x => x.Id > 10 && x.Id < 20).ToList();

            bool[] gpsAmp = GetLetterNavigation(listAmp1GNSS, TypeGNSS.Gps);
            bool[] glonassAmp = GetLetterNavigation(listAmp1GNSS, TypeGNSS.Glonass);
            bool[] beidouAmp = GetLetterNavigation(listAmp1GNSS, TypeGNSS.Beidou);


            //NavigationJamming navigationJamming = new NavigationJamming();
            //navigationJamming = SetTypeNavigation(gpsAmp, glonassAmp, beidouAmp);

            SendSetGNSSAmp(1, SetTypeNavigation(gpsAmp, glonassAmp, beidouAmp));




            List<TableSuppressGnss> listAmp2GNSS = listGNSS.Where(x => x.Id > 20 && x.Id < 30).ToList();


            gpsAmp = GetLetterNavigation(listAmp2GNSS, TypeGNSS.Gps);
            glonassAmp = GetLetterNavigation(listAmp2GNSS, TypeGNSS.Glonass);
            beidouAmp = GetLetterNavigation(listAmp2GNSS, TypeGNSS.Beidou);

            //NavigationJamming navigationJamming = new NavigationJamming();
            //navigationJamming = SetTypeNavigation(gpsAmp, glonassAmp, beidouAmp);

            SendSetGNSSAmp(2, SetTypeNavigation(gpsAmp, glonassAmp, beidouAmp));



            bool[] GetLetterNavigation(List<TableSuppressGnss> listNavigation, TypeGNSS type)
            {
                bool[] res = new bool[2];

                try
                {
                    res[0] = Convert.ToBoolean(listNavigation.Where(x => x.Type == type).FirstOrDefault().L1);
                    res[1] = Convert.ToBoolean(listNavigation.Where(x => x.Type == type).FirstOrDefault().L2);
                }
                catch
                { }

                return res;
            }

            NavigationJamming SetTypeNavigation(bool[] gps, bool[] glonass, bool[] beidou)
            {
                NavigationJamming navigation = new NavigationJamming();

                navigation.GpsL1 = gps[0];
                navigation.GpsL2 = gps[1];

                navigation.GlonassL1 = glonass[0];
                navigation.GlonassL2 = glonass[1];

                navigation.GNSSL1 = beidou[0];
                navigation.GNSSL2 = beidou[1];

                return navigation;
            }

        }

        private void ResetFlag(int Amplifier)
        {
            switch (Amplifier)
            {
                case 1:
                    _waitAnswerAmp1 = false;
                    break;

                case 2:
                    _waitAnswerAmp2 = false;
                    break;

                default:
                    break;
            }
        }

        private void TickTimerGetStatusAmp(object sender, System.Timers.ElapsedEventArgs e)
        {

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                try 
                {
                    if (!_waitAnswerAmp1)
                    {
                        comAmp1?.SendStatusAntennaState(0);
                        
                    }
                        
                    //comAmp1.SendStatus(0);
                }
                catch { }

                try
                {
                    if (!_waitAnswerAmp2)                        
                    {
                        comAmp2?.SendStatusAntennaState(0);
                        
                    }
                        
                    //comAmp2.SendStatus(0);
                }
                catch { }

            });

        }

        private void TickTimerQueueCommand(object sender, System.Timers.ElapsedEventArgs e)
        {

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                try
                {
                    if (!_waitAnswerAmp1)
                    {

                        
                        comAmp1?.SendStatusAntennaState(0);

                    }

                    //comAmp1.SendStatus(0);
                }
                catch { }

                try
                {
                    if (!_waitAnswerAmp2)
                    {
                        comAmp2?.SendStatusAntennaState(0);

                    }

                    //comAmp2.SendStatus(0);
                }
                catch { }

            });

        }
    }
} 