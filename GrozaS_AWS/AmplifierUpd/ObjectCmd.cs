﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.AmplifierUpd
{
    internal class ObjectCmd
    {
        public byte Amplifier { get; set; }

        public ECmdSHS Code { get; set; }

        public object Param { get; set; }
    }

    internal class NavigationSystem
    {
        public bool GPS_L1 { get; set; }

        public bool GPS_L2 { get; set; }

        public bool GLONASS_L1 { get; set; }

        public bool GLONASS_L2 { get; set; }

        //public bool Beidou_L1 { get; set; }

        //public bool Beidou_L2 { get; set; }

        //public bool Galileo_L1 { get; set; }

        //public bool Galileo_L2 { get; set; }

    }
}
