﻿


using RecognitionSystemProtocol;
using SHS_DLL;
using System.Windows;
using UISuppressSource;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        ComSHS comAmp1;
        ComSHS comAmp2;

        private void ConnectAmp1()
        {
            try
            {

                StateAmp1 = null;
                StateAmp1 = new System.Collections.Generic.List<State>();
                StateAmp1.Add(new State());
                StateAmp1.Add(new State());
                StateAmp1.Add(new State());
                StateAmp1.Add(new State());
                StateAmp1.Add(new State());

                _waitAnswerAmp1 = false;

                NavigationJammingAmp1 = new NavigationJamming();

                if (comAmp1 != null)
                    DisconnectAmp1();

                comAmp1 = new ComSHS(4,5);

                comAmp1.OnConnect += ComAmp_OnConnect;
                comAmp1.OnDisconnect += ComAmp_OnDisconnect;


                comAmp1.OnConfirmSet += ComAmp_OnConfirmSet;
                comAmp1.OnConfirmGetParam += ComAmp_OnConfirmGetParam;
                comAmp1.OnConfirmStatus += ComAmp_OnConfirmStatus;
                comAmp1.OnConfirmStatusAntenna += ComAmp_OnConfirmStatusAntenna;

                comAmp1.OpenPort(mainWindowViewModel.LocalPropertiesVM.Amp1.ComPort,  9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            }
            
            catch { }
        }

        
        private void DisconnectAmp1()
        {
            try
            {
                _waitAnswerAmp1 = false;

                if (comAmp1 != null)
                {
                    comAmp1.ClosePort();

                    comAmp1.OnConnect -= ComAmp_OnConnect;
                    comAmp1.OnDisconnect -= ComAmp_OnDisconnect;


                    comAmp1.OnConfirmSet -= ComAmp_OnConfirmSet;
                    comAmp1.OnConfirmGetParam -= ComAmp_OnConfirmGetParam;
                    comAmp1.OnConfirmStatus -= ComAmp_OnConfirmStatus;
                    comAmp1.OnConfirmStatusAntenna -= ComAmp_OnConfirmStatusAntenna;


                    comAmp1 = null;
                }

            }

            catch { }
        }

        private void ConnectAmp2()
        {
            try
            {
                StateAmp2 = null;
                StateAmp2 = new System.Collections.Generic.List<State>();
                StateAmp2.Add(new State());
                StateAmp2.Add(new State());
                StateAmp2.Add(new State());
                StateAmp2.Add(new State());
                StateAmp2.Add(new State());

                _waitAnswerAmp2 = false;

                NavigationJammingAmp2 = new NavigationJamming();

                if (comAmp2 != null)
                    DisconnectAmp2();

                comAmp2 = new ComSHS(4, 5);

                comAmp2.OnConnect += ComAmp_OnConnect;
                comAmp2.OnDisconnect += ComAmp_OnDisconnect;

                comAmp2.OnConfirmSet += ComAmp_OnConfirmSet;
                comAmp2.OnConfirmGetParam += ComAmp_OnConfirmGetParam;
                comAmp2.OnConfirmStatus += ComAmp_OnConfirmStatus;
                comAmp2.OnConfirmStatusAntenna += ComAmp_OnConfirmStatusAntenna;

                comAmp2.OpenPort(mainWindowViewModel.LocalPropertiesVM.Amp2.ComPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            }

            catch { }
        }


        private void DisconnectAmp2()
        {
            try
            {
                _waitAnswerAmp2 = false;

                if (comAmp2 != null)
                {
                    comAmp2.ClosePort();

                    comAmp2.OnConnect -= ComAmp_OnConnect;
                    comAmp2.OnDisconnect -= ComAmp_OnDisconnect;


                    comAmp2.OnConfirmSet -= ComAmp_OnConfirmSet;
                    comAmp2.OnConfirmGetParam -= ComAmp_OnConfirmGetParam;               
                    comAmp2.OnConfirmStatus -= ComAmp_OnConfirmStatus;
                    comAmp2.OnConfirmStatusAntenna -= ComAmp_OnConfirmStatusAntenna;


                    comAmp2 = null;
                }

            }

            catch { }
        }


    }
}
