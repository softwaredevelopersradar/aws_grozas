﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using UIMap;
using UIMap.Models;
using WpfMapControl;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
       
        private void UpdateJammerMap(List<TableJammerStation> lJammer)
        {

            ObservableCollection<VMJammer> _jammersDB = new ObservableCollection<VMJammer>();

            foreach (TableJammerStation t in lJammer)
               _jammersDB.Add(new VMJammer(t));


            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RastrMap.Jammers.Clear();
                RastrMap.Jammers = _jammersDB;
            });


            UpdateMapDirectionRX();

            UpdateMapDirectionTX();

            UpdateMapDirectionOEM(10);
        }
       
        private void UpdateSourceMap(ObservableCollection<SourceDF> sourceDF)
        {
            try
            {

                ObservableCollection<VMDrone> DroneDB = new ObservableCollection<VMDrone>();

                ObservableCollection<SourceDF> lSourceTemp = sourceDF;

                foreach (var t in lSourceTemp)
                {
                    string Type = TypeCodeCollection.ContainsKey(t.Type) ? TypeCodeCollection[t.Type] : "";
                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.Track)
                    {
                        if (tr.Coordinate.Latitude !=0 && tr.Coordinate.Longitude !=0)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    float dist = 0;

                    try
                    {
                        var ownJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault();
                        dist = ownJammer == null ? dist : t.Track.Last().Bearings.Where(x => x.Jammer == ownJammer.Id).FirstOrDefault().Distance;
                    }
                    catch
                    { }


                    ViewUAV tempView = new ViewUAV();

                    if (t.Type == 0)
                        tempView = ViewUAV.Unknown;
                    else
                    {
                        if (t.Type == 16)
                            tempView = ViewUAV.GSM;
                        else
                        {
                            if (t.Type == 1 || t.Type == 2 || t.Type == 3)
                                tempView = ViewUAV.Multicopter;
                            else
                                tempView = ViewUAV.Plane;
                        }
                        
                    }
                    

                    DroneDB.Add(new VMDrone()
                    {
                        IDSource = t.ID,
                        Name = Type,
                        View = tempView,
                        LocationDrone = new Location(t.Track.Last().Coordinate.Longitude, t.Track.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack,
                        Distance = dist,
                        Zone = (byte)t.Track.Last().Zone
                    }) ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.Drones.Clear();
                    RastrMap.Drones = DroneDB;
                });

            }
            catch { }


        }

        private void UpdateMapBearing(List <TableJammerStation> tableJammerStation, ObservableCollection<SourceDF> tableSource)
        {
            try
            {
                foreach (var jammer in tableJammerStation)
                {
                    ObservableCollection<VMBearing> vmbearing = new ObservableCollection<VMBearing>();
                    foreach (var source in tableSource)
                    {
                        if (source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).ToList().Count > 0)
                        {

                            vmbearing.Add(new VMBearing(jammer.Coordinates)
                            {
                                Direction = source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).Last().Bearing,
                                //Radius = mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.DistanceIntelegence,
                                Radius = mainWindowViewModel.LocalPropertiesVM.Map.DistanceBearingLine,
                                Visible = true,
                                IsSelected = (source.ID == SelectedID) ? true : false,
                                //IDSource = source.Track.Last().Bearings.Where(x => x.Jammer == jammer.Id).Last().TableSourceId
                                IDSource = source.ID
                            });
                        }
                    }



                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Id == jammer.Id).FirstOrDefault() != null)
                    {

                        RastrMap.Jammers.Where(x => x.Id == jammer.Id).FirstOrDefault().BearingSource.Clear();
                        foreach (var bb in vmbearing)
                            RastrMap.Jammers.Where(x => x.Id == jammer.Id).FirstOrDefault().BearingSource.Add(bb);
                        
                    }

                });

                }

                
            }
            catch { }

        }

        private void UpdateMapDirectionTX()
        {
           
            // new
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {
                        // TX1
                        if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJamming.Direction = mainWindowViewModel.DirectionBRD1_Upper.Result;                         
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming.Visible = 
                            (Math.Abs(mainWindowViewModel.DirectionBRD1_Upper.Result - mainWindowViewModel.DirectionRotateBRD1_Upper.Result) > 3
                            && mainWindowViewModel.DirectionRotateBRD1_Upper.Set) ? true : false;                        
                        }

                        else
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJamming.Visible = false;
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming.Visible = false;
                        }


                        // TX2
                        if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJammingSecond.Direction = mainWindowViewModel.DirectionBRD2_Upper.Result;                           
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Visible = 
                            (Math.Abs(mainWindowViewModel.DirectionBRD2_Upper.Result - mainWindowViewModel.DirectionRotateBRD2_Upper.Result) > 3 
                            && mainWindowViewModel.DirectionRotateBRD2_Upper.Set) ? true : false;
                        }
                        else 
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneJammingSecond.Visible = false;
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Visible = false;

                        }


                        // CX1
                        if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnection.Direction = mainWindowViewModel.DirectionBRD1_Bottom.Result;                     
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnection.Visible = 
                            (Math.Abs(mainWindowViewModel.DirectionBRD1_Bottom.Result - mainWindowViewModel.DirectionRotateBRD1_Bottom.Result) > 3
                            && mainWindowViewModel.DirectionRotateBRD1_Bottom.Set) ? true : false;                          
                        }

                        else
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnection.Visible = false;
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnection.Visible = false;

                        }

                        //CX2
                        if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnectionSecond.Direction = mainWindowViewModel.DirectionBRD2_Bottom.Result;                           
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnectionSecond.Visible = 
                            (Math.Abs(mainWindowViewModel.DirectionBRD2_Bottom.Result - mainWindowViewModel.DirectionRotateBRD2_Bottom.Result) > 3 
                            && mainWindowViewModel.DirectionRotateBRD2_Bottom.Set) ? true : false;
                        }
                        else
                        {
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneConnectionSecond.Visible = false;
                            RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnectionSecond.Visible = false;

                        }


                    }
                });
            }
            catch { }

        }

        private void UpdateMapDirectionRX()
        {
            try
            {

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)                        
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneBearing.Direction = mainWindowViewModel.DirectionRX.Result;


               
                });
            }
            catch { }
        }

        private void UpdateMapDirectionOEM(float sector)
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                    {
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Direction = mainWindowViewModel.DirectionOEM.Result;

                        //if (sector > 60)
                        RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneOptic.Sector = sector;
                        
                    }
                });
            }
            catch { }
        }

        private void UpdateMapDirectionBRD()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                //if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                //{

                //    //RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotate.Direction = mainWindowViewModel.DirectionRotateBRD1_Upper.Result;                   
                //    //RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotate.Visible = (Math.Abs(mainWindowViewModel.DirectionBRD1_Upper.Result - mainWindowViewModel.DirectionRotateBRD1_Upper.Result) < 3) ? false : true;

                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming.Direction = mainWindowViewModel.DirectionRotateBRD1_Upper.Result;
                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJamming.Visible =
                //    (Math.Abs(mainWindowViewModel.DirectionBRD1_Upper.Result - mainWindowViewModel.DirectionRotateBRD1_Upper.Result) > 3
                //            && mainWindowViewModel.DirectionRotateBRD1_Upper.Set) ? true : false;

                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Direction = mainWindowViewModel.DirectionRotateBRD2_Upper.Result;
                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateJammingSecond.Visible = 
                //    (Math.Abs(mainWindowViewModel.DirectionBRD2_Upper.Result - mainWindowViewModel.DirectionRotateBRD2_Upper.Result) > 3
                //            && mainWindowViewModel.DirectionRotateBRD2_Upper.Set) ? true : false;

                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnection.Direction = mainWindowViewModel.DirectionRotateBRD1_Bottom.Result;
                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnection.Visible = 
                //    (Math.Abs(mainWindowViewModel.DirectionBRD1_Bottom.Result - mainWindowViewModel.DirectionRotateBRD1_Bottom.Result) > 3
                //            && mainWindowViewModel.DirectionRotateBRD1_Bottom.Set) ? true : false;

                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnectionSecond.Direction = mainWindowViewModel.DirectionRotateBRD2_Bottom.Result;
                //    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().ZoneRotateConnectionSecond.Visible = 
                //    (Math.Abs(mainWindowViewModel.DirectionBRD2_Bottom.Result - mainWindowViewModel.DirectionRotateBRD2_Bottom.Result) > 3
                //            && mainWindowViewModel.DirectionRotateBRD2_Bottom.Set) ? true : false;

                //}

                    if (RastrMap.Jammers == null || RastrMap.Jammers.Count <= 0)
                    {
                        return;
                    }

                    var ownStation = RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault();

                    if (ownStation != null)
                    {

                        ownStation.ZoneRotateJamming.Direction = mainWindowViewModel.DirectionRotateBRD1_Upper.Result;
                        ownStation.ZoneRotateJamming.Visible =
                        (Math.Abs(mainWindowViewModel.DirectionBRD1_Upper.Result - mainWindowViewModel.DirectionRotateBRD1_Upper.Result) > 3
                                && mainWindowViewModel.DirectionRotateBRD1_Upper.Set) ? true : false;

                        ownStation.ZoneRotateJammingSecond.Direction = mainWindowViewModel.DirectionRotateBRD2_Upper.Result;
                        ownStation.ZoneRotateJammingSecond.Visible =
                        (Math.Abs(mainWindowViewModel.DirectionBRD2_Upper.Result - mainWindowViewModel.DirectionRotateBRD2_Upper.Result) > 3
                                && mainWindowViewModel.DirectionRotateBRD2_Upper.Set) ? true : false;

                        ownStation.ZoneRotateConnection.Direction = mainWindowViewModel.DirectionRotateBRD1_Bottom.Result;
                        ownStation.ZoneRotateConnection.Visible =
                        (Math.Abs(mainWindowViewModel.DirectionBRD1_Bottom.Result - mainWindowViewModel.DirectionRotateBRD1_Bottom.Result) > 3
                                && mainWindowViewModel.DirectionRotateBRD1_Bottom.Set) ? true : false;

                        ownStation.ZoneRotateConnectionSecond.Direction = mainWindowViewModel.DirectionRotateBRD2_Bottom.Result;
                        ownStation.ZoneRotateConnectionSecond.Visible =
                        (Math.Abs(mainWindowViewModel.DirectionBRD2_Bottom.Result - mainWindowViewModel.DirectionRotateBRD2_Bottom.Result) > 3
                                && mainWindowViewModel.DirectionRotateBRD2_Bottom.Set) ? true : false;

                    }
                });
            }
            catch { }
        }

        private void UpdateGlobusTargetsMap(ObservableCollection<GlobusTarget> globus)
        {
            try
            {

                ObservableCollection<VMGlobusTarget> GlobusTarget = new ObservableCollection<VMGlobusTarget>();

                foreach (var t in globus)
                {

                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.globusMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();


                    GlobusTarget.Add(new VMGlobusTarget()
                    {
                        IDSource = t.ID,
                        Name = t.ID.ToString(),
                        //View = (ViewZorkiR)t.zorkiRMarks.Last().Class,
                        //View = FixType(t.globusMarks.Last().Class),
                        LocationDrone = new Location(t.globusMarks.Last().Coordinate.Longitude, t.globusMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.GlobusTargets.Clear();
                    RastrMap.GlobusTargets = GlobusTarget;
                });

            }
            catch { }


        }



        private void UpdateZorkiRTargetsMap(ObservableCollection<ZorkiRTarget> zorkiR)
        {
            try
            {

                ObservableCollection<VMZorkiRTarget> ZorkiRTarget = new ObservableCollection<VMZorkiRTarget>();

                foreach (var t in zorkiR)
                {

                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.zorkiRMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();


                    ZorkiRTarget.Add(new VMZorkiRTarget()
                    {
                        IDSource = t.ID,
                        Name = t.ID.ToString(),
                        //View = (ViewZorkiR)t.zorkiRMarks.Last().Class,
                        View = FixType(t.zorkiRMarks.Last().Class),
                        LocationDrone = new Location(t.zorkiRMarks.Last().Coordinate.Longitude, t.zorkiRMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.ZorkiRTargets.Clear();
                    RastrMap.ZorkiRTargets = ZorkiRTarget;
                });

            }
            catch { }


        }


        private void UpdateAeroscopeMapTarget(ObservableCollection<AeroscopeTarget> aeroscope)
        {
            try
            {
                if (!this.mainWindowViewModel.LocalPropertiesVM.Aeroscope.Existance)
                    return;

                ObservableCollection<VMAeroscopeTarget> AeroscopeTarget = new ObservableCollection<VMAeroscopeTarget>();

                foreach (var t in aeroscope)
                {

                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.aeroscopeMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }



                    AeroscopeTarget.Add(new VMAeroscopeTarget()
                    {
                        IDSource = t.ID,
                        Type = t.Type,
                        LocationDrone = new Location(t.aeroscopeMarks.Last().Coordinate.Longitude, t.aeroscopeMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack,
                        HomePoint = new Location(t.aeroscopeMarks.Last().Home.Longitude, t.aeroscopeMarks.Last().Home.Latitude, 0),

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.AeroscopeTargets.Clear();

                   // if(AeroscopeTarget.Count!=0)
                    RastrMap.AeroscopeTargets = AeroscopeTarget;
                });

            }
            catch { }


        }

        private void UpdateEscopeMapTarget(ObservableCollection<EscopeTarget> aeroscope)
        {
            try
            {
                if (!this.mainWindowViewModel.LocalPropertiesVM.Aeroscope.Existance)
                    return;

                var escopeTarget = new ObservableCollection<VMEscopeTarget>();
                
                foreach (var t in aeroscope)
                {

                    List<Location> locTrack = Array.Empty<Location>().ToList(); 
                    var copy = new ObservableCollection<AeroscopeMark>(t.escopeMarks);
                    for (var index = 0; index < copy.Count; index++)
                    {
                        var tr = copy[index];
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(
                                new Location(
                                    tr.Coordinate.Latitude,
                                    tr.Coordinate
                                        .Longitude)); //TODO: почему тут в прямом порядке, а в других местах в обратном
                    }

                    escopeTarget.Add(new VMEscopeTarget(t.Color)
                                         {
                                             //IDSource = t.ID,
                                             SerialNumber = t.SerialNumber,
                                             Type = t.Type,
                                             LocationDrone = new Location(t.Coordinate.Longitude, t.Coordinate.Latitude, 0),
                                             LocationTrack = locTrack,
                                             HomePoint = new Location(t.Home.Longitude, t.Home.Latitude, 0),
                                             Pilot = new Location(t.Pilot.Longitude, t.Pilot.Latitude, 0),
                                         }); 
                }

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        RastrMap.EscopeTargets.Clear();

                        // if(AeroscopeTarget.Count!=0)
                        RastrMap.EscopeTargets = escopeTarget;
                    });

            }
            catch { }


        }



        private void UpdateCuirasseTargetsMap(ObservableCollection<CuirasseTarget> cuirasse)
        {
            try
            {

                ObservableCollection<VMCuirasseTarget> CuirasseTarget = new ObservableCollection<VMCuirasseTarget>();

                foreach (var t in cuirasse)
                {

                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.cuirasseMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();


                    CuirasseTarget.Add(new VMCuirasseTarget()
                    {
                        ID = t.ID,

                        LocationDrone = new Location(t.cuirasseMarks.Last().Coordinate.Longitude, t.cuirasseMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.CuirasseTargets.Clear();
                    RastrMap.CuirasseTargets = CuirasseTarget;
                });

            }
            catch { }


        }

        private void UpdateCuirassePointsMap(List<TableCuirasseMPoints> lPoints)
        {

            ObservableCollection<VMCuirassePoint> _cuirassePoints = new ObservableCollection<VMCuirassePoint>();

            foreach (TableCuirasseMPoints t in lPoints)
                _cuirassePoints.Add(new VMCuirassePoint()
                {
                    ID = t.Id,
                    LocationPoint = new Location(t.Coordinates.Latitude, t.Coordinates.Longitude, t.Coordinates.Altitude),
                    Note = t.Note
                });


            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RastrMap.CuirassePoints.Clear();
                RastrMap.CuirassePoints = _cuirassePoints;
            });
        }

        private void UpdateSpoofingTrack(TrackSpoofing trackSpoofing)
        {
            try
            {

                VMSpoofingTrack SpoofingDrone = new VMSpoofingTrack();

                    List<Location> locTrack = new List<Location>();

                    foreach (var tr in trackSpoofing.Coordinate)
                    {
                        if (tr.Latitude != 0 && tr.Longitude != 0)
                            locTrack.Add(new Location(tr.Latitude, tr.Longitude));
                    }



                SpoofingDrone.Angle = trackSpoofing.Angle;

                if (trackSpoofing.Coordinate != null && trackSpoofing.Coordinate.Count > 0)
                {
                    SpoofingDrone.LocationDrone = new Location(trackSpoofing.Coordinate.Last().Longitude, trackSpoofing.Coordinate.Last().Latitude, 0);
                    SpoofingDrone.LocationTrack = locTrack;
                }
                



                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.SpoofingTrack.LocationTrack.Clear();
                    RastrMap.SpoofingTrack = SpoofingDrone;
                });

            }
            catch { }
        }



        private void UpdateMapGrozaR(List<OperatorGrozaR> lGrozaR)
        {

            ObservableCollection<VMGrozaR> _operatorGrozaRPoints = new ObservableCollection<VMGrozaR>();

            foreach (OperatorGrozaR t in lGrozaR)
                //_operatorGrozaRPoints.Add(new VMGrozaR()
                //{
                //    ID = t.ID,
                //    LocationPoint = new Location(t.Coordinate.Longitude, t.Coordinate.Latitude),
                //    Azimuth = t.Azimuth,
                //    State = (EStateGrozaR)t.Mode,
                //    Note = t.Note,

                //});

                _operatorGrozaRPoints.Add(new VMGrozaR(t.ID, new Location(t.Coordinate.Longitude, t.Coordinate.Latitude), t.Note, t.Azimuth, (EStateGrozaR)t.Mode)
                );


            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                RastrMap.GrozaROperators.Clear();
                RastrMap.GrozaROperators = _operatorGrozaRPoints;
            });
        }


        private void HandlerDirectionEvent(object sender, float e)
        {

            //if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
            //    SetAngleBRD1((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, e);

            //if (mainWindowViewModel.LocalPropertiesVM.BRD1.SynchronizeEOM_Upper)
                SetAngleOEM(e);
        }

        private void HandlerDirectionJammingEvent(object sender, float e)
        {
            SetAngleUpperBrd1(e);
            //if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
            //{
            //    switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
            //    {
            //        case DllGrozaSProperties.Models.TypeBRD._2TS:
            //            SetAngleBRD1((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, e);
            //            break;

            //        case DllGrozaSProperties.Models.TypeBRD.Impressa:
            //            SetAngleImpressaBRD(e);
            //            break;

            //        default:
            //            break;
            //    }
            //}


            if (mainWindowViewModel.LocalPropertiesVM.BRD1.SynchronizeEOM_Upper)
                SetAngleOEM(e);
        }


        private void SetAngleUpperBrd1(float angle)
        {
            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
            {
                switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                {
                    case DllGrozaSProperties.Models.TypeBRD._2TS:
                        SetAngleBRD1((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, angle);
                        break;

                    case DllGrozaSProperties.Models.TypeBRD.Impressa:
                        SetAngleImpressaBRD(angle);
                        break;

                    default:
                        break;
                }
            }
        }



        private void HandlerDirectionJammingSecondEvent(object sender, float e)
        {
            if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                SetAngleBRD2((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper, e);

            if (mainWindowViewModel.LocalPropertiesVM.BRD2.SynchronizeEOM_Upper)
                SetAngleOEM(e);
        }

        private void HandlerDirectionConnectionEvent(object sender, float e)
        {
            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                SetAngleBRD1((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom, e);
        }

        private void HandlerDirectionConnectionSecondEvent(object sender, float e)
        {
            if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                SetAngleBRD2((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom, e);
        }

        private void UpdateRodnikTargetsMap(ObservableCollection<RodnikTarget> rodnik)
        {
            try
            {

                ObservableCollection<VMRodnikTarget> RodnikTarget = new ObservableCollection<VMRodnikTarget>();

                foreach (var t in rodnik)
                {
                    
                    List<Location> locTrack = new List<Location>();
                    foreach (var tr in t.rodnikMarks)
                    {
                        if (tr.Coordinate.Latitude != -255 && tr.Coordinate.Longitude != -255)
                            locTrack.Add(new Location(tr.Coordinate.Latitude, tr.Coordinate.Longitude));
                    }

                    Random rnd = new Random();

                    
                    RodnikTarget.Add(new VMRodnikTarget()
                    {
                        IDSource = t.ID,
                        Name = t.ID.ToString(),
                        View = (ViewRodnik)t.rodnikMarks.Last().Class,
                        LocationDrone = new Location(t.rodnikMarks.Last().Coordinate.Longitude, t.rodnikMarks.Last().Coordinate.Latitude, 0),
                        LocationTrack = locTrack

                    }); ;
                }






                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    RastrMap.RodnikTargets.Clear();
                    RastrMap.RodnikTargets = RodnikTarget;
                });

            }
            catch { }
        }

        private void HandlerJammerPositionEvent(object sender, WpfMapControl.Location e)
        {
            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(e.Latitude, 6),
                    Longitude = Math.Round(e.Longitude, 6)
                };

                ucJammerStation.SetJammerStationToPG(coord);
            }
            catch { }
        }

        private void HandlerSpoofingPositionEvent(object sender, WpfMapControl.Location e)
        {
            if (mainWindowViewModel.ActiveSpoofing)
            {
                MessageBox.Show(SMessages.messageUpdateSpoofingPosition);
                    return;
            }
                


            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(e.Latitude, 6),
                    Longitude = Math.Round(e.Longitude, 6)
                };

                mainWindowViewModel.GlobalPropertiesVM.Spoofing = coord;
            }
            catch { }
        }

        private void HandlerZorkiRPositionEvent(object sender, WpfMapControl.Location e)
        {
            UpdateZorkiRPosition(e.Latitude, e.Longitude);
        }

        private void RastrMap_OnZorkiRDirection(object sender, float e)
        {
            SendZorkiRCoord((short)e);
        }



        private void UpdateRegime(Mode mode)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (RastrMap.Jammers != null && RastrMap.Jammers.Count > 0 && RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault() != null)
                {
                    RastrMap.Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().Regime = (EMapMode)mode;
                }

            });
        }


        private ViewZorkiR FixType(ClassZorkiR realType)
        {

            ViewZorkiR result = ViewZorkiR.Unknown;
            switch (realType)
            {
                case ClassZorkiR.Human:
                    result = ViewZorkiR.Human;
                    break;
                case ClassZorkiR.UAV:
                    result = ViewZorkiR.UAV;
                    break;
                case ClassZorkiR.Car:
                    result = ViewZorkiR.Car;
                    break;
                case ClassZorkiR.Group:
                    result = ViewZorkiR.Group;
                    break;
                case ClassZorkiR.Human_manual:
                    result = ViewZorkiR.Human;
                    break;
                case ClassZorkiR.Group_manual:
                    result = ViewZorkiR.Group;
                    break;
                case ClassZorkiR.Car_manual:
                    result = ViewZorkiR.Car;
                    break;
                case ClassZorkiR.IFV:
                    result = ViewZorkiR.IFV;
                    break;
                case ClassZorkiR.Tank:
                    result = ViewZorkiR.Tank;
                    break;
                case ClassZorkiR.Helicopter:
                    result = ViewZorkiR.Helicopter;
                    break;
                case ClassZorkiR.UAV_manual:
                    result = ViewZorkiR.UAV;
                    break;
                case ClassZorkiR.Unknown:
                    result = ViewZorkiR.Unknown;
                    break;

            }

            return result;
        }

       
    }
}