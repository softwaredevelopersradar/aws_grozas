﻿

using GrozaBerezinaDLL;
using GrozaSModelsDBLib;
using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        IExGRZ_BRZ clientBerezinaPC;

        private void ConnectBerezina()
        {
            switch (mainWindowViewModel.LocalPropertiesVM.PC.Type)
            {

                case DllGrozaSProperties.Models.TypeCnt.Motorola:
                    OpenPortBerezina();
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Cisco:
                    ConnectPortBerezina(mainWindowViewModel.LocalPropertiesVM.PC.IpAddressCisco,
                        mainWindowViewModel.LocalPropertiesVM.PC.PortCisco);
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Router3G_4G:
                    ConnectPortBerezina(mainWindowViewModel.LocalPropertiesVM.PC.IpAddress3G4GRouter,
                        mainWindowViewModel.LocalPropertiesVM.PC.Port3G4GRouter);
                    break;

                default:
                    break;
            }
        }


        private void OpenPortBerezina()
        {

            clientBerezinaPC = new IExGRZ_BRZ((byte)lJammerStation.Find(x => x.Role == StationRole.Own).Id);

            InitEventBerezina();

            clientBerezinaPC.Connect(mainWindowViewModel.LocalPropertiesVM.PC.ComPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
        
        }

        
        private void ConnectPortBerezina(string IPServer, int Port)
        {

            clientBerezinaPC = new IExGRZ_BRZ((byte)lJammerStation.Find(x => x.Role == StationRole.Own).Id);

            InitEventBerezina();


            //if (GetIPAddress())

            //matedGroup.ConnectPort("175.20.40.3", 10020, "175.20.50.2", 10021);
            //else
            //    matedGroup.ConnectPort("175.20.50.2", 10021, "175.20.40.3", 10020);

            clientBerezinaPC.Connect(IPServer, Port);

        }

        private void DisconnectBerezina()
        {
            clientBerezinaPC.Disconnect();

            DeinitEventBerezina();

            clientBerezinaPC = null;
        }

        private void InitEventBerezina()
        {
            clientBerezinaPC.OnConnect += ClientBerezinaPC_OnConnect;
            clientBerezinaPC.OnDisconnect += ClientBerezinaPC_OnDisconnect;

            clientBerezinaPC.OnRequestCoord += ClientBerezinaPC_OnRequestCoord;
            clientBerezinaPC.OnRequestRangeSpec += ClientBerezinaPC_OnRequestRangeSpec;
            clientBerezinaPC.OnRequestRangeSector += ClientBerezinaPC_OnRequestRangeSector;
            clientBerezinaPC.OnRequestRegime += ClientBerezinaPC_OnRequestRegime; 
            clientBerezinaPC.OnRequestState += ClientBerezinaPC_OnRequestState;
            clientBerezinaPC.OnReceiveTextCmd += ClientBerezinaPC_OnReceiveTextCmd;
            clientBerezinaPC.OnReceiveConfirmTextCmd += ClientBerezinaPC_OnReceiveConfirmTextCmd;
            clientBerezinaPC.OnRequestSynchTime += ClientBerezinaPC_OnRequestSynchTime;
            clientBerezinaPC.OnRequestReconFWS += ClientBerezinaPC_OnRequestReconFWS;
            clientBerezinaPC.OnRequestReconFHSS += ClientBerezinaPC_OnRequestReconFHSS;
            clientBerezinaPC.OnRequestSupprFHSS += ClientBerezinaPC_OnRequestSupprFHSS;
            clientBerezinaPC.OnRequestSupprFWS += ClientBerezinaPC_OnRequestSupprFWS;
            clientBerezinaPC.OnRequestStateSupprFHSS += ClientBerezinaPC_OnRequestStateSupprFHSS;
            clientBerezinaPC.OnRequestStateSupprFWS += ClientBerezinaPC_OnRequestStateSupprFWS;




            clientBerezinaPC.OnRequestExecBear += ClientBerezinaPC_OnRequestExecBear;
            clientBerezinaPC.OnRequestSimulBear += ClientBerezinaPC_OnRequestSimulBear;

        }

        

        private void DeinitEventBerezina()
        {
            clientBerezinaPC.OnConnect -= ClientBerezinaPC_OnConnect;
            clientBerezinaPC.OnDisconnect -= ClientBerezinaPC_OnDisconnect;

            clientBerezinaPC.OnRequestCoord -= ClientBerezinaPC_OnRequestCoord;
            clientBerezinaPC.OnRequestExecBear -= ClientBerezinaPC_OnRequestExecBear;
            clientBerezinaPC.OnRequestRangeSector -= ClientBerezinaPC_OnRequestRangeSector;
            clientBerezinaPC.OnRequestRangeSpec -= ClientBerezinaPC_OnRequestRangeSpec;
            clientBerezinaPC.OnRequestReconFHSS -= ClientBerezinaPC_OnRequestReconFHSS;
            clientBerezinaPC.OnRequestReconFWS -= ClientBerezinaPC_OnRequestReconFWS;
            clientBerezinaPC.OnRequestRegime -= ClientBerezinaPC_OnRequestRegime;
            clientBerezinaPC.OnRequestSimulBear -= ClientBerezinaPC_OnRequestSimulBear;
            clientBerezinaPC.OnRequestState -= ClientBerezinaPC_OnRequestState;
            clientBerezinaPC.OnRequestStateSupprFHSS -= ClientBerezinaPC_OnRequestStateSupprFHSS;
            clientBerezinaPC.OnRequestStateSupprFWS -= ClientBerezinaPC_OnRequestStateSupprFWS;
            clientBerezinaPC.OnRequestSupprFHSS -= ClientBerezinaPC_OnRequestSupprFHSS;
            clientBerezinaPC.OnRequestSupprFWS -= ClientBerezinaPC_OnRequestSupprFWS;
            clientBerezinaPC.OnRequestSynchTime -= ClientBerezinaPC_OnRequestSynchTime;
            clientBerezinaPC.OnReceiveTextCmd -= ClientBerezinaPC_OnReceiveTextCmd;
            clientBerezinaPC.OnReceiveConfirmTextCmd -= ClientBerezinaPC_OnReceiveConfirmTextCmd;
        }

      

                
                

    }
}