﻿using AutoMapper;
using GrozaS_AWS.Controllers;
using GrozaS_AWS.Log;
using GrozaSModelsDBLib;
using GrpcDbClientLib;
using RadarRodnikControl;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using ValuesCorrectLib;

namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        MainWindowViewModel mainWindowViewModel;

        IMapper iMapper;

        System.Timers.Timer tmrAutoExecBearing = new System.Timers.Timer();

    

        System.Timers.Timer tmrUploadRS = new System.Timers.Timer();

        System.Timers.Timer tmrGetStatusAmplifier = new System.Timers.Timer();

        SoundPlayer playAlarm;

        Logging Log = new Logging();
        public MainWindow()
        {

            InitializeComponent();

            mainWindowViewModel = new MainWindowViewModel();

            
            mainWindowViewModel.DirectionRX.PropertyChanged += DirectionRX_PropertyChanged;
            mainWindowViewModel.DirectionBRD1_Upper.PropertyChanged += DirectionTX_PropertyChanged;
            mainWindowViewModel.DirectionBRD1_Bottom.PropertyChanged += DirectionTX_PropertyChanged;
            mainWindowViewModel.DirectionBRD2_Upper.PropertyChanged += DirectionTX_PropertyChanged;
            mainWindowViewModel.DirectionBRD2_Bottom.PropertyChanged += DirectionTX_PropertyChanged;

            mainWindowViewModel.DirectionOEM.PropertyChanged += DirectionOEM_PropertyChanged;

            mainWindowViewModel.AutoExecutiveBearing.PropertyChanged += AutoExecutiveBearing_PropertyChanged;
            mainWindowViewModel.PropertyChanged += MainWindowViewModel_PropertyChanged;

           

            CurrentSourcesDF.CollectionChanged += CurrentSourcesDF_CollectionChanged;

           
            this.DataContext = mainWindowViewModel;


            basicProperties.SetPassword("256");

            iMapper = MapperModels.InitializeAutoMapper().CreateMapper();


            InitGainWndEscope();

            InitLocalProperties();

            PropViewCoords.ViewCoords = (byte)mainWindowViewModel.LocalPropertiesVM.General.CoordinateView;


            InitPanorams();
            HidePanorams();

            ConnectCmpRX();

            ConnectCmpTX();

            InitTables();

            InitTemplateWnd();

            InitGainWnd();

            InitSpoofingWnd();

            InitAttenuatorWnd();


            InitJammingControl();

            InitDroneControl();

            InitChat();
                 
            ConnectOEM();

            ChangeStateDF();

            ConnectClientDB();
            ConnectClientGnss();


            SetResourceLanguage(basicProperties.Local.General.Language);
           TranslatorMainWindow.LoadDictionary(basicProperties.Local.General.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);
            JammingDrone_SetLanguage(basicProperties.Local.General.Language);
            DroneControl_SetLanguage(basicProperties.Local.General.Language);

            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);

            InitViewParamMap();
            
            newWindow.SetLanguage(basicProperties.Local.General.Language); 
            //templateWindow.SetLanguage(basicProperties.Local.General.Language);
            TemplateWindow_SetLanguage(basicProperties.Local.General.Language);
            SpoofingWindow_SetLanguage(basicProperties.Local.General.Language);
            GainWindow_SetLanguage(basicProperties.Local.General.Language);
            gainWindowEscope?.SetLanguage(basicProperties.Local.General.Language);

            //spoofingWindow.TwoSystem = false;
            //spoofingWindow.FourSystems = basicProperties.Local.SS.GLONASS;

            
            InitializeTimerAutoExecBearing();

            InitializeTimerUploadRS();

            

            InitializeTimerAeroscopeCheck();

            Log.SaveLog(" Map program started.");
            //try
            //{
            //    ReadTDFError(AppDomain.CurrentDomain.BaseDirectory + "TDFError.txt");

            //}
            //catch 
            //{
            //}
        }

        private void InitViewParamMap()
        {
            try
            {
                RastrMap.SetScale(mainWindowViewModel.LocalPropertiesVM.Map.CurrentScale);

                Coord center = new Coord()
                {
                    Latitude = mainWindowViewModel.LocalPropertiesVM.Map.Latitude,
                    Longitude = mainWindowViewModel.LocalPropertiesVM.Map.Longitude
                };
                
                mainWindowViewModel.LocationCenter = center;
            }

            catch
            { }
        }

        private void MainWindowViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "TempMode":
                    SetViewMode();

                    UpdateRegime(mainWindowViewModel.TempMode);

                    CheckVoiceAlarm();

                    Log.SetMode(mainWindowViewModel.TempMode, mainWindowViewModel.PreviousMode);

                    break;

                case "ActiveSpoofing":
                    Log.SetModeSpoofing(mainWindowViewModel.ActiveSpoofing, spoofingWindow.SatelitesGPS + spoofingWindow.SatelitesGLONASS + spoofingWindow.SatellitesBeidou + spoofingWindow.SatellitesGalileo, mainWindowViewModel.StartSpoofingTime);
                    break;

                
                default:
                    break;
            }
        }

        private void SetViewMode()
        {
            mainWindowViewModel.ViewMode = GetResourceTitle((string)Enum.GetName(typeof(Mode), mainWindowViewModel.TempMode));
        }


        private void InitializeTimerUploadRS()
        {
            tmrUploadRS.Elapsed += TickTimerUploadRS;
            tmrUploadRS.Interval = 1000;
            tmrUploadRS.AutoReset = true;
        }

        private void InitializeTimerAutoExecBearing()
        {
            tmrAutoExecBearing.Elapsed += Timer_Elapsed;
            tmrAutoExecBearing.Interval = 3000;
            tmrAutoExecBearing.AutoReset = true;
        }

      
    


    

        private void InitializeTimerGetStatusAmplifier()
        {
            tmrGetStatusAmplifier.Elapsed += TickTimerGetStatusAmp;
            tmrGetStatusAmplifier.Interval = 2000;
            tmrGetStatusAmplifier.AutoReset = true;
        }



        private void CurrentSourcesDF_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
                RefreshDependenceSource();

            //CheckVoiceAlarm();


        }


        //private void CheckVoiceAlarm()
        //{
        //    if (mainWindowViewModel.TempMode == Mode.JAMMING)
        //    {
        //        StopAudioSignal();
        //        StartAudioSignal(mainWindowViewModel.TempMode);
        //    }

        //    else
        //    {
        //        StopAudioSignal();

        //        if (CurrentSourcesDF != null && CurrentSourcesDF.Count > 0 || zorkiRTargets != null && zorkiRTargets.Count > 0)
        //            StartAudioSignal(mainWindowViewModel.TempMode);
        //        else
        //            StopAudioSignal();
        //    }


        //    //var _countAlarm = 0;

        //    //if (CurrentSourcesDF != null && CurrentSourcesDF.Count > 0 )
        //    //{
        //    //    _countAlarm = 1;
        //    //    foreach (var src in CurrentSourcesDF)
        //    //    {
        //    //        if (src.Track != null && src.Track.Count > 0 && src.Track.Last().Zone == Zones.ALARM)
        //    //            _countAlarm++;
        //    //    }

        //    //    if (_countAlarm > 0)
        //    //        StartAudioSignal();
        //    //    else
        //    //        StopAudioSignal();

        //    //}





        //}
        private async void CheckVoiceAlarm()
        {
            try
            {
                if (mainWindowViewModel.TempMode == Mode.JAMMING)
                {
                    alert.PlaySound1(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileJ, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
                }

                else
                {
                    //StopAudioSignal();

                    if (CurrentSourcesDF != null && CurrentSourcesDF.Count > 0 && CurrentSourcesDF.OrderByDescending(t => t.Track.Last().Time).FirstOrDefault().Type != 0)
                    {
                        alert.PlaySound1(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileRI, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
                        await Task.Delay(this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec * 1000);
                        this.alert.StopSound1();
                    }

                    else
                        this.alert.StopSound1();
                }
            }
            catch
            {
            }

        }

        private void StartAudioSignal(Mode mode)
        {
            switch (mode)
            {
                case Mode.STOP:

                    playAlarm.SoundLocation = mainWindowViewModel.LocalPropertiesVM.General.AudioFileRI;
                    break;

                case Mode.ELINT:
                    playAlarm.SoundLocation = mainWindowViewModel.LocalPropertiesVM.General.AudioFileRI;
                    break;

                case Mode.JAMMING:
                    playAlarm.SoundLocation = mainWindowViewModel.LocalPropertiesVM.General.AudioFileJ;
                    break;

                default:
                    break;
            }

           
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                playAlarm.PlayLooping();
            });

        }

        private void StopAudioSignal()
        {
           

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                playAlarm.Stop();
            });

        }

       

        private void RefreshDependenceSource()
        {

            try
            {
                List<TableSource> tableSourceConvert = new List<TableSource>();

                foreach (var src in CurrentSourcesDF)
                {
                    ObservableCollection<TableTrack> trackConvert = new ObservableCollection<TableTrack>();

                    foreach (var trc in src.Track)
                    {
                        ObservableCollection<TableJamBearing> bearingConvert = new ObservableCollection<TableJamBearing>();

                        foreach (var brn in trc.Bearings)
                        {
                            bearingConvert.Add(new TableJamBearing()
                            {
                                Id = brn.ID,
                                Bearing = brn.Bearing,
                                Distance = brn.Distance,
                                NumJammer = brn.Jammer
                            });
                        }



                        trackConvert.Add(new TableTrack()
                        {
                            Id = trc.ID,
                            Time = trc.Time,
                            FrequencyMHz = trc.Frequency,
                            FrequencyRX = trc.FrequencyRX,
                            BandMHz = trc.Band,
                            Coordinates = trc.Coordinate,
                            Bearing = bearingConvert

                        });
                    }

                    tableSourceConvert.Add(new TableSource()
                    {
                        Id = src.ID,
                        Type = src.Type,    
                        TypeRSM = (byte)src.TypeRSM,
                        Note = src.Note,
                        Track = trackConvert
                    });

                };

                

                DroneControl_UpdateTableSource(tableSourceConvert);
                UpdateMapBearing(lJammerStation, CurrentSourcesDF);
                UpdateSourceMap(CurrentSourcesDF);

                
            }
            catch
            { }
        }

      

        private void AutoExecutiveBearing_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (mainWindowViewModel.AutoExecutiveBearing.AutoExecBearing)
                tmrAutoExecBearing.Start();
            else
                tmrAutoExecBearing.Stop();
        }

        

        public void LocalPropertiesVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            mainWindowViewModel.DirectionRX.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            mainWindowViewModel.DirectionRX.Error = mainWindowViewModel.LocalPropertiesVM.CmpRX.ErrorDeg;

            //mainWindowViewModel.DirectionTX.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle > mainWindowViewModel.DirectionTX.Azimuth) ?
            //    (mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle - mainWindowViewModel.DirectionTX.Azimuth) : (360 - (mainWindowViewModel.DirectionTX.Azimuth - mainWindowViewModel.GlobalPropertiesVM.CmpTX.Angle));

            UpdateCourseAngleCmpTX();

            //mainWindowViewModel.DirectionTX.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;

            mainWindowViewModel.DirectionOEM.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            mainWindowViewModel.DirectionOEM.Error = mainWindowViewModel.LocalPropertiesVM.EOM.ErrorDeg;

        
        }

       

        private void Map_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
         
            RefreshDependenceSource();

            HandlerLocalProperties(this, mainWindowViewModel.LocalPropertiesVM);    
        }

        private void DirectionOEM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //UpdateMapDirectionOEM(10);
        }

        private void DirectionTX_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMapDirectionTX();
        }

        private void DirectionRX_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateMapDirectionRX();

            
        }



        private async void Window_Closing(object sender, CancelEventArgs e)
        {
            (mainWindowViewModel.clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable -= HandlerUpdate_TableSource;
            
            try
            {
                
                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper &&
                    mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Upper > 0)
                {
                    switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                    {
                        case DllGrozaSProperties.Models.TypeBRD._2TS:
                            comBRD1?.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Upper, 0);
                            break;

                        case DllGrozaSProperties.Models.TypeBRD.Impressa:
                            GoToTransportAngleImpressaBRD(mainWindowViewModel.LocalPropertiesVM.BRD1
                                .TransportAngle_Upper);
                            //SetAngleImpressaBRD((short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Upper);
                            break;

                        default:
                            break;
                    }
                }



                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom &&
                    mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom > 0)
                {
                    switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                    {
                        case DllGrozaSProperties.Models.TypeBRD._2TS:
                            comBRD1?.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom, 0);

                            break;

                        case DllGrozaSProperties.Models.TypeBRD.Impressa:
                            //SetAngleImpressaBRD((short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom);
                            break;

                        default:
                            break;
                    }
                }

            }
            catch { }

            //Thread.Sleep(1000);
            await Task.Delay(1000);

            try
            {
                if (comBRD2 != null)
                {
                    if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper && mainWindowViewModel.LocalPropertiesVM.BRD2.TransportAngle_Upper>0)
                        comBRD2.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD2.TransportAngle_Upper, 0);

                    if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom && mainWindowViewModel.LocalPropertiesVM.BRD2.TransportAngle_Bottom>0)
                        comBRD2.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD2.TransportAngle_Bottom, 0);
                }

            }
            catch { }

            SaveSourceTDF_DB();

            try
            {
                if (tcpServerPC != null)
                    tcpServerPC.DestroyServer();
            }
            catch { }

            try 
            {
                mainWindowViewModel.LocalPropertiesVM.Map.CurrentScale = RastrMap.CurrentScale;
                mainWindowViewModel.LocalPropertiesVM.Map.Latitude = RastrMap.CenterCoord.Latitude;
                mainWindowViewModel.LocalPropertiesVM.Map.Longitude = RastrMap.CenterCoord.Longitude;
            }
            catch { }

            Log.SaveLog(" Map program stopped.");

            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }


        

        

        
        void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                SendGetExecutiveBearing(CurrentSourcesDF.Last().ID, TypeQuery.ASK);
                ShowSentExecutingBearingRequest(CurrentSourcesDF.Last(), TypeQuery.ASK);
            }
            
            catch { }
        }

      




        private void ChangeLanguage(DllGrozaSProperties.Models.Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            //LoadDictionary();
            //SetCategoryGlobalNames();
            //SetCategoryLocalNames();

        }


        private void SetDynamicResources(DllGrozaSProperties.Models.Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch 
            {
                
            }

        }

        private void RastrMap_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void HandlerScaleEvent(object sender, double e)
        {
            mainWindowViewModel.LocalPropertiesVM.Map.CurrentScale = e;

        }

        private async void UIRsDetecting_OnUpdateGainUSRP(object sender, EventArgs e)
        {
            await Task.Run(() => SendGainUSRP(UIRsDetecting.GainUSRP));
        }

        private void ClearRSButton_Click(object sender, RoutedEventArgs e)
        {
            ResetResultDRS();
        }

        private void TransportBRD1Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper &&
                    mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Upper > 0)
                {
                    switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                    {
                        case DllGrozaSProperties.Models.TypeBRD._2TS:
                            comBRD1.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Upper, 0);
                            break;

                        case DllGrozaSProperties.Models.TypeBRD.Impressa:
                            GoToTransportAngleImpressaBRD(mainWindowViewModel.LocalPropertiesVM.BRD1
                                .TransportAngle_Upper);
                            break;

                        default:
                            break;
                    }
                }

                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom &&
                    mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom > 0)
                {
                    switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                    {
                        case DllGrozaSProperties.Models.TypeBRD._2TS:
                            comBRD1.SendSetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom, 0, (short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom, 0);

                            break;

                        case DllGrozaSProperties.Models.TypeBRD.Impressa:
                            //SetAngleImpressaBRD((short)mainWindowViewModel.LocalPropertiesVM.BRD1.TransportAngle_Bottom);
                            break;

                        default:
                            break;
                    }
                }

                

            }
            catch { }
        }

        private async void rec1Button_Click(object sender, RoutedEventArgs e)
        {
            alert.PlaySound1(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileRI, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
            await Task.Delay(this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec * 1000);
            this.alert.StopSound1();
        }

        private void rec2Button_Click(object sender, RoutedEventArgs e)
        {
            alert.PlaySound1(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileJ, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
        }

        private void rec3Button_Click(object sender, RoutedEventArgs e)
        {
            alert.PlaySound2(this.mainWindowViewModel.LocalPropertiesVM.General.AudioFileRadar, this.mainWindowViewModel.LocalPropertiesVM.General.AudioRIDurationSec);
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            this.alert.StopSound1();
        }
    }
}
