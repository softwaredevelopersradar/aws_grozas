﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Controllers
{
    public static class MapperModels
    {
      
        public static MapperConfiguration InitializeAutoMapper()
        {
            MapperConfiguration configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ModelsProfile>();
               
                cfg.CreateMap<DllGrozaSProperties.Models.GlobalProperties, GrozaSModelsDBLib.GlobalProperties>()
                    //.ForMember(dest => dest.CmpRX, opt => opt.MapFrom(src => src.CmpRX))
                    //.ForMember(dest => dest.CmpTX, opt => opt.MapFrom(src => src.CmpTX))
                    //.ForMember(dest => dest.Gnss, opt => opt.MapFrom(src => src.GNSS))
                    //.ForMember(dest => dest.Spoofing, opt => opt.MapFrom(src => src.Spoofing))
                    //.ForMember(dest => dest.RadioIntelegence, opt => opt.MapFrom(src => src.RadioIntelegence))
                    //.ForMember(dest => dest.Jamming, opt => opt.MapFrom(src => src.Jamming))
                    .ForMember(dest => dest.Oem, opt => opt.MapFrom(src => src.EOM))
                    .ReverseMap();

            });


            return configuration;
        }

    }
}
