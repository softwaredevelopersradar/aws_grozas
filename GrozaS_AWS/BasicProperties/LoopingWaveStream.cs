﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS
{
    using NAudio.Wave;

    public class LoopingWaveStream : WaveStream
    {
        private WaveStream sourceStream;
        private long loopStart;
        private long loopEnd;
        private long position;

        public LoopingWaveStream(WaveStream sourceStream, double loopStartTime, double loopEndTime)
        {
            this.sourceStream = sourceStream;
            this.loopStart = (long)(loopStartTime * sourceStream.WaveFormat.SampleRate) * sourceStream.WaveFormat.BlockAlign;
            this.loopEnd = (long)(loopEndTime * sourceStream.WaveFormat.SampleRate) * sourceStream.WaveFormat.BlockAlign - 44;

            position = 0;
        }

        public override WaveFormat WaveFormat => sourceStream.WaveFormat;

        public override long Length => sourceStream.Length;

        public override long Position
        {
            get => position;
            set => position = value;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int totalBytesRead = 0;

            while (totalBytesRead < count)
            {
                int bytesRead = sourceStream.Read(buffer, offset + totalBytesRead, count - totalBytesRead);
                if (bytesRead == 0)
                {
                    if (position >= loopEnd)
                    {
                        // Reached the end of the loop, go back to the start
                        position = loopStart;
                        sourceStream.Position = loopStart;
                    }
                    else
                    {
                        // Reached the end of the source stream, break the loop
                        break;
                    }
                }

                totalBytesRead += bytesRead;
                position += bytesRead;

                if (position >= loopEnd)
                {
                    // Reached the end of the loop, go back to the start
                    position = loopStart;
                    sourceStream.Position = loopStart;
                }
            }

            return totalBytesRead;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                sourceStream.Dispose();
            }
        }
    }
}
