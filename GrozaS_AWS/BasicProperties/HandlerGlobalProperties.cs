﻿using GrozaSModelsDBLib;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private void Global_Gnss_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateGlobalProperties(mainWindowViewModel.GlobalPropertiesVM);
        }

        private void Global_CmpRX_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            Property_CmpRX_Update();

            SendCourseAngle((short)mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle);

            UpdateGlobalProperties(mainWindowViewModel.GlobalPropertiesVM);
            
        }

        private void Global_CmpTX_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Property_CmpTX_Update();

            UpdateGlobalProperties(mainWindowViewModel.GlobalPropertiesVM);
        }


        private void Global_Oem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Property_Oem_Update();

            UpdateGlobalProperties(mainWindowViewModel.GlobalPropertiesVM);
        }

    

        private void Global_RadioIntelegence_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SaveSourceTDF_DB();
        }

       

        private void GlobalPropertiesVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            switch (e.PropertyName)
            {
                case "Gnss":
                    break;

                case "CmpRX":
                    break;

                case "CmpTX":
                    break;

                case "Oem":
                    break;

                case "RadioIntelegence":

                    SaveSourceTDF_DB();

                    break;

                case "Jamming":
                    break;

                case "Spoofing":
                    
                    UpdateGlobalProperties(mainWindowViewModel.GlobalPropertiesVM);
                    SendStartCoordsSpoofing();


                    break;

                default:
                    break;
            }


            
        }

        private void SaveSpoofingFile()
        {
            try
            {
                string[,] strSp = new string[2, 1];
                strSp[0, 0] = "Latitude " + mainWindowViewModel.GlobalPropertiesVM.Spoofing.Latitude.ToString();
                strSp[1, 0] = "Longitude " + mainWindowViewModel.GlobalPropertiesVM.Spoofing.Longitude.ToString();

                RepToFile.RepToFile.SaveUpdateToTxt(AppDomain.CurrentDomain.BaseDirectory, "SpoofingFile.txt", strSp);

             
            }
            catch { }
        }


        private void SetStaticAngle()
        {
            mainWindowViewModel.DirectionRX.Azimuth = (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle == -1)?
                                                       mainWindowViewModel.DirectionRX.Azimuth :
                                                       mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle;
        }

        private void Property_CmpRX_Update()
        {
            mainWindowViewModel.DirectionRX.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            mainWindowViewModel.DirectionRX.Error = mainWindowViewModel.LocalPropertiesVM.CmpRX.ErrorDeg;

            mainWindowViewModel.DirectionOEM.CourseAngle = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;

            mainWindowViewModel.AngleOrientation = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle + mainWindowViewModel.LocalPropertiesVM.CmpRX.ErrorDeg) % 360;

            UpdateCourseAngleCmpTX();
        }

        private void Property_CmpTX_Update()
        {

            //UpdateCourseAngleCmpTX();
            
        }

        private void Property_Oem_Update()
        {
            mainWindowViewModel.DirectionOEM.Error = mainWindowViewModel.LocalPropertiesVM.EOM.ErrorDeg;
        }

        private void UpdateCourseAngleCmpTX()
        {

            #region BRD1
            mainWindowViewModel.DirectionBRD1_Bottom.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));



            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
            {
                float add = mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper;

                if (add < 0)
                    add += 360;

                mainWindowViewModel.DirectionBRD1_Upper.CourseAngle = mainWindowViewModel.DirectionBRD1_Bottom.CourseAngle + add;
            }
            else
                mainWindowViewModel.DirectionBRD1_Upper.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));


            #endregion


            #region RotateBRD1

            mainWindowViewModel.DirectionRotateBRD1_Bottom.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));



            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
            {
                float add = mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper;

                if (add < 0)
                    add += 360;

                mainWindowViewModel.DirectionRotateBRD1_Upper.CourseAngle = mainWindowViewModel.DirectionRotateBRD1_Bottom.CourseAngle + add;
            }
            else
                mainWindowViewModel.DirectionRotateBRD1_Upper.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Upper - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));


            #endregion





            #region BRD2
            mainWindowViewModel.DirectionBRD2_Bottom.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));



            if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
            {
                float add = mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper;

                if (add < 0)
                    add += 360;

                mainWindowViewModel.DirectionBRD2_Upper.CourseAngle = mainWindowViewModel.DirectionBRD2_Bottom.CourseAngle + add;
            }
            else
                mainWindowViewModel.DirectionBRD2_Upper.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));


            #endregion


            #region RotateBRD2

            mainWindowViewModel.DirectionRotateBRD2_Bottom.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));



            if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
            {
                float add = mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper;

                if (add < 0)
                    add += 360;

                mainWindowViewModel.DirectionRotateBRD2_Upper.CourseAngle = mainWindowViewModel.DirectionRotateBRD2_Bottom.CourseAngle + add;
            }
            else
                mainWindowViewModel.DirectionRotateBRD2_Upper.CourseAngle = (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle > mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper) ?
                (mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper) : (360 - (mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Upper - mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle));


            #endregion





            
        }


    }
}