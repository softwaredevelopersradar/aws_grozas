﻿
using DllGrozaSProperties.Models;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Media;
using GrozaS_AWS.Log;
using ValuesCorrectLib;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {

        AudioAlert alert = new AudioAlert();
        LocalProperties defaultLocalProperties = new LocalProperties();

        public void InitDefaultProperties()
        {
            defaultLocalProperties = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "DefaultLocalProperties.yaml");

        }


        public void InitLocalProperties()
        {
            mainWindowViewModel.LocalPropertiesVM.Map.PropertyChanged -= Map_PropertyChanged; 
            mainWindowViewModel.LocalPropertiesVM.Globus.PropertyChanged -= Globus_PropertyChanged;
            basicProperties.Local.Lemt.PropertyChanged -= Lemt_PropertyChanged;


            mainWindowViewModel.LocalPropertiesVM = YamlLoad(AppDomain.CurrentDomain.BaseDirectory + "LocalProperties.yaml");

            LoadSignalFile();

            SetLocalProperties();

            if (mainWindowViewModel.LocalPropertiesVM != null)
            {
                mainWindowViewModel.LocalPropertiesVM.Map.PropertyChanged += Map_PropertyChanged;
                mainWindowViewModel.LocalPropertiesVM.Globus.PropertyChanged += Globus_PropertyChanged;
                basicProperties.Local.Lemt.PropertyChanged += Lemt_PropertyChanged;

             
                Log.PathToLog = mainWindowViewModel.LocalPropertiesVM.General.FolderRI_UAV ;
                // Log.FileToLog = "\\" + DateTime.Now.ToString("HH-mm-ss") + ".txt";
                Log.FileToLog = "\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
               
                Log.LogMaxSizeMB = mainWindowViewModel.LocalPropertiesVM.General.ScreenshotsStorageMB;
                Log.LogLifetimeDays = mainWindowViewModel.LocalPropertiesVM.General.ScreenshotsLivetimeDays;

                //Screenshoter.ScreenshotPath = mainWindowViewModel.LocalPropertiesVM.General.FolderRI_UAV;
                //Screenshoter.ScreensLifeTimeHours = mainWindowViewModel.LocalPropertiesVM.General.ScreenshotsLivetimeHours;

            }


            

        }

        private void Globus_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HandlerLocalProperties(this, mainWindowViewModel.LocalPropertiesVM);
        }

        private void Lemt_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            mainWindowViewModel.LocalPropertiesVM.Lemt.Human = basicProperties.Local.Lemt.Human;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Car = basicProperties.Local.Lemt.Car;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Helicopter = basicProperties.Local.Lemt.Helicopter;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Group = basicProperties.Local.Lemt.Group;
            mainWindowViewModel.LocalPropertiesVM.Lemt.UAV = basicProperties.Local.Lemt.UAV;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Tank = basicProperties.Local.Lemt.Tank;
            mainWindowViewModel.LocalPropertiesVM.Lemt.IFV = basicProperties.Local.Lemt.IFV;
            mainWindowViewModel.LocalPropertiesVM.Lemt.Unknown = basicProperties.Local.Lemt.Unknown;

            UpdateZorkiRTargetVisible();
        }

       






        #region HandlerLocalProperties

        public void HandlerLocalProperties(object sender, LocalProperties arg)
        {
            YamlSave(arg);

            InitLocalProperties();

            SetResourceLanguage(basicProperties.Local.General.Language);
            TranslatorTables.LoadDictionary(basicProperties.Local.General.Language);

            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);
            newWindow.SetLanguage(basicProperties.Local.General.Language);
           // templateWindow.SetLanguage(basicProperties.Local.General.Language);
            

            SetViewMode();

            SaveSourceTDF_DB();

            //DroneControl_UpdateSourcesCollection();

            JammingDrone_OnLocalPropChanged();
            //ClientGnss_OnLocalPropChanged();
            StartGnssThreads();

            InitAntennaDirections();

            UpdateJammerMap(lJammerStation);
            LoadSignalFile();

            Property_CmpRX_Update();

            //Property_CmpTX_Update();

            UpdateErrorCmpTx();

            Property_Oem_Update();

            UpdateGlobusCoordinate();
            
            return;


        }

        private void InitAntennaDirections()
        {
            mainWindowViewModel.DirectionBRD1_Upper.Address = mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper;
            mainWindowViewModel.DirectionBRD1_Bottom.Address = mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom;

            mainWindowViewModel.DirectionBRD2_Upper.Address = mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper;
            mainWindowViewModel.DirectionBRD2_Bottom.Address = mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom;

            mainWindowViewModel.DirectionRotateBRD1_Upper.Address = mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper;
            mainWindowViewModel.DirectionRotateBRD1_Bottom.Address = mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom;

            mainWindowViewModel.DirectionRotateBRD2_Upper.Address = mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper;
            mainWindowViewModel.DirectionRotateBRD2_Bottom.Address = mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom;

        }

        private void UpdateErrorCmpTx()
        {
            //mainWindowViewModel.DirectionBRD1_Upper.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;

            //mainWindowViewModel.DirectionRotateBRD1_Upper.Error = mainWindowViewModel.LocalPropertiesVM.CmpTX.ErrorDeg;
        }

        private void LoadSignalFile()
        {
            
            try
            {
                playAlarm = new SoundPlayer();
            }
            catch
            { }
        }


        public void SetLocalProperties()
        {
            try
            {
                basicProperties.Local = mainWindowViewModel.LocalPropertiesVM;
            }
            catch { }
        }




        private void HandlerLocalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                InitDefaultProperties();

                HandlerLocalProperties(this, defaultLocalProperties);

                SetLocalProperties();
            }
            catch
            { }
        }
        #endregion


        #region HandlerGlobalProperties
        public void HandlerGlobalProperties(object sender, DllGrozaSProperties.Models.GlobalProperties arg)
        {

            GrozaSModelsDBLib.GlobalProperties globalPropertiesChange = iMapper.Map<DllGrozaSProperties.Models.GlobalProperties, GrozaSModelsDBLib.GlobalProperties>(arg);
            
            UpdateGlobalProperties(globalPropertiesChange);

            SaveSourceTDF_DB();
        }


        public void UpdateGlobalProperties(GrozaSModelsDBLib.GlobalProperties globalPropertiesChange)
        {
            globalPropertiesChange.Id = 1;

            try
            {
                mainWindowViewModel.clientDB?.Tables[NameTable.GlobalProperties].Add(globalPropertiesChange);
            }

            catch
            { }

            
        }


        public void SetGlobalProperties()
        {
            try
            {
                iMapper.Map(mainWindowViewModel.GlobalPropertiesVM, basicProperties.Global);
            }
            catch
            { }
        }

        private void HandlerGlobalDefaultProperties_Click(object sender, EventArgs e)
        {
            try
            {
                mainWindowViewModel.clientDB?.Tables[NameTable.GlobalProperties].Add(new GrozaSModelsDBLib.GlobalProperties() { Id = 1 });
            }

            catch
            { }
        }


        private void UpdateZorkiRTargetVisible()
        {
            Dictionary<ViewZorkiR, bool> dictTemp = new Dictionary<ViewZorkiR, bool>();
            foreach (var zrv in MainWindowViewModel.GetEnumValues<ViewZorkiR>())
                dictTemp.Add(zrv, true);


            dictTemp[ViewZorkiR.Human] = mainWindowViewModel.LocalPropertiesVM.Lemt.Human;
            dictTemp[ViewZorkiR.Car] = mainWindowViewModel.LocalPropertiesVM.Lemt.Car;
            dictTemp[ViewZorkiR.Helicopter] = mainWindowViewModel.LocalPropertiesVM.Lemt.Helicopter;
            dictTemp[ViewZorkiR.Group] = mainWindowViewModel.LocalPropertiesVM.Lemt.Group;
            dictTemp[ViewZorkiR.UAV] = mainWindowViewModel.LocalPropertiesVM.Lemt.UAV;
            dictTemp[ViewZorkiR.Tank] = mainWindowViewModel.LocalPropertiesVM.Lemt.Tank;
            dictTemp[ViewZorkiR.IFV] = mainWindowViewModel.LocalPropertiesVM.Lemt.IFV;
            dictTemp[ViewZorkiR.Unknown] = mainWindowViewModel.LocalPropertiesVM.Lemt.Unknown;

            mainWindowViewModel.ZorkiRTargetVisible = dictTemp;
        }


        private void CheckEmptySpoofPoint()
        {
            if ((mainWindowViewModel.GlobalPropertiesVM.Spoofing.Latitude == -1 || mainWindowViewModel.GlobalPropertiesVM.Spoofing.Longitude == -1) && (mainWindowViewModel.GlobalPropertiesVM.Gnss.Latitude != -1 && mainWindowViewModel.GlobalPropertiesVM.Gnss.Longitude != -1))
            {
                mainWindowViewModel.GlobalPropertiesVM.Spoofing = new Coord() { Latitude = mainWindowViewModel.GlobalPropertiesVM.Gnss.Latitude, Longitude = mainWindowViewModel.GlobalPropertiesVM.Gnss.Longitude  }; 
            }

        }


        private void UpdateZorkiRPosition(double Latitude, double Longitude)
        {
            try
            {
                Coord coord = new Coord
                {
                    Latitude = Math.Round(Latitude, 6),
                    Longitude = Math.Round(Longitude, 6)
                };


                if (mainWindowViewModel.LocalPropertiesVM.Lemt.Latitude != coord.Latitude || mainWindowViewModel.LocalPropertiesVM.Lemt.Longitude != coord.Longitude)
                {
                    mainWindowViewModel.LocalPropertiesVM.Lemt.Latitude = coord.Latitude;
                    mainWindowViewModel.LocalPropertiesVM.Lemt.Longitude = coord.Longitude;

                    HandlerLocalProperties(this, mainWindowViewModel.LocalPropertiesVM);
                }
                


            }
            catch { }

        }
        #endregion
    }
}