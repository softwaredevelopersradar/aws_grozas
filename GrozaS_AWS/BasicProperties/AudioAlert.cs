﻿using System;

namespace GrozaS_AWS
{
    using System.IO;

    using NAudio.Wave;

    public class AudioAlert
    {
        //private string audioFile1 = String.Empty;
        //private string audioFile2 = String.Empty;

        private WaveOut waveOut1 = new WaveOut();
        private WaveOut waveOut2 = new WaveOut();

        private WaveFileReader audiofileReader1;
        private WaveFileReader audiofileReader2;

        private WaveStream audioStream1;
        private LoopingWaveStream loopingStream; 

        public void PlaySound1(string path, int duration)
        {
            try
            {
                if (this.waveOut1.PlaybackState == PlaybackState.Playing)
                {
                    StopSound1();
                }

                

                if (!File.Exists(path))
                    return;

                
                audioStream1 = new WaveFileReader(path);
                loopingStream = new LoopingWaveStream(this.audioStream1, 0, audioStream1.TotalTime.TotalSeconds); //audioStream1.TotalTime.TotalSeconds
                waveOut1 = new WaveOut();
               
                this.waveOut1.Init(loopingStream);
                this.waveOut1.Play();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }
        }

        public void PlaySound2(string path, int duration)
        {
            try
            {
                if (this.waveOut2.PlaybackState == PlaybackState.Playing)
                {
                    StopSound2();
                }

                if (!File.Exists(path))
                    return;

                audiofileReader2 = new WaveFileReader(path);
                waveOut2 = new WaveOut();
                this.waveOut2.Init(audiofileReader2);
                this.waveOut2.Play();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //throw;
            }
            
        }

        public void StopSound1()
        {
            try
            {
                this.waveOut1?.Stop();
                this.waveOut1?.Dispose();
                loopingStream?.Dispose();
                //this.audiofileReader1?.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
        }

        public void StopSound2()
        {
            try
            {
                this.waveOut2?.Stop();
                this.waveOut2?.Dispose();
                this.audiofileReader2?.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
        }
    }
}
