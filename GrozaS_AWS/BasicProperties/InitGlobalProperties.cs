﻿using AutoMapper;
using GrozaS_AWS.Controllers;
using GrozaSModelsDBLib;
using System.Linq;
using System.Threading.Tasks;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {


        public async Task InitGlobalProperties()
        {

            mainWindowViewModel.GlobalPropertiesVM = (await mainWindowViewModel.clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GrozaSModelsDBLib.GlobalProperties>()).FirstOrDefault();

            

            mainWindowViewModel.GlobalPropertiesVM.PropertyChanged += GlobalPropertiesVM_PropertyChanged;

            mainWindowViewModel.GlobalPropertiesVM.Gnss.PropertyChanged += Global_Gnss_PropertyChanged;

            mainWindowViewModel.GlobalPropertiesVM.CmpRX.PropertyChanged += Global_CmpRX_PropertyChanged;

            mainWindowViewModel.GlobalPropertiesVM.CmpTX.PropertyChanged += Global_CmpTX_PropertyChanged;

            mainWindowViewModel.GlobalPropertiesVM.Oem.PropertyChanged += Global_Oem_PropertyChanged;
          
            mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.PropertyChanged += Global_RadioIntelegence_PropertyChanged;




            Property_CmpRX_Update();

            //Property_CmpTX_Update();

            UpdateErrorCmpTx();

            Property_Oem_Update();            

            SetGlobalProperties();

            SaveSpoofingFile();

            SetStaticAngle();

            CheckEmptySpoofPoint();

            UpdateOwnJammerCoord(mainWindowViewModel.GlobalPropertiesVM.Gnss);

        }


 

    }
}