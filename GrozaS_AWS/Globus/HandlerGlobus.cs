﻿using Bearing;
using Globus;
using Globus.Events;
using GlobusControl;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using OEM;
using RadarRodnikControl;
using Rodnik;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        System.Timers.Timer tmrGlobusCheck = new System.Timers.Timer();

        private void GlobusConnection_Click(object sender, RoutedEventArgs e)
        {
            if (ClientGlobus == null && (mainWindowViewModel.StateConnectionGlobus == ConnectionStates.Unknown ||
                                        mainWindowViewModel.StateConnectionGlobus == ConnectionStates.Disconnected))
                ConnectServerGlobus();
            else
                DisconnectServerGlobus();
        }

        private void ClientGlobus_OnConnect(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionGlobus = WPFControlConnection.ConnectionStates.Connected;
            InitializeTimerGlobusCheck();
            GetCurrentMode();

        }

        private void ClientGlobus_OnDisconnect(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionGlobus = WPFControlConnection.ConnectionStates.Disconnected;
            DestructorTimerGlobusCheck();
            mainWindowViewModel.TrackedGlobusId = -2;
        }

        private void ClientGlobus_OnDestroy(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionGlobus = WPFControlConnection.ConnectionStates.Disconnected;
            DestructorTimerGlobusCheck();
        }

        private void ClientGlobus_OnCoordReply(object sender, CoordReplyEventArgs e)
        {
            mainWindowViewModel.LocalPropertiesVM.Globus.Latitude = e.coordRadar.Latitude;
            mainWindowViewModel.LocalPropertiesVM.Globus.Longitude = e.coordRadar.Longitude;
            mainWindowViewModel.LocalPropertiesVM.Globus.Height = e.coordRadar.Height;
            mainWindowViewModel.LocalPropertiesVM.Globus.MagHeading = e.coordRadar.Mag_Heading;
        }

        private void ClientGlobus_OnModeReply(object sender, ModeReplyEventArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucGlobus.WorkingMode = (byte)e.Mode;

                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void ucGlobus_OnChangedTarget(object sender, int e)
        {
            mainWindowViewModel.TrackedGlobusId = e;
        }

        private void ClientGlobus_OnTargetReport(object sender, TargetReportEventArgs e)
        {
            try
            {
                GlobusMark mark = DecodeMarkGlobus(e);

                if (mark == null) return;
                    
                if (globusTargets.Where(x => x.ID == mark.ID).ToList().Count == 0)
                    globusTargets.Add(new GlobusTarget { ID = mark.ID });

                globusTargets.Where(x => x.ID == mark.ID).FirstOrDefault().globusMarks.Add(mark);


                //TODO: проверить как при трассах
                if (mainWindowViewModel.AutoGlobusMode)
                {
                    if(mainWindowViewModel.TrackedGlobusId != mark.ID) return;
                    
                    var own = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);
                    if (own != null)
                    {
                        SetAngleOEM(Azimuth.CalcAzimuth(own.Coordinates.Latitude, own.Coordinates.Longitude, mark.Coordinate.Latitude, mark.Coordinate.Longitude));
                    }


                }

            }
            catch
            {

            }
        }

        private void ClientGlobus_OnPeriodicReply(object sender, PeriodicReplyEventArgs e)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucGlobus.PeriodicMessage = (byte)e.PeriodicMessages;

                }), DispatcherPriority.Background);
            }
            catch
            { }
        }


        private GlobusMark DecodeMarkGlobus(TargetReportEventArgs e)
        {
            GlobusMark globus = null;

            try
            {
                globus = new GlobusMark();

                globus.ID = e.ID;
                globus.Azimuth = e.Azimuth;
                globus.Elevation = e.Elevation;
                globus.Velocity = e.Velocity;
                globus.Coordinate.Longitude = e.Longitude;
                globus.Coordinate.Latitude = e.Latitude;
                globus.Coordinate.Altitude = (float)e.Altitude;
                globus.Loctime = DateTime.Now;

            }
            catch { }

            return globus;
        }


        public void UpdateGlobusCoordinate()
        {
            try
            {
                ClientGlobus?.SendCoordSet(new CoordRadar(mainWindowViewModel.LocalPropertiesVM.Globus.Longitude,
                                                          mainWindowViewModel.LocalPropertiesVM.Globus.Latitude,
                                                          mainWindowViewModel.LocalPropertiesVM.Globus.Height,
                                                           mainWindowViewModel.LocalPropertiesVM.Globus.MagHeading));
            }
            catch { }
        }

        private void ucGlobus_OnDeleteRecord(object sender, GlobusControl.GlobusModel e)
        {
            DeleteGlobusTarget(e.Id);
            UpdateCurrentView();
        }

        private void ucGlobus_OnClearRecords(object sender, EventArgs e)
        {
            ClearGlobusTarget();
            UpdateCurrentView();
            mainWindowViewModel.TrackedGlobusId = -2;
        }

         private void ucGlobus_OnActivate(object sender, EventArgs e)
        {
            try
            {
                ClientGlobus?.SendPeriodicSet(EPeriodic.Enabled);
            }
            catch { }
        }

        private void ucGlobus_OnRequestCurrentMode(object sender, EventArgs e)
        {
            GetCurrentMode();
        }

        private void GetCurrentMode()
        {
            try
            {
                ClientGlobus?.SendModeRequest();
            }
            catch { }

            Thread.Sleep(1000);

            try
            {
                ClientGlobus?.SendPeriodicRequest();
            }
            catch { }

            Thread.Sleep(1000);

            try
            {
                ClientGlobus?.SendCoordRequest();
            }
            catch { }
        }

        private void ucGlobus_OnDeactivate(object sender, EventArgs e)
        {
            try
            {
                ClientGlobus?.SendPeriodicSet(EPeriodic.Disabled);
            }
            catch { }
        }


        private void InitializeTimerGlobusCheck()
        {
            tmrGlobusCheck.Elapsed += TickTimerGlobusCheck;
            tmrGlobusCheck.Interval = 1000;

            tmrGlobusCheck.AutoReset = true;
            tmrGlobusCheck.Enabled = true;
        }

        private void DestructorTimerGlobusCheck()
        {
            tmrGlobusCheck.Elapsed -= TickTimerGlobusCheck;

            tmrGlobusCheck.Enabled = false;
        }

        void TickTimerGlobusCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            UpdateCurrentView();

        }


        private void UpdateCurrentView()
        {
            try
            {
                if (globusTargets != null)
                {

                    List<GlobusTarget> globusTemp = new List<GlobusTarget>();

                    TimeSpan tt = new TimeSpan(0, 0, mainWindowViewModel.LocalPropertiesVM.Globus.LifeTime * (-1));
                    globusTemp = globusTargets.Where(x => x.globusMarks.Last().Loctime - DateTime.Now < tt).ToList();

                    foreach (var t in globusTemp)
                        DeleteGlobusTarget(t.ID);

                    UpdateTableGlobus();
                    UpdateGlobusTargetsMap(globusTargets);


                }
            }
            catch
            { }
        }

        private void DeleteGlobusTarget(int ID)
        {
            try
            {
                globusTargets.Remove(globusTargets.Where(x => x.ID == ID).FirstOrDefault());
            }
            catch { }

            UpdateCurrentView();
        }

        private void ClearGlobusTarget()
        {
            try
            {
                globusTargets.Clear();
            }
            catch { }

            UpdateCurrentView();
        }

        private void UpdateTableGlobus()
        {
            List<GlobusModel> listGlobusModel = new List<GlobusModel>();


            foreach (var glb in globusTargets)
            {
                listGlobusModel.Add(new GlobusModel()
                {
                    Id = glb.ID,
                    Azimuth = glb.globusMarks.Last().Azimuth,
                    Coord = new TableEvents.CoordGlobus()
                    {
                        Latitude = glb.globusMarks.Last().Coordinate.Latitude,
                        Longitude = glb.globusMarks.Last().Coordinate.Longitude,
                        Altitude = glb.globusMarks.Last().Coordinate.Altitude,
                    },

                    Velocity = glb.globusMarks.Last().Velocity,
                    Elevation = glb.globusMarks.Last().Elevation,

                });



      }

        
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucGlobus.ListGlobusModel = listGlobusModel;

                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void ucGlobus_OnDoubleClick(object sender, GlobusControl.GlobusModel e)
        {
            if (globusTargets.Where(x => x.ID == e.Id).FirstOrDefault() != null)
                mainWindowViewModel.LocationCenter = globusTargets.Where(x => x.ID == e.Id).FirstOrDefault().globusMarks.Last().Coordinate;
        }

        private void UcGlobus_OnTargeting(object sender, GlobusModel e)
        {
            var own = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);
            if (own != null)
            {
                SetAngleOEM(Azimuth.CalcAzimuth(own.Coordinates.Latitude, own.Coordinates.Longitude, e.Coord.Latitude, e.Coord.Longitude));
            }
        }
    }
}
