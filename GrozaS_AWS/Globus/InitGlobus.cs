﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;
using Rodnik;
using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using Globus;

namespace GrozaS_AWS
{
    
    public partial class MainWindow : Window
    {
        //public TCPClientGlobus ClientGlobus = new TCPClientGlobus(); //TODO: вернуть, если перестанет работать
        public TCPClientGlobus ClientGlobus;

        ObservableCollection<GlobusTarget> globusTargets = new ObservableCollection<GlobusTarget>();

        private void ConnectServerGlobus()
        {
            try
            {
                ClientGlobus = new TCPClientGlobus();
                ClientGlobus.OnConnect += ClientGlobus_OnConnect;
                ClientGlobus.OnDisconnect += ClientGlobus_OnDisconnect;
                ClientGlobus.OnDestroy += ClientGlobus_OnDestroy;
                ClientGlobus.OnCoordReply += ClientGlobus_OnCoordReply;
                ClientGlobus.OnModeReply += ClientGlobus_OnModeReply;
                ClientGlobus.OnTargetReport += ClientGlobus_OnTargetReport;
                ClientGlobus.OnPeriodicReply += ClientGlobus_OnPeriodicReply;

                ClientGlobus.Connect(mainWindowViewModel.LocalPropertiesVM.Globus.IpAddressRemoute, mainWindowViewModel.LocalPropertiesVM.Globus.PortRemoute);
            }

            catch { }
        }



        private void DisconnectServerGlobus()
        {
            try
            {
                if (ClientGlobus == null)
                    return;

                ClientGlobus.Disconnect();

                ClientGlobus.OnConnect -= ClientGlobus_OnConnect;
                ClientGlobus.OnDisconnect -= ClientGlobus_OnDisconnect;
                ClientGlobus.OnDestroy -= ClientGlobus_OnDestroy;
                ClientGlobus.OnCoordReply -= ClientGlobus_OnCoordReply;
                ClientGlobus.OnModeReply -= ClientGlobus_OnModeReply;
                ClientGlobus.OnTargetReport -= ClientGlobus_OnTargetReport;
                ClientGlobus.OnPeriodicReply -= ClientGlobus_OnPeriodicReply;

                ClientGlobus = null;
            }

            catch { }
        }

    }
}

