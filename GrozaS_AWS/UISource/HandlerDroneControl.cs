﻿using System;
using GrozaSModelsDBLib;
using UISource;
using System.Threading;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Bearing;
using GrozaS_AWS.Models;
using System.IO;
using RecognitionSystemProtocol;
using GrozaS_AWS.RS;
using CtoGsProtocol;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        #region DataBase EventHandlers

        private async void DroneControl_LoadDrones()
        {
            try
            {
                var tablSource = await mainWindowViewModel.clientDB.Tables[NameTable.TableSource].LoadAsync<TableSource>();
                DroneControl_UpdateTableSource(tablSource);
            }
            catch { }
        }


        private void DroneControl_UpdateTableSource(List<TableSource> e)
        {
            var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own);
            var anotherStation = lJammerStation.Find(x => x.Role == StationRole.Linked);
            var DroneList = new ObservableCollection<RecDroneModel>();

            if (ownStation == null)
                return;

            foreach (TableSource rec in e)
            {
                var drone = new RecDroneModel()
                {
                    ID = rec.Id,
                    TypeRSM = (TypeBelong)((byte)rec.TypeRSM),
                    Band = rec.Track.Last().BandMHz,
                    Frequency = rec.Track.Last().FrequencyMHz,
                    FrequencyRX = rec.Track.Last().FrequencyRX,
                    TimeUpdate = rec.Track.Last().Time,
                    Latitude = rec.Track.Last().Coordinates.Latitude,
                    Longitude = rec.Track.Last().Coordinates.Longitude,
                    DistanceOwn = rec.Track.Last().Bearing.Where(t => t.NumJammer == ownStation.Id).FirstOrDefault().Distance,
                    BearingOwn = rec.Track.Last().Bearing.Where(t => t.NumJammer == ownStation.Id).FirstOrDefault().Bearing,
                    //Note = rec.Note
                };

                
                if (rec.Note != null)
                {
                    if (rec.Note.Length > 15 && rec.Note.Substring(14, 1) == ":")
                    {
                        var serialNum = rec.Note.Substring(0, 14);
                        var ownDrone = lOwnUAV.FirstOrDefault(t => t.SerialNumber == serialNum);
                        if (ownDrone != null) drone.Note = ownDrone.Name;
                    }
                }

                var tempAnotherBearing = anotherStation == null ? null : rec.Track.Last().Bearing.Where(t => t.NumJammer == anotherStation.Id).FirstOrDefault();

                drone.DistanceAnother = tempAnotherBearing == null ? 0 : tempAnotherBearing.Distance;
                drone.BearingAnother = tempAnotherBearing == null ? -1 : tempAnotherBearing.Bearing;

                if (TypeCodeCollection.ContainsKey(rec.Type))
                {
                    drone.Type = TypeCodeCollection[rec.Type];
                }
                else
                {
                    drone.Type = "No Image";
                }

                DroneList.Add(drone);
            }

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
              ItemDrone.Drones = DroneList;
            });
        }
        #endregion




        #region Control EventHandlers

        private void ItemDrone_OnAddRecord(object sender, DronePropertiesModel e)
        {

            AddNewSource(e.Frequency, e.FrequencyRX, e.Band, e.BearingOwn, e.BearingAnother, e.Length, e.Type);
            //try
            //{
            //    var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own).Id;

            //    var anotherStation = 0;
            //    try
            //    {
            //        var linked = lJammerStation.Find(x => x.Role == StationRole.Linked);
            //        anotherStation = linked == null ? anotherStation : linked.Id; 
            //    }
            //    catch
            //    { }


            //    ObservableCollection<TableJamBearing> bear = new ObservableCollection<TableJamBearing>();

            //    bear.Add(new TableJamBearing()
            //    {
            //        NumJammer = ownStation,
            //        Bearing = e.BearingOwn,
            //        Distance = e.Length
            //    });

            //    if (anotherStation > 0)
            //        bear.Add(new TableJamBearing()
            //        {
            //            NumJammer = anotherStation,
            //            Bearing = e.BearingAnother,
            //            Distance = 0
            //        });

            //    Random rnd = new Random();

            //    var record = new TableSource()
            //    {
            //        Type = e.Type,
            //        TypeRSM = (byte)TypeRSMart.EMPTY,   
            //        //Note = e.Note,
            //        Track = new ObservableCollection<TableTrack>()
            //    {
            //        new TableTrack()
            //        {

            //            Time = DateTime.Now,
            //            BandMHz = e.Band,
            //            FrequencyMHz = e.Frequency,
            //            FrequencyRX = e.FrequencyRX,
            //            Bearing = new ObservableCollection<TableJamBearing>(bear)

            //        }
            //    }
            //    };

            //    record.Type = IdentifyUAV(new TrackPoint() { Frequency = record.Track.Last().FrequencyMHz, Band = record.Track.Last().BandMHz });
            //    AddSourceTDF_DB(record);
            //}
            //catch
            //{ }

        }


        private  void ItemDrone_OnChangeRecord(object sender, DronePropertiesModel e)
        {

            var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own).Id;

            var anotherStation = 0;
            try
            {
                var linked = lJammerStation.Find(x => x.Role == StationRole.Linked);
                anotherStation = linked == null ? anotherStation : linked.Id; 
            }
            catch
            { }


            ObservableCollection<BearingItem> bear = new ObservableCollection<BearingItem>();

            bear.Add(new BearingItem()
            {
                Jammer = ownStation,
                Bearing = e.BearingOwn,
                Distance = e.Length
            });

            if (anotherStation > 0)
                bear.Add(new BearingItem()
                {
                    Jammer = anotherStation,
                    Bearing = e.BearingAnother,
                    Distance = 0
                });


            
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Type = e.Type;
            //CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().TypeRSM = TypeRSMart.EMPTY;
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().AddTrackPoint(new TrackPoint()
            {

                Frequency = e.Frequency,
                FrequencyRX = e.FrequencyRX,
                Band = e.Band,
                Time = DateTime.Now,
                Bearings = new ObservableCollection<BearingItem>(bear),
                Jammers = new ObservableCollection<TableJammerStation>(lJammerStation)

            });

            lastSource = null;

            SaveSourceTDF_DB();

        }


        private void ItemDrone_OnDeleteRecord(object sender, DronePropertiesModel e)
        {
          
            DeleteSourceTDF_DB(e.Id);
        }


        private void ItemDrone_OnClearRecords(object sender, EventArgs e)
        {
            ClearSourceTDF_DB();

            UIRsDetecting.UpdateSpectrumIndicators(0, 0);
        }


        private async void ItemDrone_OnDeleteTrackRecord(object sender, DronePropertiesModel e)
        {
            
            TrackPoint track = CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Last();

            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Clear();
            CurrentSourcesDF.Where(x => x.ID == e.Id).FirstOrDefault().Track.Add(track);

            lastSource = null;

            SaveSourceTDF_DB();

        }


        private void ItemDrone_OnAskBearing(object sender, RecDroneModel e)
        {

            ShowSentExecutingBearingRequest(e, TypeQuery.ASK);
            SendGetExecutiveBearing(e.ID, TypeQuery.ASK);
            
        }



        private void ItemDrone_OnSendToJamming(object sender, (int, DronePropertiesModel) e)
        {
            if (mainWindowViewModel.clientDB == null)
                return;

            var ampNum = e.Item1;
            var record = e.Item2;

            var drone = CurrentSourcesDF.FirstOrDefault(c => c.ID == record.Id);

            var jamRecord = new TableSuppressSource()
            {
                InputType = TypeInput.Auto,
                TableSourceId = drone.ID,
                FrequencyMHz = drone.Track.Last().Frequency,
                BandMHz = drone.Track.Last().Band,
                Type = drone.Type,
                LevelOwn = 0,// (short)drone.Track.Last().Bearing.Last().Level
                Modulation = Modulation.LCM,
                ScanSpeed = 10,
                Deviation = ((int)(drone.Track.Last().Band * 1000) + 4000)/2
            };

            if (jamRecord.Deviation < 2500) jamRecord.Deviation = 2500;
            if (jamRecord.Deviation > 127500) jamRecord.Deviation = 127500;

            if (record.Frequency >= 100 && record.Frequency < 500)
                jamRecord.Id = 10 * ampNum + 1;
            else if (record.Frequency >= 500 && record.Frequency < 2500)
                jamRecord.Id = 10 * ampNum + 2;
            else if (record.Frequency >= 2500 && record.Frequency <= 6000)
                jamRecord.Id = 10 * ampNum + 3;

            mainWindowViewModel.clientDB?.Tables[NameTable.TableSuppressSource].ChangeAsync(jamRecord);
        }


        private void ItemDrone_OnSendToCuirasse(object sender, DronePropertiesModel e)
        {
            try
            {
                if (udpCuirasse != null)
                {
                    Drone drone = new Drone()
                    {
                        ID = (short)e.Id,
                        FrequencyMHz = (float)e.Frequency,                       
                        BandwidthMHz = e.Band,

                    };

                    SendRequestCuirasse(drone);

                    
                }
            }
            catch
            { }
        }

        private void ItemDrone_OnSendToGrozaR(object sender, DronePropertiesModel e)
        {
            try
            {
                SendTargetPosition(new Coord(e.Latitude, e.Longitude,0) );
            }
            catch
            { }
        }

        
        private async void ItemDrone_OnSendToRS(object sender, DronePropertiesModel e)
        {
            GetTypeRS(e.Id);            
        }

           
        private void ItemDrone_OnSaveSource(object sender, ObservableCollection<RecDroneModel> e)
        {

            Log.SetSourceRI(e);
            
        }
        #endregion



    }
}
