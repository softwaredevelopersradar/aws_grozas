﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using GrozaSModelsDBLib;
using WPFControlConnection;
using ServerPC.Events;


namespace GrozaS_AWS
{
    using TableEvents;

    public partial class MainWindow : Window
    {
        private void CLControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tcpServerPC != null)
                    DestroyPC();
                else
                    CreatePC();
            }

            catch
            { }

        }

        private void CreateServerPC(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionServerCL = ConnectionStates.Connected;
            
        }
        private void DestroyServerPC(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionServerCL = ConnectionStates.Disconnected;
            mainWindowViewModel.StateConnectionClientCL = ConnectionStates.Disconnected;
        }

        private void ConnectClientPC(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionClientCL = ConnectionStates.Connected;
        }

        private void DisconnectClientPC(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionClientCL = ConnectionStates.Disconnected;
        }

        

        private void GetCoordPC(object sender, System.EventArgs e)
        {
            if (tcpServerPC != null)
            {              
                var CoordOwn = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own).Coordinates;


                var CoordLink = new Coord();

                try
                {
                    CoordLink = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Linked).Coordinates;
                }
                catch
                { }
                

                tcpServerPC.SendConfirmCoord(0, CoordOwn.Latitude, CoordOwn.Longitude, CoordLink.Latitude, CoordLink.Longitude);
            }
        }

        private void GetSourcePC(object sender, System.EventArgs e)
        {
            if (tcpServerPC != null)
            {
                List<TReconFWS> source = new List<TReconFWS>();

                var NumJammerOwn = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own).Id;

               
                var NumJammerLink = -1;

                try 
                {
                    NumJammerLink = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Linked).Id;  
                }
                catch { }
                
                string Name = "";
                foreach (var s in CurrentSourcesDF)
                {
                    if (TypeCodeCollection.ContainsKey(s.Type))
                    {
                        Name = TypeCodeCollection[s.Type];
                    }

                    short bearbLink = 361;

                    if (NumJammerLink > -1)
                    try 
                    {
                        bearbLink = (short)s.Track.Last().Bearings.Where(x => x.Jammer == NumJammerLink).FirstOrDefault().Bearing;
                    }
                    catch { }


                    source.Add(new TReconFWS() { iID = s.ID,
                        iFreq = (int)s.Track.Last().Frequency * 1000,
                        iWidth = (int)s.Track.Last().Band * 1000,
                        sBearingOwn = (short)s.Track.Last().Bearings.Where(x => x.Jammer == NumJammerOwn).FirstOrDefault().Bearing,
                        sBearingLinked = bearbLink, //(short)s.Track.Last().Bearing.Where(x => x.NumJammer == NumJammerLink).FirstOrDefault().Bearing,
                        iDistanceOwn = (int)s.Track.Last().Bearings.Where(x => x.Jammer == NumJammerOwn).FirstOrDefault().Distance,
                        strName = Name,
                        strNote = s.Note,
                        tCoord = new TCoord() { dLatitude = s.Track.Last().Coordinate.Latitude, dLongitude = s.Track.Last().Coordinate.Longitude },
                        tTime = new TTimeMy() { bHour = (byte)s.Track.Last().Time.Hour, bMinute = (byte)s.Track.Last().Time.Minute, bSecond = (byte)s.Track.Last().Time.Second }
                    }); 
                }
                tcpServerPC.SendConfirmReconFWS(0,source);
            }

            
        }

        private void SetImportantPC(object sender, RangeEventArgs e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                var NumJammer = e.Jammer;

                List<TableFreqKnown> tableFreqKnown = new List<TableFreqKnown>();

                foreach (var r in e.RangeSpec)
                    tableFreqKnown.Add(new TableFreqKnown() { FreqMaxKHz = r.iFregMax, FreqMinKHz = r.iFregMin, Note = "", IsActive = true, NumberASP = NumJammer});

                mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown].AddRange(tableFreqKnown);

                if (tcpServerPC != null)
                    tcpServerPC.SendConfirmRangeSpec(0);
            }

            

        }

        private void SetRestrictedPC(object sender, RangeEventArgs e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                List<TableFreqForbidden> tableFreqForbidden = new List<TableFreqForbidden>();

                var NumJammer = e.Jammer;

                foreach (var r in e.RangeSpec)
                    tableFreqForbidden.Add(new TableFreqForbidden() { FreqMaxKHz = r.iFregMax, FreqMinKHz = r.iFregMin, Note = "", IsActive = true, NumberASP = NumJammer });

                mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden].AddRange(tableFreqForbidden);

                if (tcpServerPC != null)
                    tcpServerPC.SendConfirmRangeForbid(0);
            }

            

        }

       
    }
}