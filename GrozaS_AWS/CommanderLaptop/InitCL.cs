﻿using PC;
using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public TCPServerPC tcpServerPC;

        private void CreatePC()
        {
            if (tcpServerPC != null)
                DestroyPC();

            tcpServerPC = new TCPServerPC(lJammerStation.Find(x => x.Role == GrozaSModelsDBLib.StationRole.Own).Id);

            tcpServerPC.OnCreate += CreateServerPC;
            tcpServerPC.OnDestroy += DestroyServerPC;
            tcpServerPC.OnConnect += ConnectClientPC;
            tcpServerPC.OnDisconnect += DisconnectClientPC;

            tcpServerPC.OnGetCoord += GetCoordPC;

            tcpServerPC.OnGetSource += GetSourcePC;

            tcpServerPC.OnSetImportant += SetImportantPC;

            tcpServerPC.OnSetRestricted += SetRestrictedPC;


            tcpServerPC.CreateServer(mainWindowViewModel.LocalPropertiesVM.LC.IpAddress, mainWindowViewModel.LocalPropertiesVM.LC.Port);
           
        }

        private void DestroyPC()
        {
            if (tcpServerPC != null)
            {
                tcpServerPC.DestroyServer();

                tcpServerPC.OnDestroy -= DestroyServerPC;
                tcpServerPC.OnConnect -= ConnectClientPC;
                tcpServerPC.OnDisconnect -= DisconnectClientPC;

                tcpServerPC.OnGetCoord -= GetCoordPC;

                tcpServerPC.OnGetSource -= GetSourcePC;

                tcpServerPC.OnSetImportant -= SetImportantPC;

                tcpServerPC.OnSetRestricted -= SetRestrictedPC;

                tcpServerPC = null;
            }

        }

    }
}