﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;

using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using ZorkiR;
using InheritorsEventArgs;
using GrozaSModelsDBLib;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        ObservableCollection<AeroscopeTarget> aeroscopeTargets = new ObservableCollection<AeroscopeTarget>();

        System.Timers.Timer tmrAeroscopeCheck = new System.Timers.Timer();

        private void UpdateListAeroscopeMark(TableEventArgs<TableAeroscopeTrajectory> e)
        {
            try
            {
                var listNew = e.Table;
                AeroscopeMark mark = DecodeMarkAeroscope(listNew);

                if (mark != null)
                {

                    if (aeroscopeTargets.Where(x => x.SerialNumber == mark.SerialNumber).ToList().Count == 0)
                    {
                        //mark.ID = aeroscopeIndexId++;
                        //aeroscopeTargets.Add(new AeroscopeTarget { ID = aeroscopeIndexId++, SerialNumber = mark.SerialNumber });
                        aeroscopeTargets.Add(new AeroscopeTarget { ID = mark.ID, SerialNumber = mark.SerialNumber });
                    }

                    //mark.ID = aeroscopeTargets.First(x => x.SerialNumber == mark.SerialNumber).ID;
                    aeroscopeTargets.Where(x => x.ID == mark.ID).FirstOrDefault().aeroscopeMarks.Add(mark);
                }
            }
            catch
            {
            }
        }

        //private AeroscopeMark DecodeMarkAeroscope(TableEventArgs<TableAeroscopeTrajectory> e)
        private AeroscopeMark DecodeMarkAeroscope(IList<TableAeroscopeTrajectory> e)
        {
            AeroscopeMark aeroscope = null;

            try
            {
                aeroscope = new AeroscopeMark();
                //var rec = e.FirstOrDefault();
                var rec = e.OrderBy(t => t.Time).Last();
                var fromAeroscopeTable = lAeroscope.First(t => t.SerialNumber == rec.SerialNumber);
                //aeroscope.ID = e.Table.FirstOrDefault().Id;
                //aeroscope.ID = lAeroscope.FindIndex(t => t.SerialNumber == e.Table.LastOrDefault().SerialNumber);
                //aeroscope.ID = rec.SerialNumber.GetHashCode();
                //aeroscope.SerialNumber = e.Table.LastOrDefault().SerialNumber;
                aeroscope.ID = fromAeroscopeTable.Id;
                aeroscope.SerialNumber = rec.SerialNumber;
                aeroscope.Home.Latitude = fromAeroscopeTable.HomeLatitude;
                aeroscope.Home.Longitude = fromAeroscopeTable.HomeLongitude;
                //aeroscope.Pitch = e.Table.LastOrDefault().Pitch;
                //aeroscope.Roll = e.Table.LastOrDefault().Roll;
                //aeroscope.Yaw = e.Table.LastOrDefault().Yaw;

                //aeroscope.Vup = e.Table.LastOrDefault().V_up;
                //aeroscope.Vnorth = e.Table.LastOrDefault().V_north;
                //aeroscope.Veast = e.Table.LastOrDefault().V_east;

                //aeroscope.Coordinate.Longitude = e.Table.LastOrDefault().Coordinates.Longitude;
                //aeroscope.Coordinate.Latitude = e.Table.LastOrDefault().Coordinates.Latitude;
                //aeroscope.Coordinate.Altitude = e.Table.LastOrDefault().Coordinates.Altitude;

                //var newlist = e.Table.Where(t => t.SerialNumber == rec.SerialNumber).OrderBy(i => i.Time);
                //var lastpoint = newlist.Last();
                //aeroscope.Coordinate.Longitude = lastpoint.Coordinates.Longitude;
                //aeroscope.Coordinate.Latitude = lastpoint.Coordinates.Latitude;
                //aeroscope.Coordinate.Altitude = lastpoint.Coordinates.Altitude;

                aeroscope.Coordinate.Longitude = rec.Coordinates.Longitude;
                aeroscope.Coordinate.Latitude = rec.Coordinates.Latitude;
                aeroscope.Coordinate.Altitude = rec.Coordinates.Altitude;

                aeroscope.Loctime = DateTime.Now;

            }
            catch { }

            return aeroscope;
        }


        private void ClearAeroscope()
        {
            try 
            {
                aeroscopeTargets.Clear();
                //ClearTableEscope(); //TODO: временно для Escope
            }
            catch { }

            UpdateAeroscopeMapTarget(aeroscopeTargets);
        }


        private void DeleteAeroscopeMapTarget(string  serial)
        {
            try
            {
                aeroscopeTargets.Remove(aeroscopeTargets.Where(x => x.SerialNumber == serial).FirstOrDefault());
            }
            catch { }

            UpdateAeroscopeMapTarget(aeroscopeTargets);
        }



        private void InitializeTimerAeroscopeCheck()
        {
            tmrAeroscopeCheck.Elapsed += TickTimerAeroscopeCheck;
            tmrAeroscopeCheck.Interval = 1000;

            tmrAeroscopeCheck.AutoReset = true;
            tmrAeroscopeCheck.Enabled = true;
        }

        private void DestructorTimerAeroscopeCheck()
        {
            tmrAeroscopeCheck.Elapsed -= TickTimerAeroscopeCheck;

            tmrAeroscopeCheck.Enabled = false;
        }

        void TickTimerAeroscopeCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            UpdateAeroscopeMapTarget(aeroscopeTargets);
            UpdateEscopeMapTarget(escopeTargets);
        }


        private void ucAeroscope_OnCentering(object sender, AeroscopeControl.TableAeroscopeAll e)
        {
            if (aeroscopeTargets.Where(x => x.SerialNumber == e.TableAeroscope.SerialNumber).FirstOrDefault() != null)
                mainWindowViewModel.LocationCenter = aeroscopeTargets.Where(x => x.SerialNumber == e.TableAeroscope.SerialNumber).FirstOrDefault().aeroscopeMarks.Last().Coordinate;
        }

    }
}

