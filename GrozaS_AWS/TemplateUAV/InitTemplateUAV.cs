﻿
using DBSourceCtrl.Models;
using GrozaSModelsDBLib;
using ItemFreqCtrl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private GrozaS_AWS.TemplateUAV.TemplateUAV templateWindow;


        private void InitTemplateWnd()
        {
            templateWindow = new TemplateUAV.TemplateUAV();

            templateWindow.OnAddTemplateUAV += TemplateWindow_OnAddTemplateUAV;
            templateWindow.OnChangeTemplateUAV += TemplateWindow_OnChangeTemplateUAV;
            templateWindow.OnClearTemplateUAV += TemplateWindow_OnClearTemplateUAV;
            templateWindow.OnDeleteTemplateUAV += TemplateWindow_OnDeleteTemplateUAV;
            templateWindow.OnLoadDefaultTemplateUAV += TemplateWindow_OnLoadDefaultTemplateUAV;
            templateWindow.OnIsWindowPropertyOpen += TemplateWindow_OnIsWindowPropertyOpen;
        }

        public void TemplateWindow_SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            if(templateWindow== null) 
                return;
            templateWindow.Title = SWindowNames.nameTemplatesWnd;
            templateWindow.SetLanguage(language);
        }

        private void TemplateWindow_OnIsWindowPropertyOpen(object sender, DBSourceCtrl.DronePropertyView e)
        {
            templateWindow.SetPropertyLanguage(basicProperties.Local.General.Language);
        }

        private void TemplateWindow_OnLoadDefaultTemplateUAV(object sender, EventArgs e)
        {
            DefaultTemplates();
        }

        private void UpdateTablePattern()
        {
    
            try
            {

                Dispatcher.BeginInvoke(new Action(() =>
            {
                ObservableCollection<TemplateSource> templateSource = new ObservableCollection<TemplateSource>();

                foreach (var t in lTablePattern)
                {
                    ObservableCollection<FreqWork> freqWork = new ObservableCollection<FreqWork>();
                    foreach (var f in t.FrequencyList)
                    {
                        freqWork.Add(new FreqWork
                        {
                            FrequencyMin = f.FrequencyMin,
                            FrequencyMax = f.FrequencyMax,
                            Band = f.Band
                        });
                    };


                    templateSource.Add(new TemplateSource
                    {
                        ID = t.Id,
                        Name = t.Name,
                        FreqWork = new ObservableCollection<FreqWork>(freqWork),
                        View = (DBSourceCtrl.ViewUAV)t.View,
                        Image = ImageOperating.GetImageStream(ImageOperating.ByteArrayToImage(t.ImageByte))
                    });
                }

                templateWindow.listTemplateUAV.TemplateSources = templateSource;
            }), DispatcherPriority.Background);
            }

            catch { }


        }

        private void TemplateWindow_OnDeleteTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                
                tablePattern.Id = e.Id;

                DeletePatternRecord(tablePattern);

            }
        }

        private void TemplateWindow_OnClearTemplateUAV(object sender, EventArgs e)
        {
            ClearPatternRecord();
        }

        private void TemplateWindow_OnChangeTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                tablePattern.FrequencyList = new ObservableCollection<OperatingFrequency>();
                foreach (var f in e.FreqWork)
                    tablePattern.FrequencyList.Add(new OperatingFrequency()
                    {
                        FrequencyMin = f.FrequencyMin,
                        FrequencyMax = f.FrequencyMax,
                        Band = f.Band
                    });

                //tablePattern.ImagePath = e.ImagePath;
                tablePattern.ImageByte = ImageOperating.ImageSourceToBytes(e.Image);
                //tablePattern.Image = e.Image;
                tablePattern.Name = e.Name;
                tablePattern.View = (byte)e.View;

                tablePattern.Id = e.Id;

                ChangePatternRecord(tablePattern);

            }
        }

        private void TemplateWindow_OnAddTemplateUAV(object sender, DBSourceCtrl.Models.PropertyTemplate e)
        {
            

            if (e != null)
            {
                TablePattern tablePattern = new TablePattern();
                tablePattern.FrequencyList = new ObservableCollection<OperatingFrequency>();
                foreach (var f in e.FreqWork)
                    tablePattern.FrequencyList.Add(new OperatingFrequency()
                    {
                        FrequencyMin = f.FrequencyMin,
                        FrequencyMax = f.FrequencyMax,
                        Band = f.Band
                    });
                
                if (e.Image == null)
                {
                    e.Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                                             + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                                             + ";component/"
                                                             + "Images/NoImage.png", UriKind.Absolute));
                }
                tablePattern.ImageByte = ImageOperating.ImageSourceToBytes(e.Image);

                tablePattern.Name = e.Name;
                tablePattern.View = (byte)e.View;

                AddPatternRecord(tablePattern);
            }
        }


        //private Dictionary<string, ImageSource> CreateImageDictionary()
        //{
        //    var tempCollection = new Dictionary<string, ImageSource>();

        //    foreach (KeyValuePair<byte, string> code in TypeCodeCollection.Where(x => x.Key != 255).ToList())
        //    { TypeCodeCollection.Remove(code.Key); }

        //    var converter = new ImageSourceConverter();
        //    string fullPath = Path.GetFullPath(@"TDF");

        //    tempCollection.Add("Unknown", (ImageSource)converter.ConvertFromString(fullPath + "/0_Unknown.png"));
        //    TypeCodeCollection.Add(0, "Unknown");

        //    tempCollection.Add("Mavic 2", (ImageSource)converter.ConvertFromString(fullPath + "/1_DJI Mavic 2.png"));
        //    TypeCodeCollection.Add(1, "Mavic 2");

        //    tempCollection.Add("Phantom-4", (ImageSource)converter.ConvertFromString(fullPath + "/2_DJI Phantom 4.png"));
        //    TypeCodeCollection.Add(2, "Phantom-4");

        //    tempCollection.Add("Mavic Air", (ImageSource)converter.ConvertFromString(fullPath + "/3_DJI Mavic Air.png"));
        //    TypeCodeCollection.Add(3, "Mavic Air");

        //    tempCollection.Add("Berkut", (ImageSource)converter.ConvertFromString(fullPath + "/4_Berkut.png"));
        //    TypeCodeCollection.Add(4, "Berkut");

        //    tempCollection.Add("Busel-M", (ImageSource)converter.ConvertFromString(fullPath + "/5_Busel-M.png"));
        //    TypeCodeCollection.Add(5, "Busel-M");

        //    tempCollection.Add("Orlan-10", (ImageSource)converter.ConvertFromString(fullPath + "/6_Orlan-10.png"));
        //    TypeCodeCollection.Add(6, "Orlan-10");

        //    tempCollection.Add("Orbiter-2A", (ImageSource)converter.ConvertFromString(fullPath + "/7_Orbiter-2A.png"));
        //    TypeCodeCollection.Add(7, "Orbiter-2A");

        //    tempCollection.Add("RQ-11B Raven", (ImageSource)converter.ConvertFromString(fullPath + "/8_RQ-11B Raven.png"));
        //    TypeCodeCollection.Add(8, "RQ-11B Raven");

        //    tempCollection.Add("Skylark-1 LE", (ImageSource)converter.ConvertFromString(fullPath + "/9_Skylark-1 LE.png"));
        //    TypeCodeCollection.Add(9, "Skylark-1 LE");

        //    tempCollection.Add("RQ-11A Raven", (ImageSource)converter.ConvertFromString(fullPath + "/10_RQ-11A Raven.png"));
        //    TypeCodeCollection.Add(10, "RQ-11A Raven");

        //    tempCollection.Add("Da-Vinci", (ImageSource)converter.ConvertFromString(fullPath + "/11_Da-Vinci.png"));
        //    TypeCodeCollection.Add(11, "Da-Vinci");

        //    tempCollection.Add("Orbiter-2M", (ImageSource)converter.ConvertFromString(fullPath + "/12_Orbiter-2M.png"));
        //    TypeCodeCollection.Add(12, "Orbiter-2M");

        //    tempCollection.Add("Schiebel", (ImageSource)converter.ConvertFromString(fullPath + "/13_Schiebel.png"));
        //    TypeCodeCollection.Add(13, "Schiebel");

        //    tempCollection.Add("Heron-1 TP", (ImageSource)converter.ConvertFromString(fullPath + "/14_Heron-1 TP.png"));
        //    TypeCodeCollection.Add(14, "Heron-1 TP");


        //    return tempCollection;
        //}

        private void DefaultTemplates()
        {
            ClearPatternRecord();

            string fullPath = Path.GetFullPath(@"Templates");

            var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources",
                ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));



            var converter = new ImageSourceConverter();
            
            List<TablePattern> defaultTablePattern = new List<TablePattern>()
            {

            //0_Unknown

                new TablePattern()
                {
                     Id = 18,
                     Name = "Unknown",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 100,
                             FrequencyMax = 100,
                             Band = 1
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/0_Unknown.png")) 
            //Image = (ImageSource)converter.ConvertFromString(fullPath+"/0_Unknown.png")
                },

            //1_DJI Phantom-4
                new TablePattern()
                {
                     Id = 1,
                     Name = "Phantom-4",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 9
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/1_DJI Phantom 4.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/1_DJI Phantom 4.png")

                },


            //2_DJI Mavic Air

                new TablePattern()
                {
                     Id = 2,
                     Name = "Mavic Air",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 5
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 5
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/2_DJI Mavic Air.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/2_DJI Mavic Air.png")

                },

            //3_DJI Mavic 2

                new TablePattern()
                {
                     Id = 3,
                     Name = "Mavic 2",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 10
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 10
                         },
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2400,
                             FrequencyMax = 2480,
                             Band = 20
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 5725,
                             FrequencyMax = 5850,
                             Band = 20
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/3_DJI Mavic 2.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/3_DJI Mavic 2.png")
                },

             //4_Berkut

                new TablePattern()
                {
                     Id = 4,
                     Name = "Berkut",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2330,
                             FrequencyMax = 2330,
                             Band = 20
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/4_Berkut.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/4_Berkut.png")
                },

            //5_Busel-M

                new TablePattern()
                {
                     Id = 5,
                     Name = "Busel-M",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1100,
                             FrequencyMax = 1170,
                             Band = 9
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/5_Busel-M.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/5_Busel-M.png")
                },

            //6_Orlan-10

                new TablePattern()
                {
                     Id = 6,
                     Name = "Orlan-10",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1200,
                             FrequencyMax = 1270,
                             Band = 9
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/6_Orlan-10.png"))
                    // Image = (ImageSource)converter.ConvertFromString(fullPath+"/6_Orlan-10.png")
                },

            //7_Orbiter-2A

                new TablePattern()
                {
                     Id = 7,
                     Name = "Orbiter-2A",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {                         
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2290,
                             FrequencyMax = 2320,
                             Band = 30
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/7_Orbiter-2A.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/7_Orbiter-2A.png")
                },

            //8_RQ-11B Raven
               
                new TablePattern()
                {
                     Id = 8,
                     Name = "RQ-11B Raven",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1100,
                             FrequencyMax = 1390,
                             Band = 5
                         },


                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/8_RQ-11B Raven.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/8_RQ-11B Raven.png")
                },

            //9_Skylark-1 LE

                new TablePattern()
                {
                     Id = 9,
                     Name = "Skylark-1 LE",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1400,
                             FrequencyMax = 1690,
                             Band = 6
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/9_Skylark-1 LE.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/9_Skylark-1 LE.png")


                },

            //10_RQ-11A Raven

                new TablePattern()
                {
                     Id = 10,
                     Name = "RQ-11A Raven",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2200,
                             FrequencyMax = 2295,
                             Band = 7.5f
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/10_RQ-11A Raven.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/10_RQ-11A Raven.png")
                },

            //11_Da-Vinci

                new TablePattern()
                {
                     Id = 11,
                     Name = "Da-Vinci",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 2320,
                             FrequencyMax = 2320,
                             Band = 5
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/11_Da-Vinci.png"))
                    // Image = (ImageSource)converter.ConvertFromString(fullPath+"/11_Da-Vinci.png")
                },

            //12_Orbiter-2M

                new TablePattern()
                {
                     Id = 12,
                     Name = "Orbiter-2M",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4400,
                             FrequencyMax = 4690,
                             Band = 5
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/12_Orbiter-2M.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/12_Orbiter-2M.png")
                },


            //13_Schiebel

                new TablePattern()
                {
                     Id = 13,
                     Name = "Schiebel",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4700,
                             FrequencyMax = 4800,
                             Band = 5
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/13_Schiebel.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/13_Schiebel.png")
                },

            //14_Heron-1 TP

                new TablePattern()
                {
                     Id = 14,
                     Name = "Heron-1 TP",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4900,
                             FrequencyMax = 5000,
                             Band = 5
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/14_Heron-1 TP.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/14_Heron-1 TP.png")
                },

            //15_Luna X2000

                new TablePattern()
                {
                     Id = 15,
                     Name = "Luna X2000",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 5400,
                             FrequencyMax = 5700,
                             Band = 5 //уточнить
                         },
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/15_Luna X2000.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/15_Luna X2000.png")
                },

            //16_3G
                new TablePattern()
                {
                     Id = 16,
                     Name = "3G",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 1920,
                             FrequencyMax = 1980,
                             Band = 5 //уточнить
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 2110 ,
                             FrequencyMax = 2170 ,
                             Band = 5
                         },

                         new OperatingFrequency()
                         {
                             FrequencyMin = 890,
                             FrequencyMax = 900,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 935,
                             FrequencyMax = 960,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 1710,
                             FrequencyMax = 1785,
                             Band = 5
                         },
                        new OperatingFrequency()
                         {
                             FrequencyMin = 1805,
                             FrequencyMax = 1880,
                             Band = 5
                         }
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/16_3G.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/16_3G.png")
                },

                //17_Orbiter-2B
                new TablePattern()
                {
                     Id = 17,
                     Name = "Orbiter-2B",
                     FrequencyList = new ObservableCollection<OperatingFrequency>()
                     {
                         new OperatingFrequency()
                         {
                             FrequencyMin = 4500,
                             FrequencyMax = 4700,
                             Band = 10
                         }
                         //new OperatingFrequency()
                         //{
                         //    FrequencyMin = 5065,
                         //    FrequencyMax = 5090,
                         //    Band = 10
                         //}
                     },
                     ImageByte = ImageOperating.ImageSourceToBytes((ImageSource)converter.ConvertFromString(fullPath+"/7_Orbiter-2A.png"))
                     //Image = (ImageSource)converter.ConvertFromString(fullPath+"/7_Orbiter-2A.png")
                },




            //    // another UAV from table


            ////16_Phantom-2

            //    new TablePattern()
            //    {
            //         Id = 16,
            //         Name = "Phantom-2",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 8
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 2

            //    },

            ////17_Phantom-3

            //    new TablePattern()
            //    {
            //         Id = 17,
            //         Name = "Phantom-3",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 9
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////18_Phantom-4 Pro

            //    new TablePattern()
            //    {
            //         Id = 18,
            //         Name = "Phantom-4 Pro",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 10
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 10
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 20
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 20
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },


            // //19_Matrice-600

            //    new TablePattern()
            //    {
            //         Id = 19,
            //         Name = "Matrice-600",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 9
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////20_Phantom-3SE

            //    new TablePattern()
            //    {
            //         Id = 20,
            //         Name = "Phantom-3SE",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2400,
            //                 FrequencyMax = 2480,
            //                 Band = 5
            //             },

            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 5725,
            //                 FrequencyMax = 5850,
            //                 Band = 5
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////21_SuperCam 350

            //    new TablePattern()
            //    {
            //         Id = 21,
            //         Name = "SuperCam 350",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 1215,
            //                 FrequencyMax = 1215,
            //                 Band = 10
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////22_Orbitr-2B

            //    new TablePattern()
            //    {
            //         Id = 22,
            //         Name = "Orbitr-2B",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 4500,
            //                 FrequencyMax = 4600,
            //                 Band = 8
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    },

            ////23_Parrot

            //    new TablePattern()
            //    {
            //         Id = 23,
            //         Name = "Parrot",
            //         FrequencyList = new ObservableCollection<OperatingFrequency>()
            //         {
            //             new OperatingFrequency()
            //             {
            //                 FrequencyMin = 2340,
            //                 FrequencyMax = 2340,
            //                 Band = 5
            //             }
            //         },
            //         ImagePath = fullPath+"/0_Unknown.png" // надо Phantom 3
            //    }

            };

            AddPatternList(defaultTablePattern);

        }



    }
}