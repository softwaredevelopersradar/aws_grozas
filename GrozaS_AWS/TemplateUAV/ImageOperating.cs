﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GrozaS_AWS
{
    public static class ImageOperating
    {
        public static byte[] ImageSourceToBytes(ImageSource imageSource)
        {
            try
            {
                var encoder = new PngBitmapEncoder();
                byte[] bytes = null;
                var bitmapSource = imageSource as BitmapSource;

                if (bitmapSource != null)
                {
                    encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                    using (var stream = new MemoryStream())
                    {
                        encoder.Save(stream);
                        bytes = stream.ToArray();
                    }
                }

                return bytes;
            }
            catch
            {
                return Array.Empty<byte>();
            }
        }

        public static BitmapSource ConvertToImageSource(byte[] byteImage)
        {
            try
            {
                return GetImageStream(ByteArrayToImage(byteImage));
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                using (var ms = new MemoryStream(byteArrayIn))
                {
                    var returnImage = Image.FromStream(ms);

                    return returnImage;
                }
            }
            catch
            {
                return null;
            }
        }

        public static BitmapSource GetImageStream(Image myImage)
        {
            try
            {
                var bitmap = new Bitmap(myImage);
                IntPtr bmpPt = bitmap.GetHbitmap();
                BitmapSource bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    bmpPt,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

                //freeze bitmapSource and clear memory to avoid memory leaks
                bitmapSource.Freeze();
                DeleteObject(bmpPt);

                return bitmapSource;
            }
            catch
            {
                return null;
            }
        }

        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);
    }


    

    
}
