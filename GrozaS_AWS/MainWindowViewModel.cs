﻿using BRD;
using GrpcDbClientLib;
using DLL_Compass;
using DllGrozaSProperties.Models;
using GrozaS_AWS.AddPanel;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GlobusControl;
using WPFControlConnection;
using WpfMapControl;
using ImpressaMastDLL;

namespace GrozaS_AWS
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public ServiceClient clientDB;

        MarkSizeWnd markSizeWnd;

        

        private LocalProperties _localPropertiesVM;
        public LocalProperties LocalPropertiesVM
        {
            get { return _localPropertiesVM; }
            set
            {
                if (_localPropertiesVM != value)
                {
                    _localPropertiesVM = value;
                    OnPropertyChanged();

                }
            }
        }


       



        private GrozaSModelsDBLib.GlobalProperties _globalPropertiesVM;
        public GrozaSModelsDBLib.GlobalProperties GlobalPropertiesVM
        {
            get { return _globalPropertiesVM; }
            set
            {
                if (_globalPropertiesVM != value)
                {
                    _globalPropertiesVM = value;
                    OnPropertyChanged();

                }
            }
        }

        public SizeValue sizeDPSetting { get; set; }
        public SizeValue sizeDPTables { get; set; }
        public SizeValue sizeDPJamming { get; set; }
     

        private ConnectionStates _stateConnectionTDF = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionTDF
        {
            get { return _stateConnectionTDF; }
            set
            {
                if (_stateConnectionTDF != value)
                {
                    _stateConnectionTDF = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionGNSS = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGNSS
        {
            get { return _stateConnectionGNSS; }
            set
            {
                if (_stateConnectionGNSS != value)
                {
                    _stateConnectionGNSS = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionOEM = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionOEM
        {
            get { return _stateConnectionOEM; }
            set
            {
                if (_stateConnectionOEM != value)
                {
                    _stateConnectionOEM = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionDB = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionDB
        {
            get { return _stateConnectionDB; }
            set
            {
                if (_stateConnectionDB != value)
                {
                    _stateConnectionDB = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionCmpRX = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionCmpRX
        {
            get { return _stateConnectionCmpRX; }
            set
            {
                if (_stateConnectionCmpRX != value)
                {
                    _stateConnectionCmpRX = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionCmpTX = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionCmpTX
        {
            get { return _stateConnectionCmpTX; }
            set
            {
                if (_stateConnectionCmpTX != value)
                {
                    _stateConnectionCmpTX = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionBRD = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionBRD
        {
            get { return _stateConnectionBRD; }
            set
            {
                if (_stateConnectionBRD != value)
                {
                    _stateConnectionBRD = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionBRDJamming = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionBRDJamming
        {
            get { return _stateConnectionBRDJamming; }
            set
            {
                if (_stateConnectionBRDJamming != value)
                {
                    _stateConnectionBRDJamming = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionBRDCnt = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionBRDCnt
        {
            get { return _stateConnectionBRDCnt; }
            set
            {
                if (_stateConnectionBRDCnt != value)
                {
                    _stateConnectionBRDCnt = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionLNK = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionLNK
        {
            get { return _stateConnectionLNK; }
            set
            {
                if (_stateConnectionLNK != value)
                {
                    _stateConnectionLNK = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionServerCL = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionServerCL
        {
            get { return _stateConnectionServerCL; }
            set
            {
                if (_stateConnectionServerCL != value)
                {
                    _stateConnectionServerCL = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionClientCL = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionClientCL
        {
            get { return _stateConnectionClientCL; }
            set
            {
                if (_stateConnectionClientCL != value)
                {
                    _stateConnectionClientCL = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionED = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionED
        {
            get { return _stateConnectionED; }
            set
            {
                if (_stateConnectionED != value)
                {
                    _stateConnectionED = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionRodnik = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionRodnik
        {
            get { return _stateConnectionRodnik; }
            set
            {
                if (_stateConnectionRodnik != value)
                {
                    _stateConnectionRodnik = value;
                    OnPropertyChanged();

                }
            }
        }



        private ConnectionStates _stateConnectionZorkiR = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionZorkiR
        {
            get { return _stateConnectionZorkiR; }
            set
            {
                if (_stateConnectionZorkiR != value)
                {
                    _stateConnectionZorkiR = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionRS = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionRS
        {
            get { return _stateConnectionRS; }
            set
            {
                if (_stateConnectionRS != value)
                {
                    _stateConnectionRS = value;
                    OnPropertyChanged();

                }
            }
        }


        private ConnectionStates _stateConnectionClientCP = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionClientCP
        {
            get { return _stateConnectionClientCP; }
            set
            {
                if (_stateConnectionClientCP != value)
                {
                    _stateConnectionClientCP = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionAmplifier1 = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionAmplifier1
        {
            get { return _stateConnectionAmplifier1; }
            set
            {
                if (_stateConnectionAmplifier1 != value)
                {
                    _stateConnectionAmplifier1 = value;
                    OnPropertyChanged();

                }
            }
        }



        private ConnectionStates _stateConnectionAmplifier2 = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionAmplifier2
        {
            get { return _stateConnectionAmplifier2; }
            set
            {
                if (_stateConnectionAmplifier2 != value)
                {
                    _stateConnectionAmplifier2 = value;
                    OnPropertyChanged();

                }
            }
        }



        private ConnectionStates _stateConnectionCuirasse = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionCuirasse
        {
            get { return _stateConnectionCuirasse; }
            set
            {
                if (_stateConnectionCuirasse != value)
                {
                    _stateConnectionCuirasse = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionSpoofing = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionSpoofing
        {
            get { return _stateConnectionSpoofing; }
            set
            {
                if (_stateConnectionSpoofing != value)
                {
                    _stateConnectionSpoofing = value;
                    OnPropertyChanged();
                }
            }
        }

        private ConnectionStates _stateConnectionGlobus = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGlobus
        {
            get { return _stateConnectionGlobus; }
            set
            {
                if (_stateConnectionGlobus != value)
                {
                    _stateConnectionGlobus = value;
                    OnPropertyChanged();
                }
            }
        }

        private ConnectionStates _stateConnectionGrozaR_UHF = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGrozaR_UHF
        {
            get { return _stateConnectionGrozaR_UHF; }
            set
            {
                if (_stateConnectionGrozaR_UHF != value)
                {
                    _stateConnectionGrozaR_UHF = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionGrozaR_URL = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionGrozaR_URL
        {
            get { return _stateConnectionGrozaR_URL; }
            set
            {
                if (_stateConnectionGrozaR_URL != value)
                {
                    _stateConnectionGrozaR_URL = value;
                    OnPropertyChanged();

                }
            }
        }

        private ConnectionStates _stateConnectionEscope = ConnectionStates.Unknown;

        public ConnectionStates StateConnectionEscope
        {
            get { return _stateConnectionEscope; }
            set
            {
                if (_stateConnectionEscope != value)
                {
                    _stateConnectionEscope = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionRX;

        public Direction DirectionRX
        {
            get { return _directionRX; }
            set
            {
                if (_directionRX != value)
                {
                    _directionRX = value;
                    OnPropertyChanged();

                }
            }
        }




        //private Direction _directionTX1;

        //public Direction DirectionBRD1_Upper
        //{
        //    get { return _directionTX1; }
        //    set
        //    {
        //        if (_directionTX1 != value)
        //        {
        //            _directionTX1 = value;
        //            OnPropertyChanged();

        //        }
        //    }
        //}

        //private Direction _directionTX2;

        //public Direction DirectionBRD1_Bottom
        //{
        //    get { return _directionTX2; }
        //    set
        //    {
        //        if (_directionTX2 != value)
        //        {
        //            _directionTX2 = value;
        //            OnPropertyChanged();

        //        }
        //    }
        //}


        private Direction _directionBRD1_Upper;

        public Direction DirectionBRD1_Upper
        {
            get { return _directionBRD1_Upper; }
            set
            {
                if (_directionBRD1_Upper != value)
                {
                    _directionBRD1_Upper = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionBRD1_Bottom;

        public Direction DirectionBRD1_Bottom
        {
            get { return _directionBRD1_Bottom; }
            set
            {
                if (_directionBRD1_Bottom != value)
                {
                    _directionBRD1_Bottom = value;
                    OnPropertyChanged();

                }
            }
        }

       


        private Direction _directionRotateBRD1_Upper;

        public Direction DirectionRotateBRD1_Upper
        {
            get { return _directionRotateBRD1_Upper; }
            set
            {
                if (_directionRotateBRD1_Upper != value)
                {
                    _directionRotateBRD1_Upper = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionRotateBRD1_Bottom;

        public Direction DirectionRotateBRD1_Bottom
        {
            get { return _directionRotateBRD1_Bottom; }
            set
            {
                if (_directionRotateBRD1_Bottom != value)
                {
                    _directionRotateBRD1_Bottom = value;
                    OnPropertyChanged();

                }
            }
        }




        private Direction _directionBRD2_Upper;

        public Direction DirectionBRD2_Upper
        {
            get { return _directionBRD2_Upper; }
            set
            {
                if (_directionBRD2_Upper != value)
                {
                    _directionBRD2_Upper = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionBRD2_Bottom;

        public Direction DirectionBRD2_Bottom
        {
            get { return _directionBRD2_Bottom; }
            set
            {
                if (_directionBRD2_Bottom != value)
                {
                    _directionBRD2_Bottom = value;
                    OnPropertyChanged();

                }
            }
        }


        //private Direction _directionCX1;


        //public Direction DirectionCX1
        //{
        //    get { return _directionCX1; }
        //    set
        //    {
        //        if (_directionCX1 != value)
        //        {
        //            _directionCX1 = value;
        //            OnPropertyChanged();

        //        }
        //    }
        //}

        //private Direction _directionCX2;

        //public Direction DirectionCX2
        //{
        //    get { return _directionCX2; }
        //    set
        //    {
        //        if (_directionCX2 != value)
        //        {
        //            _directionCX2 = value;
        //            OnPropertyChanged();

        //        }
        //    }
        //}

        private Direction _directionOEM;

        public Direction DirectionOEM
        {
            get { return _directionOEM; }
            set
            {
                if (_directionOEM != value)
                {
                    _directionOEM = value;
                    OnPropertyChanged();

                }
            }
        }









        //private Direction _directionBRD_TX1;

        //public Direction DirectionBRD_TX1
        //{
        //    get { return _directionBRD_TX1; }
        //    set
        //    {
        //        if (_directionBRD_TX1 != value)
        //        {
        //            _directionBRD_TX1 = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private Direction _directionBRD_TX2;

        //public Direction DirectionBRD_TX2
        //{
        //    get { return _directionBRD_TX2; }
        //    set
        //    {
        //        if (_directionBRD_TX2 != value)
        //        {
        //            _directionBRD_TX2 = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}









        //private Direction _directionBRD_CX1;

        //public Direction DirectionBRD_CX1
        //{
        //    get { return _directionBRD_CX1; }
        //    set
        //    {
        //        if (_directionBRD_CX1 != value)
        //        {
        //            _directionBRD_CX1 = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}

        //private Direction _directionBRD_CX2;

        //public Direction DirectionBRD_CX2
        //{
        //    get { return _directionBRD_CX2; }
        //    set
        //    {
        //        if (_directionBRD_CX2 != value)
        //        {
        //            _directionBRD_CX2 = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}




        private Direction _directionRotateBRD2_Upper;

        public Direction DirectionRotateBRD2_Upper
        {
            get { return _directionRotateBRD2_Upper; }
            set
            {
                if (_directionRotateBRD2_Upper != value)
                {
                    _directionRotateBRD2_Upper = value;
                    OnPropertyChanged();

                }
            }
        }

        private Direction _directionRotateBRD2_Bottom;

        public Direction DirectionRotateBRD2_Bottom
        {
            get { return _directionRotateBRD2_Bottom; }
            set
            {
                if (_directionRotateBRD2_Bottom != value)
                {
                    _directionRotateBRD2_Bottom = value;
                    OnPropertyChanged();

                }
            }
        }





        private Coord _locationCenter;

        public Coord LocationCenter
        {
            get { return _locationCenter; }
            set
            {
                
                _locationCenter = value;
                OnPropertyChanged();


            }
        }

      


        private bool _autoUpdateSource = false;

        public bool AutoUpdateSource
        {
            get { return _autoUpdateSource; }
            set
            {
                _autoUpdateSource = value;
                OnPropertyChanged();
            }
        }

        private DateTime _startModeTime = DateTime.Now;

        public DateTime StartModeTime
        {
            get { return _startModeTime; }
            set
            {
                _startModeTime = value;
                OnPropertyChanged();
            }
        }

        private (Mode, TimeSpan) _previousMode = (Mode.STOP, default(TimeSpan));

        public (Mode, TimeSpan) PreviousMode
        {
            get { return _previousMode; }
            set
            {
                _previousMode = value;
                OnPropertyChanged();
            }
        }

        private Mode _tempMode = Mode.STOP;

        public Mode TempMode
        {
            get { return _tempMode; }
            set
            {
                _previousMode = (_tempMode, DateTime.Now - _startModeTime);
                
                _tempMode = value;
                StartModeTime = DateTime.Now;
                OnPropertyChanged();
            }
        }

        private string _viewMode = "";

        public string ViewMode
        {
            get { return _viewMode; }
            set
            {
                _viewMode = value;
                OnPropertyChanged();
            }
        }


        private bool _autoMode= false;

        public bool AutoMode
        {
            get { return _autoMode; }
            set
            {
                _autoMode = value;
                OnPropertyChanged();
            }
        }

        public Direction SetAngleBRD(float AngleNeed, Direction DirectionInput)
        {            
            var dif = (AngleNeed > DirectionInput.Result) ? (AngleNeed - DirectionInput.Result) : (360 - (DirectionInput.Result - AngleNeed));

            float val = DirectionInput.Azimuth + dif;
            while (val > 360)
                val -= 360;

            Direction DirectionOut = new Direction() { Address = DirectionInput.Address, Set = DirectionInput.Set, Error = DirectionInput.Error, CourseAngle = DirectionInput.CourseAngle, Azimuth = val };
            return DirectionOut;

        }

        public Direction SetAngleOEM(float AngleNeed, Direction DirectionInput)
        {            
            Direction DirectionOut = new Direction() { Error = DirectionInput.Error*(-1), CourseAngle = DirectionInput.CourseAngle*(-1), Azimuth = AngleNeed };
            return DirectionOut;

        }


        


        private ExBearing _exBearingSendRequest = new ExBearing();

        public ExBearing ExBearingSendRequest
        {
            get { return _exBearingSendRequest; }
            set
            {
                _exBearingSendRequest = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingSendAnswer = new ExBearing();

        public ExBearing ExBearingSendAnswer
        {
            get { return _exBearingSendAnswer; }
            set
            {
                _exBearingSendAnswer = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingReceiveRequest = new ExBearing();

        public ExBearing ExBearingReceiveRequest
        {
            get { return _exBearingReceiveRequest; }
            set
            {
                _exBearingReceiveRequest = value;
                OnPropertyChanged();
            }
        }


        private ExBearing _exBearingReceiveAnswer = new ExBearing();

        public ExBearing ExBearingReceiveAnswer
        {
            get { return _exBearingReceiveAnswer; }
            set
            {
                _exBearingReceiveAnswer = value;
                OnPropertyChanged();
            }
        }



        private AutoExBearing _autoExecutiveBearing;

        public AutoExBearing AutoExecutiveBearing
        {
            get { return _autoExecutiveBearing; }
            set
            {
                _autoExecutiveBearing = value;
                OnPropertyChanged();
            }
        }



        private string _pathImageOrientation;

        public string PathImageOrientation
        {
            get { return _pathImageOrientation; }
            set
            {
                _pathImageOrientation = value;
                OnPropertyChanged();
            }
        }

        private float _angleOrientation = 0;

        public float AngleOrientation
        {
            get { return _angleOrientation; }
            set
            {
                _angleOrientation = value;
                OnPropertyChanged();
            }
        }


        private DateTime _startSpoofingTime = default(DateTime);

        public DateTime StartSpoofingTime
        {
            get { return _startSpoofingTime; }
            set
            {
                _startSpoofingTime = value;
                OnPropertyChanged();
            }
        }

        private bool _activeSpoofing = false;

        public bool ActiveSpoofing
        {
            get { return _activeSpoofing; }
            set
            {
                if (!_activeSpoofing && value)
                    _startSpoofingTime = DateTime.Now;

                _activeSpoofing = value;
                OnPropertyChanged();
            }
        }

        private StateChannelTDF _channelTDF_RS = new StateChannelTDF();
        public StateChannelTDF ChannelTDF_RS
        {
            get { return _channelTDF_RS; }
            set
            {
                _channelTDF_RS = value;
                OnPropertyChanged();
            }
        }

        private bool _autoGlobusMode = false;

        public bool AutoGlobusMode
        {
            get { return _autoGlobusMode; }
            set
            {
                _autoGlobusMode = value;
                OnPropertyChanged();
            }
        }

        private GlobusModel _trackedGlobus = new GlobusModel(){Id = -2};

        public GlobusModel TrackedGlobus
        {
            get { return _trackedGlobus; }
            set
            {
                _trackedGlobus = value;
                OnPropertyChanged();
            }
        }

        private int _trackedGlobusId = -2;

        public int TrackedGlobusId
        {
            get { return _trackedGlobusId; }
            set
            {
                _trackedGlobusId = value;
                OnPropertyChanged();
            }
        }


        private MastParamBroadcast _mastParamBroadcast = new MastParamBroadcast();
        public MastParamBroadcast mastParamBroadcast
        {
            get { return _mastParamBroadcast; }
            set
            {
                if (_mastParamBroadcast.Equals(value)) return;
                _mastParamBroadcast = value;
                OnPropertyChanged();
            }

        }


        private OPUParamBroadcast _opuParamBroadcast = new OPUParamBroadcast();
        public OPUParamBroadcast opuParamBroadcast
        {
            get { return _opuParamBroadcast; }
            set
            {
                if (_opuParamBroadcast.Equals(value)) return;
                _opuParamBroadcast = value;
                OnPropertyChanged();
            }

        }

        public void SetDefaultDPanel()
        {
            sizeDPSetting.Visible = true;
            sizeDPTables.Visible = true;
            sizeDPJamming.Visible = false;
           

            sizeDPSetting.Current = DefaultSize.WidthDPanelSetting;
            sizeDPTables.Current = DefaultSize.HeightDPanelJammer;
            sizeDPJamming.Current = 0;// DefaultSize.WidthDPanelJamming;

            

            sizeDPSetting.SetDefault();
            sizeDPTables.SetDefault();
            sizeDPJamming.SetDefault();
    
        }

        #region RS
        private string _errorRS = string.Empty;

        public string ErrorRS
        {
            get { return _errorRS; }
            set
            {
                if (_errorRS != value)
                {
                    _errorRS = value;
                    OnPropertyChanged();

                }
            }
        }
        #endregion

        public MainWindowViewModel()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");


            InitMarkSizeWnd();

            

            sizeDPSetting.PropertyChanged += SizeDP_PropertyChanged;
            sizeDPTables.PropertyChanged += SizeDP_PropertyChanged;
            sizeDPJamming.PropertyChanged += SizeDP_PropertyChanged;
          

           

            GlobalPropertiesVM = new GrozaSModelsDBLib.GlobalProperties();
            LocalPropertiesVM = new LocalProperties();

          

            DirectionRX = new Direction();
            DirectionBRD1_Upper = new Direction();
            DirectionBRD1_Bottom = new Direction();
            DirectionBRD2_Upper = new Direction();
            DirectionBRD2_Bottom = new Direction();
            DirectionOEM = new Direction();
            DirectionRotateBRD1_Upper = new Direction();
            DirectionRotateBRD1_Bottom = new Direction();
            DirectionRotateBRD2_Upper = new Direction();
            DirectionRotateBRD2_Bottom = new Direction();


            AutoExecutiveBearing = new AutoExBearing();

            InitZorkiRTargetVisible();

        }

        private void InitMarkSizeWnd()
        {
            markSizeWnd = SerializerJSON.Deserialize<MarkSizeWnd>(AppDomain.CurrentDomain.BaseDirectory + "SizePanel.json");

            if (markSizeWnd == null)
            {
                markSizeWnd = new MarkSizeWnd();
                sizeDPSetting = new SizeValue(DefaultSize.WidthDPanelSetting);
                sizeDPTables = new SizeValue(DefaultSize.HeightDPanelJammer);
                sizeDPJamming = new SizeValue(DefaultSize.WidthDPanelJamming);
              
                SetDefaultDPanel();
            }

            else
            {
                sizeDPSetting = markSizeWnd.sizeDPSetting;
                sizeDPTables = markSizeWnd.sizeDPTables;
                sizeDPJamming = markSizeWnd.sizeDPJamming;


                


            }
        }
        

        private void SizeDP_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            markSizeWnd.sizeDPSetting = sizeDPSetting;
            markSizeWnd.sizeDPTables = sizeDPTables;
            markSizeWnd.sizeDPJamming = sizeDPJamming;
         

            SerializerJSON.Serialize<MarkSizeWnd>(markSizeWnd, AppDomain.CurrentDomain.BaseDirectory + "SizePanel.json");

        }

        private Dictionary<ViewZorkiR, bool> _zorkiRTargetVisible = new Dictionary<ViewZorkiR, bool>();
        public Dictionary<ViewZorkiR, bool> ZorkiRTargetVisible
        {
            get { return _zorkiRTargetVisible; }
            set
            {
                _zorkiRTargetVisible = value;
                OnPropertyChanged();
            }
        }

          
        public static IEnumerable<T> GetEnumValues<T>() where T: Enum
        { 
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
        private void InitZorkiRTargetVisible()
        {
            foreach(var zrv in GetEnumValues<ViewZorkiR>())
                _zorkiRTargetVisible.Add(zrv, true);            
        }


        





        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));            
        }

        
    #endregion
}
}
