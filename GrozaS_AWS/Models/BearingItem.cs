﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class BearingItem : INotifyPropertyChanged
    {
        private int _id;
        public int ID
        {
            get { return _id; }
            set
            {
                if (_id.Equals(value)) return;
                _id = value;
                OnPropertyChanged();
            }

        }

        private float _bearing;
        public float Bearing
        {
            get { return _bearing; }
            set
            {
                if (_bearing.Equals(value)) return;
                _bearing = value;
                OnPropertyChanged();
            }

        }

        private float _distance;
        public float Distance
        {
            get { return _distance; }
            set
            {
                if (_distance.Equals(value)) return;
                _distance = value;
                OnPropertyChanged();
            }

        }


        private int _jammer;
        public int Jammer
        {
            get { return _jammer; }
            set
            {
                if (_jammer.Equals(value)) return;
                _jammer = value;
                OnPropertyChanged();
            }

        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
