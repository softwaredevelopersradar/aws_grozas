﻿using System;
using GrozaSModelsDBLib;
using System.Collections.ObjectModel;
using Mapsui.Styles;

namespace GrozaS_AWS.Models
{
    public class EscopeTarget
    {
        //private int _iD;

        //public int ID
        //{
        //    get { return _iD; }
        //    set
        //    {
        //        if (_iD.Equals(value)) return;
        //        _iD = value;

        //    }
        //}

        private string _serialNumber = string.Empty;

        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                if (_serialNumber.Equals(value)) return;
                _serialNumber = value;

            }
        }


        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }



        private float _elevation = -1;

        public float Elevation
        {
            get { return _elevation; }
            set
            {
                if (_elevation.Equals(value)) return;
                _elevation = value;
            }
        }

        private DateTime _loctime;

        public DateTime Loctime
        {
            get { return _loctime; }
            set
            {
                if (_loctime.Equals(value)) return;
                _loctime = value;

            }
        }

        private string _type = String.Empty;
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }


        private Coord _home = new Coord();

        public Coord Home
        {
            get { return _home; }
            set
            {
                if (_home.Equals(value)) return;
                _home = value;

            }
        }

        private Coord _pilot = new Coord();

        public Coord Pilot
        {
            get { return _pilot; }
            set
            {
                if (_pilot.Equals(value)) return;
                _pilot = value;

            }
        }

        private Color _color = new Color();

        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color.Equals(value)) return;
                _color = value;
            }
        }


        public ObservableCollection<AeroscopeMark> escopeMarks = new ObservableCollection<AeroscopeMark>();

    }
}
