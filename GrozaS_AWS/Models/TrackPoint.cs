﻿using Bearing;
using GrozaS_AWS.TDF;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class TrackPoint : INotifyPropertyChanged
    {

        private int _id;
        public int ID
        {
            get { return _id; }
            set
            {
                if (_id.Equals(value)) return;
                _id = value;
                OnPropertyChanged();
            }

        }

        private double _frequency;
        public double Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency.Equals(value)) return;
                _frequency = value;
                OnPropertyChanged();
            }

        }

        private double _frequencyRX;
        public double FrequencyRX
        {
            get { return _frequencyRX; }
            set
            {
                if (_frequencyRX.Equals(value)) return;
                _frequencyRX = value;
                OnPropertyChanged();
            }

        }

        

        private float _band;
        public float Band
        {
            get { return _band; }
            set
            {
                if (_band.Equals(value)) return;
                _band = value;
                OnPropertyChanged();
            }

        }

        private DateTime _time;
        public DateTime Time
        {
            get { return _time; }
            set
            {
                if (_time.Equals(value)) return;
                _time = value;
                OnPropertyChanged();
            }

        }


        private Coord _coordinate = new Coord();
        public Coord Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;
                OnPropertyChanged();
            }

        }

        private List<Range> _zoneValue = new List<Range>();
        public List<Range> ZoneValue
        {
            get { return _zoneValue; }
            set
            {
                if (_zoneValue.Equals(value)) return;
                _zoneValue = value;
                OnPropertyChanged();

                CalculateZone();
            }

        }

        private Zones _zone = 0;
        public Zones Zone
        {
            get { return _zone; }
            set
            {
                if (_zone.Equals(value)) return;
                _zone = value;
                OnPropertyChanged();
            }

        }

        private ObservableCollection<BearingItem> _bearings = new ObservableCollection<BearingItem>();
        public ObservableCollection<BearingItem> Bearings
        {
            get { return _bearings; }
            set
            {
                if (_bearings.Equals(value)) return;
                _bearings = value;

                _bearings.CollectionChanged += _bearings_CollectionChanged;
                OnPropertyChanged();

                CalculatePoint();
            }

        }

        private void _bearings_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CalculatePoint();
        }

        private ObservableCollection<TableJammerStation> _jammers = new ObservableCollection<TableJammerStation>();
        public ObservableCollection<TableJammerStation> Jammers
        {
            get { return _jammers; }
            set
            {
                if (_jammers.Equals(value)) return;
                _jammers = value;
                OnPropertyChanged();

                CalculatePoint();
            }

        }



        private void CalculateCoord()
        {
            double tempLat = 0;
            double tempLong = 0;
            
            var bearingNonzero = Bearings?.Where(x => x.Bearing >= 0).FirstOrDefault();

            if (Bearings != null && Bearings.Count > 0 && bearingNonzero != null )
            {
                try
                {
                    float Distance = bearingNonzero.Distance;
                    int Jammer = bearingNonzero.Jammer;
                    float Bearing = bearingNonzero.Bearing;

                    if (Distance > 0 && Bearings != null && Jammers != null && Jammers.Count > 0)
                        ClassBearing.f_Bearing(Bearings.Where(x => x.Jammer == Jammer).FirstOrDefault().Bearing,
                                Distance, 300,
                                Jammers.Where(x => x.Id == Jammer).FirstOrDefault().Coordinates.Latitude,
                                Jammers.Where(x => x.Id == Jammer).FirstOrDefault().Coordinates.Longitude,
                                1,
                                ref tempLat, ref tempLong);


                    if (Bearings.Count > 1 && Bearings[0].Bearing >= 0 && Bearings[1].Bearing >= 0 &&
                        Jammers != null && Jammers.Count > 1)
                    {

                        var FirstJammer = Bearings[0].Jammer;
                        var SecondJammer = Bearings[1].Jammer;


                        ClassBearing.f_Triangulation(1000, 1000,
                                                 Bearings.Where(x => x.Jammer == FirstJammer).FirstOrDefault().Bearing,
                                                 Bearings.Where(x => x.Jammer == SecondJammer).FirstOrDefault().Bearing,
                                                 Jammers.Where(x => x.Id == FirstJammer).FirstOrDefault().Coordinates.Latitude,
                                                 Jammers.Where(x => x.Id == FirstJammer).FirstOrDefault().Coordinates.Longitude,
                                                 Jammers.Where(x => x.Id == SecondJammer).FirstOrDefault().Coordinates.Latitude,
                                                 Jammers.Where(x => x.Id == SecondJammer).FirstOrDefault().Coordinates.Longitude,
                                                 1, 300000, ref tempLat, ref tempLong);
                    }

                }
                catch
                {

                }
            }



            Coordinate = new Coord() { Latitude = tempLat, Longitude = tempLong };

        }

        private void CalculateDistance()
        {
            if (Bearings!= null && Jammers != null && Jammers.Count >1)
            try
            {
                foreach (var br in Bearings)
                {
                    var coord = Jammers.Where(x => x.Id == br.Jammer).FirstOrDefault().Coordinates;

                    if (coord.Latitude != -1 &&
                        coord.Longitude != -1 &&
                        Coordinate.Latitude != 0 &&
                        Coordinate.Longitude != 0)
                    br.Distance = (float)Math.Round(ClassBearing.f_D_2Points(coord.Latitude,
                        coord.Longitude, Coordinate.Latitude, Coordinate.Longitude, 1), MidpointRounding.AwayFromZero);
                }
            }
            catch { }
        }

        private void CalculateZone()
        {
            

            if (Bearings != null && Jammers != null && Jammers .Count > 0 && ZoneValue.Count>1)
            try
            {
                var ownJammer = Jammers.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id;
                var dist = Bearings.Where(x => x.Jammer == ownJammer).FirstOrDefault().Distance;

                int i = 0;
                    while (i < 3)
                    {
                       

                        if (ZoneValue[i].Contains(dist))
                        {
                            i++;
                            //var dd = i++;
                            Zone = (Zones)i++; 
                            i = 3;
                        }
                        i++;

                    }
                    
                        
                
                    
                
            }
            catch { }
        }


        public TrackPoint()
        {
            CalculateCoord();

            CalculateDistance();

            CalculateZone();
        }

        public void CalculatePoint()
        {
            CalculateCoord();

            CalculateDistance();

            CalculateZone();
        }



        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion
    }
}
