﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    internal class GlobusMark
    {
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }


        private DateTime _loctime;

        public DateTime Loctime
        {
            get { return _loctime; }
            set
            {
                if (_loctime.Equals(value)) return;
                _loctime = value;

            }
        }

        private double _azimuth = -1;

        public double Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;

            }
        }


        private double _velocity;

        public double Velocity
        {
            get { return _velocity; }
            set
            {
                if (_velocity.Equals(value)) return;
                _velocity = value;

            }
        }

        private double _elevation;

        public double Elevation
        {
            get { return _elevation; }
            set
            {
                if (_elevation.Equals(value)) return;
                _elevation = value;

            }
        }


        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            private set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }
    }
}
