﻿public struct NavigationJamming
{
    public bool GpsL1;
    public bool GpsL2;

    public bool GlonassL1;
    public bool GlonassL2;

    public bool GNSSL1;
    public bool GNSSL2;
}