﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    

    public class AeroscopeMark
    {
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        private string _serialNumber = String.Empty;

        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                if (_serialNumber.Equals(value)) return;
                _serialNumber = value;

            }
        }


        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }



        private float _elevation = -1;

        public float Elevation
        {
            get { return _elevation; }
            set
            {
                if (_elevation.Equals(value)) return;
                _elevation = value;               
            }
        }

        private DateTime _loctime;

        public DateTime Loctime
        {
            get { return _loctime; }
            set
            {
                if (_loctime.Equals(value)) return;
                _loctime = value;

            }
        }



        private float _roll = 0;

        public float Roll
        {
            get { return _roll; }
            set
            {
                if (_roll.Equals(value)) return;
                _roll = value;

               
            }
        }



        private float _pitch = 0;

        public float Pitch
        {
            get { return _pitch; }
            set
            {
                if (_pitch.Equals(value)) return;
                _pitch = value;


            }

        }


        private float _yaw = 0;

        public float Yaw
        {
            get { return _yaw; }
            set
            {
                if (_yaw.Equals(value)) return;
                _yaw = value;


            }

        }


        private float _vup;

        public float Vup
        {
            get { return _vup; }
            set
            {
                if (_vup.Equals(value)) return;
                _vup = value;

            }
        }



        private float _veast;

        public float Veast
        {
            get { return _veast; }
            set
            {
                if (_veast.Equals(value)) return;
                _veast = value;

            }
        }


        private float _vnorth;

        public float Vnorth
        {
            get { return _vnorth; }
            set
            {
                if (_vnorth.Equals(value)) return;
                _vnorth = value;

            }
        }

        private Coord _home = new Coord();

        public Coord Home
        {
            get { return _home; }
            set
            {
                if (_home.Equals(value)) return;
                _home = value;

            }
        }

        
        public AeroscopeMark()
        {

        }

    }
}
