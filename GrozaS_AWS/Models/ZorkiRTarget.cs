﻿using Bearing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    class ZorkiRTarget
    {
        
        private short _iD;

        public short ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        public ObservableCollection<ZorkiRMark> zorkiRMarks = new ObservableCollection<ZorkiRMark>();

        
        public ZorkiRTarget()
        {
            //zorkiRMarks.CollectionChanged += ZorkiRMarks_CollectionChanged;
        }

        private void ZorkiRMarks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CountDistance();
        }

        private void CountDistance()
        {
            try
            {
                float dist = 0;

                int i = zorkiRMarks.Count - 1;

                if (zorkiRMarks.Count > 1)
                {

                    dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            zorkiRMarks[i].Coordinate.Latitude,
                            zorkiRMarks[i].Coordinate.Longitude,
                            zorkiRMarks[i - 1].Coordinate.Latitude,
                            zorkiRMarks[i - 1].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);


                    while (i > -1)
                    {
                        dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            zorkiRMarks[i - 1].Coordinate.Latitude,
                            zorkiRMarks[i - 1].Coordinate.Longitude,
                            zorkiRMarks[i].Coordinate.Latitude,
                            zorkiRMarks[i].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);

                        if (dist >= 5000)
                        {
                            for (int j = 0; j < zorkiRMarks.Count - i; j++)
                                zorkiRMarks.Remove(zorkiRMarks[0]);
                            i = -1;
                        }
                        i--;

                    }
                }
            }
            catch { }
        }


    }
}
