﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class RodnikTarget
    {

        

        private byte _RLS;
        
        public byte RLS
        {
            get { return _RLS; }
            set
            {
                if (_RLS.Equals(value)) return;
                _RLS = value;
               
            }
        }

        private short _iD;

        public short ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        public ObservableCollection<RodnikMark> rodnikMarks = new ObservableCollection<RodnikMark>();

        

        public RodnikTarget()
        {
            rodnikMarks.CollectionChanged += RodnikMarks_CollectionChanged;
        }

        private void RodnikMarks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CountDistance();
        }

        private void CountDistance()
        {
            try
            {
                float dist = 0;

                int i = rodnikMarks.Count - 1;

                if (rodnikMarks.Count > 1)
                {

                    dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            rodnikMarks[i].Coordinate.Latitude,
                            rodnikMarks[i].Coordinate.Longitude,
                            rodnikMarks[i - 1].Coordinate.Latitude,
                            rodnikMarks[i - 1].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);


                    while (i > -1)
                    {
                        dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            rodnikMarks[i - 1].Coordinate.Latitude,
                            rodnikMarks[i - 1].Coordinate.Longitude,
                            rodnikMarks[i].Coordinate.Latitude,
                            rodnikMarks[i].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);

                        if (dist >= 5000)
                        {
                            for (int j = 0; j < rodnikMarks.Count - i; j++)
                                rodnikMarks.Remove(rodnikMarks[0]);
                            i = -1;
                        }
                        i--;

                    }
                }
            }
            catch { }
        }


    }
}
