﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    internal class GlobusTarget
    {
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        public ObservableCollection<GlobusMark> globusMarks = new ObservableCollection<GlobusMark>();


        public GlobusTarget()
        {
            //zorkiRMarks.CollectionChanged += ZorkiRMarks_CollectionChanged;
        }
    }
}
