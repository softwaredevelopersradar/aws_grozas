﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class CuirasseMark
    {
        


        private short _iD;

        public short ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        private double _frequency=-1;

        public double Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency.Equals(value)) return;
                _frequency = value;

            }
        }




        private float _band;

        public float Band
        {
            get { return _band; }
            set
            {
                if (_band.Equals(value)) return;
                _band = value;

            }
        }

        private DateTime _loctime;

        public DateTime Loctime
        {
            get { return _loctime; }
            set
            {
                if (_loctime.Equals(value)) return;
                _loctime = value;

            }
        }

  
        private ClassCuirasse _type;

        public ClassCuirasse Type
        {
            get { return _type; }
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }

      

        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }



     

        public CuirasseMark()
        {
           
        }

    }
}
