﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class AutoExBearing: INotifyPropertyChanged
    {
        private bool _autoExecBearing = false;

        public bool AutoExecBearing
        {
            get { return _autoExecBearing; }
            set
            {

                _autoExecBearing = value;
                OnPropertyChanged();

            }
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}