﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class SingleViewBandAWS: INotifyPropertyChanged
    {
        private int _number;
        public int Number
        {
            get { return _number; }
            set
            {
                if (_number.Equals(value)) return;
                _number = value;
                
            }

        }

        private int _freqMin;
        public int FreqMin
        {
            get { return _freqMin; }
            set
            {
                if (_freqMin.Equals(value)) return;
                _freqMin = value;

            }

        }

        private int _freqMax;
        public int FreqMax
        {
            get { return _freqMax; }
            set
            {
                if (_freqMax.Equals(value)) return;
                _freqMax = value;

            }

        }

        private int _att1;
        public int Att1
        {
            get { return _att1; }
            set
            {
                if (_att1.Equals(value)) return;
                _att1 = value;
                OnPropertyChanged();
            }

        }

        private int _att2;
        public int Att2
        {
            get { return _att2; }
            set
            {
                if (_att2.Equals(value)) return;
                _att2 = value;
                OnPropertyChanged();
            }

        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }

    }
}
