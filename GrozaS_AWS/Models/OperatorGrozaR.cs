﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class OperatorGrozaR
    {
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        private ModeGrozaR _mode;

        public ModeGrozaR Mode
        {
            get { return _mode; }
            set
            {
                if (_mode.Equals(value)) return;
                _mode = value;

            }
        }
        

        private Coord _coordinate = new Coord();

        public Coord Coordinate
        {
            get { return _coordinate; }
            private set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;

            }
        }

        private short _azimuth = -1;

        public short Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;

            }
        }

        private string _note = "";

        public string Note
        {
            get { return _note; }
            set
            {
                if (_note.Equals(value)) return;
                _note = value;

            }
        }

        private TypeConnectionZorkiR _typeConnection = TypeConnectionZorkiR.Modem_3G_4G;

        public TypeConnectionZorkiR TypeConnection
        {
            get { return _typeConnection; }
            set
            {
                if (_typeConnection.Equals(value)) return;
                _typeConnection = value;

            }
        }

    }
}
