﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class StateChannelTDF:INotifyPropertyChanged
    {
        private MODE_RS _stateRS = MODE_RS.ACTIVE;

        public MODE_RS StateRS
        {
            get { return _stateRS; }
            set
            {
                _stateRS = value;
                OnPropertyChanged();
            }
        }

        private bool _getRS = false;

        public bool GetRS
        {
            get { return _getRS; }
            set
            {
                _getRS = value;
                OnPropertyChanged();
            }
        }


        private bool _senToRS = false;

        public bool SenToRS
        {
            get { return _senToRS; }
            set
            {
                _senToRS = value;
                OnPropertyChanged();
            }
        }

        private int _idGetRS = 0;

        public int IDGetRS
        {
            get { return _idGetRS; }
            set
            {
                _idGetRS = value;
                OnPropertyChanged();
            }
        }


        private bool _recordRS = false;

        public bool RecordRS
        {
            get { return _recordRS; }
            set
            {
                _recordRS = value;
                OnPropertyChanged();
            }
        }

        private bool _recordSetRS = false;

        public bool RecordSetRS
        {
            get { return _recordSetRS; }
            set
            {
                _recordSetRS = value;
                OnPropertyChanged();
            }
        }


        private TableOwnUAV _itemRecordRS = new TableOwnUAV();

        public TableOwnUAV ItemRecordRS
        {
            get { return _itemRecordRS; }
            set
            {
                _itemRecordRS = value;
                OnPropertyChanged();
            }
        }


         

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
