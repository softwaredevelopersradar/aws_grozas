﻿using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace GrozaS_AWS.Models
{
    using GrozaSModelsDBLib;

    using UISuppressSource;

    public class YamlJammingParameters
    {
        public Dictionary<string, JammingParameters> ReadRanges(string file)
        {

            //var ranges = GenerateDefaultRanges();
            //YamlSave(ranges, file);

            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(file, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd(); //File.ReadAllText(file);
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            var deserializer = new DeserializerBuilder().Build();

            CollectionDroneJammingParams deserialized = null;
            try
            {
                deserialized = deserializer.Deserialize<CollectionDroneJammingParams>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (deserialized == null)
            {
                deserialized = GenerateDefaultJammingParams();
                YamlSave(deserialized, file);
            }

            return deserialized?.Drones;
        }

        private CollectionDroneJammingParams GenerateDefaultJammingParams()
        {
            var ranges = new CollectionDroneJammingParams();
            ranges.Drones = new Dictionary<string, JammingParameters>();
            ranges.Drones.Add("Mavic Air", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 42, ScanSpeedKHzs = 10});
            ranges.Drones.Add("Mavic 2", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 42, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Phantom-4", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 42, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Orlan-10", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 12, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Busel-M", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 12, ScanSpeedKHzs = 10 });

            ranges.Drones.Add("Berkut", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 24, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("RQ-11B Raven", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("RQ-11A Raven", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 10, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Luna X2000", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Schiebel", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });

            ranges.Drones.Add("Orbiter-2M", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Orbiter-2B", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 12, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Orbiter-2A", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 14, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Heron-1 TP", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Skylark-1 LE", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 10, ScanSpeedKHzs = 10 });
            ranges.Drones.Add("Da-Vinci", new JammingParameters() { Modulation = Modulation.LCM, BandMHz = 9, ScanSpeedKHzs = 10 });
            return ranges;
        }

        public void YamlSave(CollectionDroneJammingParams ranges, string file)
        {
            try
            {
                //var serializer = new SerializerBuilder().EmitDefaults().Build();
                //var yaml = serializer.Serialize(ranges);


                var serializer = new SerializerBuilder();
                var yaml = serializer.Build().Serialize(ranges);

                using (StreamWriter sw = new StreamWriter(file, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


    }
}
