﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    class AeroscopeTarget
    {
        
        private int _iD;

        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }


        private string _serialNumber = String.Empty;

        public string SerialNumber
        {
            get { return _serialNumber; }
            set
            {
                if (_serialNumber.Equals(value)) return;
                _serialNumber = value;

            }
        }

        private string _type = String.Empty;
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type.Equals(value)) return;
                _type = value;

            }
        }


        private float _uUIDLength = -1;

        public float UUIDLength
        {
            get { return _uUIDLength; }
            set
            {
                if (_uUIDLength.Equals(value)) return;
                _uUIDLength = value;
            }
        }


        private float _uUID = -1;

        public float UUID
        {
            get { return _uUID; }
            set
            {
                if (_uUID.Equals(value)) return;
                _uUID = value;
            }
        }


        private Coord _coordinateHome = new Coord();

        public Coord CoordinateHome
        {
            get { return _coordinateHome; }
            private set
            {
                if (_coordinateHome.Equals(value)) return;
                _coordinateHome = value;

            }
        }

        public ObservableCollection<AeroscopeMark> aeroscopeMarks = new ObservableCollection<AeroscopeMark>();

        
        public AeroscopeTarget()
        {
            //zorkiRMarks.CollectionChanged += ZorkiRMarks_CollectionChanged;
        }

        private void AeroscopeMarks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
           
        }

       


    }
}
