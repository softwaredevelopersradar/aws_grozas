﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class Direction:INotifyPropertyChanged
    {

        private bool _set = false;
        public bool Set
        {
            get { return _set; }
            set
            {
                if (_set.Equals(value)) return;
                _set = value;
             
            }

        }

        private float _address;
        public float Address
        {
            get { return _address; }
            set
            {
                if (_address.Equals(value)) return;
                _address = value;
                UpdateResult();
            }

        }

        private float _courseAngle;
        public float CourseAngle
        {
            get { return _courseAngle; }
            set
            {
                if (_courseAngle.Equals(value)) return;
                _courseAngle = value;
                UpdateResult();
            }

        }

        private float _azimuth;
        public float Azimuth
        {
            get { return _azimuth; }
            set
            {
                if (_azimuth.Equals(value)) return;
                _azimuth = value;
                OnPropertyChanged();
                UpdateResult();
            }

        }

        private float _error;
        public float Error
        {
            get { return _error; }
            set
            {
                if (_error.Equals(value)) return;
                _error = value;
                UpdateResult();
            }

        }

        private float _result;
        public float Result
        {
            get { return _result; }
            set
            {
                if (_result.Equals(value)) return;
                _result = value;
                OnPropertyChanged();
            }

        }

        private void UpdateResult()
        {
            var res = CourseAngle + Azimuth + Error;
            while (res >= 360)
                res -= 360;

            while (res < 0)
                res += 360;
            Result = res;
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }
        #endregion


    }
}
