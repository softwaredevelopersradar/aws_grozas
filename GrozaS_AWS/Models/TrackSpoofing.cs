﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class TrackSpoofing
    {
        private ObservableCollection<Coord> _coordinate = new ObservableCollection<Coord>();
        public ObservableCollection<Coord> Coordinate
        {
            get { return _coordinate; }
            set
            {
                if (_coordinate.Equals(value)) return;
                _coordinate = value;              
            }

        }


        private float _angle;

        public float Angle
        {
            get => _angle;
            set
            {
                if (_angle.Equals(value)) return;
                _angle = value;
              
            }
        }
    }
}
