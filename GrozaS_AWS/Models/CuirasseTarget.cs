﻿using Bearing;
using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Models
{
    public class CuirasseTarget
    {

       

        private short _iD;

        public short ID
        {
            get { return _iD; }
            set
            {
                if (_iD.Equals(value)) return;
                _iD = value;

            }
        }

        private short _iD_Groza;

        public short ID_Groza
        {
            get { return _iD_Groza; }
            set
            {
                if (_iD_Groza.Equals(value)) return;
                _iD_Groza = value;

            }
        }

        public ObservableCollection<CuirasseMark> cuirasseMarks = new ObservableCollection<CuirasseMark>();

        

        public CuirasseTarget()
        {
            cuirasseMarks.CollectionChanged += CuirasseMark_CollectionChanged;
        }

        private void CuirasseMark_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CountDistance();
        }

        private void CountDistance()
        {
            try
            {
                float dist = 0;

                int i = cuirasseMarks.Count - 1;

                if (cuirasseMarks.Count > 1)
                {

                    dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            cuirasseMarks[i].Coordinate.Latitude,
                            cuirasseMarks[i].Coordinate.Longitude,
                            cuirasseMarks[i - 1].Coordinate.Latitude,
                            cuirasseMarks[i - 1].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);


                    while (i > -1)
                    {
                        dist += (float)Math.Round(
                            ClassBearing.f_D_2Points(
                            cuirasseMarks[i - 1].Coordinate.Latitude,
                            cuirasseMarks[i - 1].Coordinate.Longitude,
                            cuirasseMarks[i].Coordinate.Latitude,
                            cuirasseMarks[i].Coordinate.Longitude, 1),
                            MidpointRounding.AwayFromZero);

                        if (dist >= 5000)
                        {
                            for (int j = 0; j < cuirasseMarks.Count - i; j++)
                                cuirasseMarks.Remove(cuirasseMarks[0]);
                            i = -1;
                        }
                        i--;

                    }
                }
            }
            catch { }
        }


    }
}
