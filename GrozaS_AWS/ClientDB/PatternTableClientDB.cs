﻿
using System.Collections.Generic;
using System.Windows;

using GrozaSModelsDBLib;

namespace GrozaS_AWS
{
    public partial class MainWindow: Window
    {
        List<TablePattern> lTablePattern = new List<TablePattern>();

        private void AddPatternRecord(TablePattern tablePattern)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].Add(tablePattern);
            }
            catch
            { }
        }

        private void AddPatternList(List<TablePattern> listTablePattern)
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].AddRangeAsync(listTablePattern);
            }
            catch
            { }
        }

        private void ClearPatternRecord()
        {
            try
            {
                if (mainWindowViewModel.clientDB == null)
                    return;

                mainWindowViewModel.clientDB?.Tables[NameTable.TablePattern].CLearAsync();
            }
            catch
            { }
        }

        private void ChangePatternRecord(TablePattern tablePattern)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                //tablePattern.ImagePath = "";
                mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].ChangeAsync(tablePattern);
            }
                

        }

        private void DeletePatternRecord(TablePattern tablePattern)
        {
            if (mainWindowViewModel.clientDB != null)
                mainWindowViewModel.clientDB.Tables[NameTable.TablePattern].DeleteAsync(tablePattern);
        }


    }
}
