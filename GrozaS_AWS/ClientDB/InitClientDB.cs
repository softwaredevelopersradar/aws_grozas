﻿using GrpcDbClientLib;
using GrozaSModelsDBLib;
using InheritorsEventArgs;
using System;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
       

        //TODO: по номеру арма из настроек брать
        private new string Name { get; set; } = "ARM";

        private void ConnectClientDB()
        {
            if (mainWindowViewModel.clientDB != null)
                DisconnectClientDB();
            try
            {
                //endPoint = mainWindowViewModel.LocalPropertiesVM.DB.IpAddress.ToString() + ":" + mainWindowViewModel.LocalPropertiesVM.DB.Port;
                mainWindowViewModel.clientDB = new ServiceClient(this.Name, mainWindowViewModel.LocalPropertiesVM.DB.IpAddress.ToString(), mainWindowViewModel.LocalPropertiesVM.DB.Port);

                mainWindowViewModel.clientDB.OnConnect += HandlerConnect_ClientDb;
                mainWindowViewModel.clientDB.OnDisconnect += HandlerDisconnect_ClientDb;
                mainWindowViewModel.clientDB.OnErrorDataBase += HandlerError_ClientDb;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable += HandlerUpdate_TableJammer;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += HandlerUpdate_TableFreqRangesRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += HandlerUpdate_TableSectorsRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += HandlerUpdate_TableSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable += HandlerUpdate_TableSuppressSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable += HandlerUpdate_TableSuppressGnss;
                (mainWindowViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;
                (mainWindowViewModel.clientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += HandlerUpdate_TablePattern;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable += HandlerUpdate_TableOwnUAV;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableCuirasseMPoints] as ITableUpdate<TableCuirasseMPoints>).OnUpTable += HandlerUpdate_TableCuirasseMPoints;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscope] as ITableUpdate<TableAeroscope>).OnUpTable += HandlerUpdate_TableAeroscope;
                //(mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteRecord_TableAeroscope;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += HandlerUpdate_TableAeroscopeTrajectory;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableOperatorGun] as ITableUpdate<TableOperatorGun>).OnUpTable += HandlerUpdate_TableOperatorGun;

                mainWindowViewModel.clientDB.Connect();
            }
            catch{ }
        }

      

        private void DisconnectClientDB()
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Disconnect();

                mainWindowViewModel.clientDB.OnConnect -= HandlerConnect_ClientDb;
                mainWindowViewModel.clientDB.OnDisconnect -= HandlerDisconnect_ClientDb;
                mainWindowViewModel.clientDB.OnErrorDataBase -= HandlerError_ClientDb;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable -= HandlerUpdate_TableJammer;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= HandlerUpdate_TableFreqRangesRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable -= HandlerUpdate_TableFreqKnown;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= HandlerUpdate_TableFreqForbidden;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable -= HandlerUpdate_TableSectorsRecon;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable -= HandlerUpdate_TableSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable -= HandlerUpdate_TableSuppressSource;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable -= HandlerUpdate_TableSuppressGnss;
                (mainWindowViewModel.clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= HandlerUpdate_GlobalProperties;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV] as ITableUpdate<TableOwnUAV>).OnUpTable -= HandlerUpdate_TableOwnUAV;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableCuirasseMPoints] as ITableUpdate<TableCuirasseMPoints>).OnUpTable -= HandlerUpdate_TableCuirasseMPoints;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscope] as ITableUpdate<TableAeroscope>).OnUpTable -= HandlerUpdate_TableAeroscope;
                //(mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord -= OnDeleteRecord_TableAeroscope;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableAeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable -= HandlerUpdate_TableAeroscopeTrajectory;
                (mainWindowViewModel.clientDB.Tables[NameTable.TableOperatorGun] as ITableUpdate<TableOperatorGun>).OnUpTable -= HandlerUpdate_TableOperatorGun;

                mainWindowViewModel.clientDB = null;
            }
        }
    }
}