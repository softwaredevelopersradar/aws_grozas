﻿namespace GrozaS_AWS
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows;
    using EMonolithLib.Models.Geo.Coordinates;
    using EscopeControl;
    using EScopeDomain.Enums;
    using EScopeDomain.Models;
    using EScopeProcessor;
    using GrozaSModelsDBLib;
    using GrozaS_AWS.Converter;
    using GrozaS_AWS.Escope;
    using GrozaS_AWS.Models;
    using WPFControlConnection;

    public partial class MainWindow : Window
    {

        #region Generator temp
        private UAVGenerator uavGenerator;
        private void ControlGenerator(bool turnOn)
        {
            if (turnOn)
            {
                var UAVGenerationSettings = new UAVGenerationSettings()
                                                {
                                                    UAVAmount = 5,
                                                    UAVSpeedMaximum = 17,
                                                    ElapsedMillisecondsMaximum = 213,
                                                    TimerIntervalMs = 1000,
                                                    SerialNumberLength = 18,
                                                    UavPointIncrement =
                                                        new UavPointIncrement()
                                                            {
                                                                Altitude = 3,
                                                                Latitude = 3,
                                                                Longitude = 3,
                                                                CoordinatesDenominator = 100,
                                                            },
                                                    StartPosition =
                                                        new EMonolithLib.Models.Geo.Coordinates.GeodesicCoordinates()
                                                            {
                                                                Altitude = 54,
                                                                Latitude = 53.931440,
                                                                Longitude = 27.636372,
                                                            },
                                                };
                uavGenerator = new UAVGenerator(UAVGenerationSettings);
                uavGenerator.OnGenerateScopeResponseFinished += EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;

                uavGenerator.Run();
                return;
            }
            else
            {
                uavGenerator.Stop();
                uavGenerator.OnGenerateScopeResponseFinished -= EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;
                uavGenerator = null;
            }
            
        }

        #endregion


        private EScopeMain escopeMain;

        private ObservableCollection<EscopeTarget> escopeTargets = new ObservableCollection<EscopeTarget>();


        private void EscopeConnection_Click(object sender, RoutedEventArgs e)
        {
            if (mainWindowViewModel.StateConnectionEscope == ConnectionStates.Connected)
            {
                DisconnectEscope();
            }
            else this.ConnectEscope();
        }

        private void ConnectEscope()
        {
            //ControlGenerator(true);

            try
            {
                escopeMain = new EScopeMain();

                var coordinates = this.GetOwnStationCoordinates();
                if (coordinates != null)
                {
                    escopeMain.ReferencePoint = new GeodesicCoordinates2D() { Latitude = coordinates.Latitude, Longitude = coordinates.Longitude };
                }

                escopeMain.EScopeProcessor_OnProcessingFinish += EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;
                escopeMain.EScopeProcessor_OnErrorReceive += EscopeMain_EScopeProcessor_OnErrorReceive;

                try
                {
                    SaveLogEscope("\n" + DateTime.Now.ToString("HH:mm:ss.ff").PadRight(13) + " Connected Escope");
                    LogEscopeHeader();
                }
                catch { }


                mainWindowViewModel.StateConnectionEscope = ConnectionStates.Connected;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                mainWindowViewModel.StateConnectionEscope = ConnectionStates.Disconnected;
            }
        }

        private Coord GetOwnStationCoordinates()
        {
            var ownStation = this.lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own);
            if (ownStation == null)
                return null;
                
            if ((ownStation.Coordinates.Latitude == -1 && ownStation.Coordinates.Longitude == -1) || (ownStation.Coordinates.Latitude == 0 && ownStation.Coordinates.Longitude == 0))
            {
                return null;
            }
            
            return ownStation.Coordinates;
        }

        private void DisconnectEscope()
        {
            //ControlGenerator(false);
            mainWindowViewModel.StateConnectionEscope = ConnectionStates.Disconnected;

            try
            {
                SaveLogEscope("\n" + DateTime.Now.ToString("HH:mm:ss.ff").PadRight(13) + " Disconnected Escope");
            }
            catch { }

            if (escopeMain != null)
            {

                escopeMain.EScopeProcessor_OnProcessingFinish -= EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;
                escopeMain.EScopeProcessor_OnErrorReceive -= EscopeMain_EScopeProcessor_OnErrorReceive;
                try
                {
                    escopeMain.Dispose();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }



                escopeMain = null;


            }
            this.ClearEscope();
        }

        private void EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully(ScopeResponse parseResult)
        {
            try
            {
                if (parseResult.Checksum != 0)
                    return;

                if (parseResult.ScopeParseResult.ProtocolType == ScopeProtocolType.License)
                    return;
                
                var recForMap = this.DecodeMarkEscopeForMap(parseResult);
                var recForTable = this.DecodeEscopeForTable(parseResult);

                if (recForMap == null)
                    return;

                try
                {
                    LogEscopePoint(recForTable);
                }
                catch
                {
                }

                var color = Colorizer.AssignColor(recForTable.SerialNumber);

                recForTable.Color = color;
                this.AddToTableEscope(recForTable);

                if (recForMap.Coordinate.Latitude == 0 || recForMap.Coordinate.Longitude == 0)
                {
                    return;
                }

                var target = new EscopeTarget()
                                 {
                                     SerialNumber = recForMap.SerialNumber,
                                     Type = parseResult.ScopeParseResult.UAVType.ToString(),
                                     Home = recForMap.Home.Clone(),
                                     Coordinate = recForMap.Coordinate.Clone(),
                                     Color = Mapsui.Styles.Color.FromArgb(color.A, color.R, color.G, color.B)
                                 };

                target.Pilot = parseResult.ScopeParseResult.PilotCoordinates != null
                                   ? new Coord(
                                       parseResult.ScopeParseResult.PilotCoordinates.Latitude,
                                       parseResult.ScopeParseResult.PilotCoordinates.Longitude,
                                       0)
                                   : target.Pilot;

                var escopeTarget = escopeTargets.FirstOrDefault(x => x.SerialNumber == recForMap.SerialNumber);
                if (escopeTarget == null)
                {
                    escopeTargets.Add(target);
                }
                else
                {
                    escopeTarget.Coordinate = target.Coordinate.Clone();
                    escopeTarget.Home = target.Home.Clone();
                    escopeTarget.Pilot = target.Pilot.Clone();
                    escopeTarget.Type = target.Type.ToString();
                }

                escopeTargets.First(x => x.SerialNumber == recForMap.SerialNumber).escopeMarks.Add(recForMap);
            }
            catch
            {
            }
        }
        
        private AeroscopeMark DecodeMarkEscopeForMap(ScopeResponse e)
        {
            AeroscopeMark aeroscope = null;

            try
            {
                aeroscope = new AeroscopeMark();

                aeroscope.SerialNumber = e.ScopeParseResult.SerialNumber;

                aeroscope.Home.Latitude = e.ScopeParseResult.HomePointCoordinates.Latitude;
                aeroscope.Home.Longitude = e.ScopeParseResult.HomePointCoordinates.Longitude;
                aeroscope.Home.Altitude = (float)e.ScopeParseResult.HomePointCoordinates.Altitude;

                aeroscope.Coordinate.Longitude = e.ScopeParseResult.UAVCoordinates.Longitude;
                aeroscope.Coordinate.Latitude = e.ScopeParseResult.UAVCoordinates.Latitude;
                aeroscope.Coordinate.Altitude = (float)e.ScopeParseResult.UAVCoordinates.Altitude;

                aeroscope.Loctime = DateTime.Now;
            }
            catch { }

            return aeroscope;
        }

        private EscopeUAVModel DecodeEscopeForTable(ScopeResponse e)
        {
            EscopeUAVModel aeroscope = null;

            try
            {
                aeroscope = new EscopeUAVModel();

                aeroscope.SerialNumber = e.ScopeParseResult.SerialNumber;

                aeroscope.UAVCoordinates.Longitude = e.ScopeParseResult.UAVCoordinates.Longitude;
                aeroscope.UAVCoordinates.Latitude = e.ScopeParseResult.UAVCoordinates.Latitude;
                aeroscope.UAVCoordinates.Altitude = (float)e.ScopeParseResult.UAVCoordinates.Altitude;

                aeroscope.HomePointCoordinates.Latitude = e.ScopeParseResult.HomePointCoordinates.Latitude;
                aeroscope.HomePointCoordinates.Longitude = e.ScopeParseResult.HomePointCoordinates.Longitude;
                aeroscope.HomePointCoordinates.Altitude = (float)e.ScopeParseResult.HomePointCoordinates.Altitude;
                
                aeroscope.PilotCoordinates.Longitude = e.ScopeParseResult.PilotCoordinates.Longitude;
                aeroscope.PilotCoordinates.Latitude = e.ScopeParseResult.PilotCoordinates.Latitude;
                aeroscope.PilotCoordinates.Altitude = (float)e.ScopeParseResult.PilotCoordinates.Altitude;

                aeroscope.Time = DateTime.Now;
                aeroscope.Elevation = (float)e.ScopeParseResult.UAVCoordinates.Altitude;
                aeroscope.Type = e.ScopeParseResult.UAVType.ToString();
                aeroscope.Speed = e.ScopeParseResult.UAVSpeed;


                if (e.PositionMetrics.HasValue)
                {
                    aeroscope.Distance = (float)(e.PositionMetrics.Value.DistanceToBasePoint);
                    aeroscope.Azimuth = e.PositionMetrics.Value.Azimuth;
                }

                aeroscope.IsOwn = lOwnUAV.Select(t => t.SerialNumber)
                    .Contains(e.ScopeParseResult.SerialNumber);
            }
            catch { }

            return aeroscope;
        }

        private void EscopeMain_EScopeProcessor_OnErrorReceive(EScopeDomain.Models.Error error)
        {
            Console.WriteLine("{0} - {1}", error.TimeStamp, error.Description);
        }

        private void AddToTableEscope(EscopeUAVModel record)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (record != null)
                {
                    this.ucEscope.AddSignalUAV(record);
                }
            });
        }


        private void ClearEscope()
        {
            try
            {
                this.escopeTargets.Clear();
                ClearTableEscope();
            }
            catch { }

            UpdateEscopeMapTarget(escopeTargets);
        }

        private void LogEscopePoint(EscopeUAVModel e)
        {
            string text = e.Time.ToString("HH:mm:ss.ff").PadRight(13);
            text += e.SerialNumber.PadRight(19);
            text += e.Type.ToString().PadRight(27);

            text += e.IsOwn? "+".PadRight(5) : String.Empty.PadRight(5);

            text += e.Azimuth.ToString("0.0").PadRight(14);
            text += e.Distance.ToString("0.0").PadRight(14);

            text += e.UAVCoordinates.Latitude.ToString("0.000000").PadRight(11) +
                    e.UAVCoordinates.Longitude.ToString("0.000000").PadRight(11) + e.Elevation.ToString("0.0").PadRight(6) + " || ";


            text += e.PilotCoordinates.Latitude.ToString("0.000000").PadRight(11) +
                    e.PilotCoordinates.Longitude.ToString("0.000000").PadRight(11) + " || ";
            text += e.HomePointCoordinates.Latitude.ToString("0.000000").PadRight(11) +
                    e.HomePointCoordinates.Longitude.ToString("0.000000").PadRight(11) + " || ";

            SaveLogEscope(text);
        }

        private void LogEscopeHeader()
        {
            string text = "\n" + "TIME".PadRight(13);
            text += "SERIAL No.".PadRight(19);
            text += "TYPE".PadRight(27);

            text += "OWN".PadRight(5);

            text += "AZIMUTH [°]".PadRight(14);
            text += "RANGE [m]".PadRight(14);

            text += "UAV".PadRight(28) + " || ";
            text += "PILOT".PadRight(22) + " || ";
            text += "HOME".PadRight(22) + " || ";
            text += "\n";

            SaveLogEscope(text);
        }


        private void SaveLogEscope(string strLog)
        {
            var PathToLog = mainWindowViewModel.LocalPropertiesVM.General.FolderRI_UAV;
            if (Directory.Exists(PathToLog))
            {
                string actualDirectory = PathToLog + "\\" + DateTime.Now.ToString("yyyy-MM-dd");

                if (!Directory.Exists(actualDirectory))
                    Directory.CreateDirectory(actualDirectory);

                string filename = actualDirectory + "\\escope" + ".txt";

                using (StreamWriter w = new StreamWriter(filename, true))
                {
                    w.WriteLine(strLog);
                }

            }
        }

        //#region Timer

        //private void InitializeTimerEscopeCheck()
        //{
        //    tmrEscopeCheck.Elapsed += TickTimerEscopeCheck;
        //    tmrEscopeCheck.Interval = 1000;

        //    tmrEscopeCheck.AutoReset = true;
        //    tmrEscopeCheck.Enabled = true;
        //}

        //private void TickTimerEscopeCheck(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    UpdateEscopeMapTarget(escopeTargets);

        //}
        //#endregion

        //private void DestructorTimerEscopeCheck()
        //{
        //    tmrEscopeCheck.Elapsed -= TickTimerAeroscopeCheck;
        //    tmrEscopeCheck.Enabled = false;
        //}



        //private Dictionary<string, int> escopeSerialNumDictionary;

        //private int escopeSerialNumCounter = 0;

        //private int escopeTrackCounter = 0;

        //private void EscopeConnection_Click(object sender, RoutedEventArgs e)
        //{
        //    if (mainWindowViewModel.StateConnectionEscope == ConnectionStates.Connected)
        //    {
        //        DisconnectEscope();
        //    }
        //    else this.ConnectEscope();
        //}

        //private void ConnectEscope()
        //{
        //   // ControlGenerator(true);

        //    escopeSerialNumDictionary = new Dictionary<string, int>();

        //    try
        //    {
        //        escopeMain = new EScopeMain();
        //        escopeMain.EScopeProcessor_OnProcessingFinish += EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;
        //        escopeMain.EScopeProcessor_OnErrorReceive += EscopeMain_EScopeProcessor_OnErrorReceive;

        //        mainWindowViewModel.StateConnectionEscope = ConnectionStates.Connected;
        //    }
        //    catch(Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        mainWindowViewModel.StateConnectionEscope = ConnectionStates.Disconnected;
        //    }



        //}

        //private void DisconnectEscope()
        //{
        //   // ControlGenerator(false);
        //    mainWindowViewModel.StateConnectionEscope = ConnectionStates.Disconnected;
        //    if (escopeMain != null)
        //    {

        //        escopeMain.EScopeProcessor_OnProcessingFinish -= EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully;
        //        escopeMain.EScopeProcessor_OnErrorReceive -= EscopeMain_EScopeProcessor_OnErrorReceive;
        //        try
        //        {
        //            escopeMain.Dispose();
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message);
        //        }



        //        escopeMain = null;


        //    }
        //    ClearAeroscope();
        //}

        //private void EscopeMain_EScopeProcessor_OnParseFinishedSuccessfully(ScopeResponse parseResult)
        //{
        //    try
        //    {
        //        AeroscopeMark mark = DecodeMarkEscope(parseResult);

        //        if (mark == null)
        //            return;

        //        var target = new AeroscopeTarget
        //                                     {
        //                                         ID = escopeSerialNumCounter,
        //                                         SerialNumber = mark.SerialNumber,
        //                                         Type = parseResult.ScopeParseResult.UAVType.ToString(),
        //                                         CoordinateHome =
        //                                             {
        //                                                 Latitude = mark.Home.Latitude, Longitude = mark.Home.Longitude
        //                                             }
        //                                     };

        //        if (aeroscopeTargets.Where(x => x.SerialNumber == mark.SerialNumber).ToList().Count == 0)
        //        {
        //            if (!this.escopeSerialNumDictionary.Keys.Contains(mark.SerialNumber))
        //            {
        //                this.escopeSerialNumCounter++;
        //                this.escopeSerialNumDictionary.Add(mark.SerialNumber, escopeSerialNumCounter);
        //            }

        //            aeroscopeTargets.Add(target); 
        //        }

        //        aeroscopeTargets.First(x => x.SerialNumber == mark.SerialNumber).aeroscopeMarks.Add(mark);
        //        this.lAeroscope = ToTableAeroscope(aeroscopeTargets);
        //        UpdateTableAeroscope();
        //        lATrajectory.Add(ToTableAeroscopeTrack(mark));
        //        UpdateTableAeroscopeTrajectory();
        //    }
        //    catch
        //    {
        //    }
        //}


        //// private List<TableAeroscope> ToTableAeroscope(IEnumerable<AeroscopeTarget> list)
        ////{
        ////    return list.Select(
        ////        t => new TableAeroscope()
        ////                 {
        ////                     Id = t.ID,
        ////                     SerialNumber = t.SerialNumber,
        ////                     UUIDLength = 0,
        ////                     UUID = string.Empty,
        ////                     HomeLatitude = t.CoordinateHome.Latitude,
        ////                     HomeLongitude = t.CoordinateHome.Longitude,
        ////                     Type = t.Type,
        ////                     IsActive = true
        ////                 }).ToList();

        ////}

        //// private List<TableAeroscopeTrajectory> ToTableAeroscopeTrack(IEnumerable<AeroscopeMark> list)
        //// {
        ////     return list.Select(
        ////         t => new TableAeroscopeTrajectory()
        ////                  {
        ////                      Id = t.ID,
        ////                      SerialNumber = t.SerialNumber, Coordinates = t.Coordinate.Clone(), Num = t.ID, Time = t.Loctime,
        ////                      Elevation = t.Coordinate.Altitude

        ////         }).ToList();

        //// }

        ////private TableAeroscopeTrajectory ToTableAeroscopeTrack(AeroscopeMark t)
        ////{
        ////    return new TableAeroscopeTrajectory()
        ////                 {
        ////                     Id = t.ID,
        ////                     SerialNumber = t.SerialNumber,
        ////                     Coordinates = t.Coordinate.Clone(),
        ////                     Num = t.ID,
        ////                     Time = t.Loctime, Elevation = t.Coordinate.Altitude

        ////                 };

        ////}


        //private AeroscopeMark DecodeMarkEscope(ScopeResponse e)
        //{
        //    AeroscopeMark aeroscope = null;

        //    try
        //    {
        //        aeroscope = new AeroscopeMark();

        //        if (this.escopeSerialNumDictionary.Keys.Contains(e.ScopeParseResult.SerialNumber))
        //        {
        //            aeroscope.ID = this.escopeSerialNumDictionary[e.ScopeParseResult.SerialNumber];
        //        }
        //        else
        //        {
        //            escopeTrackCounter++;
        //            aeroscope.ID = this.escopeTrackCounter;
        //            this.escopeSerialNumDictionary.Add(e.ScopeParseResult.SerialNumber, escopeTrackCounter);
        //        }

        //        aeroscope.SerialNumber = e.ScopeParseResult.SerialNumber;

        //        aeroscope.Home.Latitude = e.ScopeParseResult.HomePointCoordinates.Latitude;
        //        aeroscope.Home.Longitude = e.ScopeParseResult.HomePointCoordinates.Longitude;
        //        aeroscope.Home.Altitude = (float)e.ScopeParseResult.HomePointCoordinates.Altitude;

        //        aeroscope.Coordinate.Longitude = e.ScopeParseResult.UAVCoordinates.Longitude;
        //        aeroscope.Coordinate.Latitude = e.ScopeParseResult.UAVCoordinates.Latitude;
        //        aeroscope.Coordinate.Altitude = (float)e.ScopeParseResult.UAVCoordinates.Altitude;

        //        aeroscope.Loctime = DateTime.Now;
        //    }
        //    catch { }

        //    return aeroscope;
        //}

        //private void EscopeMain_EScopeProcessor_OnErrorReceive(EScopeDomain.Models.Error error)
        //{
        //    Console.WriteLine("{0} - {1}", error.TimeStamp, error.Description);
        //}

        //private void UpdateTableAeroscope()
        //{
        //    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
        //        {
        //            if (lAeroscope.Count == 0)
        //            {
        //                ucAeroscope.UpdateAeroscope(lAeroscope);
        //            }
        //            else
        //            {
        //                ucAeroscope.AddAeroscope(lAeroscope);
        //            }
        //        });
        //}



        //private void UpdateTableAeroscopeTrajectory()
        //{
        //    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
        //    {
        //        if (lATrajectory.Count != 0)
        //        {
        //            ucAeroscope.AddATrajectory(lATrajectory);
        //        }
        //    });

        //}

        private void ClearTableEscope()
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    ucEscope.UpdateSignalsUAV(Array.Empty<EscopeUAVModel>().ToList());
                });
        }

        private void DeleteEscopeTarget(string serialNumber)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    this.ucEscope.DeleteAeroscope(serialNumber);

                    DeleteEscopeMapTarget(serialNumber);
                });
        }

        private void DeleteEscopeMapTarget(string serial)
        {
            try
            {
                this.escopeTargets.Remove(this.escopeTargets.FirstOrDefault(x => x.SerialNumber == serial));
            }
            catch { }

            UpdateEscopeMapTarget(escopeTargets);
        }
    }
}
