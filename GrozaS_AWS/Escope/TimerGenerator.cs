﻿using System.Timers;

namespace GrozaS_AWS.Escope
{
    public abstract class TimerGenerator
    {
        protected readonly Timer _timer;

        public TimerGenerator(int timerInterval)
        {
            _timer = new Timer()
                         {
                             AutoReset = true,
                             Enabled = false,
                             Interval = timerInterval,
                         };
            _timer.Elapsed += Timer_Elapsed;
        }

        protected abstract void Timer_Elapsed(object sender, ElapsedEventArgs e);

        public virtual void Run()
        {
            _timer.Start();
        }

        public virtual void Stop()
        {
            _timer.Stop();
        }
    }
}
