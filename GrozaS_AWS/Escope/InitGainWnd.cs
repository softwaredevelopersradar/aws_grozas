﻿
using System.Windows;


namespace GrozaS_AWS
{
    using EScopeDomain.Models.Network;

    public partial class MainWindow : Window
    {
        private GrozaS_AWS.GainWindowEscope gainWindowEscope;

        private void InitGainWndEscope()
        {
            gainWindowEscope = new GainWindowEscope();
            this.gainWindowEscope.OnUpdateGainEscope += GainWindow_OnUpdateGainEscope;
            //gainWindow.OnStartRebooting += GainWindow_OnStartRebooting;
            //gainWindow.OnUpdateGainUSRP += GainWindow_OnUpdateGainUSRP;
        }

        private void GainWindow_OnUpdateGainEscope(object sender, ReceiverCommand e)
        {
            try
            {
                if (escopeMain != null)
                    escopeMain.UploadGain(e);
            }
            catch
            {
            }
        }

        private void GainWndEscopeButton_Click(object sender, RoutedEventArgs e)
        {
            if (gainWindowEscope.IsVisible)
                gainWindowEscope.Hide();
            else
                gainWindowEscope.Show();
        }

        //public void GainWindow_SetLanguage(DllGrozaZProperties.Models.Languages language)
        //{
        //    //if (gainWindow == null)
        //    //    return;
        //    //gainWindow.Title = SWindowNames.nameGainWnd;
        //    ////gainWindow.Language = (ELanguages)basicProperties.Local.General.Language;
        //}

        //private void SendEscopeSettings(byte gain24, byte gain58, short threshold1ch24, short threshold1ch58, short threshold2ch24, short threshold2ch58)
        //{
        //    try
        //    {
        //        var settings = new ReceiverCommand()
        //                        {
        //                            ChannelGain2P4 = gain24,
        //                            ChannelGain5P8 = gain58,
        //                            Threshold1Subchannel2P4 = threshold1ch24,
        //                            Threshold1Subchannel5P8 = threshold1ch58,
        //                            Threshold2Subchannel2P4 = threshold2ch24,
        //                            Threshold2Subchannel5P8 = threshold2ch58
        //                        };

        //    }
        //    catch
        //    {
        //    }

        //}
    }
} 