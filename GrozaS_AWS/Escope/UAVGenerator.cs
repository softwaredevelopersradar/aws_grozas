﻿using System;
using System.Linq;
using EMonolithLib.Models.Geo.Coordinates;
using EScopeDomain.Models;
using System.Timers;

namespace GrozaS_AWS.Escope
{
    public class UAVGenerator : TimerGenerator
    {
        const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        private readonly UAVGenerationSettings _settings;
        private readonly ScopeResponse[] _uavs;
        private readonly int[] _uavsConstOffset;
        private readonly string _serialPrefix = "GEN_";
        private Random rand = new Random();
        public delegate void GeneratedScopeResponseHandler(ScopeResponse scopeResponse);

        public event GeneratedScopeResponseHandler OnGenerateScopeResponseFinished;

        public UAVGenerator(UAVGenerationSettings generationSettings)
            : base(generationSettings.TimerIntervalMs)
        {
            _settings = generationSettings;
            _uavsConstOffset = new int[_settings.UAVAmount];
            _uavs = new ScopeResponse[_settings.UAVAmount];
            GenerateUAVs();
        }

        protected override void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < _uavs.Length; i++)
            {
                _uavs[i] = new ScopeResponse()
                {
                    ElapsedMilliseconds = string.Format("{0} ms", rand.Next(_settings.ElapsedMillisecondsMaximum)),
                    ScopeParseResult = new ScopeParseResult()
                    {
                        SerialNumber = _uavs[i].ScopeParseResult.SerialNumber,
                        InputBuffer = null,
                        PacketLength = 88,
                        ProtocolType = EScopeDomain.Enums.ScopeProtocolType.One,
                        UAVSpeed = GenerateUAVSpeed(),
                        UAVType = EScopeDomain.Enums.UAVType.DJI_Mavic_2_Enterprise,
                        PilotCoordinates = new EMonolithLib.Models.Geo.Coordinates.GeodesicCoordinates()
                        {
                            Altitude = 0,
                            Latitude = 53.932410,
                            Longitude = 27.636692,
                        },
                        HomePointCoordinates = new EMonolithLib.Models.Geo.Coordinates.GeodesicCoordinates()
                        {
                            Altitude = 0,
                            Latitude = 53.932509,
                            Longitude = 27.635912,
                        },
                        UAVCoordinates = new GeodesicCoordinates()
                        {
                            Altitude = _uavs[i].ScopeParseResult.UAVCoordinates.Altitude + rand.Next(-_settings.UavPointIncrement.Altitude, _settings.UavPointIncrement.Altitude + 1),
                            Latitude = _uavs[i].ScopeParseResult.UAVCoordinates.Latitude + _uavsConstOffset[i] + rand.Next(-_settings.UavPointIncrement.Latitude, _settings.UavPointIncrement.Latitude + 1) / _settings.UavPointIncrement.CoordinatesDenominator,
                            Longitude = _uavs[i].ScopeParseResult.UAVCoordinates.Longitude + _uavsConstOffset[i] + rand.Next(-_settings.UavPointIncrement.Longitude, _settings.UavPointIncrement.Longitude + 1) / _settings.UavPointIncrement.CoordinatesDenominator,
                        },
                    }
                };

                OnGenerateScopeResponseFinished?.Invoke(_uavs[i]);
            }
        }

        private int GenerateUAVSpeed()
        {
            return rand.Next(_settings.UAVSpeedMaximum);
        }

        private void GenerateUAVs()
        {
            for (int i = 0; i < _settings.UAVAmount; i++)
            {
                _uavs[i] = new ScopeResponse()
                {
                    ElapsedMilliseconds = string.Format("{0} ms", rand.Next(_settings.ElapsedMillisecondsMaximum)),
                    ScopeParseResult = new ScopeParseResult()
                    {
                        SerialNumber = GenerateSerialNumber(),
                        InputBuffer = null,
                        PacketLength = 88,
                        ProtocolType = EScopeDomain.Enums.ScopeProtocolType.One,
                        UAVSpeed = GenerateUAVSpeed(),
                        UAVType = EScopeDomain.Enums.UAVType.DJI_Mavic_2_Enterprise,
                        PilotCoordinates = new EMonolithLib.Models.Geo.Coordinates.GeodesicCoordinates()
                        {
                            Altitude = 0,
                            Latitude = 53.932410,
                            Longitude = 27.636692,
                        },
                        HomePointCoordinates = new EMonolithLib.Models.Geo.Coordinates.GeodesicCoordinates()
                        {
                            Altitude = 0,
                            Latitude = 53.932509,
                            Longitude = 27.635912,
                        },
                        UAVCoordinates = _settings.StartPosition,
                    }
                };
            }
        }

        private void GenerateUAVOffsets()
        {
            var shinyNumber = (int)Math.Sqrt(_settings.UavPointIncrement.Altitude * _settings.UavPointIncrement.Latitude * _settings.UavPointIncrement.Longitude) / 100;

            for (int i = 0; i < _uavs.Length; i++)
            {
                _uavsConstOffset[i] = rand.Next(-shinyNumber, shinyNumber);
            }
        }

        private string GenerateSerialNumber()
        {
            return
                new string(
                    Enumerable.Repeat(Chars, _settings.SerialNumberLength - _serialPrefix.Length)
                                .Select(s => s[rand.Next(s.Length)])
                                .ToArray()
                );
        }
    }
}
