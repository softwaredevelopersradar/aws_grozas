﻿




using EscopeControl;

namespace GrozaS_AWS
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using GrozaS_AWS.Models;

    public class SaveTrajectory
    {
        
        /// <summary>
        /// Сохранить траектории Aeroscope
        /// </summary>
        /// <param name="listAeroscope"> Источники </param>
        /// <param name="listATrajectory"> Траектории </param>
        public static void SaveTrajectoryAeroscope(ObservableCollection<EscopeTarget> listATrajectory)
        {
            try
            {
                string path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Trajectories\\Escope\\");
                string date = DateTime.Now.ToShortDateString();
                path = path + date;
                if (!Directory.Exists(path))
                {
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                }

                var groups = listATrajectory.GroupBy(t => t.SerialNumber).Select(t=>t.Key);
                foreach (var group in groups)
                {
                    var drone = listATrajectory.First(t => t.SerialNumber == group);
                    var records = drone.escopeMarks.OrderBy(t=>t.Loctime).ToList();
                    // Создать файл
                    object filename = @path + "\\" + drone.Type + "_" + drone.SerialNumber.ToString() + "_" + DateTime.Now.Hour.ToString() + "." + DateTime.Now.Minute.ToString() + ".txt";

                    using (StreamWriter w = new StreamWriter(filename.ToString(), true))
                    {
                        for (int j = 0; j < records.Count; j++)
                        {
                            w.WriteLine(String.Format("{0, -20} ",
                                records[j].Loctime.Hour.ToString("00") + ":" + records[j].Loctime.Minute.ToString("00") + ":" + records[j].Loctime.Second.ToString("00") + "." + records[j].Loctime.Millisecond.ToString("000") + " " +
                                records[j].Coordinate.Latitude.ToString("0.000000") + " " +
                                records[j].Coordinate.Longitude.ToString("0.000000") + " " +
                                records[j].Coordinate.Altitude.ToString("0.0")));
                        }
                    }
                }
                

                //for (int i = 0; i < listAeroscope.Count; i++)
                //{
                //    if (listAeroscope[i].IsActive)
                //    {
                //        List<TableAeroscopeTrajectory> lATrajectory = listATrajectory.Where(x => x.SerialNumber == listAeroscope[i].SerialNumber).ToList();
                //        if (lATrajectory.Count > 0)
                //        {
                            
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        

        
    }
}
