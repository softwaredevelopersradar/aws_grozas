﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace GrozaS_AWS
{
    using EScopeDomain.Models.Network;

    /// <summary>
    /// Interaction logic for GainWindow.xaml
    /// </summary>
    public partial class GainWindowEscope : Window, INotifyPropertyChanged
    {
        public GainWindowEscope()
        {
            InitializeComponent();

            DataContext = this;
        }

        

        public event EventHandler<ReceiverCommand> OnUpdateGainEscope;


        private byte _gain24 = 60;
        public byte Gain24
        {
            get =>_gain24;
            set
            {
                _gain24 = value;
                OnPropertyChanged();
            }
        }

        private byte _gain58 = 60;
        public byte Gain58
        {
            get => _gain58;
            set
            {
                _gain58 = value;
                OnPropertyChanged();
            }
        }

        private short _threshold1Ch24 = 600;
        public short Threshold1Ch24
        {
            get => _threshold1Ch24;
            set
            {
                _threshold1Ch24 = value;
                OnPropertyChanged();
            }
        }

        private short _threshold1Ch58 = 600;
        public short Threshold1Ch58
        {
            get => _threshold1Ch58;
            set
            {
                _threshold1Ch58 = value;
                OnPropertyChanged();
            }
        }

        private short _threshold2Ch24 = 600;
        public short Threshold2Ch24
        {
            get => _threshold2Ch24;
            set
            {
                _threshold2Ch24 = value;
                OnPropertyChanged();
            }
        }

        private short _threshold2Ch58 = 600;
        public short Threshold2Ch58
        {
            get => _threshold2Ch58;
            set
            {
                _threshold2Ch58 = value;
                OnPropertyChanged();
            }
        }


        //public bool IsRebooting
        //{
        //    get { return (bool)GetValue(IsRebootingProperty); }
        //    set { SetValue(IsRebootingProperty, value); }
        //}

        //public static readonly DependencyProperty IsRebootingProperty =
        //    DependencyProperty.Register("IsRebooting", typeof(bool),
        //    typeof(GainWindow), new FrameworkPropertyMetadata(false));

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    //ifIsRebooting = true;
        //    //pbStatusReboot.Visibility = Visibility.Visible; 
        //    if (!IsRebooting)
        //    {
        //        ChangedRebootingStatus(true);
        //        OnStartRebooting?.Invoke(this, null);
        //    }
        //}

        private void Slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            OnUpdateGainEscope?.Invoke(this, new ReceiverCommand()
                                                 {
                                                     ChannelGain2P4 = Gain24,
                                                     ChannelGain5P8 = Gain58,
                                                     Threshold1Subchannel2P4 = Threshold1Ch24,
                                                     Threshold1Subchannel5P8 = Threshold1Ch58,
                                                     Threshold2Subchannel2P4 = Threshold2Ch24,
                                                     Threshold2Subchannel5P8 = Threshold2Ch58
                                                 });
        }

        public void SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                            UriKind.Relative);
                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.RU.xaml",
                            UriKind.Relative);
                        break;
                   
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                             UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }
        //private void Slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        //{
        //    OnUpdateGainUSRP?.Invoke(this, null);
        //}
    }
}
