﻿namespace GrozaS_AWS.Escope
{
    using EMonolithLib.Models.Geo.Coordinates;

    public class UAVGenerationSettings
    {
        public int UAVAmount { get; set; }

        public int TimerIntervalMs { get; set; }

        public int ElapsedMillisecondsMaximum { get; set; }

        public int UAVSpeedMaximum { get; set; }

        public int SerialNumberLength { get; set; }

        public GeodesicCoordinates StartPosition { get; set; }

        public UavPointIncrement UavPointIncrement { get; set; }
    }

    public struct UavPointIncrement
    {
        public int Altitude { get; set; }

        public int Latitude { get; set; }

        public int Longitude { get; set; }

        public double CoordinatesDenominator { get; set; }
    }
}
