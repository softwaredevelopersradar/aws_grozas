﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;
using Rodnik;
using System.Collections.ObjectModel;
using GrozaS_AWS.Models;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public UDPRodnik udpRodnik;

        ObservableCollection<RodnikTarget> rodnikTargets = new ObservableCollection<RodnikTarget>();

        private void ConnectRodnik()
        {
            if (udpRodnik != null)
                DisconnectRodnik();

            udpRodnik = new UDPRodnik();

            udpRodnik.OnConnectPort += ConnectPortRodnik;
            udpRodnik.OnDisconnectPort += DisconnectPortRodnik;

            udpRodnik.OnReceiveMark += UdpRodnik_OnReceiveMark;

            udpRodnik.Connect(mainWindowViewModel.LocalPropertiesVM.Rodnik.IpAddressLocal, mainWindowViewModel.LocalPropertiesVM.Rodnik.PortLocal,
                                   mainWindowViewModel.LocalPropertiesVM.Rodnik.IpAddressRemoute, mainWindowViewModel.LocalPropertiesVM.Rodnik.PortRemoute);


            ClearTest();

        }

        

        private void DisconnectRodnik()
        {

            if (udpRodnik != null)
            {
                udpRodnik.Disconnect();

                udpRodnik.OnConnectPort -= ConnectPortRodnik;
                udpRodnik.OnDisconnectPort -= DisconnectPortRodnik;


                udpRodnik = null;

                
            }

        }

        private void ClearTest()
        {
            try 
            {
                rodnikTargets.Clear();
                UpdateRodnikTargetsMap(rodnikTargets);
            }
            catch { }
        }

    }
}

