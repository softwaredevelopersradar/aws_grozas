﻿using CtoGsProtocol;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using OEM;
using RESCuirasseMControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TableEvents;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private void UpdateTableRESCuirasseM()
        {
            // TEST Update list
            List<RESCuirasseMModel> listRESCuirasseMModels = new List<RESCuirasseMModel>()
            {
                new RESCuirasseMModel
                {
                    Id = 2,
                    Id_Groza = 22,
                    Frequency = 435.83F,
                    Band = 4.6F,
                    Type = 4,
                    Coordinates = new CoordCuirasseM() { Altitude = 400, Latitude = 55.65432444, Longitude = 29.45245245 }
                }
            };



            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucRESCuirasseM.ListRESCuirasseMModel = listRESCuirasseMModels;
                }), DispatcherPriority.Background);
            }
            catch
            { }
            
           

        }
        private void CuirasseConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (udpCuirasse != null && udpCuirasse.IsConnected)
                    DisconnectCuirasse();
                else
                    ConnectCuirasse();
            }

            catch { }
        }


        private void DeleteCuirasseTarget(short ID)
        {
            try
            {
                cuirasseTargets.Remove(cuirasseTargets.Where(x => x.ID == ID).FirstOrDefault());
            }
            catch { }
        }

        private void ClearCuirasseTarget()
        {
            try
            {
                cuirasseTargets.Clear();
            }
            catch { }
        }

        private void UpdateTableCuirasse()
        {
            List<RESCuirasseMModel> listCuirasseModels = new List<RESCuirasseMModel>();


            foreach (var cur in cuirasseTargets)
            {
                listCuirasseModels.Add(new RESCuirasseMModel()
                {
                    Id = cur.ID,
                    Id_Groza = cur.ID_Groza,
                    Frequency = cur.cuirasseMarks.Last().Frequency,
                    Band = cur.cuirasseMarks.Last().Band,
                    Type = (byte)cur.cuirasseMarks.Last().Type,
                    Coordinates = new CoordCuirasseM()
                    {
                        Latitude = cur.cuirasseMarks.Last().Coordinate.Latitude,
                        Longitude = cur.cuirasseMarks.Last().Coordinate.Longitude,
                        Altitude = cur.cuirasseMarks.Last().Coordinate.Altitude
                    }

                }); ; ; ;
            }

            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    ucRESCuirasseM.ListRESCuirasseMModel = listCuirasseModels;
                }), DispatcherPriority.Background);
            }
            catch
            { }



        }

        private void SendRequestCuirasse(Drone drone)
        {
            try 
            {
                udpCuirasse.SendDronetoC(drone);
            }
            catch { }
        }

        private void ucRESCuirasseM_OnDeleteRecord(object sender, RESCuirasseMControl.RESCuirasseMModel e)
        {
            DeleteCuirasseTarget((short)e.Id);
            UpdateTableCuirasse();

            UpdateCuirasseTargetsMap(cuirasseTargets);
        }

        private void ucRESCuirasseM_OnClearRecords(object sender, EventArgs e)
        {
            ClearCuirasseTarget();

            UpdateTableCuirasse();

            UpdateCuirasseTargetsMap(cuirasseTargets);
        }

        private void ucRESCuirasseM_OnDoubleClick(object sender, RESCuirasseMControl.RESCuirasseMModel e)
        {
            Coord coordCenter = cuirasseTargets.Where(x => x.ID == e.Id).FirstOrDefault().cuirasseMarks.Last().Coordinate;

            mainWindowViewModel.LocationCenter = coordCenter;
        }

        private void ucCuirasseMPoints_OnCentering(object sender, TableEvent e)
        {
            Coord coordCenter = (e.Record as TableCuirasseMPoints).Coordinates;

            mainWindowViewModel.LocationCenter = coordCenter;
        }
        

        private void ucCuirasseMPoints_OnUpdateCuirasseMPoints(object sender, EventArgs e)
        {
            SendCoordToCuirasse();
        }

        private void SendCoordToCuirasse()
        {
            try
            {
                TypeAndCoords[] _coordsOwn = new TypeAndCoords[1];
                Coord CoordOwn = new Coord();
                CoordOwn = lJammerStation.FirstOrDefault(x => x.Role == StationRole.Own).Coordinates;

                _coordsOwn[0] = new TypeAndCoords();

                _coordsOwn[0].coords.Latitude = (double)CoordOwn.Latitude;
                _coordsOwn[0].coords.Longitude = (double)CoordOwn.Longitude;
                _coordsOwn[0].coords.Altitude = (float)CoordOwn.Altitude;

                udpCuirasse.SendTypeAndCoordsMessage(_coordsOwn);
            }
            catch
            {

            }
        }
    }
}
