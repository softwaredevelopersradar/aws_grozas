﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;
using System.Collections.ObjectModel;
using GrozaS_AWS.Models;
using CtoGsProtocol;
using GrozaSModelsDBLib;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        CtoGsUDPClient udpCuirasse = new CtoGsUDPClient();

        ObservableCollection<CuirasseTarget> cuirasseTargets = new ObservableCollection<CuirasseTarget>();

        private void ConnectCuirasse()
        {
            if (udpCuirasse != null)
                DisconnectCuirasse();

            udpCuirasse = new CtoGsUDPClient();

            udpCuirasse.OnConnected += UdpCuirasse_OnConnected;
            udpCuirasse.GetCuirasseMessage += UdpCuirasse_GetCuirasseMessage;
            udpCuirasse.GetDeleteMessage += UdpCuirasse_GetDeleteMessage;
            udpCuirasse.GetTypeAndCoordsMessage += UdpCuirasse_GetTypeAndCoordsMessage;

            udpCuirasse.ConnectToUDP(mainWindowViewModel.LocalPropertiesVM.Cuirasse_M.IpAddressLocal,
                                     mainWindowViewModel.LocalPropertiesVM.Cuirasse_M.PortLocal,
                                     mainWindowViewModel.LocalPropertiesVM.Cuirasse_M.IpAddressRemoute,
                                     mainWindowViewModel.LocalPropertiesVM.Cuirasse_M.PortRemoute);

            

        }

        private void UdpCuirasse_GetTypeAndCoordsMessage(TypeAndCoordsMessage answer)
        {
            try
            {
                if (answer != null)
                {
                    OnClearRecords(this, NameTable.TableCuirasseMPoints);
                    List<TableCuirasseMPoints> cuirassePoints = new List<TableCuirasseMPoints>();

                    if (answer.typeAndCoords != null && answer.typeAndCoords.Length > 0)
                    {
                        for (int i = 0; i < answer.typeAndCoords.Length; i++)
                        {
                            cuirassePoints.Add(new TableCuirasseMPoints()
                            {
                                Id = (int)answer.typeAndCoords[i].objectType,
                                Coordinates = new Coord()
                                {
                                    Latitude = answer.typeAndCoords[i].coords.Latitude,
                                    Longitude = answer.typeAndCoords[i].coords.Longitude,
                                    Altitude = answer.typeAndCoords[i].coords.Altitude
                                },
                                Note = ""
                            });
                        }

                        UpdateCuirassePointsDB(cuirassePoints);
                    }
                }
            }
            catch
            {
            }
        }


        private void UpdateCuirassePointsDB(List<TableCuirasseMPoints> cuirassePoints)
        {
            if (mainWindowViewModel.clientDB != null)
                mainWindowViewModel.clientDB.Tables[NameTable.TableCuirasseMPoints].AddRange(cuirassePoints);

        }

        private void UdpCuirasse_GetDeleteMessage(DeleteMessage answer)
        {
            try
            {
                if (answer != null)
                {
                    if (answer.ID == 0)
                        ClearCuirasseTarget();

                    else
                        DeleteCuirasseTarget((short)answer.ID);

                    UpdateCuirasseTargetsMap(cuirasseTargets);
                    UpdateTableCuirasse();
                }
            }
            catch
            {
            }
        }

        private void UdpCuirasse_GetCuirasseMessage(CuirasseMessage answer)
        {
            try
            {
                if (answer != null)
                {
                    
                    if (cuirasseTargets.Where(x => x.ID == answer.cUAV.ID).ToList().Count == 0)
                        cuirasseTargets.Add(new CuirasseTarget { ID = (short)answer.cUAV.ID, ID_Groza = (short)answer.cUAV.UAVDrone.ID});

                    cuirasseTargets.Where(x => x.ID == answer.cUAV.ID).FirstOrDefault().cuirasseMarks.Add(new CuirasseMark() 
                                                                            {Frequency = answer.cUAV.UAVDrone.FrequencyMHz,
                                                                             Band = answer.cUAV.UAVDrone.BandwidthMHz,
                                                                             Type = (ClassCuirasse)answer.cUAV.UAVDrone.Type,
                                                                             ID = (short)answer.cUAV.UAVDrone.ID,
                                                                             
                                                                             

                        Coordinate = new GrozaSModelsDBLib.Coord()
                        {
                            Latitude = answer.cUAV.UAVCoords.Latitude,
                            Longitude = answer.cUAV.UAVCoords.Longitude,
                            Altitude = answer.cUAV.UAVCoords.Altitude
                        },

                    });

                    UpdateCuirasseTargetsMap(cuirasseTargets);

                    
                    UpdateTableCuirasse();
                }


            }
            catch {    
            }
        }

        private void UdpCuirasse_OnConnected(bool onConnected)
        {
            mainWindowViewModel.StateConnectionCuirasse = onConnected ? 
                                WPFControlConnection.ConnectionStates.Connected : WPFControlConnection.ConnectionStates.Disconnected;

            SendCoordToCuirasse();

        }

        private void DisconnectCuirasse()
        {

            if (udpCuirasse != null && udpCuirasse.IsConnected)
            {
                udpCuirasse.DisconnectFromServer();

                udpCuirasse.OnConnected -= UdpCuirasse_OnConnected;
                udpCuirasse.GetCuirasseMessage -= UdpCuirasse_GetCuirasseMessage;
                udpCuirasse.GetDeleteMessage -= UdpCuirasse_GetDeleteMessage;
                udpCuirasse.GetTypeAndCoordsMessage -= UdpCuirasse_GetTypeAndCoordsMessage;

                udpCuirasse = null;               
            }
        }

    }
}


