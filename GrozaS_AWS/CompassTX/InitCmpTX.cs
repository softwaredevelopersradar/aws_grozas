﻿using DLL_Compass;
using System.Windows;
using TDF;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public Compass compassTX = new Compass();


        private void ConnectCmpTX()
        {
            if (compassTX != null)
                DisconnectCmpTX();

            try
            {
                compassTX = new Compass();

                compassTX.OnConnectPort += OpenPortCmpTX;
                compassTX.OnDisconnectPort += ClosePortCmpTX;
                compassTX.OnReadByte += ReadByteCmpTX;
                compassTX.OnWriteByte += WriteByteCmpTX;

                compassTX.OpenPort(mainWindowViewModel.LocalPropertiesVM.CmpTX.ComPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
            catch 
            { }
            

           
        }

        private void DisconnectCmpTX()
        {
            if (compassTX != null)
            {
                compassTX.ClosePort();

                compassTX.OnConnectPort -= OpenPortCmpTX;
                compassTX.OnDisconnectPort -= ClosePortCmpTX;
                compassTX.OnReadByte -= ReadByteCmpTX;
                compassTX.OnWriteByte -= WriteByteCmpTX;

                compassTX = null;
            }

        }

    }
}
