﻿using DataTransferClientLibrary;
using System.Windows;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using UserControl_Chat;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        public DataTransferClient dTClient;

        private void InitDataTransfer()
        {
            //dTClient = new DataTransferClient(mainWindowViewModel.LocalPropertiesVM.ED.ComPort, mainWindowViewModel.LocalPropertiesVM.ED.);

            //dTClient.OnMessageReceived += (sender, message) => { DrawMessageToChat(message); };
            //dTClient.OnDisconnect += (sender, EventArgs) => { HandlerDisconnect_DTClient(sender); };
            //dTClient.OnConnect += (sender, EventArgs) => { HandlerConnect_DTClient(sender); };
            //Events.OnSendStationsMessage += SendMessagesToStations;
        }


        private void DrawMessageToChat(DataTransferModel.DataTransfer.Message curMessage)
        {
            List<UserControl_Chat.Message> curMessages = new List<UserControl_Chat.Message>();
            curMessages.Add(new UserControl_Chat.Message
            {
                MessageFiled = curMessage.Text,
                Id = curMessage.SenderId,
                IsTransmited = true,
                IsSendByMe = Roles.Received

            });

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                chatBuble.SetMessage(curMessages[0].MessageFiled);
                newWindow.curChat.DrawMessageToChat(curMessages);
            });
        }


        private void SendMessagesToStations(List<UserControl_Chat.Message> stationsMessages)
        {
            foreach (UserControl_Chat.Message curMessage in stationsMessages)
                dTClient.SendMessage(curMessage.Id, curMessage.MessageFiled);
        }



        private void EdConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dTClient.IsConnected)
                {
                    dTClient.Disconnect();
                    mainWindowViewModel.StateConnectionED = ConnectionStates.Disconnected;
                }
                else
                {
                    dTClient.Connect(string.Format("test ARM name: {0}", Dns.GetHostByName(Dns.GetHostName()).AddressList[0].ToString()));
                    if (dTClient.IsConnected)
                        mainWindowViewModel.StateConnectionED = ConnectionStates.Connected;
                    else
                        mainWindowViewModel.StateConnectionED = ConnectionStates.Disconnected;
                }
            }
            catch
            {
                dTClient.Disconnect();
                mainWindowViewModel.StateConnectionED = ConnectionStates.Disconnected;
            }
        }

        private void HandlerDisconnect_DTClient(object sender)
        {
            mainWindowViewModel.StateConnectionED = ConnectionStates.Disconnected;
        }

        private void HandlerConnect_DTClient(object sender)
        {
            mainWindowViewModel.StateConnectionED = ConnectionStates.Connected;
        }
    }
}
