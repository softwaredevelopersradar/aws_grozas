﻿using System;
using System.Windows;
using DllGrozaSProperties.Models;
using UISpoof.Classes;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private SpoofingWindow.SpoofingWnd spoofingWindow;

        

        private void InitSpoofingWnd()
        {
            spoofingWindow = new SpoofingWindow.SpoofingWnd();
            spoofingWindow.OnChangeParamSpoofing += SpoofingWindow_OnChangeParamSpoofing;

            switch (mainWindowViewModel.LocalPropertiesVM.SS.Type)
            {
                case TypeSpoofing.One:
                    spoofingWindow.TwoSystem = false;
                    spoofingWindow.FourSystems = false;
                    break;
                case TypeSpoofing.Two:
                    spoofingWindow.TwoSystem = true;
                    spoofingWindow.FourSystems = false;
                    break;
                case TypeSpoofing.Four:
                    spoofingWindow.TwoSystem = false;
                    spoofingWindow.FourSystems = true;
                    break;
            }
        }

        public void SpoofingWindow_SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            if (spoofingWindow == null)
                return;
            spoofingWindow.Title = SWindowNames.nameSpoofingWnd;
            spoofingWindow.TempLanguage = (ELanguages)basicProperties.Local.General.Language;
        }

        private void SpoofingWindow_OnChangeParamSpoofing(object sender, SimulationParam e)
        {
            SendParamSpoofing(e);
        }

        private void SendParamSpoofing(SimulationParam e)
        {
            AwsToConsoleTransferLib.ControlParameters param = new AwsToConsoleTransferLib.ControlParameters(e.Elevation, (int)e.Speed, e.Angle, CodeRange(e.Range));

            SendSetParamSpoofing(param);
        }

        private void SendStartCoordsSpoofing()
        {
            AwsToConsoleTransferLib.CoordinatesParameters param = new AwsToConsoleTransferLib.CoordinatesParameters(mainWindowViewModel.GlobalPropertiesVM.Spoofing.Latitude,
                                                                                                        mainWindowViewModel.GlobalPropertiesVM.Spoofing.Longitude);

            SendSetStartCoordinates(param);
        }

        private byte CodeRange(byte range)
        {
            byte code = 0;

            if (range <= 1)
                code = 1;
           
            if (range >1 && range <=2)
                code = 2;

            if (range >2 && range <=5)
                code = 3;

            if (range >5 && range <=10)
                code = 4;

            if (range >10 && range <=20)
                code = 5;

            if (range >20 && range <=30)
                code = 6;

            if (range >30)
                code = 7;


            return code;


        }

        private byte CodeRangeBack(byte codeRange)
        {
            byte range = 0;

            switch (codeRange)
            {
                case 1:
                    range = 1;
                    break;

                case 2:
                    range = 2;
                    break;

                case 3:
                    range = 5;
                    break;

                case 4:
                    range = 10;
                    break;

                case 5:
                    range = 20;
                    break;

                case 6:
                    range = 30;
                    break;

                case 7:
                    range = 60;
                    break;

                default:
                    range = 1;
                    break;
            }
            
            return range;

        }
    }
}