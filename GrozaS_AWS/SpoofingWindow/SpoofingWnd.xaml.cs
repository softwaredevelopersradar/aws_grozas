﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using UISpoof.Classes;

namespace GrozaS_AWS.SpoofingWindow
{
    /// <summary>
    /// Interaction logic for SpoofingWnd.xaml
    /// </summary>
    public partial class SpoofingWnd : Window, INotifyPropertyChanged
    {
        public event EventHandler<SimulationParam> OnChangeParamSpoofing;

        public SpoofingWnd()
        {
            InitializeComponent();

            Mode = 0;
            
            DataContext = this;
        }

      

        private SimulationParam _simulationParamInput = new SimulationParam();

        public SimulationParam SimulationParamInput
        {
            get
            {
                return _simulationParamInput;
            }
            set
            {
                _simulationParamInput = value;                
                OnPropertyChanged();
            }
        }



        private SimulationParam _simulationParamOutput = new SimulationParam();

        public SimulationParam SimulationParamOutput
        {
            get
            {
                return _simulationParamOutput;
            }
            set
            {
                _simulationParamOutput = value;
                OnPropertyChanged();
            }
        }



        private EStatus _mode = new EStatus();

        public EStatus Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                OnPropertyChanged();
            }
        }

        private string _satelitesGPS = "";

        public string SatelitesGPS
        {
            get
            {
                return _satelitesGPS;
            }
            set
            {
                _satelitesGPS = value;
                OnPropertyChanged();
            }
        }


        private string _satelitesGLONASS = "";

        public string SatelitesGLONASS
        {
            get
            {
                return _satelitesGLONASS;
            }
            set
            {
                _satelitesGLONASS = value;
                OnPropertyChanged();
            }
        }

        private string _satellitesBeidou = "";

        public string SatellitesBeidou
        {
            get
            {
                return _satellitesBeidou;
            }
            set
            {
                _satellitesBeidou = value;
                OnPropertyChanged();
            }
        }


        private string _satellitesGalileo = "";

        public string SatellitesGalileo
        {
            get
            {
                return _satellitesGalileo;
            }
            set
            {
                _satellitesGalileo = value;
                OnPropertyChanged();
            }
        }


        private ELanguages _tempLanguage = new ELanguages();

        public ELanguages TempLanguage
        {
            get
            {
                return _tempLanguage;
            }
            set
            {
                _tempLanguage = value;
                OnPropertyChanged();
            }
        }

        private bool _systemGPS = true;

        public bool SystemGPS
        {
            get
            {
                return _systemGPS;
            }
            set
            {
                _systemGPS = value;
                OnPropertyChanged();
            }
        }


        private bool _systemGLONASS = false;

        public bool SystemGLONASS
        {
            get
            {
                return _systemGLONASS;
            }
            set
            {
                _systemGLONASS = value;
                OnPropertyChanged();
            }
        }


        private bool _twoSystem;

        public bool TwoSystem
        {
            get
            {
                return _twoSystem;
            }
            set
            {
                _twoSystem = value;
                OnPropertyChanged();
            }
        }

        private bool _fourSystem;

        public bool FourSystems
        {
            get
            {
                return _fourSystem;
            }
            set
            {
                _fourSystem = value;
                OnPropertyChanged();
            }
        }

        private Systems _systems = new Systems(true, true, true, true);

        public Systems Systems
        {
            get
            {
                return _systems;
            }
            set
            {
                _systems = value;
                OnPropertyChanged();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }



        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

        private void HandlerChangeInputParamEvent(object sender, SimulationParam e)
        {
            OnChangeParamSpoofing?.Invoke(this, e);
        }

     
    }
}
