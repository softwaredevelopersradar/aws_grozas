﻿namespace GrozaS_AWS.Converter
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using System.Reflection;
    using System.Threading;

    public static class Colorizer
    {
        private static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random());
        
      
        private static readonly ConcurrentDictionary<string, System.Windows.Media.Color> _assignedColors = new ConcurrentDictionary<string, System.Windows.Media.Color>();

        private static readonly ConcurrentDictionary<string, System.Windows.Media.Color> _allColors = GetAllColors();
        private static readonly ImmutableArray<string> _knownColors = _allColors.Keys.ToArray()
                                            .Where(x =>
                                                        !x.Equals("AliceBlue") &&
                                                        !x.Equals("AntiqueWhite") &&
                                                       // !x.Equals("Aqua") &&
                                                        !x.Equals("Aquamarine") &&
                                                        !x.Equals("Azure") &&
                                                        !x.Equals("Beige") &&
                                                        !x.Equals("Bisque") &&
                                                        !x.Equals("BlanchedAlmond") &&
                                                        !x.Equals("BurlyWood") &&
                                                        !x.Equals("Chartreuse") &&
                                                        !x.Equals("Cornsilk") &&
                                                        !x.Equals("DarkGray") &&
                                                        !x.Equals("DarkKhaki") &&
                                                        !x.Equals("DarkSalmon") &&
                                                        !x.Equals("DarkSeaGreen") &&
                                                        !x.Equals("DarkSlateGrey") &&
                                                        !x.Equals("DarkSlateGray") &&
                                                        !x.Equals("FloralWhite") &&
                                                        !x.Equals("Gainsboro") &&
                                                        !x.Equals("GhostWhite") &&
                                                        !x.Equals("Grey") &&
                                                        !x.Equals("Gray") &&
                                                        !x.Equals("HoneyDew") &&
                                                        !x.Equals("Ivory") &&
                                                        !x.Equals("Khaki") &&
                                                        !x.Equals("Lavender") &&
                                                        !x.Equals("LavenderBlush") &&
                                                        !x.Equals("LemonChiffon") &&
                                                        !x.Equals("LightCyan") &&
                                                        !x.Equals("LightGoldenrodYellow") &&
                                                        !x.Equals("LightGray") &&
                                                        !x.Equals("LightGrey") &&
                                                        !x.Equals("LightSlateGrey") &&
                                                        !x.Equals("LightSlateGray") &&
                                                        !x.Equals("LightYellow") &&
                                                        !x.Equals("Linen") &&
                                                        !x.Equals("MintCream") &&
                                                        !x.Equals("MistyRose") &&
                                                        !x.Equals("Moccasin") &&
                                                        !x.Equals("NavajoWhite") &&
                                                        !x.Equals("OldLace") &&
                                                        !x.Equals("PaleGoldenrod") &&
                                                        !x.Equals("PaleTurquoise") &&
                                                        !x.Equals("PaleGreen") &&
                                                        !x.Equals("PapayaWhip") &&
                                                        !x.Equals("PeachPuff") &&
                                                        !x.Equals("PowderBlue") &&
                                                        !x.Equals("RosyBrown") &&
                                                        !x.Equals("Salmon") &&
                                                        !x.Equals("SandyBrown") &&
                                                        !x.Equals("Silver") &&
                                                        !x.Equals("SeaShell") &&
                                                        !x.Equals("SkyBlue") &&
                                                        !x.Equals("Snow") &&
                                                        !x.Equals("Tan") &&
                                                        !x.Equals("Thistle") &&
                                                        !x.Equals("Transparent") &&
                                                        !x.Equals("Wheat") &&
                                                        !x.Equals("White") &&
                                                        !x.Equals("WhiteSmoke")&&
                                                        !x.Equals("DimGray") &&
                                                        !x.Equals("SlateGray") &&
                                                        !x.Equals("SteelBlue") &&
                                                        !x.Equals("CadetBlue") &&
                                                        !x.Equals("DarkCyan") 

                                                    )
                                            .ToImmutableArray();
       
        public static ImmutableArray<string> KnownColors
        {
            get => _knownColors;
        }

        public static System.Windows.Media.Color RandomColor
        {
            get => _allColors[KnownColors[GetRandomNumber(KnownColors.Length)]];
        }

        public static int GetRandomNumber(int max)
        {
            return random.Value.Next(max);
        }

        public static System.Windows.Media.Color AssignColor(string keyString)
        {
            if (_assignedColors.TryGetValue(keyString, out var color))
                return color;
            
            System.Windows.Media.Color newColor = RandomColor;
            for (int i = 0; i<_knownColors.Length; i++)
            {
                if (_assignedColors.Values.Contains(newColor))
                {
                    newColor = RandomColor;
                }
                else
                {
                    break;
                }
                
            }
            _assignedColors.TryAdd(keyString, newColor);

            return newColor;
        }

        public static void RemoveAssignedColor(string keyString)
        {
            _assignedColors.TryRemove(keyString, out var color);
        }

        public static void ClearAssignedColors()
        {
            _assignedColors.Clear();
        }

        private static ConcurrentDictionary<string, System.Windows.Media.Color> GetAllColors()
        {
            var f = typeof(System.Windows.Media.Colors).GetProperties(BindingFlags.Public | BindingFlags.Static);
            var ps = f.Select(p => new KeyValuePair<string, System.Windows.Media.Color>(p.Name, (System.Windows.Media.Color)p.GetValue(null)));
            var w = new ConcurrentDictionary<string, System.Windows.Media.Color>(ps);


            return w;
        }
    }
}
