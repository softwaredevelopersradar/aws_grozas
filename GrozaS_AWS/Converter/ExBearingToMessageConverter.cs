﻿using System;
using System.Globalization;
using System.Windows.Data;
using GrozaS_AWS.Models;
using UINotificationWindow;


namespace GrozaS_AWS
{
    public class ExBearingToMessageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var bearingLNK = (ExBearing)value;
            
            return new LinkedMessageModel() { IdStation = bearingLNK.StationNumber, Frequency = bearingLNK.Frequency, Bearing = bearingLNK.Bearing, TimeUpdate = bearingLNK.Time };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var message = (LinkedMessageModel)value;

            return new ExBearing() { StationNumber = message.IdStation, Frequency = message.Frequency, Bearing = message.Bearing, Time = message.TimeUpdate };
        }
    }
}
