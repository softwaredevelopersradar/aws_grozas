﻿using GrozaSModelsDBLib;
using System;
using System.Windows.Data;
using WpfMapControl;

namespace GrozaS_AWS
{
    public class CoordLocationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is Coord))
                return Binding.DoNothing;

            Coord coord = (Coord)value;

            Random random = new Random();
            var t = (double)(random.Next(1, 3)) / 1000000;
            Location location = new Location(coord.Longitude + t, coord.Latitude + t);

            return location;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Location location = (Location)value;
            Coord coord = new Coord() { Latitude = location.Latitude, Longitude = location.Longitude, Altitude = 0 };


            return coord;
        }
    }
}

