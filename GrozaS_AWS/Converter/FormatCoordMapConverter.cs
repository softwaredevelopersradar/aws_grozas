﻿using DllGrozaSProperties.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GrozaS_AWS
{
    public class FormatCoordMapConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is ViewCoord))
                return Binding.DoNothing;


            FormatCoord gg = (FormatCoord)value;
            return (FormatCoord)value;

            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
