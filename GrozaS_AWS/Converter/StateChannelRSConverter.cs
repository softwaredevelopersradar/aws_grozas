﻿using System;

using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace GrozaS_AWS
{
    public class StateChannelRSConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color c;
            switch ((MODE_RS)value)
            {
                case MODE_RS.ACTIVE:
                    c = Color.FromRgb(255,0,0);
                    break;


                case MODE_RS.NOTACTIVE:
                    c = Color.FromRgb(0, 0, 0);
                    break;


                default:
                    c = Color.FromRgb(0, 0, 0);
                    break;

            }
            return new SolidColorBrush(c);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MODE_RS c = MODE_RS.ACTIVE;
           
            return c;
        }
    }
}
