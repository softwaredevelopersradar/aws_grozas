﻿using System;
using System.Windows;
using System.Windows.Data;

namespace GrozaS_AWS
{
    public class RSErrorVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is string))
                return Binding.DoNothing;

            if ((string)value == string.Empty)
                return Visibility.Hidden;
            return Visibility.Visible;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
