﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows;
using System.Xml;
using ValuesCorrectLib;

namespace GrozaS_AWS
{
    

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
       
        public  string GetResourceTitle (string key)
        {
            try
            {                
                return (string)this.Resources[key];
            }

            catch
            {
                return "";
            }
            
        }

        
        private void SetResourceLanguage(DllGrozaSProperties.Models.Languages language)
        {

            this.Resources.MergedDictionaries.Clear();
         
            ResourceDictionary dict = new ResourceDictionary();
            ResourceDictionary dictAddPanel = new ResourceDictionary();
            ResourceDictionary dictConnectionPanel = new ResourceDictionary();
            ResourceDictionary dictUISource = new ResourceDictionary();
            ResourceDictionary dictInformPanel = new ResourceDictionary();
            ResourceDictionary dictMapButtons = new ResourceDictionary();
            ResourceDictionary dictMapContextMenu = new ResourceDictionary();
            ResourceDictionary dictUIChat = new ResourceDictionary();
            ResourceDictionary dictRSMessages = new ResourceDictionary();
            ResourceDictionary dictUIJamming = new ResourceDictionary();
            ResourceDictionary dictUITemplates = new ResourceDictionary();
            ResourceDictionary dictUISetting = new ResourceDictionary();


            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictRSMessages.Source = new Uri("/GrozaS_AWS;component/Languages/RS/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIJamming.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUITemplates.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISetting.Source = new Uri("/GrozaS_AWS;component/Languages/UISetting/StringResource.EN.xaml",
                                      UriKind.Relative);

                        break;

                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.RU.xaml",
                                           UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.RU.xaml",
                                           UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictRSMessages.Source = new Uri("/GrozaS_AWS;component/Languages/RS/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUIJamming.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUITemplates.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.RU.xaml",
                                      UriKind.Relative);
                        dictUISetting.Source = new Uri("/GrozaS_AWS;component/Languages/UISetting/StringResource.RU.xaml",
                                      UriKind.Relative);

                        break;

                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.AZ.xaml",
                                           UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictRSMessages.Source = new Uri("/GrozaS_AWS;component/Languages/RS/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUIJamming.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUITemplates.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.AZ.xaml",
                                      UriKind.Relative);
                        dictUISetting.Source = new Uri("/GrozaS_AWS;component/Languages/UISetting/StringResource.AZ.xaml",
                                      UriKind.Relative);

                        break;


                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.SRB.xaml",
                                           UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.SRB.xaml",
                                           UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictRSMessages.Source = new Uri("/GrozaS_AWS;component/Languages/RS/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictUIJamming.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictUITemplates.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.SRB.xaml",
                                      UriKind.Relative);
                        dictUISetting.Source = new Uri("/GrozaS_AWS;component/Languages/UISetting/StringResource.SR.xaml",
                                      UriKind.Relative);

                        break;

                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/TranslatorTables/TranslatorTablesGrozaS.EN.xaml",
                                      UriKind.Relative);
                        dictAddPanel.Source = new Uri("/GrozaS_AWS;component/Languages/AddPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictConnectionPanel.Source = new Uri("/GrozaS_AWS;component/Languages/ConnectionPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISource.Source = new Uri("/GrozaS_AWS;component/Languages/UISource/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictInformPanel.Source = new Uri("/GrozaS_AWS;component/Languages/InformPanel/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapButtons.Source = new Uri("/GrozaS_AWS;component/Languages/MapButtons/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictMapContextMenu.Source = new Uri("/GrozaS_AWS;component/Languages/MapContextMenu/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIChat.Source = new Uri("/GrozaS_AWS;component/Languages/UIChat/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictRSMessages.Source = new Uri("/GrozaS_AWS;component/Languages/RS/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUIJamming.Source = new Uri("/GrozaS_AWS;component/Languages/UIJamming/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUITemplates.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.EN.xaml",
                                      UriKind.Relative);
                        dictUISetting.Source = new Uri("/GrozaS_AWS;component/Languages/UISetting/StringResource.EN.xaml",
                                      UriKind.Relative);

                        break;
                }

                

                this.Resources.MergedDictionaries.Add(dict);
                this.Resources.MergedDictionaries.Add(dictAddPanel);
                this.Resources.MergedDictionaries.Add(dictConnectionPanel);
                this.Resources.MergedDictionaries.Add(dictUISource);
                this.Resources.MergedDictionaries.Add(dictInformPanel);
                this.Resources.MergedDictionaries.Add(dictMapButtons);
                this.Resources.MergedDictionaries.Add(dictMapContextMenu);
                this.Resources.MergedDictionaries.Add(dictUIChat);
                this.Resources.MergedDictionaries.Add(dictRSMessages);
                this.Resources.MergedDictionaries.Add(dictUIJamming);
                this.Resources.MergedDictionaries.Add(dictUITemplates);
                this.Resources.MergedDictionaries.Add(dictUISetting);

            }
            catch 
            { }
        }

        

        private void basicProperties_OnLanguageChanged(object sender, DllGrozaSProperties.Models.Languages language)
        {            
            SetResourceLanguage(language);
            TranslatorTables.LoadDictionary(language);
            TranslatorMainWindow.LoadDictionary(language);
            RastrMap.SetResourceLanguage((MapLanguages)basicProperties.Local.General.Language);

            newWindow?.SetLanguage(basicProperties.Local.General.Language);
            templateWindow?.SetLanguage(basicProperties.Local.General.Language);
            SetViewMode();
            
            JammingDrone_SetLanguage(language);
            DroneControl_SetLanguage(language);
            SpoofingWindow_SetLanguage(language);
            TemplateWindow_SetLanguage(language);
            GainWindow_SetLanguage(language);
            gainWindowEscope?.SetLanguage(language);

        }


        public class TranslatorMainWindow
        {
            static Dictionary<string, string> TranslateDic;
            public static void LoadDictionary(DllGrozaSProperties.Models.Languages language)
            {
                try
                {
                    XmlDocument xDoc = new XmlDocument();



                    if (File.Exists(Directory.GetCurrentDirectory() + "\\Languages\\Messages\\TranslatorMessage.xml"))
                        xDoc.Load(Directory.GetCurrentDirectory() + "\\Languages\\Messages\\TranslatorMessage.xml");


                    TranslateDic = new Dictionary<string, string>();

                    // получим корневой элемент
                    XmlElement xRoot = xDoc.DocumentElement;
                    foreach (XmlNode x2Node in xRoot.ChildNodes)
                    {
                        if (x2Node.NodeType == XmlNodeType.Comment)
                            continue;

                        // получаем атрибут ID
                        if (x2Node.Attributes.Count > 0)
                        {
                            XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                            if (attr != null)
                            {
                                foreach (XmlNode childnode in x2Node.ChildNodes)
                                {
                                    // если узел - language
                                    if (childnode.Name == language.ToString())
                                    {
                                        if (!TranslateDic.ContainsKey(attr.Value))
                                            TranslateDic.Add(attr.Value, childnode.InnerText);
                                    }
                                }
                            }
                        }
                    }

                    if (TranslateDic.Count > 0 && TranslateDic != null)
                    {
                        RenameMessages(TranslateDic);
                    }
                }
                catch(Exception ex)
                { }

            }
        }

        public static void RenameMessages(Dictionary<string, string> TranslateDic)
        {

            SMessages.textHeaderWarning = TranslateDic.ContainsKey("textHeaderWarning") ? TranslateDic["textHeaderWarning"] : "";
            SMessages.textHeaderError = TranslateDic.ContainsKey("textHeaderError") ? TranslateDic["textHeaderError"] : "";
            SMessages.textHeaderInform = TranslateDic.ContainsKey("textHeaderInform") ? TranslateDic["textHeaderInform"] : "";

            SMessages.messageUpdateSpoofingPosition = TranslateDic.ContainsKey("messageUpdateSpoofingPosition") ? TranslateDic["messageUpdateSpoofingPosition"] : "";
            SMessages.messageErrorDoubleSpoofing = TranslateDic.ContainsKey("messageErrorDoubleSpoofing") ? TranslateDic["messageErrorDoubleSpoofing"] : "";

            SWindowNames.nameTemplatesWnd = TranslateDic.ContainsKey("nameTemplatesWnd") ? TranslateDic["nameTemplatesWnd"] : "";
            SWindowNames.nameSpoofingWnd = TranslateDic.ContainsKey("nameSpoofingWnd") ? TranslateDic["nameSpoofingWnd"] : "";
            SWindowNames.nameGainWnd = TranslateDic.ContainsKey("nameGainWnd") ? TranslateDic["nameGainWnd"] : "";
            SWindowNames.nameAttenuatorWnd = TranslateDic.ContainsKey("nameAttenuatorWnd") ? TranslateDic["nameAttenuatorWnd"] : "";
        }

        public struct SMessages
        {
            public static string textHeaderWarning = "Warning";
            public static string textHeaderError = "Error";
            public static string textHeaderInform = "Inform";


            public static string messageUpdateSpoofingPosition = "Cannot update coordinates during active spoofing mode";
            public static string messageErrorDoubleSpoofing = "Spoofing mode is already load with another amplifier";


        }

        public struct SWindowNames
        {
            public static string nameTemplatesWnd = "Templates UAV";
            public static string nameSpoofingWnd = "Spoofing";
            public static string nameGainWnd = "Gain";
            public static string nameAttenuatorWnd = "Attenuator";
        }

    }
}
