﻿
using BRD.Events;
using ClientServerLib;
using GrozaSModelsDBLib;
using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using DllGrozaSProperties.Models;
using UISpoof.Classes;

namespace GrozaS_AWS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        private void DefaultPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            mainWindowViewModel.SetDefaultDPanel();
        }


        private void UpdateCmpRXPanelToggleButton_Click(object sender, RoutedEventArgs e)
        {
            GetCompassRX();

            //spoofingWindow.SatelitesGPS = "G12, G23, G2, G24, G31";
            //mainWindowViewModel.ActiveSpoofing = !mainWindowViewModel.ActiveSpoofing;

            //ClientSpoofing_ReplySpoofModeDetailed(this, new AwsToConsoleTransferLib.DetailedStateReplyEventArgs(0,0, new byte[4]));

            //ResultMessageRS(1);

            //mainWindowViewModel.ActiveSpoofing = !mainWindowViewModel.ActiveSpoofing;
        }

        private  async void UpdateCmpTXToggleButton_Click(object sender, RoutedEventArgs e)
        {
            GetCompassTX();


            //await httpHub.PostTargetInformationAsync(new StationDto()
            //{
            //    TargetCoordinates = new Coordinates() { Latitude = 53.457812, Longitude = 27.235689 },
            //    ConcreteOperator = "0421X4578",// 255 - everybody                
            //});

            Random rnd = new Random();

            GrozaR_OnPosition(this, new GrozaR_UHF_DLL.Events.PositionEventArgs((byte)rnd.Next(1,6), 53.457812 +(double)(rnd.Next(100,900))*0.001, 27.235689, (short)rnd.Next(0, 359), (byte)rnd.Next(0, 5)));
        }



        private void SetAngleBRD1Button_Click(object sender, RoutedEventArgs e)
        {
            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
            {
                switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
                {
                    case DllGrozaSProperties.Models.TypeBRD._2TS:
                        SetAngleBRD1((byte)mainWindowViewModel.DirectionBRD1_Upper.Address, mainWindowViewModel.DirectionRX.Result);
                        break;

                    case DllGrozaSProperties.Models.TypeBRD.Impressa:                        
                        SetAngleImpressaBRD(mainWindowViewModel.DirectionRX.Result);
                        break;

                    default:
                        break;
                }
            }

            

        }

        private void GetAgleBRD1Button_Click(object sender, RoutedEventArgs e)
        {

            if (comBRD1 != null)
            {
                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                    comBRD1.SendGetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom);

                Thread.Sleep(1000);

                if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                    comBRD1.SendGetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper);
                Thread.Sleep(1000);
            }

            
        }

        private void StopBRD1Button_Click(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
            {
                case DllGrozaSProperties.Models.TypeBRD._2TS:
                    if (comBRD1 != null)
                    {
                        if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                        {
                            mainWindowViewModel.DirectionRotateBRD1_Upper = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD1_Upper.Result, mainWindowViewModel.DirectionBRD1_Upper);
                            comBRD1.SendStop((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper, 0);
                            mainWindowViewModel.DirectionRotateBRD1_Upper.Set = false;
                        }


                        Thread.Sleep(1000);

                        if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                        {
                            mainWindowViewModel.DirectionRotateBRD1_Bottom = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD1_Bottom.Result, mainWindowViewModel.DirectionBRD1_Bottom);
                            comBRD1.SendStop((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Bottom, 0);

                            mainWindowViewModel.DirectionRotateBRD1_Bottom.Set = false;
                        }
                        Thread.Sleep(1000);
                    }
                    break;

                case DllGrozaSProperties.Models.TypeBRD.Impressa:

                    if (impressaBRD != null)
                    {
                        if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                        {
                            mainWindowViewModel.DirectionRotateBRD1_Upper = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD1_Upper.Result, mainWindowViewModel.DirectionBRD1_Upper);
                            impressaBRD.StopOPU();
                            mainWindowViewModel.DirectionRotateBRD1_Upper.Set = false;
                        }
                    }
                    break;

                default:
                    break;
            }


            



            

            UpdateMapDirectionBRD();

        }

        private void SetAngleBRD2Button_Click(object sender, RoutedEventArgs e)
        {

            if (comBRD2 != null)
                SetAngleBRD2((byte)mainWindowViewModel.DirectionBRD2_Upper.Address, mainWindowViewModel.DirectionRX.Result);

        }

        private void GetAgleBRD2Button_Click(object sender, RoutedEventArgs e)
        {

            if (comBRD2 != null)
            {
                if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                    comBRD2.SendGetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom);
                Thread.Sleep(1000);

                if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                    comBRD2.SendGetAngle((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper);
                Thread.Sleep(1000);

            }

        }

        private void StopBRD2Button_Click(object sender, RoutedEventArgs e)
        {
            if (comBRD2 != null)
            {
                if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                {
                    mainWindowViewModel.DirectionRotateBRD2_Upper = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD2_Upper.Result, mainWindowViewModel.DirectionBRD2_Upper);
                    comBRD2.SendStop((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper, 0);
                    mainWindowViewModel.DirectionRotateBRD2_Upper.Set = false;

                }


                Thread.Sleep(1000);

                if (mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                {
                    mainWindowViewModel.DirectionRotateBRD2_Bottom = mainWindowViewModel.SetAngleBRD(mainWindowViewModel.DirectionBRD2_Bottom.Result, mainWindowViewModel.DirectionBRD2_Bottom);
                    comBRD2.SendStop((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Bottom, 0);

                    mainWindowViewModel.DirectionRotateBRD2_Bottom.Set = false;
                }
                Thread.Sleep(1000);
            }
            

            UpdateMapDirectionBRD();

        }








        private void CalibrationBRDButton_Click(object sender, RoutedEventArgs e)
        {
            Button temp = (Button)sender;

            switch (temp.Name)
            {
                case "SetAngleBRD1Button":
                    if (comBRD1 != null)
                    {
                        comBRD1.Calibration((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper);
                    }
                    break;

                case "SetAngleBRD2Button":

                    if (comBRD2 != null)
                    {
                        comBRD2.Calibration((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper);
                    }

                    break;

                default:
                    break;
            }

            
        }

        private void TemplateUAVButton_Click(object sender, RoutedEventArgs e)
        {
            //SaveSourceTDF_DB();

            if (templateWindow.IsVisible)
                templateWindow.Hide();
            else
                templateWindow.Show();
        }


        private void IntelegenceModeButton_Click(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.DF.Type)
            {
                case DllGrozaSProperties.Models.TypeDF.ZhSV:
                    SendMode(Mode.ELINT);

                    break;
                case DllGrozaSProperties.Models.TypeDF.ShIV:
                    SetModeDDF(TransmissionPackage.Mode.RadioIntelligens);
                    break;

                   
            }

        }

        private void JammingModeButton_Click(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.DF.Type)
            {
                case DllGrozaSProperties.Models.TypeDF.ZhSV:
                    SendMode(Mode.JAMMING);

                    break;
                case DllGrozaSProperties.Models.TypeDF.ShIV:
                    SetModeDDF(TransmissionPackage.Mode.Jamming);
                    break;


            }
        }

        private void StopModeButton_Click(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.DF.Type)
            {
                case DllGrozaSProperties.Models.TypeDF.ZhSV:
                    SendMode(Mode.STOP);

                    break;
                case DllGrozaSProperties.Models.TypeDF.ShIV:
                    SetModeDDF(TransmissionPackage.Mode.Stop);
                    break;


            }
        }

        private void SpoofingModeButton_Click(object sender, RoutedEventArgs e)
        {                        
            if (!mainWindowViewModel.ActiveSpoofing)
                SendParamSpoofing(spoofingWindow.SimulationParamInput);

            Thread.Sleep(200);
            if (mainWindowViewModel.LocalPropertiesVM.SS.Type == TypeSpoofing.Four)
            {
                SendSetModeSpoofing(!mainWindowViewModel.ActiveSpoofing, spoofingWindow.Systems);
            }
            else
            {
                SendSetModeSpoofing(!mainWindowViewModel.ActiveSpoofing, GetSpoofingSystem(spoofingWindow.SystemGPS, spoofingWindow.SystemGLONASS, mainWindowViewModel.LocalPropertiesVM.SS.TwoChannels));
            }
            
        }


        private ESpoofSystem GetSpoofingSystem(bool GPS, bool GLONASS, bool TwoChannel)
        {
            
            if (GPS && GLONASS && !TwoChannel)
                return ESpoofSystem.GPS_GLONASS;

            if (GPS && GLONASS && TwoChannel)
                return ESpoofSystem.GPS_GLONASS_CH2;

            if (GPS && !GLONASS)
                return ESpoofSystem.GPS;

            if (!GPS && GLONASS)
                return ESpoofSystem.GLONASS;

            return ESpoofSystem.GPS;
        }

        private void GainWndButton_Click(object sender, RoutedEventArgs e)
        {
           
            if (gainWindow.IsVisible)
                gainWindow.Hide();
            else
                gainWindow.Show();
        }


        private void SpoofWndButton_Click(object sender, RoutedEventArgs e)
        {
            if (spoofingWindow.IsVisible)
                spoofingWindow.Hide();
            else
                spoofingWindow.Show();
        }


        private void AttenuatorButton_Click(object sender, RoutedEventArgs e)
        {
            if (attenuatorWindow.IsVisible)
                attenuatorWindow.Hide();
            else
                attenuatorWindow.Show();
        }

    }
}