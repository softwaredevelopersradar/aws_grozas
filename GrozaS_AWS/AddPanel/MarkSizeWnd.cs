﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.AddPanel
{
    public class MarkSizeWnd
    {
        public SizeValue sizeDPSetting { get; set; } = new SizeValue();
        public SizeValue sizeDPTables { get; set; } = new SizeValue();
        public SizeValue sizeDPJamming { get; set; } = new SizeValue();
        public SizeValue sizeDPSpoofing { get; set; } = new SizeValue();

        public MarkSizeWnd()
        {
            sizeDPSetting  = new SizeValue();
            sizeDPTables  = new SizeValue();
            sizeDPJamming  = new SizeValue();
            sizeDPSpoofing  = new SizeValue();

    }


}
}
