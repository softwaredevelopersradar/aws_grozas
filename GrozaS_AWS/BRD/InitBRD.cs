﻿
using BRD;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public ComBRD comBRD1;

        public ComBRD comBRD2;

        private void ConnectBRDJamming()
        {
            if (comBRD1 != null)
                DisconnectBRDJamming();


            //comBRD1 = new ComBRD((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Jamming1);
            comBRD1 = new ComBRD();

            comBRD1.OnOpenPort += OpenPortBRD;
            comBRD1.OnClosePort += ClosePortBRD;
            comBRD1.OnEcho += EchoBRD;
            comBRD1.OnFreeRotate += FreeRotateBRD;
            comBRD1.OnGetAngle += GetAngleBRD;
            comBRD1.OnSetAngle += SetAngleBRD;
            comBRD1.OnStop += StopBRD;

            comBRD1.OpenPort(mainWindowViewModel.LocalPropertiesVM.BRD1.ComPort, 19200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            //comBRD1.Calibration();

            comBRD1.Calibration((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Upper);
        }

        private void DisconnectBRDJamming()
        {
            if (comBRD1 != null)
            {
                comBRD1.ClosePort();

                comBRD1.OnOpenPort -= OpenPortBRD;
                comBRD1.OnClosePort -= ClosePortBRD;
                comBRD1.OnEcho -= EchoBRD;
                comBRD1.OnFreeRotate -= FreeRotateBRD;
                comBRD1.OnGetAngle -= GetAngleBRD;
                comBRD1.OnSetAngle -= SetAngleBRD;
                comBRD1.OnStop -= StopBRD;

                comBRD1 = null;
            }

        }

        private void ConnectBRDConnection()
        {
            if (comBRD2 != null)
                DisconnectBRDConnection();


            //comBRD2 = new ComBRD((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Jamming1);
            comBRD2 = new ComBRD();

            comBRD2.OnOpenPort += OpenPortBRD;
            comBRD2.OnClosePort += ClosePortBRD;
            comBRD2.OnEcho += EchoBRD;
            comBRD2.OnFreeRotate += FreeRotateBRD;
            comBRD2.OnGetAngle += GetAngleBRD;
            comBRD2.OnSetAngle += SetAngleBRD;
            comBRD2.OnStop += StopBRD;

            comBRD2.OpenPort(mainWindowViewModel.LocalPropertiesVM.BRD2.ComPort, 19200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            comBRD2.Calibration((byte)mainWindowViewModel.LocalPropertiesVM.BRD2.Address_Upper);
        }

        private void DisconnectBRDConnection()
        {
            if (comBRD2 != null)
            {
                comBRD2.ClosePort();

                comBRD2.OnOpenPort -= OpenPortBRD;
                comBRD2.OnClosePort -= ClosePortBRD;
                comBRD2.OnEcho -= EchoBRD;
                comBRD2.OnFreeRotate -= FreeRotateBRD;
                comBRD2.OnGetAngle -= GetAngleBRD;
                comBRD2.OnSetAngle -= SetAngleBRD;
                comBRD2.OnStop -= StopBRD;

                comBRD2 = null;
            }

        }

    }
}
