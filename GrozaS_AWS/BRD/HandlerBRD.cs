﻿
using BRD;
using BRD.Events;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private void BRDControlConnectionJamming_ButServerClick(object sender, RoutedEventArgs e)
        {
            switch (mainWindowViewModel.LocalPropertiesVM.BRD1.Type)
            {
                case DllGrozaSProperties.Models.TypeBRD._2TS:
                    try
                    {
                        if (comBRD1 != null)
                            DisconnectBRDJamming();
                        else
                            ConnectBRDJamming();

                    }
                    catch
                    { }
                    break;

                case DllGrozaSProperties.Models.TypeBRD.Impressa:
                    try
                    {
                        if (impressaBRD != null)
                            DisconnectImpressaBRD();
                        else
                            ConnectImpressaBRD();

                    }
                    catch
                    { }
                    break;

                default:
                    break;
            }                
        }

        private void BRDControlConnectionCnt_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comBRD2 != null)
                    DisconnectBRDConnection();
                else
                {
                    ConnectBRDConnection();
                }

            }
            catch
            { }
        }

        private void OpenPortBRD(object sender, EventArgs e)
        {
            if ((ComBRD)sender == comBRD1)
            {
                mainWindowViewModel.StateConnectionBRDJamming = ConnectionStates.Connected;

            }

            if ((ComBRD)sender == comBRD2)
            {
                mainWindowViewModel.StateConnectionBRDCnt = ConnectionStates.Connected;
            }
            
        }

        private void ClosePortBRD(object sender, EventArgs e)
        {
            if ((ComBRD)sender == comBRD1)
            {
                mainWindowViewModel.StateConnectionBRDJamming = ConnectionStates.Disconnected;

            }

            if ((ComBRD)sender == comBRD2)
            {
                mainWindowViewModel.StateConnectionBRDCnt = ConnectionStates.Disconnected;
            }
        }

        private void EchoBRD(object sender, ErrorEventArgs e)
        { }

        private void FreeRotateBRD(object sender, ErrorEventArgs e)
        { }

        private void GetAngleBRD(object sender, AngleEventArgs e)
        {
            if ((ComBRD)sender == comBRD1)
            {

                if (mainWindowViewModel.DirectionBRD1_Upper.Address == e.Address && mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                {
                    mainWindowViewModel.DirectionBRD1_Upper.Azimuth = e.Angle;
                }
                    

                if (mainWindowViewModel.DirectionBRD1_Bottom.Address == e.Address && mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                {

                    mainWindowViewModel.DirectionBRD1_Bottom.Azimuth = e.Angle;


                 
                    float delta = mainWindowViewModel.DirectionBRD1_Bottom.Azimuth - mainWindowViewModel.LocalPropertiesVM.BRD1.CompassAngle_Bottom;
                    if (delta > 0)
                        mainWindowViewModel.DirectionBRD1_Upper.Error = delta;
                    else
                        mainWindowViewModel.DirectionBRD1_Upper.Error = 360 + delta;


                }
                    

            }

            if ((ComBRD)sender == comBRD2)
            {
               
                if (mainWindowViewModel.DirectionBRD2_Upper.Address == e.Address && mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                    mainWindowViewModel.DirectionBRD2_Upper.Azimuth = e.Angle;

                if (mainWindowViewModel.DirectionBRD2_Bottom.Address == e.Address && mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                {

                    mainWindowViewModel.DirectionBRD2_Bottom.Azimuth = e.Angle;


                    float delta = mainWindowViewModel.DirectionBRD2_Bottom.Azimuth - mainWindowViewModel.LocalPropertiesVM.BRD2.CompassAngle_Bottom;
                    if (delta > 0)
                        mainWindowViewModel.DirectionBRD2_Upper.Error = delta;
                    else
                        mainWindowViewModel.DirectionBRD2_Upper.Error = 360 + delta;
                }
                    
            }



        }

        private void SetAngleBRD(object sender, ErrorEventArgs e)
        { }

        private void StopBRD(object sender, ErrorEventArgs e)
        { }

        

        

        private void SetAngleBRD(float e)
        {
            try
            {

                mainWindowViewModel.DirectionRotateBRD1_Upper = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD1_Upper);

                comBRD1.SendSetAngle(0, (short)mainWindowViewModel.DirectionRotateBRD1_Upper.Azimuth, 0);

                UpdateMapDirectionBRD();
            }
            catch { }
        }

        private void SetAngleBRD1(byte address, float e)
        {
            if (comBRD1 == null)
                return;
            try
            {
                short azimuth = -1;
                if (mainWindowViewModel.DirectionRotateBRD1_Upper.Address == address && mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                {
                    mainWindowViewModel.DirectionRotateBRD1_Upper = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD1_Upper);
                    azimuth = (short)mainWindowViewModel.DirectionRotateBRD1_Upper.Azimuth;

                    mainWindowViewModel.DirectionRotateBRD1_Upper.Set = true;
                }



                if (mainWindowViewModel.DirectionRotateBRD1_Bottom.Address == address && mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Bottom)
                {
                    mainWindowViewModel.DirectionRotateBRD1_Bottom = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD1_Bottom);
                    azimuth = (short)mainWindowViewModel.DirectionRotateBRD1_Bottom.Azimuth;

                    mainWindowViewModel.DirectionRotateBRD1_Bottom.Set = true;
                }
                    

                if (comBRD1.SendSetAngle(address, 0, azimuth, 0))
                    UpdateMapDirectionBRD();
            }
            catch { }
        }

        private void SetAngleBRD2(byte address, float e)
        {
            try
            {
                short azimuth = -1;

                if (mainWindowViewModel.DirectionRotateBRD2_Upper.Address == address && mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Upper)
                {
                    mainWindowViewModel.DirectionRotateBRD2_Upper = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD2_Upper);
                    azimuth = (short)mainWindowViewModel.DirectionRotateBRD2_Upper.Azimuth;

                    mainWindowViewModel.DirectionRotateBRD2_Upper.Set = true;
                }



                if (mainWindowViewModel.DirectionRotateBRD2_Bottom.Address == address && mainWindowViewModel.LocalPropertiesVM.BRD2.Existance_Bottom)
                {
                    mainWindowViewModel.DirectionRotateBRD2_Bottom = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD2_Bottom);
                    azimuth = (short)mainWindowViewModel.DirectionRotateBRD2_Bottom.Azimuth;

                    mainWindowViewModel.DirectionRotateBRD2_Bottom.Set = true;
                }
                    

                if (comBRD2.SendSetAngle(address, 0, azimuth, 0))                
                    UpdateMapDirectionBRD();
            }
            catch { }
        }


    }
}