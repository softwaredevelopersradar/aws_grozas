﻿
using BRD;
using BRD.Events;
using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using ImpressaMastDLL;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
       


        private void UpdateParamMast(object sender, EventArgsMastParamBroadcast param)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                mainWindowViewModel.mastParamBroadcast = param.MastParamBroadcast;

                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_current = param.MastParamBroadcast.Fireco_BC_DM_current;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_dins = param.MastParamBroadcast.Fireco_BC_DM_dins;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_douts = param.MastParamBroadcast.Fireco_BC_DM_douts;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_encod_h = param.MastParamBroadcast.Fireco_BC_DM_encod_h;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_encod_l = param.MastParamBroadcast.Fireco_BC_DM_encod_l;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_Error = param.MastParamBroadcast.Fireco_BC_DM_Error;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_MotorState = param.MastParamBroadcast.Fireco_BC_DM_MotorState;
                mainWindowViewModel.mastParamBroadcast.Fireco_BC_DM_SysState = param.MastParamBroadcast.Fireco_BC_DM_SysState;

            }), DispatcherPriority.Background);
        }

        private void UpdateParamOPU(object sender, EventArgsOPUParamBroadcast param)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                mainWindowViewModel.opuParamBroadcast = param.OPUParamBroadcast;

                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_STATE = param.OPUParamBroadcast.APU_BC_DM_STATE;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ANGLE_L = param.OPUParamBroadcast.APU_BC_DM_ANGLE_L;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ANGLE_H = param.OPUParamBroadcast.APU_BC_DM_ANGLE_H;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_RPM = param.OPUParamBroadcast.APU_BC_DM_RPM;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_CURENT = param.OPUParamBroadcast.APU_BC_DM_CURENT;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ERROR = param.OPUParamBroadcast.APU_BC_DM_ERROR;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ENC_L = param.OPUParamBroadcast.APU_BC_DM_ENC_L;
                mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ENC_H = param.OPUParamBroadcast.APU_BC_DM_ENC_H;

            }), DispatcherPriority.Background);

            byte[] bytes = { mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ANGLE_L, mainWindowViewModel.opuParamBroadcast.APU_BC_DM_ANGLE_H };
            if (mainWindowViewModel.LocalPropertiesVM.BRD1.Existance_Upper)
                mainWindowViewModel.DirectionBRD1_Upper.Azimuth = (BitConverter.ToInt16(bytes, 0))/10;


        }



        private void OpenPortImpressaBRD(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionBRDJamming = ConnectionStates.Connected;
        }

        private void ClosePortImpressaBRD(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionBRDJamming = ConnectionStates.Disconnected;
        }



        private void SetAngleImpressaBRD(float e)
        {
            if (impressaBRD == null)
                return;
            try
            {

                mainWindowViewModel.DirectionRotateBRD1_Upper = mainWindowViewModel.SetAngleBRD(e, mainWindowViewModel.DirectionBRD1_Upper);

                //impressaBRD.GoToAngleOPU((ushort)mainWindowViewModel.DirectionRotateBRD1_Upper.Azimuth);
                impressaBRD.GoToAngleOPU((ushort)CheckForbiddenAngle(mainWindowViewModel.DirectionRotateBRD1_Upper.Azimuth, 
                    mainWindowViewModel.LocalPropertiesVM.BRD1.ForbiddenAngleMin_Upper, mainWindowViewModel.LocalPropertiesVM.BRD1.ForbiddenAngleMax_Upper), 
                    (byte)mainWindowViewModel.LocalPropertiesVM.BRD1.RotationSpeed_Upper, (byte)mainWindowViewModel.LocalPropertiesVM.BRD1.RotationAcceleration_Upper);

                UpdateMapDirectionBRD();
            }
            catch { }
        }

        private void GoToTransportAngleImpressaBRD(float e)
        {
            if (impressaBRD == null)
                return;
            try
            {
                impressaBRD.GoToAngleOPU((ushort)e, (byte)mainWindowViewModel.LocalPropertiesVM.BRD1.RotationSpeed_Upper, (byte)mainWindowViewModel.LocalPropertiesVM.BRD1.RotationAcceleration_Upper);
                UpdateMapDirectionBRD();
                // impressaBRD.GoToTransportOPU();
            }
            catch { }
        }



        private float CheckForbiddenAngle(float currentAngle, float minForbiddenAngle, float maxForbiddenAngle)
        {
            if (minForbiddenAngle > maxForbiddenAngle)
            {
                if (currentAngle >= minForbiddenAngle || currentAngle <= maxForbiddenAngle)
                {
                    var middle = (minForbiddenAngle + maxForbiddenAngle + 360) / 2;
                    currentAngle = currentAngle < middle % 360 ? minForbiddenAngle : maxForbiddenAngle;
                }
            }
            else
            {
                if (currentAngle >= minForbiddenAngle && currentAngle <= maxForbiddenAngle)
                {
                    var middle = (minForbiddenAngle + maxForbiddenAngle) / 2;
                    currentAngle = currentAngle < middle ? minForbiddenAngle : maxForbiddenAngle;
                }
            }

            return currentAngle;
        }
    }
}