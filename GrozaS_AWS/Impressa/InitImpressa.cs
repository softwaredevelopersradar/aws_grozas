﻿

using ImpressaMastDLL;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        ImpressaDLL impressaBRD;

       

        private void ConnectImpressaBRD()
        {
            if (impressaBRD != null)
                DisconnectImpressaBRD();


            //comBRD1 = new ComBRD((byte)mainWindowViewModel.LocalPropertiesVM.BRD1.Address_Jamming1);
            impressaBRD = new ImpressaDLL();

            

            impressaBRD.OnOpenPort += OpenPortImpressaBRD;
            impressaBRD.OnClosePort += ClosePortImpressaBRD;
            impressaBRD.OnUpdateParamMast += UpdateParamMast;
            impressaBRD.OnUpdateParamOPU += UpdateParamOPU;

            impressaBRD.Open(mainWindowViewModel.LocalPropertiesVM.BRD1.ComPort);
           
        }

        private void DisconnectImpressaBRD()
        {
            if (impressaBRD != null)
            {
                impressaBRD.Close();

                impressaBRD.OnOpenPort -= OpenPortImpressaBRD;
                impressaBRD.OnClosePort -= ClosePortImpressaBRD;
                impressaBRD.OnUpdateParamMast -= UpdateParamMast;
                impressaBRD.OnUpdateParamOPU -= UpdateParamOPU;


                impressaBRD = null;
            }

        }

      

    }
}
