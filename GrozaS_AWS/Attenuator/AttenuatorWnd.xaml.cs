﻿using GrozaS_AWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UIAttenuatorControl;

namespace GrozaS_AWS.Attenuator
{
    /// <summary>
    /// Interaction logic for AttenuatorWnd.xaml
    /// </summary>
    public partial class AttenuatorWnd : Window
    {
        public event EventHandler<SVB> OnChangeAttenuator;
        public AttenuatorWnd()
        {
            InitializeComponent();

        }

        

        private void AttenuatorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void AttenuatorBands_OnChangeAttenuator(object sender, SVB e)
        {
            OnChangeAttenuator?.Invoke(this, e);
        }

        public void SetBands(List<SingleViewBandAWS> singleViewBandAWs)
        {
            AttenuatorBands.LSingleViewBand =
                        new System.Collections.Generic.List<UIAttenuatorControl.SVB>();

            foreach (var val in singleViewBandAWs)
            {
                AttenuatorBands.LSingleViewBand.Add(new UIAttenuatorControl.SVB()
                {
                    Num = (byte)val.Number,
                    FreqMin = val.FreqMin,
                    FreqMax = val.FreqMax,
                    Att1 = (byte)val.Att1,
                    Att2 = (byte)val.Att2
                });


            }

            AttenuatorBands.IndexSVB = 0;
        }
    }
}
