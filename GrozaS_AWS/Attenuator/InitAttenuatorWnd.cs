﻿
using GrozaS_AWS.Attenuator;
using GrozaS_AWS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private AttenuatorWnd attenuatorWindow;

        private void InitAttenuatorWnd()
        {
            attenuatorWindow = new AttenuatorWnd();
            attenuatorWindow.OnChangeAttenuator += AttenuatorWindow_OnChangeAttenuator;
        }

       

        private void AttenuatorWindow_OnChangeAttenuator(object sender, UIAttenuatorControl.SVB e)
        {

            singleViewBand.Where(x => x.Number == e.Num).FirstOrDefault().Att1 = e.Att1;
            singleViewBand.Where(x => x.Number == e.Num).FirstOrDefault().Att2 = e.Att2;
        }

        

        public void AttenuatorWindow_SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            if (attenuatorWindow == null)
                return;
            attenuatorWindow.Title = SWindowNames.nameAttenuatorWnd;           
        }


        private void AttenuatorWindow_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SingleViewBandAWS singleViewBandAWS = (SingleViewBandAWS)sender;

            UpdateAttenuatorBand(singleViewBandAWS);

        }



    }
} 