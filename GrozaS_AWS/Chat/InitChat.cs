﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using UserControl_Chat;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private Chat newWindow;
        private Buble chatBuble;

        private void InitChat()
        {
            newWindow = new Chat();
            chatBuble = new Buble();
            newWindow.OnReturnApprovedMessages += NewWindow_OnReturnApprovedMessages;
            newWindow.SetStations();
            Events.OnClosingChat += DeactivatChatButton;
          
        }

        private void NewWindow_OnReturnApprovedMessages(object sender, List<Message> messages)
        {
            try
            {
                if (messages.Last().Id == 0)
                {
                    ClientBErezina_SendTextMessage(messages.Last().MessageFiled);
                    return;
                }
                SendMessage(messages.Last().Id, messages.Last().MessageFiled);                
            }
            catch { }
        }

        private void UpdateSideMenu(List<TableJammerStation> ASPList)
        {
            newWindow.UpdateSideMenu(ASPList);
        }

        private void ChatToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (newWindow.IsVisible)
                newWindow.Hide();
            else
                newWindow.Show();
        }


        private void DeactivatChatButton()
        {
            ChatToggleButton.IsChecked = false;
        }
    }
}
