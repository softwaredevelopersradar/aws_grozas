﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GrozaS_AWS
{
    class BubleVM : INotifyPropertyChanged
    {
        private Buble buble;
        private string message;

        public BubleVM(Buble curBuble)
        {
            this.buble = curBuble;
        }

        public string Message
        {
            get { return message; }
            set
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
