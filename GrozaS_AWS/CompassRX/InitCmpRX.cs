﻿using DLL_Compass;
using System.Windows;
using TDF;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public Compass compassRX = new Compass();

        private void ConnectCmpRX()
        {
            if (compassRX != null)
                DisconnectCmpRX();

            compassRX = new Compass();

            compassRX.OnConnectPort += OpenPortCmpRX;
            compassRX.OnDisconnectPort += ClosePortCmpRX;
            compassRX.OnReadByte += ReadByteCmpRX;
            compassRX.OnWriteByte += WriteByteCmpRX;

            

            try 
            {
                compassRX.OpenPort(mainWindowViewModel.LocalPropertiesVM.CmpRX.ComPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
            catch 
            {
                //MessageBox.Show(mainWindowViewModel.LocalPropertiesVM.CmpRX.ComPort.ToString()+e.Message);
            }
            

           
        }

        private void DisconnectCmpRX()
        {
            if (compassRX != null)
            {
                compassRX.ClosePort();

                compassRX.OnConnectPort -= OpenPortCmpRX;
                compassRX.OnDisconnectPort -= ClosePortCmpRX;
                compassRX.OnReadByte -= ReadByteCmpRX;
                compassRX.OnWriteByte -= WriteByteCmpRX;

                compassRX = null;
            }

        }

    }
}
