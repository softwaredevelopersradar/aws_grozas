﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WPFControlConnection;
using static DLL_Compass.Compass;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        private void CmpRXControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {            
            try
            {
                if (compassRX != null)
                    DisconnectCmpRX();
                else
                    ConnectCmpRX();
            }

            catch
            { }
        }

        private  async void OpenPortCmpRX()
        {
            mainWindowViewModel.StateConnectionCmpRX = ConnectionStates.Connected;

            await Task.Delay(3000);

            GetCompassRX();
        }

        private void ClosePortCmpRX()
        {
            mainWindowViewModel.StateConnectionCmpRX = ConnectionStates.Disconnected;
        }

        private void ReadByteCmpRX(byte[] data)
        {
            
        }

        private void WriteByteCmpRX(byte[] data)
        {
            
        }

        private void GetCompassRX()
        {
            if (compassRX != null)
                try
                {
                    //Random rnd = new Random();

                    //mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle = rnd.Next(0, 359);


                    mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle = compassRX._angles.head;

                }
                catch { }

        }

    }
}
