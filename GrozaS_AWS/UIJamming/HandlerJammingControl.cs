﻿
using System;
using System.Collections.Generic;
using GrozaSModelsDBLib;
using UISuppressSource;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
//using CommonModels.Models;

using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        //private List<int> ReservedId = new List<int>() { 11, 12, 13, 14, 21, 22, 23, 24 };

        //private void JammingDrone_CheckRecordsInDb(List<TableSuppressSource> data)
        //{
        //    foreach(TableSuppressSource source in data)
        //    {
        //        if(source.)
        //    }
        //}

        private void JammingDrone_SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                _jamManager1.SetLanguge(language);

                _jamManager2.SetLanguge(language);
            });
        }

        private void JammingDrone_UpdateSuppressSources(List<TableSuppressSource> data)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (TableSuppressSource record in data)
                {
                    if (record.Id / 10 == 1)
                    {
                        _jamManager1.UpdateSuppressSource((FrequencyRange)(record.Id % 10), record);
                        continue;
                    }

                    if (record.Id / 10 == 2)
                    {
                        _jamManager2.UpdateSuppressSource((FrequencyRange)(record.Id % 10), record);
                    }
                }
            });
        }


        private void JammingDrone_UpdateSuppressGnss(List<TableSuppressGnss> data)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (TableSuppressGnss record in data)
                {
                    if (record.Id / 10 == 1)
                    {
                        _jamManager1.UpdateSuppressGnss(record);
                        continue;
                    }

                    if (record.Id / 10 == 2)
                    {
                        _jamManager2.UpdateSuppressGnss(record);
                    }
                }
            });
        }


        #region States
        private void JammingDrone_UpdateStateRange100_500(UIJammingManager manager, State status)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                 manager.UpdateStateRange100_500(status);
            });
        }

        private void JammingDrone_UpdateStateRange500_2500(UIJammingManager manager, State status)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateStateRange500_2500(status);
            });
        }

        private void JammingDrone_UpdateStateRange2500_6000(UIJammingManager manager, State status)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateStateRange2500_6000(status);
            });
        }

        private void JammingDrone_UpdateStateGnss(UIJammingManager manager, State status)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateStateGnss(status);
            });
        }

        private void JammingDrone_UpdateStateSpoof(UIJammingManager manager, State status)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateStateSpoof(status);
            });
        }

        private void JammingDrone_UpdateAllStates(UIJammingManager manager, List<State> states)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateAllState(states);
            });
        }

        private void JammingDrone_UpdateAntennaState(UIJammingManager manager, AntennaMode state)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.UpdateAntenna(state);
            });
        }

        private void JammingDrone_UpdateIsActiveSpoofing(UIJammingManager manager, bool isActive)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                manager.IsActiveSpoofing = isActive;
            });
        }
       
        #endregion


        private void JammingDrone_OnLocalPropChanged()
        {
            if (mainWindowViewModel.clientDB != null && mainWindowViewModel.clientDB.IsConnected())
            {
                JammingDrone_LoadSuppressSources();
            }
        }


        #region Control EventHandlers

        private void JammingDrone_OnChangeRecord(object sender, (FrequencyRange?, TableSuppressSource) e)
        {
            var record = e.Item2;
            record.Id = CountId(sender, (int)e.Item1);

            if (mainWindowViewModel.clientDB == null)
                return;

            mainWindowViewModel.clientDB?.Tables[NameTable.TableSuppressSource].ChangeAsync(record);
        }

        private int CountId(object sender, int range)
        {
            if ((UIJammingManager)sender == _jamManager1)
            {
                return 10 + range;
            }

            if ((UIJammingManager)sender == _jamManager2)
            {
                return 20 + range;
            }

            return -1;
        }


        

        private void JammingDrone_GnssStateChanged(object sender, (bool, List<TableSuppressGnss>) e)
        {
            var record = e.Item2.FirstOrDefault();
            record.Id = CountId(sender, (int)record.Type);
        
            int amp = (int)record.Id / 10;

            if (e.Item1 == false)
                SendRadiatOffAmp(amp, 4);

            else
            {
                List<TableSuppressGnss> listNav =  e.Item2.Where(x=>x.Type == TypeGNSS.Gps).ToList();

                bool[] Gps = GetLetterNavigation(e.Item2, TypeGNSS.Gps);
                bool[] Glonass = GetLetterNavigation(e.Item2, TypeGNSS.Glonass);
                bool[] Beidou = GetLetterNavigation(e.Item2, TypeGNSS.Beidou);

                if (Gps[0] || Gps[1] || Glonass[0] || Glonass[1] || Beidou[0] || Beidou[1])
                    SendSetGNSSAmp(amp, SetTypeNavigation(Gps, Glonass, Beidou));
                else
                    SendRadiatOffAmp(amp, 4);
            }

            NavigationJamming SetTypeNavigation(bool[] gps, bool[] glonass, bool[] beidou)
            {
                NavigationJamming navigation = new NavigationJamming();

                if (beidou[0])
                {
                    navigation.GpsL1 = true;
                    navigation.GlonassL1 = true;
                    navigation.GNSSL1 = true;
                }
                else
                {
                    navigation.GpsL1 = gps[0];                    
                    navigation.GlonassL1 = glonass[0];                  
                    navigation.GNSSL1 = beidou[0];
                   
                }

                if (beidou[1])
                {
                    navigation.GpsL2 = true;
                    navigation.GlonassL2 = true;
                    navigation.GNSSL2 = true;
                }
                else
                {
                    navigation.GpsL2 = gps[1];
                    navigation.GlonassL2 = glonass[1];
                    navigation.GNSSL2 = beidou[1];

                }

                return navigation;
            }

            bool[] GetLetterNavigation(List<TableSuppressGnss> listNavigation, TypeGNSS type)
            {
                bool[] res = new bool[2];

                try
                {
                    res[0] = listNavigation.Where(x => x.Type == type).FirstOrDefault().L1;
                    res[1] = listNavigation.Where(x => x.Type == type).FirstOrDefault().L2;
                }
                catch
                { }

                return res;
            }
        }


        private void JammingDrone_OnDoubleClickRange(object sender, TableSuppressSource e)
        {
            var findDrone = CurrentSourcesDF.FirstOrDefault(x => x.ID == e.TableSourceId);
            if (findDrone == null)
                return;

            Coord coordCenter = findDrone.Track.Last().Coordinate;
            mainWindowViewModel.LocationCenter = coordCenter;
        }

        private void _control_OnAskStatus(object sender, EventArgs e)
        {
            if ((UIJammingManager)sender == _jamManager1)
            {
                SendGetStatusAmp(1);
            }

            if ((UIJammingManager)sender == _jamManager2)
            {
                SendGetStatusAmp(2);
            }
        }

        private void _control_OnStopAll(object sender, EventArgs e)
        {
            if ((UIJammingManager)sender == _jamManager1)
            {
                _jamManager1.IsActiveSpoofing = false;

                SendSetModeSpoofing(false, 1);

                SendRadiatOffAmp(1,0);
            }

            if ((UIJammingManager)sender == _jamManager2)
            {
                _jamManager2.IsActiveSpoofing = false;

                SendSetModeSpoofing(false, 2);

                SendRadiatOffAmp(2,0);
            }
        }

        //private void JD_OnActivationSpoofChanged(object sender, bool e)
        //{
        //    if ((UIJammingManager)sender == _jamManager1)
        //    {
        //        if (e)
        //        {
        //            ProcessAmpUpd1 = new System.Diagnostics.Process();
        //            ProcessAmpUpd1.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "\\Spoof_Amp\\" + "GPS Generation 1.exe";


        //            System.Diagnostics.Process[] pr2 = System.Diagnostics.Process.GetProcesses();

        //            bool blProc = false;
        //            int i = 0;
        //            while (i < pr2.Length)
        //            {
        //                if (pr2[i].ProcessName == "GPS Generation 1.exe")
        //                {
        //                    blProc = true;
        //                    i = pr2.Length;
        //                }
        //                i++;
        //            }

        //            if (!blProc)
        //                try
        //                {
        //                    ProcessAmpUpd1.Start();
        //                }
        //                catch
        //                { }
                        
        //            SendSetSpoofAmp(1);
        //        }
                    
        //        else
        //            SendRadiatOffAmp(1, 5);

        //    }

        //    if ((UIJammingManager)sender == _jamManager2)
        //    {
        //        if (e)
        //        {
        //            ProcessAmpUpd2 = new System.Diagnostics.Process();
        //            ProcessAmpUpd2.StartInfo.FileName = AppDomain.CurrentDomain.BaseDirectory + "\\Spoof_Amp\\" + "GPS Generation 2.exe";


        //            System.Diagnostics.Process[] pr2 = System.Diagnostics.Process.GetProcesses();

        //            bool blProc = false;
        //            int i = 0;
        //            while (i < pr2.Length)
        //            {
        //                if (pr2[i].ProcessName == "GPS Generation 2.exe")
        //                {
        //                    blProc = true;
        //                    i = pr2.Length;
        //                }
        //                i++;
        //            }

        //            if (!blProc)
        //                try
        //                {
        //                    ProcessAmpUpd2.Start();
        //                }
        //                catch
        //                { }

        //            //if (StateAmp1.Last().Radiation == StateMode.Ok)
        //            //{
        //            //    MessageBox.Show(SMessages.messageErrorDoubleSpoofing);
        //            //}

        //            //else
        //                SendSetSpoofAmp(2);
        //        }
            
                    
        //        else
        //        SendRadiatOffAmp(2, 5);


        //}
        //}


        private void JD_OnActivationSpoofChanged(object sender, bool e)
        {
            //TODO: написать проверку 
            if (((UIJammingManager)sender == _jamManager1 && _jamManager2.IsActiveSpoofing) ||
                ((UIJammingManager)sender == _jamManager2 && _jamManager1.IsActiveSpoofing))
            {
                MessageBox.Show(SMessages.messageErrorDoubleSpoofing);
                return;
            }
                

            if ((UIJammingManager)sender == _jamManager1)
            {
                if (e)
                {
                    SendParamSpoofing(spoofingWindow.SimulationParamInput);
                    Thread.Sleep(200);
                    SendSetModeSpoofing(true, 1);                    

                    SendSetSpoofAmp(1);
                }

                else
                {
                    SendSetModeSpoofing(false, 1);
                    SendRadiatOffAmp(1, 5);
                }
                   

                JammingDrone_UpdateIsActiveSpoofing(_jamManager1, e);

            }

            if ((UIJammingManager)sender == _jamManager2)
            {
                if (e)
                {

                    SendParamSpoofing(spoofingWindow.SimulationParamInput);
                    Thread.Sleep(200);
                    SendSetModeSpoofing(true, 2);

                    SendSetSpoofAmp(2);
                }
                else
                {
                    SendSetModeSpoofing(false, 2);
                    SendRadiatOffAmp(2, 5);
                }
                    

                JammingDrone_UpdateIsActiveSpoofing(_jamManager2, e);
            }
        }


        private void JD_OnActivationChanged(object sender, List<(FrequencyRange?, bool)> e)
        {
            if ((UIJammingManager)sender == _jamManager1)
            {

                List<TableSuppressSource> listSources = new List<TableSuppressSource>();
              
                foreach (var s in e)
                {
                    int id_find = 10 + (int)s.Item1;
                    listSources.Add(lSuppressSource.Where(x => x.Id == id_find).FirstOrDefault());

                    if (!s.Item2)
                    {
                        SendRadiatOffAmp(1, (byte)s.Item1);
                        Log.SetSourceJ(false, listSources);
                        return;
                    }
                        
                    
                }

                SendSetLetterAmp(1, 0, listSources);
                Log.SetSourceJ(true, listSources);


            }


            if ((UIJammingManager)sender == _jamManager2)
            {

                List<TableSuppressSource> listSources = new List<TableSuppressSource>();

                foreach (var s in e)
                {
                    int id_find = 20 + (int)s.Item1;
                    listSources.Add(lSuppressSource.Where(x => x.Id == id_find).FirstOrDefault());

                    if (!s.Item2)
                    {
                        SendRadiatOffAmp(2, (byte)s.Item1);
                        Log.SetSourceJ(false, listSources);
                        return;
                    }

                    
                }

                SendSetLetterAmp(2, 0, listSources);
                Log.SetSourceJ(true, listSources);


            }

          
        }


        private void _control_OnAntennaChanged(object sender, UISuppressSource.AntennaMode e)
        {
            if ((UIJammingManager)sender == _jamManager1)
            {
                SendTypeAntenna(1, (TypeAntenna)((byte)e));
            }

            if ((UIJammingManager)sender == _jamManager2)
            {
                SendTypeAntenna(2, (TypeAntenna)((byte)e));
            }
        }

        #endregion
    }
}
