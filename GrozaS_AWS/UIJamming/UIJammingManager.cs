﻿using System;
using System.Collections.Generic;
using UISuppressSource;
using GrozaSModelsDBLib;

namespace GrozaS_AWS
{
    public class UIJammingManager
    {
        private JammingView _control;

        public event EventHandler<(FrequencyRange?, TableSuppressSource)> OnChangeRecord;       
        public event EventHandler<(bool, List<TableSuppressGnss>)> GnssStateChanged;
        public event EventHandler<TableSuppressSource> OnDoubleClickRange;

        
        public event EventHandler<List<(FrequencyRange?, bool)>> OnActivationChanged;
        public event EventHandler<bool> OnActivationSpoofChanged;
        public event EventHandler<AntennaMode> OnAntennaChanged;

        public event EventHandler OnStopAll;
        public event EventHandler OnAskStatus;


        public bool IsActiveSpoofing
        {
            get { return _control.IsSpoofingActive; }
            set
            {
                if (_control.IsSpoofingActive == value) return;

                _control.IsSpoofingActive = value;
            }
        }

        public UIJammingManager(JammingView control)
        {
            _control = control;
            InitJammingControl();
        }

        public void InitJammingControl()
        {
            _control.OnDoubleClickRange += JammingDrone_OnDoubleClickRange;
            _control.OnChangeRecord += JammingDrone_OnChangeRecord;
            _control.GnssStateChanged += JammingDrone_GnssStateChanged;           
           
            _control.OnActivationChanged += JD_OnActivationChanged;
            _control.OnActivationSpoofChanged += JD_OnActivationSpoofChanged;
            _control.OnStopAll += _control_OnStopAll; 
            _control.OnAskStatus += _control_OnAskStatus;
            _control.OnAntennaChanged += _control_OnAntennaChanged;
        }

       

        private void _control_OnAskStatus(object sender, EventArgs e)
        {
            OnAskStatus?.Invoke(this, null);
        }

        private void _control_OnStopAll(object sender, EventArgs e)
        {
            OnStopAll?.Invoke(this, null);
        }

        private void JD_OnActivationSpoofChanged(object sender, bool e)
        {
            OnActivationSpoofChanged?.Invoke(this, e);
        }

        private void JD_OnActivationChanged(object sender, List<(FrequencyRange?, bool)> e)
        {
            OnActivationChanged?.Invoke(this, e);
        }


        #region Commands
        public void InitDroneTypeDictionary(Dictionary<byte, string> types)
        {
            _control.InitTypes(types);
        }

        public void InitJammingParametersByTypes(Dictionary<string, JammingParameters> types)
        {
            _control.InitJammingParametersByTypes(types);
        }
        

        public void UpdateSuppressSource(FrequencyRange range, TableSuppressSource data)
        {
            _control.SetSuppressSource(range, data);
        }


        public void UpdateSuppressGnss(TableSuppressGnss data)
        {
            _control.SetSuppressGnss(data);
        }


        public void UncheckedAllButtons()
        { 
            _control.UncheckedAllButtons();
        }

        public void SetLanguge(DllGrozaSProperties.Models.Languages language)
        {
            _control.CLanguage = language;
        }

        public void UpdateAntenna(AntennaMode data)
        {
            _control.Antenna = data;
        }
        
        #endregion


        #region States
        public void UpdateStateRange100_500(State state)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            _control.SetStateRange100_500(state);
            //});
        }

        public void UpdateStateRange500_2500(State state)
        {
           _control.SetStateRange500_2500(state);
        }

        public void UpdateStateRange2500_6000(State state)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            _control.SetStateRange2500_6000(state);
            //});
        }

        public void UpdateStateGnss(State state)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            _control.SetStateGnss(state);
            //});
        }

        public void UpdateStateSpoof(State state)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            _control.SetStateSpoof(state);
            //});
        }

        public void UpdateAllState(List<State> states)
        {
            if (states.Count != 5)
                return;

            _control.SetStateRange100_500(states[0]);
            _control.SetStateRange500_2500(states[1]);
            _control.SetStateRange2500_6000(states[2]);
            _control.SetStateGnss(states[3]);
            _control.SetStateSpoof(states[4]);
        }
        #endregion



        #region Control EventHandlers

        private void JammingDrone_OnChangeRecord(object sender, (FrequencyRange?, TableSuppressSource) e)
        {
            OnChangeRecord?.Invoke(this, e);
        }


        //private void JammingDrone_GnssStateChanged(object sender, TableSuppressGnss e)
        //{
        //    GnssStateChanged?.Invoke(this, e);
        //}

        private void JammingDrone_GnssStateChanged(object sender, (bool, List<TableSuppressGnss>) e)
        {
            GnssStateChanged?.Invoke(this, e);
        }


        private void JammingDrone_OnDoubleClickRange(object sender, TableSuppressSource e)
        {
            OnDoubleClickRange?.Invoke(this, e);
        }

        private void _control_OnAntennaChanged(object sender, AntennaMode e)
        {
            OnAntennaChanged?.Invoke(this, e);
        }
        #endregion
    }
}

