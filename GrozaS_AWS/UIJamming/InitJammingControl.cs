﻿
using System.Collections.Generic;
using GrozaSModelsDBLib;

namespace GrozaS_AWS
{
    using System;

    using GrozaS_AWS.Models;

    using UISuppressSource;

    public partial class MainWindow
    {
        private UIJammingManager _jamManager1;
        private UIJammingManager _jamManager2;

        private YamlJammingParameters _jammingYaml = new YamlJammingParameters();

        public void InitJammingControl()
        {
            _jamManager1 = new UIJammingManager(JammingDrone);
            _jamManager2 = new UIJammingManager(JammingDrone2);

            SubscribeJamManagerEvents(_jamManager1);
            SubscribeJamManagerEvents(_jamManager2);

            var jammingParams = _jammingYaml.ReadRanges(AppDomain.CurrentDomain.BaseDirectory + "JammingByTypes.yaml");
            JammingDrone_InitJammingParametersByTypes(jammingParams);
        }


        private void SubscribeJamManagerEvents(UIJammingManager manager)
        {
            manager.OnDoubleClickRange += JammingDrone_OnDoubleClickRange;
            manager.OnChangeRecord += JammingDrone_OnChangeRecord;
            manager.GnssStateChanged += JammingDrone_GnssStateChanged;            
            manager.OnActivationChanged += JD_OnActivationChanged;
            manager.OnActivationSpoofChanged += JD_OnActivationSpoofChanged;
            manager.OnStopAll += _control_OnStopAll;
            manager.OnAskStatus += _control_OnAskStatus;
            manager.OnAntennaChanged += _control_OnAntennaChanged;
        }

        

        public void JammingDrone_InitDroneTypeDictionary(Dictionary<byte, string> types)
        {
            _jamManager1.InitDroneTypeDictionary(types);
            _jamManager2.InitDroneTypeDictionary(types);
        }

        public void JammingDrone_InitJammingParametersByTypes(Dictionary<string, JammingParameters> types)
        {
            _jamManager1.InitJammingParametersByTypes(types);
            _jamManager2.InitJammingParametersByTypes(types);
        }
        


        private async void JammingDrone_LoadSuppressSources()
        {
            try
            {
                var tableJamming = await mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressSource].LoadAsync<TableSuppressSource>();
                JammingDrone_UpdateSuppressSources(tableJamming);
            }
            catch { }
        }


        private async void JammingDrone_LoadSuppressGnss()
        {
            try
            {
                var tableJamming = await mainWindowViewModel.clientDB.Tables[NameTable.TableSuppressGnss].LoadAsync<TableSuppressGnss>();
                JammingDrone_UpdateSuppressGnss(tableJamming);
            }
            catch { }
        }
    }
}
