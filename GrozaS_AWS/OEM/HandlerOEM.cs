﻿using GrozaS_AWS.Models;
using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WPFControlConnection;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        private void OEMControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {            
            try
            {
                if (udpOEM != null && udpOEM.IsConnected)
                    DisconnectOEM();
                else
                    ConnectOEM();
            }

            catch
            { }            
        }

        private void ConnectPortOEM(object sender,EventArgs e)
        {
            mainWindowViewModel.StateConnectionOEM = ConnectionStates.Connected;
        }

        private void DisconnectPortOEM(object sender, EventArgs e)
        {
            mainWindowViewModel.StateConnectionOEM = ConnectionStates.Disconnected;
        }

        private void ConnectEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void DisconnectEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void TargetEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void AnglesEchoOEM(object sender, AnglesEventArgs e)
        {           
            try
            {
                UpdateMapDirectionOEM(e.Azimuth);
            }
            catch { }
        }

        private void VisionEchoOEM(object sender, AnglesEventArgs e)
        {
            
            mainWindowViewModel.DirectionOEM.Azimuth = e.Azimuth;
        }

        private void SetAngleOEM(float e)
        {
            try
            {               
                udpOEM.SendTargetDesignation(mainWindowViewModel.SetAngleOEM(e, mainWindowViewModel.DirectionOEM).Result, 0);
            }
            catch { }
        }
    }
}
