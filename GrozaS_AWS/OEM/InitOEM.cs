﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public UdpOEM udpOEM;

        private void ConnectOEM()
        {
            if (udpOEM != null)
                DisconnectOEM();

            try
            {
                udpOEM = new UdpOEM();

                udpOEM.OnConnectPort += ConnectPortOEM;
                udpOEM.OnDisconnectPort += DisconnectPortOEM;
                udpOEM.OnConnectEcho += ConnectEchoOEM;
                udpOEM.OnDisconnectEcho += DisconnectEchoOEM;
                udpOEM.OnTargetEcho += TargetEchoOEM;
                udpOEM.OnAnglesEcho += AnglesEchoOEM;
                udpOEM.OnVisionEcho += VisionEchoOEM;


                udpOEM.Connect(mainWindowViewModel.LocalPropertiesVM.EOM.IpAddressLocal, mainWindowViewModel.LocalPropertiesVM.EOM.PortLocal,
                                       mainWindowViewModel.LocalPropertiesVM.EOM.IpAddressRemoute, mainWindowViewModel.LocalPropertiesVM.EOM.PortRemoute);


                udpOEM.SendConnect();
            }
            catch{ }

        }

        private void DisconnectOEM()
        {

            if (udpOEM != null)
            {
                udpOEM.Disconnect();

                udpOEM.OnConnectPort -= ConnectPortOEM;
                udpOEM.OnDisconnectPort -= DisconnectPortOEM;
                udpOEM.OnConnectEcho -= ConnectEchoOEM;
                udpOEM.OnDisconnectEcho -= DisconnectEchoOEM;
                udpOEM.OnTargetEcho -= TargetEchoOEM;
                udpOEM.OnAnglesEcho -= AnglesEchoOEM;
                udpOEM.OnVisionEcho -= VisionEchoOEM;

                udpOEM = null;
            }

        }

    }
}

