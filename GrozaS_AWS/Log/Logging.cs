﻿using GrozaSModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UISource;

namespace GrozaS_AWS.Log
{
    internal class Logging
    {
        System.Timers.Timer tmrScreenCheck = new System.Timers.Timer();
        public string PathToLog { get; set; }
        public string FileToLog { get; set; }

        public int LogLifetimeDays { get; set; } = 1;

        public int LogMaxSizeMB { get; set; } = 1024; //20480

        public Logging()
        {
            InitializeTimerScreenshotCheck();
        }

        private void InitializeTimerScreenshotCheck()
        {
            tmrScreenCheck.Elapsed += TickTimerScreenCheck;
            tmrScreenCheck.Interval = 600000; //10 min

            tmrScreenCheck.AutoReset = true;
            tmrScreenCheck.Enabled = true;
        }


        void TickTimerScreenCheck(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string[] folders = Directory.GetDirectories(PathToLog);
                string todayFolder = string.Empty;
                foreach (string folder in folders)
                {
                    DateTime folderDate;
                    if (DateTime.TryParseExact(Path.GetFileName(folder), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out folderDate))
                    {
                        TimeSpan difference = DateTime.Now - folderDate;
                        if (difference.TotalDays < 1)
                        {
                            todayFolder = folder;
                        }

                        if (difference.TotalDays > LogLifetimeDays)
                        {
                            Directory.Delete(folder, true);
                        }
                    }
                }

                DeleteOldDirectoriesBySize(folders, todayFolder);
            }
            catch
            { }

        }

        public void DeleteOldDirectoriesBySize(string[] folders, string todayFolder)
        {
            Array.Sort(folders, StringComparer.OrdinalIgnoreCase);
            Array.Reverse(folders);

            var startDeleting = false;
            long totalMB = 0;

            foreach (string folder in folders)
            {
                if (startDeleting)
                {
                    Directory.Delete(folder, true);
                    continue;
                }

                //long totalFolderSizeKB = 0;

                long folderSizeByte = GetFolderSize(folder);
                //string[] files = Directory.GetFiles(folder);
                //foreach (string file in files)
                //{
                //    FileInfo fileInfo = new FileInfo(file);
                //    totalFolderSizeKB += fileInfo.Length / 1024;
                //}

                //totalMB += totalFolderSizeKB / 1024;

                totalMB += folderSizeByte / 1024 / 1024;

                if (totalMB > LogMaxSizeMB)
                {
                    startDeleting = true;
                    if (!folder.Equals(todayFolder))
                    {
                        Directory.Delete(folder, true);
                    }
                    
                }
            }
        }

        public static long GetFolderSize(string folderPath)
        {
            DirectoryInfo directory = new DirectoryInfo(folderPath);

            if (!directory.Exists)
            {
                throw new DirectoryNotFoundException($"Directory '{folderPath}' not found.");
            }

            long folderSize = 0;
            
            FileInfo[] files = directory.GetFiles();
            foreach (FileInfo file in files)
            {
                folderSize += file.Length;
            }
            
            DirectoryInfo[] subDirectories = directory.GetDirectories();
            foreach (DirectoryInfo subDirectory in subDirectories)
            {
                folderSize += GetFolderSize(subDirectory.FullName);
            }

            return folderSize;
        }


        public void SetMode(Mode mode)
        
        {
            string Regime = "";
            switch (mode)
            {
                case Mode.STOP:
                    Regime = "  Set Stop";
                    break;

                case Mode.ELINT:
                    Regime = "  Set RadioIntellegence";
                    break;

                case Mode.JAMMING:
                    Regime = "  Set Jamming";
                    break;

                default:
                    break;
            }

            string strFull = Regime;
           
            SaveLog(strFull);
        }

        public void SetMode(Mode mode, (Mode, TimeSpan) previous)

        {
            string Regime = " Mode " + previous.Item1.ToString() + " was stopped. Total duration: " + previous.Item2.TotalSeconds.ToString("0.0") + " s." + "\n";

            Regime += " Set " + mode.ToString() + " mode";

            //switch (mode)
            //{
            //    case Mode.STOP:
            //        Regime += "  Set Stop";
            //        break;

            //    case Mode.ELINT:
            //        Regime += "  Set RadioIntellegence";
            //        break;

            //    case Mode.JAMMING:
            //        Regime += "  Set Jamming";
            //        break;

            //    default:
            //        break;
            //}

            string strFull = Regime;

            SaveLog(strFull);
        }

        public void SetModeSpoofing(bool active, string satellites, DateTime startSpoof)

        {
            string strFull = active ? "  Start spoofing " + satellites : "  Stop spoofing. Total duration: " + (DateTime.Now - startSpoof).TotalSeconds.ToString("0.0") + " s.";
            

            SaveLog(strFull);
        }

        public void SetSourceRI(ObservableCollection<RecDroneModel> e)
        {
            List<string> list = new List<string>();
            foreach (RecDroneModel source in e)
            {
                list.Add("  ID: " + source.ID.ToString());
                list.Add("  Type: " + source.Type.ToString());
                list.Add("  TypeRSM: " + source.TypeRSM.ToString());
                list.Add("  Frequency: " + source.Frequency.ToString() + " MHz");
                list.Add("  FrequencyRX: " + source.FrequencyRX.ToString() + " MHz");
                list.Add("  Band: " + source.Band.ToString() + " MHz");
                list.Add("  Update time: " + source.TimeUpdate.ToString());
                list.Add("  Bearing Own: " + source.BearingOwn.ToString() + "°");
                list.Add("  Distance Own: " + (source.DistanceOwn / 1000).ToString("F3") + " km");
                list.Add("  Bearing Linked: " + source.BearingAnother.ToString() + "°");
                list.Add("  Distance Linked: " + (source.DistanceAnother / 1000).ToString("F3") + " km");
                list.Add("  Latitude: " + source.Latitude.ToString());
                list.Add("  Longitude: " + source.Longitude.ToString() + "\n");
                list.Add("\n");
            }

            string[,] strFull = new string[list.Count, 1];
            for (int i = 0; i < list.Count; i++)
                strFull[i, 0] = list[i];


            SaveLog(strFull);
        }

        public void SetSourceRI(TableSource source, Dictionary<byte, string> typeCodeCollection)
        {
            var type = typeCodeCollection.Keys.Contains(source.Type) ? typeCodeCollection[source.Type] : source.Type.ToString();

            string message = "  Detected signal: " + "\n" +
                             //"  ID: " + source.Id.ToString() + "\n" +
                             "  Type: " + type + "\n" +
                             "  Frequency: " + source.Track.Last().FrequencyMHz.ToString() + " MHz" + "\n" +
                             "  Band: " + source.Track.Last().BandMHz.ToString() + " MHz" + "\n" +
                             "  Update time: " + source.Track.Last().Time.ToString() + "\n" +
                             "  Bearing: " + source.Track.Last().Bearing.Last().Bearing.ToString() + "°" +
                             "\n";

            SaveLog(message);
        }


        public void SetSourceRI(ObservableCollection<TableSource> e)
        {
            List<string> list = new List<string>();

            list.Add("Update table Source");

            foreach (TableSource source in e)
            {
                list.Add("  ID: " + source.Id.ToString());
                list.Add("  Type: " + source.Type.ToString());
                list.Add("  TypeRSM: " + source.TypeRSM.ToString());
                list.Add("  Frequency: " + source.Track.Last().FrequencyMHz.ToString() + " MHz");
                list.Add("  FrequencyRX: " + source.Track.Last().FrequencyRX.ToString() + " MHz");
                list.Add("  Band: " + source.Track.Last().BandMHz.ToString() + " MHz");
                list.Add("  Update time: " + source.Track.Last().Time.ToString());
                list.Add("  Bearing Own: " + source.Track.Last().Bearing.Last().ToString() + "°");                
                list.Add("  Bearing Linked: " + source.Track.Last().ToString() + "°");                
                list.Add("  Latitude: " + source.Track.Last().Coordinates.Latitude.ToString());
                list.Add("  Longitude: " + source.Track.Last().Coordinates.Longitude.ToString() + "\n");
                list.Add("\n");
            }

            string[,] strFull = new string[list.Count, 1];
            for (int i = 0; i < list.Count; i++)
                strFull[i, 0] = list[i];


            SaveLog(strFull);
        }

        public void SetSourceJ(bool state, List<TableSuppressSource> e)
        {
            List<string> list = new List<string>();

            list.Add(state ? "Jamming switch on" : "Jamming switch off");

            foreach (TableSuppressSource source in e)
            {
                list.Add("  ID: " + source.Id.ToString());
                list.Add("  Frequency: " + source.FrequencyMHz.ToString());
                list.Add("  Band: " + source.Deviation.ToString());
                list.Add("  Modulation: " + source.Modulation.ToString());
                list.Add("  Manipulation: " + source.Manipulation.ToString() + " MHz");
                list.Add("  ScanSpeed: " + source.ScanSpeed.ToString() + " MHz" + "\n");                
                list.Add("\n");
            }

            string[,] strFull = new string[list.Count, 1];
            for (int i = 0; i < list.Count; i++)
                strFull[i, 0] = list[i];


            SaveLog(strFull);
        }


        public void SetSourceJ(bool state, TableSuppressSource e)
        {
            List<string> list = new List<string>();

            list.Add( state ? "Jamming switch on" : "Jamming switch off");
            
            list.Add("  ID: " + e.Id.ToString());
            list.Add("  Frequency: " + e.FrequencyMHz.ToString());
            list.Add("  Band: " + e.Deviation.ToString());
            list.Add("  Modulation: " + e.Modulation.ToString());
            list.Add("  Manipulation: " + e.Manipulation.ToString() + " MHz");
            list.Add("  ScanSpeed: " + e.ScanSpeed.ToString() + " MHz" + "\n");
            list.Add("\n");


            string[,] strFull = new string[list.Count, 1];
            for (int i = 0; i < list.Count; i++)
                strFull[i, 0] = list[i];


            SaveLog(strFull);
        }


        private void SaveLog(string[,] strLog)
        {
            if (Directory.Exists(PathToLog))
            {
                string actualDirectory = PathToLog + "\\" + DateTime.Now.ToString("yyyy-MM-dd");

                if (!Directory.Exists(actualDirectory))
                    Directory.CreateDirectory(actualDirectory);

                LogFile.BaseLog.AddTTxt(actualDirectory, FileToLog, strLog);

            }
        }



        public async void SaveLog(string strLog)
        {
            try
            {
                if (!Directory.Exists(PathToLog))
                {
                    return;
                }

                string actualDirectory = PathToLog + "\\" + DateTime.Now.ToString("yyyy-MM-dd");

                if (!Directory.Exists(actualDirectory))
                    Directory.CreateDirectory(actualDirectory);

                try
                {
                    using (StreamWriter w = File.AppendText(actualDirectory + "\\" + FileToLog))
                        AppendLog(strLog, w);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                await Task.Delay(500);
                Screenshoter.CreateScreenshot(actualDirectory);
            }
            catch
            {
            }
        }


        private static void AppendLog(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.WriteLine(DateTime.Now.ToString("HH:mm:ss"));
                txtWriter.WriteLine(logMessage);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
