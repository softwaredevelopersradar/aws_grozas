﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

//using System.Windows.Forms;

namespace GrozaS_AWS.Log
{
    public class Screenshoter
    {
        //public static string ScreenshotPath { get; set; } = "./";
        public static string ScreenshotFolder { get; set; } = "Screen";
        //public static int ScreensLifeTimeHours { get; set; } = 1;

        //public static int ScreensMaxSizeMB { get; set; } = 1024; //20480



        //public static void CreateScreenshot()
        //{
        //    try
        //    {
        //        // Capture the entire desktop area
        //        Rectangle screenBounds = GetDesktopBounds();
        //        using (var bitmap = new Bitmap(screenBounds.Width, screenBounds.Height))
        //        {
        //            using (var graphics = Graphics.FromImage(bitmap))
        //            {
        //                graphics.CopyFromScreen(screenBounds.Left, screenBounds.Top, 0, 0, screenBounds.Size);
        //            }

        //            if (!Directory.Exists(ScreenshotPath))
        //            {
        //                return;
        //            }

        //            string actualDirectory = ScreenshotPath + "\\" + ScreenshotFolder;

        //            if (!Directory.Exists(actualDirectory))
        //                Directory.CreateDirectory(actualDirectory);

        //            // Generate a unique filename for the screenshot
        //            string fileName = Path.Combine(actualDirectory, $"{DateTime.Now:yyyy-MM-dd HH-mm-ss}.png");

        //            // Save the screenshot as PNG
        //            bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var t = 9;
        //    }
        //}

        public static void CreateScreenshot(string screenshotPath)
        {
            try
            {
                Rectangle screenBounds = GetDesktopBounds();
                using (var bitmap = new Bitmap(screenBounds.Width, screenBounds.Height))
                {
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.CopyFromScreen(screenBounds.Left, screenBounds.Top, 0, 0, screenBounds.Size);
                    }

                    if (!Directory.Exists(screenshotPath))
                    {
                        return;
                    }

                    string actualDirectory = screenshotPath + "\\" + ScreenshotFolder;

                    if (!Directory.Exists(actualDirectory))
                        Directory.CreateDirectory(actualDirectory);

                    string fileName = Path.Combine(actualDirectory, $"{DateTime.Now:yyyy-MM-dd HH-mm-ss}.jpeg");

                    bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
            catch 
            {
            }
        }

        static Rectangle GetDesktopBounds()
        {
            int left = int.MaxValue;
            int top = int.MaxValue;
            int right = int.MinValue;
            int bottom = int.MinValue;

            foreach (var screen in Screen.AllScreens)
            {
                left = Math.Min(left, screen.Bounds.Left);
                top = Math.Min(top, screen.Bounds.Top);
                right = Math.Max(right, screen.Bounds.Right);
                bottom = Math.Max(bottom, screen.Bounds.Bottom);
            }

            return Rectangle.FromLTRB(left, top, right, bottom);
        }

        ////public static void DeleteOldScreenshots()
        ////{
        ////    try
        ////    {
        ////        var path = ScreenshotPath + "\\" + ScreenshotFolder;
        ////        if (!Directory.Exists(path))
        ////        {
        ////            return;
        ////        }

        ////        string[] screenshotFiles = Directory.GetFiles(path);

        ////        // Iterate through each file and delete if it's older than a certain time threshold (e.g., 7 days)
        ////        DateTime thresholdDate = DateTime.Now.AddHours(-ScreensLifeTimeHours);

        ////        //long size = 0;
        ////        foreach (string file in screenshotFiles)
        ////        {
        ////            FileInfo fileInfo = new FileInfo(file);

        ////            //if (fileInfo.LastWriteTime < thresholdDate || size / (1024 * 1024 * 1024) > ScreensMaxSizeGB )
        ////            //{
        ////            //    File.Delete(file);
        ////            //}
        ////            //else
        ////            //{
        ////            //    size += fileInfo.Length;
        ////            //}

        ////            if (fileInfo.LastWriteTime < thresholdDate)
        ////            {
        ////                File.Delete(file);
        ////            }
        ////        }
        ////    }
        ////    catch 
        ////    {
        ////    }
        ////}


        //public static void DeleteOldScreenshots()
        //{
        //    var path = ScreenshotPath + "\\" + ScreenshotFolder;
        //    if (!Directory.Exists(path))
        //    {
        //        return;
        //    }

        //    string[] screenshotFiles = Directory.GetFiles(path);


        //    DateTime thresholdDate = DateTime.Now.AddHours(-ScreensLifeTimeHours);
            
        //    foreach (string file in screenshotFiles)
        //    {
        //        FileInfo fileInfo = new FileInfo(file);

        //        if (fileInfo.LastWriteTime < thresholdDate)
        //        {
        //            File.Delete(file);
        //        }
        //    }
            
        //    DeleteOldFileBySize(path);
        //}

        //public static double GetFolderSize(string folderPath)
        //{
        //    DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);
        //    long folderSize = directoryInfo.EnumerateFiles("*.*", SearchOption.AllDirectories).Sum(file => file.Length);
        //    return (double)folderSize / (1024 * 1024);
        //}


        //public static void DeleteOldFileBySize(string folderPath)
        //{
        //    DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);
        //    long folderSizeMBytes = directoryInfo.EnumerateFiles("*", SearchOption.AllDirectories).Sum(file => file.Length / 1024) / 1024;
        //    //long maxSizeBytes = ScreensMaxSizeMB * 1024;
        //    if (folderSizeMBytes < ScreensMaxSizeMB)
        //    {
        //        return;
        //    }

        //    long diff = folderSizeMBytes - ScreensMaxSizeMB;

        //    var files = directoryInfo.EnumerateFiles("*", SearchOption.AllDirectories)
        //            .OrderByDescending(f => f.CreationTime)
        //            .ToList();

        //    long deletedFilesSizeKB = 0;
        //    foreach (var file in files)
        //    {
        //        if ((deletedFilesSizeKB + file.Length / 1024) / 1024 > diff)
        //        {
        //            break;
        //        }

        //        deletedFilesSizeKB += file.Length / 1024;
        //        file.Delete();
        //    }
        //}
    }
}
