﻿namespace GrozaS_AWS
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ClientArm;

    using ClientTcpDF;

    using dCountPanels;

    using GrozaSModelsDBLib;

    using SpectrumPanoramaControl;

    using ClientDDF;
    using SpectrumControlLibrary.ViewModels;
    using SpectrumControlLibrary.Models;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public partial class PanoramsWindows 
    {
        private ClientArm _clientDdf;

        private SpectrumControlViewModel vm;

        private float _angleCar;

        private List<FreqRanges> _freqRangesRecon = new List<FreqRanges>();

        private int _globalRangeYMax = 150;

        private int _globalRangeYMin = 0;

        private bool _isAverage;

        private bool _isLog;

        private bool _isPowY;

        private bool _isScaleSet;
            
        private bool _isSqrt;

        private List<bool> _isActiveAntenna = new List<bool>() { false, false, false, false, false, false };

        public float AngleCar
        {
            get => _angleCar;
            set
            {
                _angleCar = value;
                PSControl.SetF(value);
                PSControl.SetTheta(value);
                if (value != 0)
                    PSControl.SetAngleChartWithCar(value);
            }
        }

        public DFTcpClient Client { get; set; }

        private int CountControl { get; set; } = 0;

        public int CountSectors { get; set; } = 16;

        private DynamicPanels<SPControl> DynamicPanels { get; set; }

        private List<FreqRanges> FreqRangesRecon
        {
            get => _freqRangesRecon;
            set
            {
                _freqRangesRecon = value;

                var selected = new List<int>();

                foreach (var val in _freqRangesRecon.Where(val => val.IsActive))
                {
                    for (var i = (int)Math.Floor((val.FreqMinKHz / 1000 - GlobalRangeXMin) / GlobalBandWidthMHz);
                         i <= (int)Math.Floor((val.FreqMaxKHz / 1000 - GlobalRangeXMin) / GlobalBandWidthMHz);
                         i++)
                        if (i<60)
                        selected.Add(i);
                }

                FillRPRect(selected.ToArray());
            }
        }

        public int GlobalBandWidthMHz { get; set; } = 100;

        public int GlobalNumberOfBands { get; set; } = 60;

        public int GlobalRangeXMin { get; set; } = 0;

        private int Th { get; set; } = -50;

        private int GlobalRangeYMax
        {
            get => _globalRangeYMax;
            set
            {
                _globalRangeYMax = value;
                pLibrary.GlobalRangeYmax = value;
            }
        }

        private int GlobalRangeYMin
        {
            get => _globalRangeYMin;
            set
            {
                _globalRangeYMin = value;
                pLibrary.GlobalRangeYmin = value;
            }
        }

        public string IpServer { get; private set; }

        private bool IsAverage
        {
            get => _isAverage;
            set
            {
                _isAverage = value;
                btnAverage.IsChecked = value;
            }
        }

        private bool IsLog
        {
            get => _isLog;
            set
            {
                _isLog = value;
                btnLog.IsChecked = value;
            }
        }

        private bool IsPowY
        {
            get => _isPowY;
            set
            {
                _isPowY = value;
                btnPowerX.IsChecked = value;
            }
        }

        private bool IsScaleSet
        {
            get => _isScaleSet;
            set
            {
                _isScaleSet = value;
                btnAutoScale.IsChecked = value;
            }
        }

        private bool IsSqrt
        {
            get => _isSqrt;
            set
            {
                _isSqrt = value;
                btnSqrt.IsChecked = value;
            }
        }

        private List<double> LocalAntennaPower { get; set; } = new List<double>();

        

        private byte MaxControlInDynamic { get; set; }

        private byte MinControlInDynamic => 1;

        public int Port { get; private set; }

        private Random Random { get; } = new Random();

        private double ScaleIncrement { get; set; }

        public int SelectedAntenna { get; private set; }

        public void UpdateFreqRangesRecon(List<FreqRanges> freqRanges)
        {
            FreqRangesRecon = freqRanges;

            var activeSvb = new List<int>();
            var svbWidth = GlobalBandWidthMHz;

            foreach (var val in FreqRangesRecon.Where(val => val.IsActive))
            {
                for (var i = (int)Math.Floor((val.FreqMinKHz / 1000 - GlobalRangeXMin) / GlobalBandWidthMHz);
                     i <= (int)Math.Floor((val.FreqMaxKHz / 1000 - GlobalRangeXMin) / GlobalBandWidthMHz);
                     i++)
                    activeSvb.Add(i);
            }

            activeSvb = activeSvb.Distinct().ToList();

            ClearSpectrumInSvb();

            //foreach (var s in vm.SpectrumModel.SsrParametrs)
            //    s.IsActive = false;

            //for (int i = 0; i < activeSvb.Count; i++)
            //    vm.SpectrumModel.SsrParametrs[activeSvb[i]].IsActive = true;
               
           


            //for (var i = 0; i < _channels[0].singleViewBands.Count; i++)
            //{
            //    var active = activeSvb.Contains(i);

            //    _channels[0].singleViewBands[i].Active = active;
            //    _channels[1].singleViewBands[i].Active = active;
            //}
        }

        public void UpdateTcpClient(ref ClientArm clientDDF)
        {
            this._clientDdf = clientDDF;
            UpdateSsrs();
        }

        private async void UpdateSsrs()
        {
            var ssrs = await _clientDdf.GetSvbsSetting();
            //vm.SpectrumModel.SsrParametrs

            var ssr = new List<SsrParameters>();

            foreach (var val in ssrs.ListSvb)
            {
                ssr.Add(new SsrParameters((short)val.NumberSvb, val.FreqMin, val.FreqMax));
            }
            vm.SpectrumModel = new SpectrumModel(ssr);
        }


     
    }
}