﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Panorams.EventsArgs
{
    public class SpectrumSourceEventArgs:EventArgs
    {
        public double Frequency { get; private set; }

        public float Band { get; private set; }

        public SpectrumSourceEventArgs(double frequency, float band)
        {
            Frequency = frequency;
            Band = band;
        }
    }
}
