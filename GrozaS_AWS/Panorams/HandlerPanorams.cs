﻿namespace GrozaS_AWS
{
    using System.Windows;

    using ClientArm;

    public partial class MainWindow
    {
        private PanoramsWindows panoramsWindows;
        private bool isHidePanoramsWindow = true;

        private void TDFControlConnection_OnButServerClick(object sender, RoutedEventArgs e)
        {
            InitPanorams();
            panoramsWindows.Client.Init(panoramsWindows.IpServer, panoramsWindows.Port);
        }

        public void InitPanorams()
        {
            panoramsWindows = new PanoramsWindows(ref tcpClientDDF);
            panoramsWindows.Closing += PanoramsWindows_Closing;
            panoramsWindows.AngleCar = mainWindowViewModel.GlobalPropertiesVM.CmpRX.Angle;
            panoramsWindows.OnAddSpectrumSource += PanoramsWindows_OnAddSpectrumSource;

        }

        private void PanoramsWindows_OnAddSpectrumSource(object sender, Panorams.EventsArgs.SpectrumSourceEventArgs e)
        {
            AddNewSource(e.Frequency, e.Frequency, e.Band, 361, 361, 0, 0);

            SetModeDDF(TransmissionPackage.Mode.Bearing);
        }

        public void UpdateTcpClientDDF(ref ClientArm tcpClientDDF)
        {
            panoramsWindows?.UpdateTcpClient(ref tcpClientDDF);
        }

        private void PanoramsWindows_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            panoramsWindows.Hide();
            isHidePanoramsWindow = true;
        }

        private void Create_new_window_panorams_click(object sender, RoutedEventArgs e)
        {
            if (panoramsWindows == null)
                InitPanorams();

            if (isHidePanoramsWindow)
            {
                ShowPanorams();
            }
            else
            {
                HidePanorams();
            }
        }

        private void ShowPanorams()
        {
            panoramsWindows.Show();
            isHidePanoramsWindow = false;
        }

        private void HidePanorams()
        {
            panoramsWindows.Hide();
            isHidePanoramsWindow = true;
        }
    }
}