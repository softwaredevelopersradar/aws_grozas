﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Panorams
{
    internal class PanoramsSetting
    {
        public byte maxCountPanoramsInDynamic { get; set; }

        public int minVisibleY { get; set; }

        public int maxVisibleY { get; set; }

        public List<Tuple<double, double>> freqInPanorams { get; set; }

        public PanoramsSetting()
        {
        }
    }
}