﻿namespace GrozaS_AWS
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using ClientArm;  
    using dCountPanels;
    using DllGrozaSProperties.Models.Local;
    using GrozaS_AWS.Models;
    using GrozaS_AWS.Panorams;
    using GrozaS_AWS.Panorams.EventsArgs;
    using GrozaSModelsDBLib;
    using Newtonsoft.Json;
    using SpectrumControlLibrary.Models;
    using SpectrumControlLibrary.ViewModels;

    using SpectrumPanoramaControl;
    using TransmissionPackage;
    using UIMap.Models;

    /// <summary>
    /// Interaction logic for PanoramsWindows.xaml
    /// </summary>
    public partial class PanoramsWindows : Window, INotifyPropertyChanged
    {
        SpectrumControlViewModel panoramaModel;

        ParamAntennaPower paramAntennaPower = new ParamAntennaPower();

        

        private ObservableCollection<AntennaPower> _listAntenna = new ObservableCollection<AntennaPower>();
        public ObservableCollection<AntennaPower> ListAntenna
        {
            get { return _listAntenna; }
            set
            {
                _listAntenna = value;                
                OnPropertyChanged();
            }
        }


        private bool _activeTechnicMode = true;
        public bool ActiveTechnicMode
        {
            get { return _activeTechnicMode; }
            set
            {
                _activeTechnicMode = value;
                SetAntennaActiveServer();
                OnPropertyChanged();
            }
        }


        private ModeSpectrum _activeSpectrumMode = ModeSpectrum.Instantaneous;
        public ModeSpectrum ActiveSpectrumMode
        {
            get { return _activeSpectrumMode; }
            set
            {
                _activeSpectrumMode = value;
                
                OnPropertyChanged();
                SetActiveSpectrumMode();
            }
        }


        private bool _activeBearingMode = false;
        public bool ActiveBearingMode
        {
            get { return _activeBearingMode; }
            set
            {
                _activeBearingMode = value;                
                OnPropertyChanged();
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion




        public event EventHandler <SpectrumSourceEventArgs> OnAddSpectrumSource;
        public PanoramsWindows(ref ClientArm client)
        {
            DataContext = this;
            SetDeploymentKey();
            InitializeComponent();

            InitPanoramaModel();

            InitControls();
            InitHotKeys();
           
            PSControl.SetCarAngle(Random.NextDouble() * 360);

            _clientDdf = client;


            vm.PropertyChanged += Vm_PropertyChanged;

           

            ListAntenna = new ObservableCollection<AntennaPower>();
            ListAntenna.Add(new AntennaPower() { Antenna = 1, Channel = 1, Power = 0});
            ListAntenna.Add(new AntennaPower() { Antenna = 2, Channel = 2, Power = 0 });
            ListAntenna.Add(new AntennaPower() { Antenna = 3, Channel = 1, Power = 0 });
            ListAntenna.Add(new AntennaPower() { Antenna = 4, Channel = 2, Power = 0 });
            ListAntenna.Add(new AntennaPower() { Antenna = 5, Channel = 1, Power = 0 });
            ListAntenna.Add(new AntennaPower() { Antenna = 6, Channel = 2, Power = 0 });
        }

        private void Vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "BandVisibility":
                    SetActiveBearing();
                    break;

                case "ConstantLines":
                    SetThreshold();
                    break;


                default:
                    break;
            }
        }


        private void SetActiveBearing()
        {            
            _clientDdf.SetActiveBearingAsync(new TransmissionPackage.BandBearingRequest()
            {
                FreqMin = vm.Band.Visible ? (Int32)(vm.Band.ValueBegin * 1000) : 0,
                FreqMax = vm.Band.Visible ? (Int32)(vm.Band.ValueEnd * 1000) : 0
            });
        }

        private void SetThreshold()
        {        
            _clientDdf.SetThAsync((Int32)(vm.ConstantLines[0].Value));
        }

        private static List<double> GenerateData(int countPoint)
        {
            var random = new Random();
            var doubles = new List<double>();
            for (var i = 0; i < countPoint; i++)
            {
                if (i > 300 && i < 305)
                {
                    doubles.Add(random.Next(-14, -10));
                    continue;
                }

                doubles.Add(random.Next(-120, -80));
            }

            return doubles;
        }

        private void InitPanoramaModel()
        {
            vm = (SpectrumControlViewModel)pLibraryNew.DataContext;

            
            //vm.MasterColor = Colors.Aqua;
            //vm.SlaveColor = Colors.Yellow;
            vm.IntensityControlViewModel.IntensityVisibility = Visibility.Collapsed;


        }

        private void FillRPRect(int[] array)
        {
            
            pLibrary.FillRPRectangles(array);


            List<int> listActiveSVB = new List<int>(array.ToList());


            for (int i = 0; i < vm.SpectrumModel.SsrParametrs.Count; i++)
            {
                vm.SpectrumModel.SsrParametrs[i].IsActive = listActiveSVB.Contains(i + 1);

                //vm.SpectrumModel.SsrParametrs[i]
            }


        }

        private void AddOneItemOnCB()
        {
            CountControl++;

            var comboBoxItem = new ComboBoxItem() { Content = CountControl.ToString() };

            cbSelectDynamic.Items.Add(comboBoxItem);
            cbSelectDynamic.SelectedIndex = CountControl - 1;
        }   

        private void BtnCtrlDClick(object sender, RoutedEventArgs e)
        {
            RemoveLastTabInDynamicPanel();
        }

        private void BtnCtrlLClick(object sender, RoutedEventArgs e)
        {
            LoadPanoramsSettings(false);
        }
        private void BtnCtrlBtn1Click(object sender, RoutedEventArgs e)
        {
            RefreshParamsInGraphics();
        }

        private void BtnCtrlRClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() =>
                {
                    Random rand = new Random();
                    //vm.SpectrumModel.SsrParametrs[rand.Next(0,10)].DataMaster[0] = GenerateData(1000);
                    //vm.SpectrumModel.SsrParametrs[rand.Next(0,10)].DataSlave[0] = GenerateData(1200);
                });
            
            // RefreshInDynamicPanelWithBand(cbSelectDynamic.SelectedIndex);
        }

        private void BtnCtrlСClick(object sender, RoutedEventArgs e)
        {
            CreateTabInDynamicPanel();
        }




        private void UpdateSsrBar(List<SingleViewBandAWS> listSSR)
        {
            
        }
        public void RefreshFrameInSvb(int numberSvb, int numberChannel , IEnumerable<float> spectrum)
        {

            try
            {
                numberSvb = numberSvb > vm.SpectrumModel.SsrParametrs.Count ? vm.SpectrumModel.SsrParametrs.Count - 1 : numberSvb;

                
                    if (numberChannel == 0)
                        vm.SpectrumModel.SsrParametrs[numberSvb].DataMaster[0] = new List<double>(spectrum.Select(Convert.ToDouble).ToList());
                    else
                        vm.SpectrumModel.SsrParametrs[numberSvb].DataSlave[0] = new List<double>(spectrum.Select(Convert.ToDouble).ToList());


                    vm.SpectrumModel.SsrParametrs[numberSvb].TriggerPropertyChanged();
                

                
            }

            catch { 
            
            }
        }


        public async void RefreshBandBearing(ResultBandBearingResponse resultBandBearing)
        {


            try
            {


                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    PSControl.SetF(resultBandBearing.Bearing);
                    PSControl.SetTheta(resultBandBearing.RMS);
                    //PSControl.Draw()
                });


            }

            catch { }
        }

        public  void ClearSpectrumInSvb()
        {
            try
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    vm.ClearSpectrum();
                });
                // refresh in dynamic 
            }

            catch { }
        }


        public void RefreshFrameInSvb(int numberSvb, int numberChannel, int numberAntenna, IEnumerable<float> spectrum)
        {
            if (!_isActiveAntenna[numberAntenna])
                return;
            try
            {
                numberSvb = numberSvb > vm.SpectrumModel.SsrParametrs.Count ? vm.SpectrumModel.SsrParametrs.Count - 1 : numberSvb;


                Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    try
                    {
                        if (numberChannel == 0)
                            vm.SpectrumModel.SsrParametrs[numberSvb].DataMaster[numberAntenna] = new List<double>(spectrum.Select(Convert.ToDouble).ToList());
                        else
                            vm.SpectrumModel.SsrParametrs[numberSvb].DataSlave[numberAntenna] = new List<double>(spectrum.Select(Convert.ToDouble).ToList());
                        vm.SpectrumModel.SsrParametrs[numberSvb].TriggerPropertyChanged();
                  
                    }
                    catch { }

                    });            
                }

            

            catch { }

        }

        private void CreateTabInDynamicPanel()
        {
            if (MaxControlInDynamic <= CountControl)
                return;

            var isAddTwo = CountControl % 2 == 0 && CountControl != 0;
            for (var i = 0; i < (isAddTwo ? 2 : 1); i++)
            {
                AddOneItemOnCB();
                InitSelectDynamicPanel(CountControl - 1);
                RefreshInDynamicPanelWithBand(CountControl - 1);
            }
        }

      
        private void InitControls()
        {
            var calc = new Calc()
            {
                Orientation = dCountPanels.Orientation.Vertical,
                Dimension = Dimension.CustomColumn
            };

            SetColorLineInGrid(Resources["ControlForeground"] as Brush, 0.6);
            DynamicPanels = new DynamicPanels<SPControl>(DynamicGrid, calc);

            pLibrary.GlobalNumberOfBands = GlobalNumberOfBands;
            pLibrary.GlobalBandWidthMHz = GlobalBandWidthMHz;
            pLibrary.GlobalRangeXmin = GlobalRangeXMin;

            pLibrary.GlobalRangeYmin = GlobalRangeYMin;
            pLibrary.GlobalRangeYmax = GlobalRangeYMax;

            pLibrary.spControl.IsFirstChoise = false;
            pLibrary.spControl.topPanelVisible = false;
            pLibrary.spControl.VisibleAdaptiveThreshold = false;
            pLibrary.spControl.Threshold = Th;
            //   pLibrary.spControl.VisibleThres = false;


            pLibrary.spControl.ThresholdChange += SpControl_ThresholdChange;
        }

        private void SpControl_ThresholdChange(int value)
        {
            _clientDdf?.SetThAsync(value);
        }

        private void InitHotKeys()
        {
            var newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.C, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, BtnCtrlСClick));

            newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, BtnCtrlDClick));

            newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.R, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, BtnCtrlRClick));

            newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.L, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, BtnCtrlLClick));

            newCmd = new RoutedCommand();
            newCmd.InputGestures.Add(new KeyGesture(Key.D1, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(newCmd, BtnCtrlBtn1Click));
        }

        private void InitSelectDynamicPanel(int number)
        {
            DynamicPanels.DoIt(CountControl);

            DynamicPanels[number].MinHeight = 200;
            DynamicPanels[number].IsFirstChoise = false;
            DynamicPanels[number].topPanelVisible = false;
            DynamicPanels[number].VisibleAdaptiveThreshold = false;
            DynamicPanels[number].VisibleMiddleFreq = true;
            DynamicPanels[number].VisibleThres = false;
            DynamicPanels[number].VisibleYScale = false;

            DynamicPanels[number].GlobalBandWidthMHz = GlobalBandWidthMHz;
            DynamicPanels[number].GlobalNumberOfBands = GlobalNumberOfBands;

            DynamicPanels[number].GlobalRangeXmin = GlobalRangeXMin;
            DynamicPanels[number].GlobalRangeYmax = GlobalRangeYMax;
            DynamicPanels[number].GlobalRangeYmin = GlobalRangeYMin;
        }

        private void LoadPanoramsSettings(bool withPlot = true)
        {
            try
            {
                var panoramsSetting = SerializerJSON.Deserialize<PanoramsSetting>("panorams_setting.json");

                if (panoramsSetting == null)
                {
                    MaxControlInDynamic = 6;
                    IsLog = false;
                    IsPowY = false;
                    IsScaleSet = false;
                    IsSqrt = false;
                    IsAverage = false;
                    ScaleIncrement = 0.1d;
                    GlobalRangeYMin = -120;
                    GlobalRangeYMax = 0;

                    AddOneItemOnCB();
                    InitSelectDynamicPanel(0);
                    return;
                }

                MaxControlInDynamic = panoramsSetting.maxCountPanoramsInDynamic;

                GlobalRangeYMin = panoramsSetting.minVisibleY;
                GlobalRangeYMax = panoramsSetting.maxVisibleY;

                if (!withPlot) return;
                for (var i = 0; i < panoramsSetting.freqInPanorams.Count; i++)
                {
                    AddOneItemOnCB();
                    InitSelectDynamicPanel(i);
                }
            }
            catch
            {
                // ignored
            }
        }
        
        private void RefreshInDynamicPanelWithBand(int number)
        {
            var startBandFreq = pLibrary.spControl.StartBandFreq;
            var endBandFreq = pLibrary.spControl.EndBandFreq;

            var maxRange = GlobalBandWidthMHz * GlobalNumberOfBands + GlobalRangeXMin;

            if (startBandFreq == 0 && endBandFreq == 5)
                return;

            var startFreq = startBandFreq < endBandFreq ? startBandFreq < GlobalRangeXMin ? GlobalRangeXMin :
                                                          startBandFreq :
                            endBandFreq < GlobalRangeXMin ? GlobalRangeXMin : endBandFreq;

            var endFreq = startBandFreq < endBandFreq ? endBandFreq < maxRange ? endBandFreq :
                                                        maxRange :
                          startBandFreq < maxRange ? startBandFreq : maxRange;

        }

        private void RefreshParamsInGraphics()
        {
            try
            {
            //     RandomAntennaPower(CountSectors);
                LocalAntennaPower = new List<double>() { 200, 150, 120 , 86, 76 , 80, 120, 140 };
                


                Dispatcher.BeginInvoke(
                    new Action(
                        () =>
                        {
                            PSControl.Draw(LocalAntennaPower.ToArray());
                            PSControl.SetF(2400);
                            PSControl.SetTheta(4);
                        }));
            }
            catch
            {
                // ignored
            }
        }

        private void RemoveAllTabInDynamicPanel()
        {
            CountControl = 0;
            DynamicPanels.DoIt(CountControl);
            cbSelectDynamic.SelectedIndex = -1;
        }

        private void RemoveLastTabInDynamicPanel()
        {
            if (CountControl == MinControlInDynamic) return;
            CountControl--;
            cbSelectDynamic.Items.RemoveAt(CountControl);
            cbSelectDynamic.SelectedIndex = CountControl - 1;

            DynamicPanels.DoIt(CountControl);
        }

        private void SavePanoramsSettings()
        {
            try
            {
                var freqList = new List<Tuple<double, double>>();

                for (var i = 0; i < DynamicPanels.Count(); i++)
                    freqList.Add(new Tuple<double, double>(DynamicPanels[i].MinVisibleX, DynamicPanels[i].MaxVisibleX));

                var panoramsSetting = new PanoramsSetting
                {
                    freqInPanorams = freqList,
                    maxCountPanoramsInDynamic = MaxControlInDynamic,
                    minVisibleY = GlobalRangeYMin,
                    maxVisibleY = GlobalRangeYMax,
                };

                JsonConvert.SerializeObject(panoramsSetting);

                SerializerJSON.Serialize(panoramsSetting, "panorams_setting.json");
            }
            catch
            {
                // ignored
            }
        }

        private void SetColorLineInGrid(Brush colorBrush, double thickness)
        {
            var T = Type.GetType(
                "System.Windows.Controls.Grid+GridLinesRenderer,"
                + " PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");

            var glr = Activator.CreateInstance(T);
            glr.GetType().GetField("s_oddDashPen", BindingFlags.Static | BindingFlags.NonPublic)
                ?.SetValue(glr, new Pen(colorBrush, thickness));
            glr.GetType().GetField("s_evenDashPen", BindingFlags.Static | BindingFlags.NonPublic)
                ?.SetValue(glr, new Pen(colorBrush, thickness));
        }

        private void SetDeploymentKey()
        {
            var deploymentKey =
                "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ"
                + " + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 "
                + "/ ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + "
                + "OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + "
                + "VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";

            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            SavePanoramsSettings();
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            UpdateFreqRangesRecon(((MainWindow)Application.Current.MainWindow)?.lFreqRangesRecon);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadPanoramsSettings();
        }
        
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedAntenna = cbSelectedAntenna.SelectedIndex;
        }

        private void AddSourceButton_Click(object sender, RoutedEventArgs e)
        {
            //ListAntenna[2].Power = Random.Next(1, 562);
            //UpdateAntennaPower(2, ListAntenna[2].Power);

            if (vm.Band.ValueEnd == vm.Band.ValueBegin)
                return;
            //OnAddSpectrumSource?.Invoke(this, new SpectrumSourceEventArgs((double)(vm.Band.ValueEnd + vm.Band.ValueBegin)/2, (float)(vm.Band.ValueEnd - vm.Band.ValueBegin)));
        }

        private bool CheckAntennaPower()
        {

            if (vm.Band.ValueBegin < 100)
            {
                paramAntennaPower.NumSVB = -1;
                paramAntennaPower.StartPosition = -1;
                paramAntennaPower.StopPosition = -1;
                return false;
            }

            
            
            int SizeBand = 100;
            int CountPointBand = 1310;

            paramAntennaPower.NumSVB = (int)(vm.Band.ValueBegin / 100);

            int startFreq = (int)((vm.Band.ValueBegin) % 100);
            int stopFreq = (int)((vm.Band.ValueEnd) % 100);

            paramAntennaPower.StartPosition = (CountPointBand * startFreq) / SizeBand;
            paramAntennaPower.StopPosition = (CountPointBand * stopFreq) / SizeBand;

            return true;


        }
        private void UpdateAntennaPower(int Antenna,List<float> amp )
        {
            double power = CountAntennaPower(amp);
            ListAntenna.Remove(ListAntenna[Antenna]);
            ListAntenna.Insert(Antenna, new AntennaPower() { Antenna = Antenna + 1, Channel = 1, Power = power }) ;
        }


        private double CountAntennaPower(List<float> amp)
        {
            return 0;
        }

        
        private async void ActiveAntenna_Click(object sender, RoutedEventArgs e)
        {            
            if (sender is ToggleButton btn)
            {
                var _selectedIndexAntenna = int.Parse(btn.Tag.ToString());

                _isActiveAntenna[_selectedIndexAntenna] = Convert.ToBoolean(btn.IsChecked);

                _clientDdf?.SetAntennsActive(_isActiveAntenna);

                ClearSpectrumInSvb();
            }
        }

        private async void SetActiveSpectrumMode()
        {
            _clientDdf?.SetSpectrumModeAsync((TransmissionPackage.ModeSpectrum)ActiveSpectrumMode);
        }


        private async void SetAntennaActiveServer()
        {            
             List<List<float>> emptySpectrum = Enumerable.Range(0, 6).Select(i => new List<float>()).ToList();
                for (int i = 0; i < vm.SpectrumModel.SsrParametrs.Count; i++)                    
                        for (int k = 0; k <2; k++)
                             RefreshFrameInSvb(i, k, new List<float>());            
        }

        private void GetBearingButton_Click(object sender, RoutedEventArgs e)
        {

            SetActiveBearing();

            
        }
    }
}