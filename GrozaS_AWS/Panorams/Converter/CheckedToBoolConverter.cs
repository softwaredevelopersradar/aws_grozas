﻿using System;
using System.Globalization;
using System.Windows.Data;
namespace GrozaS_AWS
{

    public class CheckedToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value.ToString() == parameter.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var actualValue = (bool)value;
            var param = int.Parse(parameter.ToString());

            return actualValue ? param : 0;
        }
    }
}