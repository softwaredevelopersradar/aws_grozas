﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.Panorams
{

    public class ParamAntennaPower
    {
        public int NumSVB { get; set; }
        public int StartPosition { get; set; }
        public int StopPosition { get; set; }
    }


    public class AntennaPower
    {
        private int _antenna;
        public int Antenna
        {
            get { return _antenna; }
            set
            {
                _antenna = value;
               
                OnPropertyChanged();
            }
        }


        private int _channel;
        public int Channel
        {
            get { return _channel; }
            set
            {
                _channel = value;

                OnPropertyChanged();
            }
        }

        private double _power;
        public double Power
        {
            get { return _power; }
            set
            {
                _power = value;

                OnPropertyChanged();
            }
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

    }
}
