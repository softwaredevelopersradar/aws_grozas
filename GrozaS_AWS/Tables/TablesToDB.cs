﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TableEvents;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        // Станции помех
        public List<TableJammerStation> lJammerStation = new List<TableJammerStation>();
        // Известные частоты (ИЧ)
        public List<FreqRanges> lFreqKnown = new List<FreqRanges>();
        // Диапазоны радиоразведки (ДРР)
        public List<FreqRanges> lFreqRangesRecon = new List<FreqRanges>();
        // Запрещенные частоты (ЗЧ)
        public List<FreqRanges> lFreqForbidden = new List<FreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsRecon> lSectorsRecon = new List<TableSectorsRecon>();
        // Свои БПЛА 
        public List<TableOwnUAV> lOwnUAV = new List<TableOwnUAV>();

        public List<TableSuppressSource> lSuppressSource = new List<TableSuppressSource>();

        public List<TableSuppressGnss> lSuppressGNSS = new List<TableSuppressGnss>();
        // ЛПП Кираса-М
        public List<TableCuirasseMPoints> lCuirasseMPoints = new List<TableCuirasseMPoints>();

        //public List<TableSource> lSource = new List<TableSource>();

        ObservableCollection<SourceDF> CurrentSourcesDF = new ObservableCollection<SourceDF>();
        // Аэроскоп БПЛА
        public List<TableAeroscope> lAeroscope = new List<TableAeroscope>();
        private int aeroscopeIndexId = 1;
        // Траектория А БПЛА
        public List<TableAeroscopeTrajectory> lATrajectory = new List<TableAeroscopeTrajectory>();
        // Гроза-Р
        //public List<TableOperatorGun> lOperatorGun = new List<TableOperatorGun>();
        public List<OperatorGrozaR> lOperatorGrozaR = new List<OperatorGrozaR>();





        // Добавить запись
        private void OnAddRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {

                if (e.NameTable == NameTable.TableOwnUAV)
                    return;

                CheckCoordGNSS(ref e);

                mainWindowViewModel.clientDB.Tables[e.NameTable].Add(e.Record);

            }
        }

        // Удалить все записи
        private void OnClearRecords(object sender, NameTable nameTable)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                if (nameTable == NameTable.TableOwnUAV)
                    return;
                               
                mainWindowViewModel.clientDB.Tables[nameTable].Clear();
            }
        }

        // Удалить запись
        private void OnDeleteRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                if (e.NameTable == NameTable.TableOwnUAV)
                    return;
          
                mainWindowViewModel.clientDB.Tables[e.NameTable].Delete(e.Record);
            }
        }

        // Изменить запись
        private void OnChangeRecord(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {

                if (e.NameTable == NameTable.TableOwnUAV)
                    return;


                CheckCoordGNSS(ref e);
                mainWindowViewModel.clientDB.Tables[e.NameTable].Change(e.Record);
            }
        }




        

    }
}
