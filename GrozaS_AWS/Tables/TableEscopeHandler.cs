﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
    using EscopeControl;

    using GrozaSModelsDBLib;

    using TableEvents;
namespace GrozaS_AWS
{


    public partial class MainWindow
    {
        private void ucEscope_OnDeleteRecord(object sender, string e)
        {
            DeleteEscopeTarget(e);
        }

        private void ucEscope_OnClearRecords(object sender, EventArgs e)
        {
            ClearEscope();
        }

        private void ucEscope_OnCentering(object sender, EscopeUAVModel e)
        {
            var target = this.escopeTargets.FirstOrDefault(x => x.SerialNumber == e.SerialNumber);
            if (target != null)
            {
                mainWindowViewModel.LocationCenter = target.Coordinate.Clone();
            }
        }

        private void ucEscope_OnSaveTracks(object sender, List<EscopeControl.EscopeUAVModel> e)
        {
            SaveTrajectory.SaveTrajectoryAeroscope(escopeTargets);
        }

        private void UcEscope_OnAddAsOwn(object sender, EscopeControl.EscopeUAVModel e)
        {
            if (e != null && !e.SerialNumber.Equals(String.Empty) && mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Add( new TableOwnUAV(){ SerialNumber = e.SerialNumber, Name = e.Type});
            }
        }

       
    }
}
