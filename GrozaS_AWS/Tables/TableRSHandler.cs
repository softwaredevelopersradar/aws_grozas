﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using UIMap;

namespace GrozaS_AWS
{
    
    public partial class MainWindow 
    {

        byte UploadRS = 0;
        private async void UcOwnUAV_OnSendRecord(object sender, TableOwnUAV e)
        {

            


            try
            {
                //if (clientRS != null && !mainWindowViewModel.ChannelTDF_RS.RecordRS)
                //{
                //    mainWindowViewModel.ChannelTDF_RS.ItemRecordRS = e;
                //    mainWindowViewModel.ChannelTDF_RS.RecordRS = true;

                //    if (mainWindowViewModel.ChannelTDF_RS.StateRS == MODE_RS.NOTACTIVE)
                //        SendModeRS(MODE_RS.ACTIVE);
                //    else
                //        RecordRS();
                //}
                if (clientRS != null)
                {
                    mainWindowViewModel.ChannelTDF_RS.ItemRecordRS = e;

                    RecordRS();
                }
            }
            catch
            { }


            //TableOwnUAVFreq uav = new TableOwnUAVFreq();

                //byte[] serial = Encoding.ASCII.GetBytes(e.SerialNumber);

                //uav.BandMHz = e.Frequencies[0].BandMHz;
                //uav.FrequencyMHz = e.Frequencies[0].FrequencyMHz;
                //uav.IdSR = e.Frequencies[0].IdSR;


                //UploadRS = 1;
                //tmrUploadRS.Enabled = true;

                //await SendGainUSRP(gainWindow.GainUSRP);

                //var answer = await SendRecordSignalOWnUAV(serial,uav,gainWindow.GainAntenna, gainWindow.GainPreselector);

                //UploadRS = 0;

                //if (answer != null)
                //{
                //    ResultMessageRS(answer.Header.ErrorCode);


                //    if (answer.Header.ErrorCode != 0)

                //    answer = await clientRS.GetActiveListFriendlyUAVsString();


                //    UpdateListOwnUAV(answer);

                //}
        }




        // Добавить запись
        private async void OnAddRecordOwnUAV(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Add((TableOwnUAV)e.Record);
            }
        }

        // Удалить все записи
        private async void OnClearRecordsOwnUAV(object sender, NameTable nameTable)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Clear();
            }
        }

        // Удалить запись
        private async void OnDeleteRecordOwnUAV(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Delete((TableOwnUAV)e.Record);
            }
        }

        // Изменить запись
        private async void OnChangeRecordOwnUAV(object sender, TableEvent e)
        {
            if (mainWindowViewModel.clientDB != null)
            {
                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Change((TableOwnUAV)e.Record);
            }
        }



        private void TickTimerUploadRS(object sender, System.Timers.ElapsedEventArgs e)
        {

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (UploadRS > 0)
                {
                    pbStatusRS.Visibility = System.Windows.Visibility.Visible;
                    pbStatusRS.Value = UploadRS++;
                    if (UploadRS == 255)
                        UploadRS = 1;
                 
                    txtStatusRS.Visibility = System.Windows.Visibility.Visible;                 
                }
                else
                {
                    pbStatusRS.Visibility = System.Windows.Visibility.Hidden;
                    txtStatusRS.Visibility = System.Windows.Visibility.Hidden;
                    pbStatusRS.Value = UploadRS;
                    tmrUploadRS.Enabled = false;
                }

            });

        }
    }
}