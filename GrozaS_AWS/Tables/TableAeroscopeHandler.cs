﻿using System;
using TableEvents;    
using GrozaSModelsDBLib;
using TableOperations;
using System.Threading;

namespace GrozaS_AWS
{
    public partial class MainWindow
    {
        private void ucAeroscope_OnDeleteRecord(object sender, TableEvent e)
        {
            DeleteAeroscopeTarget(e.Record as TableAeroscope);
        }

        private void ucAeroscope_OnClearRecords(object sender, NameTable e)
        {
            ClearAeroscope();
        }

        private void ucAeroscope_OnSaveTracks(object sender, EventArgs e)
        {
           // SaveTrajectory.SaveTrajectoryAeroscope(lAeroscope, lATrajectory);
        }

        private void DeleteAeroscopeTarget(TableAeroscope e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                int ind = lAeroscope.FindIndex(x => x.SerialNumber == e.SerialNumber);
                if (ind != -1)
                {
                    lAeroscope.RemoveAt(ind);
                    ucAeroscope.DeleteAeroscope(e);
                    //ucMap.UpdateMapTrajectories(lAeroscope, lATrajectory);

                    DeleteAeroscopeMapTarget(e.SerialNumber);
                }
            });
        }
    }
}
