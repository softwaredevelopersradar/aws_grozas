﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;
using UIMap;

namespace GrozaS_AWS
{
    
    public partial class MainWindow 
    {


        private void CheckCoordGNSS(ref TableEvent e)
        {
            if (e.NameTable == NameTable.TableJammerStation)
            {
                TableJammerStation tableJammerStation = e.Record as TableJammerStation;

                tableJammerStation.Coordinates = (tableJammerStation.IsGnssUsed) ?
                                                    mainWindowViewModel.GlobalPropertiesVM.Gnss : tableJammerStation.Coordinates;

            }
        }

        private void UcJammerStation_OnGetCoords(object sender, SelectedRowEvents e)
        {
            try
            {
                SendCoord(e.Id);
            }
            catch { }
        }

        private void UcJammerStation_OnGetTime(object sender, SelectedRowEvents e)
        {
            try
            {
                SendSynchTime(e.Id);
            }
            catch { }
        }

        private void UcJammerStation_OnSelectedRow(object sender, SelectedRowEvents e)
        {
            if (e.Id > 0)
            {
                LoadTablesByFilter(e.Id);
            }
        }
        private void UcJammerStation_OnDoubleClickStation(object sender, SelectedRowEvents e)
        {
            try
            {

                Coord coordCenter = lJammerStation.Where(x => x.Id == e.Id).FirstOrDefault().Coordinates;

                mainWindowViewModel.LocationCenter = coordCenter;
            }
            catch
            { }

        }

    }
}