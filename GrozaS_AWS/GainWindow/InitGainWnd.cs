﻿
using System;
using System.Threading.Tasks;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private GrozaS_AWS.GainWindow.GainWindow gainWindow;

        private void InitGainWnd()
        {
            gainWindow = new GainWindow.GainWindow();
            gainWindow.OnStartRebooting += GainWindow_OnStartRebooting;
            //gainWindow.OnUpdateGainUSRP += GainWindow_OnUpdateGainUSRP;
        }

        public void GainWindow_SetLanguage(DllGrozaSProperties.Models.Languages language)
        {
            if (gainWindow == null)
                return;
            gainWindow.Title = SWindowNames.nameGainWnd;
            //gainWindow.Language = (ELanguages)basicProperties.Local.General.Language;
        }

        //private async void GainWindow_OnUpdateGainUSRP(object sender, EventArgs e)
        //{
        //    await Task.Run(() => SendGainUSRP(gainWindow.GainUSRP));
        //}

        private async void GainWindow_OnStartRebooting(object sender, EventArgs e)
        {
            await Task.Run(() => SendStartRebooting());

            gainWindow.ChangedRebootingStatus(false);
        }
    }
} 