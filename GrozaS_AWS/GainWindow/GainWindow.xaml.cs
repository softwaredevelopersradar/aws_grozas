﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GrozaS_AWS.GainWindow
{
    /// <summary>
    /// Interaction logic for GainWindow.xaml
    /// </summary>
    public partial class GainWindow : Window, INotifyPropertyChanged
    {
        public GainWindow()
        {
            InitializeComponent();

            DataContext = this;
        }

        public event EventHandler OnStartRebooting;

        //public event EventHandler OnUpdateGainUSRP;


        private byte _gainAntenna = 0;
        public byte GainAntenna
        {
            get
            {
                return _gainAntenna;
            }
            set
            {
                _gainAntenna = value;
                OnPropertyChanged();
            }
        }

        private byte _gainPreselector = 10;
        public byte GainPreselector
        {
            get
            {
                return _gainPreselector;
            }
            set
            {
                _gainPreselector = value;
                OnPropertyChanged();
            }
        }

        //private byte _gainUSRP = 30;
        //public byte GainUSRP
        //{
        //    get
        //    {
        //        return _gainUSRP;
        //    }
        //    set
        //    {
        //        _gainUSRP = value;
        //        OnPropertyChanged();
        //    }
        //}

        private bool _isRebooting = false;
        public bool IsRebooting
        {
            get
            {
                return _isRebooting;
            }
            set
            {
                _isRebooting = value;
                OnPropertyChanged();
            }
        }

        //public bool IsRebooting
        //{
        //    get { return (bool)GetValue(IsRebootingProperty); }
        //    set { SetValue(IsRebootingProperty, value); }
        //}

        //public static readonly DependencyProperty IsRebootingProperty =
        //    DependencyProperty.Register("IsRebooting", typeof(bool),
        //    typeof(GainWindow), new FrameworkPropertyMetadata(false));

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //ifIsRebooting = true;
            //pbStatusReboot.Visibility = Visibility.Visible; 
            if (!IsRebooting)
            {
                ChangedRebootingStatus(true);
                OnStartRebooting?.Invoke(this, null);
            }
        }

        public void ChangedRebootingStatus(bool status)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                pbStatusReboot.Visibility = status == false ? Visibility.Hidden : Visibility.Visible;

            });
            
            IsRebooting = status;
        }

        //private void Slider_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        //{
        //    OnUpdateGainUSRP?.Invoke(this, null);
        //}
    }
}
