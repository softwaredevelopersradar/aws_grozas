﻿using GrozaSModelsDBLib;

using MatedJS;
using System;
using System.Linq.Expressions;
using System.Net;
using System.Windows;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        public MatedGroup matedGroup;


        private void ConnectLNK()
        {           
            switch (mainWindowViewModel.LocalPropertiesVM.ED.Type)
            {
                case DllGrozaSProperties.Models.TypeCnt.Motorola:
                    OpenPort();
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Cisco:
                    ConnectPort(mainWindowViewModel.LocalPropertiesVM.ED.IpAddressCommunicationCenterLocal,
                        mainWindowViewModel.LocalPropertiesVM.ED.PortCommunicationCenterLocal,
                        mainWindowViewModel.LocalPropertiesVM.ED.IpAddressCommunicationCenterRemote,
                        mainWindowViewModel.LocalPropertiesVM.ED.PortCommunicationCenterRemote);
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Router3G_4G:
                    ConnectPort(mainWindowViewModel.LocalPropertiesVM.ED.IpAddress3G4GRouterLocal,
                        mainWindowViewModel.LocalPropertiesVM.ED.Port3G4GRouterLocal,
                        mainWindowViewModel.LocalPropertiesVM.ED.IpAddress3G4GRouterRemote,
                        mainWindowViewModel.LocalPropertiesVM.ED.Port3G4GRouterRemote);
                    break;

                default:
                    break;
            }
        }

        private void DisconnectLNK()
        {
            switch (mainWindowViewModel.LocalPropertiesVM.ED.Type)
            {
                case DllGrozaSProperties.Models.TypeCnt.Motorola:
                    ClosePort();
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Cisco:                    
                    DisconnectPort();
                    break;

                case DllGrozaSProperties.Models.TypeCnt.Router3G_4G:
                    DisconnectPort();
                    break;

                default:
                    break;
            }
        }

       

        private void OpenPort()
        {
            if (lJammerStation != null && lJammerStation.Count > 0)
            {
                matedGroup = new MatedGroup((byte)lJammerStation.Find(x => x.Role == StationRole.Own).Id);

                InitEventMated();

                matedGroup.OpenPort(mainWindowViewModel.LocalPropertiesVM.ED.ComPort, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
        }

        private void ClosePort()
        {
            matedGroup.ClosePort();

            DeinitEventMated();

            matedGroup = null;
        }

        public static bool GetIPAddress()
        {
            IPAddress[] hostAddresses = Dns.GetHostAddresses("");

            bool b = false;
            foreach (IPAddress hostAddress in hostAddresses)
                if (hostAddress.ToString() == "175.20.40.5")
                    b = true;
            return b; // or IPAddress.None if you prefer
        }

        private void ConnectPort(string IPLocal, int PortLocal, string IPRemote, int PortRemote)
        {
         
            matedGroup = new MatedGroup((byte)lJammerStation.Find(x => x.Role == StationRole.Own).Id);            

            InitEventMated();


            //if (GetIPAddress())
            
            //matedGroup.ConnectPort("175.20.40.3", 10020, "175.20.50.2", 10021);
            //else
            //    matedGroup.ConnectPort("175.20.50.2", 10021, "175.20.40.3", 10020);

            matedGroup.ConnectPort(IPLocal, PortLocal, IPRemote, PortRemote);

        }

        private void DisconnectPort()
        {
            matedGroup.DisconnectPort();

            DeinitEventMated();

            matedGroup = null;
        }

        private void InitEventMated()
        {
            try
            {
                matedGroup.OnOpenPort += OpenPortLNK;
                matedGroup.OnClosePort += ClosePortLNK;

                matedGroup.OnTextMessage += TextMessageLNK;
                matedGroup.OnConfirmTextMessage += ConfirmTextMessageLNK;

                matedGroup.OnGetCoord += GetCoordLNK;
                matedGroup.OnGetTime += GetTimeLNK;
                matedGroup.OnGetState += GetStateLNK;
                matedGroup.OnGetExecutingBearing += GetExecutingBearingLNK;

                matedGroup.OnResultCoord += ResultCoordLNK;
                matedGroup.OnResultTime += ResultTimeLNK;
                matedGroup.OnConfirmState += ConfirmStateLNK;
                matedGroup.OnConfirmExecutingBearing += ConfirmExecutingBearingLNK;
                matedGroup.OnResultExecutingBearing += ResultExecutingBearingLNK;
            }

            catch { }
        }

        private void DeinitEventMated()
        {
            matedGroup.OnOpenPort -= OpenPortLNK;
            matedGroup.OnClosePort -= ClosePortLNK;

            matedGroup.OnTextMessage -= TextMessageLNK;
            matedGroup.OnConfirmTextMessage -= ConfirmTextMessageLNK;

            matedGroup.OnGetCoord -= GetCoordLNK;
            matedGroup.OnGetTime -= GetTimeLNK;
            matedGroup.OnGetState -= GetStateLNK;
            matedGroup.OnGetExecutingBearing -= GetExecutingBearingLNK;

            matedGroup.OnResultCoord -= ResultCoordLNK;
            matedGroup.OnResultTime -= ResultTimeLNK;
            matedGroup.OnConfirmState -= ConfirmStateLNK;
            matedGroup.OnConfirmExecutingBearing -= ConfirmExecutingBearingLNK;
            matedGroup.OnResultExecutingBearing -= ResultExecutingBearingLNK;
        }
    }
}