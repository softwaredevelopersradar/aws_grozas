﻿using GrozaSModelsDBLib;
using System;
using System.Linq;
using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private bool SendSynchTime(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendGetTime(JammerReceiver, DateTime.Now);
        }

        private bool SendCoord(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;
            Coord coord = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Coordinates;
            return matedGroup.SendGetCoord(JammerReceiver, coord.Latitude, coord.Longitude);
        }

        private bool SendState(int JammerReceiver)
        {
            if (matedGroup == null)
                return false;
         
            return matedGroup.SendGetState(JammerReceiver);
        }

        private bool SendMessage(int JammerReceiver, string text)
        {
            if (matedGroup == null)
                return false;

            return matedGroup.SendTextMessage(JammerReceiver, text);
        }

        private bool SendGetExecutiveBearing(int ID, TypeQuery typeQuery)
        {
            if (matedGroup == null)
                return false;

            try
            {                

                var OwnJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id;

               
                return matedGroup.SendGetExecutingBearing(lJammerStation.Find(x => x.Role == StationRole.Linked).Id,
                                                                                      mainWindowViewModel.ExBearingSendRequest.ID,
                                                                                      (int)mainWindowViewModel.ExBearingSendRequest.Frequency,
                                                                                      (short)mainWindowViewModel.ExBearingSendRequest.Bearing,
                                                                                      mainWindowViewModel.ExBearingSendRequest.Time);
            }
            catch
            {
                return false;
            }

        }
       
    }
}