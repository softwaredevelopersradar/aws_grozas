﻿using GrozaS_AWS.Models;
using GrozaSModelsDBLib;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows;
using GrozaS_AWS.Log;
using TDF.Events;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        SourceDF lastSource = new SourceDF();

        private void RegimeTDF(object sender, RegimeEventArgs e)
        {
            try
            {
                mainWindowViewModel.TempMode = (Mode)(e.Regime);

                BlockEscope((Mode)(e.Regime) == Mode.JAMMING);
            }
            catch
            {
            }
        }

        private void ModeRSTDF(object sender, RegimeEventArgs e)
        {

            mainWindowViewModel.ChannelTDF_RS.StateRS = (MODE_RS)(e.Regime);

            switch (mainWindowViewModel.ChannelTDF_RS.StateRS)
            {
                case MODE_RS.ACTIVE:
                    if (mainWindowViewModel.ChannelTDF_RS.GetRS && mainWindowViewModel.ChannelTDF_RS.SenToRS == false)
                    {
                        mainWindowViewModel.ChannelTDF_RS.SenToRS = true;
                        //CheckSignRS();
                    }

                    if (mainWindowViewModel.ChannelTDF_RS.RecordRS && !mainWindowViewModel.ChannelTDF_RS.RecordSetRS)
                    {
                        mainWindowViewModel.ChannelTDF_RS.RecordSetRS = true;
                        //RecordRS();
                    }
                        
                    break;

                case MODE_RS.NOTACTIVE:
                    break;

                default:
                    break;
            }
                

        }
        

        #region BearingReceive

        private void ChannelTDF(object sender, ChannelEventArgs e)
        {
            if (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle == -1)
                mainWindowViewModel.DirectionRX.Azimuth = (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle > -1 && mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle <= 360) ?
                                                       mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle : ( (float)(e.Channel) * 22.5f + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;

            if (mainWindowViewModel.AutoUpdateSource)
            {

                TableTrack tableTrack = new TableTrack()
                {
                    Bearing = new ObservableCollection<TableJamBearing>()
                    {
                        new TableJamBearing
                        {
                            NumJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id ,
                            Bearing = mainWindowViewModel.DirectionRX.Result,// (float)(e.Channel) * 22.5f,
                            Level = -50,
                            Distance = 0
                        }
                    }
                };

               
                AddTrackSourceTDF(tableTrack);

                if (mainWindowViewModel.LocalPropertiesVM.EOM.SurveilanceMode)
                    SetAngleOEM(mainWindowViewModel.DirectionRX.Result);


            }

            if (mainWindowViewModel.AutoMode && mainWindowViewModel.TempMode == Mode.JAMMING)
                SetAngleUpperBrd1(mainWindowViewModel.DirectionRX.Result);
                //SetAngleBRD(mainWindowViewModel.DirectionRX.Result);

        }

        private void BearingTDF(object sender, BearingEventArgs e)
        {

            if (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle == -1)
                mainWindowViewModel.DirectionRX.Azimuth = (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle > -1 && mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle <= 360) ?
                                                      mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle : (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;

            if (mainWindowViewModel.AutoUpdateSource)
            {

                TableTrack tableTrack = new TableTrack()
                {
                    Bearing = new ObservableCollection<TableJamBearing>()
                    {
                        new TableJamBearing
                        {
                            NumJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id ,
                            Bearing = mainWindowViewModel.DirectionRX.Result,// (float)(e.Channel) * 22.5f,
                            Level = -50,
                            Distance = 0
                        }
                    }
                };

               
                AddTrackSourceTDF(tableTrack);
                if (mainWindowViewModel.LocalPropertiesVM.EOM.SurveilanceMode)
                    SetAngleOEM(mainWindowViewModel.DirectionRX.Result);
            }

            //if (mainWindowViewModel.AutoMode && mainWindowViewModel.TempMode == Mode.JAMMING)
            //    SetAngleBRD(mainWindowViewModel.DirectionRX.Result);
            if (mainWindowViewModel.AutoMode && mainWindowViewModel.TempMode == Mode.JAMMING)
                SetAngleUpperBrd1(mainWindowViewModel.DirectionRX.Result);

        }

        private void BearingDistanceTDF(object sender, BearingDistanceEventArgs e)
        {
            if (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle == -1)
                mainWindowViewModel.DirectionRX.Azimuth = (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle > -1 && mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle <= 360) ?
                                                      mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle : (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;

            if (mainWindowViewModel.AutoUpdateSource)
            {
                TableTrack tableTrack = new TableTrack()
                {
                    
                    Bearing = new ObservableCollection<TableJamBearing>()
                    {
                        new TableJamBearing
                        {
                            NumJammer = lJammerStation.Where(x => x.Role == StationRole.Own).FirstOrDefault().Id ,
                            Bearing = mainWindowViewModel.DirectionRX.Result,// (float)(e.Channel) * 22.5f,
                            Level = -50,
                            Distance = e.Distance
                        }
                    }
                };
               
                AddTrackSourceTDF(tableTrack);
                if (mainWindowViewModel.LocalPropertiesVM.EOM.SurveilanceMode)
                    SetAngleOEM(mainWindowViewModel.DirectionRX.Result);
            }

            //if (mainWindowViewModel.AutoMode && mainWindowViewModel.TempMode == Mode.JAMMING)
            //    SetAngleBRD(mainWindowViewModel.DirectionRX.Result);
            if (mainWindowViewModel.AutoMode && mainWindowViewModel.TempMode == Mode.JAMMING)
                SetAngleUpperBrd1(mainWindowViewModel.DirectionRX.Result);
        }

        #endregion

        

        #region SourceReceive

        private async void SourceTDF(object sender, SourceEventArgs e)
        {            
            mainWindowViewModel.DirectionRX.Azimuth = (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;
          
            try
            {
                var source = new TableSource()
                {
                    Type = e.Type,
                    TypeRSM = (byte)TypeRSMart.EMPTY,
                    Note = "",


                    Track = new ObservableCollection<TableTrack>(){
                            new TableTrack(){
                                Coordinates = new Coord() ,
                                Time = DateTime.Now,
                                BandMHz = e.Band,
                                FrequencyMHz = e.Freq,
                                FrequencyRX = e.Freq,


                                Bearing = new ObservableCollection<TableJamBearing>(){
                                          new TableJamBearing() {
                                              Level = 20,
                                              Bearing = mainWindowViewModel.DirectionRX.Result,//(float)(e.Channel)*22.5f,
                                              NumJammer= lJammerStation.Where(t => t.Role == StationRole.Own).FirstOrDefault().Id,
                                              Distance = 0},

                                }
                            }
                    }
                };

                DetermineSourceTDF(source);

                Log.SetSourceRI(source, TypeCodeCollection);
            }
            catch { }
        }

        private void SourceDistanceTDF(object sender, SourceDistanceEventArgs e)
        {

            mainWindowViewModel.DirectionRX.Azimuth = (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;

           
            try
            {
                var source = new TableSource()
                {
                    Type = e.Type,
                    TypeRSM = (byte)TypeRSMart.EMPTY,
                    Note = "",


                    Track = new ObservableCollection<TableTrack>(){
                            new TableTrack()
                            {
                                Coordinates = new Coord() ,
                                Time = DateTime.Now,
                                BandMHz = e.Band,
                                FrequencyMHz = e.Freq,
                                FrequencyRX = e.Freq,


                                Bearing = new ObservableCollection<TableJamBearing>(){
                                          new TableJamBearing() {
                                              Level = 20,
                                              Bearing = mainWindowViewModel.DirectionRX.Result,//(float)(e.Channel)*22.5f,
                                              NumJammer= lJammerStation.Where(t => t.Role == StationRole.Own).FirstOrDefault().Id,
                                              Distance = e.Distance},

                                }
                            }
                    }
                };

                DetermineSourceTDF(source);
                Log.SetSourceRI(source, TypeCodeCollection);

            }
            catch { }
        }

        private void SourceDistanceFRXTDF(object sender, SourceDistanceFRXEventArgs e)
        {

            mainWindowViewModel.DirectionRX.Azimuth = (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;


            try
            {
                var source = new TableSource()
                {
                    Type = e.Type,
                    TypeRSM = (byte)TypeRSMart.EMPTY,
                    Note = "",


                    Track = new ObservableCollection<TableTrack>(){
                            new TableTrack()
                            {
                                Coordinates = new Coord() ,
                                Time = DateTime.Now,
                                BandMHz = e.Band,
                                FrequencyMHz = e.Freq,
                                FrequencyRX = e.FreqRX,



                                Bearing = new ObservableCollection<TableJamBearing>(){
                                          new TableJamBearing() {
                                              Level = 20,
                                              Bearing = mainWindowViewModel.DirectionRX.Result,//(float)(e.Channel)*22.5f,
                                              NumJammer= lJammerStation.Where(t => t.Role == StationRole.Own).FirstOrDefault().Id,
                                              Distance = e.Distance},

                                }
                            }
                    }
                };

                DetermineSourceTDF(source);
                Log.SetSourceRI(source, TypeCodeCollection);

            }
            catch { }
        }

        private void SourceDistanceSignalTDF(object sender, SourceDistanceSignalEventArgs e)
        {

            mainWindowViewModel.DirectionRX.Azimuth = (e.Bearing + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;


            try
            {
                var source = new TableSource()
                {
                    Type = e.Type,
                    TypeRSM = (byte)TypeRSMart.EMPTY,
                    Note = "",


                    Track = new ObservableCollection<TableTrack>(){
                            new TableTrack()
                            {
                                Coordinates = new Coord() ,
                                Time = DateTime.Now,
                                BandMHz = e.Band,
                                FrequencyMHz = e.Freq,
                                FrequencyRX = e.FreqRX,



                                Bearing = new ObservableCollection<TableJamBearing>(){
                                          new TableJamBearing() {
                                              Level = 20,
                                              Bearing = mainWindowViewModel.DirectionRX.Result,//(float)(e.Channel)*22.5f,
                                              NumJammer= lJammerStation.Where(t => t.Role == StationRole.Own).FirstOrDefault().Id,
                                              Distance = e.Distance},

                                }
                            }
                    }
                };

                if (e.Signal == 1)
                    source.Type = IdentifyUAV(new TrackPoint() { Frequency = source.Track.Last().FrequencyMHz, Band = source.Track.Last().BandMHz });

                DetermineSourceTDF(source);
                Log.SetSourceRI(source, TypeCodeCollection);

            }
            catch { }
        }


        private void CSourceTDF(object sender, CSourceEventArgs e)
        {
            
            mainWindowViewModel.DirectionRX.Azimuth = ((float)(e.Channel) * 22.5f + mainWindowViewModel.LocalPropertiesVM.DF.ErrorDeg) % 360;

            try
            {
                var source = new TableSource()
                {
                    Type = e.Type,
                    TypeRSM = (byte)TypeRSMart.EMPTY,
                    Note = "",


                    Track = new ObservableCollection<TableTrack>()
                    {
                            new TableTrack()
                            {
                                Coordinates = new Coord() ,
                                Time = DateTime.Now,
                                BandMHz = e.Band,
                                FrequencyMHz = e.Freq,
                                FrequencyRX = e.Freq,
                                Bearing = new ObservableCollection<TableJamBearing>()
                                {
                                          new TableJamBearing()
                                          {
                                              Level = 20,
                                              Bearing = mainWindowViewModel.DirectionRX.Result,//(float)(e.Channel)*22.5f,
                                              NumJammer= lJammerStation.Where(t => t.Role == StationRole.Own).FirstOrDefault().Id,
                                              Distance = 0
                                          },

                                }
                            }
                    }
                };

                DetermineSourceTDF(source);
                Log.SetSourceRI(source, TypeCodeCollection);
            }
            catch { }
        }

        #endregion



        private byte IdentifyUAV(TrackPoint tableTrack)
        {
            byte _type = 0;

            try
            {
                foreach (var tem in lTablePattern)
                {
                    foreach (var fr in tem.FrequencyList)
                        if (tableTrack.Frequency >= (fr.FrequencyMin- mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy) &&
                            tableTrack.Frequency <= (fr.FrequencyMax+ mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.FrequencyAccuracy) &&
                            Math.Abs(tableTrack.Band - fr.Band) <= mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.BandAccuracy)
                            _type = (byte)tem.Id;
                }
            }
            
            catch{ }

            return _type;
        }



    }
}