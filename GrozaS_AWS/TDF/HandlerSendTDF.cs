﻿using System.Windows;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        private void SendCourseAngle(short Angle)
        {
            try
            {

                tcpClientTDF.SendCourseAngle(Angle);
            }
            catch
            { }
        }

        private void SendMode(Mode mode)
        {
            try
            {
                if (tcpClientTDF != null)
                    tcpClientTDF.SendRequestRegime((byte)mode);
            }
            catch { }
        }

        private void SendJammingSource(int Frequency, int Band, byte Type)
        {
            try
            {
                if (tcpClientTDF != null)
                    tcpClientTDF.SendJammingSource(Frequency, Band, Type);
            }
            catch { }
        }

        private void SendModeRS(MODE_RS mode)
        {
            try
            {
                if (tcpClientTDF != null)
                    tcpClientTDF.SendSetModeRS((byte)mode);
            }
            catch { }
        }
    }
}