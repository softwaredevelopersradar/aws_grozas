﻿namespace GrozaS_AWS
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Threading;
    using GrozaSModelsDBLib;

    using TransmissionPackage;

    using WPFControlConnection;
    using static SpectrumPanoramaControl.SPControl;

    public partial class MainWindow : Window
    {
        private int counter = 0;
        private SemaphoreSlim bearingSemaphore = new SemaphoreSlim(1, 1);
        private TimeSpan timeoutBearingDrop = TimeSpan.FromMilliseconds(100); // should be 30fps
        private void SetModeDDF(Mode mode) 
        { 
            tcpClientDDF?.SetModeAsync(mode);
        }


        private void SetThresholdDDF(int threshold)
        {
            tcpClientDDF?.SetThAsync(threshold);
        }


        private SemaphoreSlim spectrumSemaphore = new SemaphoreSlim(1, 1);
        private TimeSpan timeoutBeforeDrop = TimeSpan.FromMilliseconds(32); // should be 30fps
        private Stopwatch sw = new Stopwatch();
        

        private List<TimeSpan> drawTime = new List<TimeSpan>();


        private void TcpClientDDF_OnGetScan(object sender, SpectrumResponseAll scan)
        {
            if (isHidePanoramsWindow ) 
                return;

 
            if(!sw.IsRunning)
            {
                sw.Start();
            }                        
              
            if(sw.Elapsed < timeoutBeforeDrop)
            {
 
                return;
            }
            sw.Restart();
            try
            {
                switch (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.ScanChannel)
                {
                    case ScanChannels.Ch_1:
                        panoramsWindows.RefreshFrameInSvb(scan.NumberSvb, 0, scan.Scan[0].Amplitude);
                        break;
                    case ScanChannels.Ch_2:
                        panoramsWindows.RefreshFrameInSvb(scan.NumberSvb, 1, scan.Scan[1].Amplitude);
                        break;
                    case ScanChannels.Ch_1_2:
                        break;
                    case ScanChannels.Ch_12:

                        if (panoramsWindows.ActiveTechnicMode)
                            panoramsWindows.RefreshFrameInSvb(scan.NumberSvb, scan.Scan[0].Channel, scan.Scan[0].NumberAntenna, scan.Scan[0].Amplitude);
                        else
                            panoramsWindows.RefreshFrameInSvb(scan.NumberSvb, 0, scan.Scan[0].Amplitude);

                        
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            finally
            {

            }            

        }


        private  void TcpClientDDF_OnGetBandBearing(object sender, TransmissionPackage.ResultBandBearingResponse bearing)
        {

            panoramsWindows.RefreshBandBearing(bearing);


            if (! bearingSemaphore.Wait(timeoutBearingDrop))
            {
                try
                {
                    Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        testLbl.Content = counter++.ToString();

                    });
                }
                catch { }
                return;
            }

            try
            {
                mainWindowViewModel.DirectionRX.Azimuth = (mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle > -1 && mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle <= 360) ?
                                                          mainWindowViewModel.GlobalPropertiesVM.RadioIntelegence.StaticAngle : bearing.Bearing;
            }

            finally
            {

                bearingSemaphore.Release();
            }

        }

        private void TcpClientDDF_OnDisconnect(object sender, System.EventArgs e)
        {
            DisconnectDDF();
        }

        private void TcpClientDDF_OnConnect(object sender, System.EventArgs e)
        {
            mainWindowViewModel.StateConnectionTDF = ConnectionStates.Connected;
            tcpClientDDF.GetSpectrum();
            tcpClientDDF.GetBandBearing();
        }
    }
}
