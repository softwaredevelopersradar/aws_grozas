﻿namespace GrozaS_AWS
{
    using System;
    using System.Linq;
    using ClientDDF;
    using System.Windows;

    using ClientArm;

    using GrozaSModelsDBLib;
    using WPFControlConnection;
    using GrozaS_AWS.Models;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;

    public partial class MainWindow : Window
    {
        public ClientArm tcpClientDDF;

        public List<SingleViewBandAWS> singleViewBand = new List<SingleViewBandAWS>();

        private void ConnectDDF()
        {
            if (tcpClientDDF != null)
                DisconnectDDF();

            try
            {
                tcpClientDDF = new ClientArm(
                    "Arm",
                    "1",
                    mainWindowViewModel.LocalPropertiesVM.DF.IpAddress,
                    mainWindowViewModel.LocalPropertiesVM.DF.Port);

                tcpClientDDF.OnConnect += TcpClientDDF_OnConnect;
                tcpClientDDF.OnDisconnect += TcpClientDDF_OnDisconnect;

                tcpClientDDF.OnGetScan += TcpClientDDF_OnGetScan;
                tcpClientDDF.OnGetBandBearing += TcpClientDDF_OnGetBandBearing;



                tcpClientDDF.Connect();

                UpdateSVB_AWS();


                if (panoramsWindows != null)
                {
                    UpdateTcpClientDDF(ref tcpClientDDF);
                }
            }
            catch { }
        }

        

        private async void UpdateSVB_AWS()
        {
            var ssrs = await tcpClientDDF.GetSvbsSetting();

            foreach (var val in ssrs.ListSvb)
            {
                singleViewBand.Add(new SingleViewBandAWS() { Number = val.NumberSvb, 
                                                             FreqMin = val.FreqMin, 
                                                             FreqMax = val.FreqMax,
                                                             Att1 = val.Att1,
                                                             Att2 = val.Att2});
                
                singleViewBand.Last().PropertyChanged += AttenuatorWindow_PropertyChanged;
            }

            attenuatorWindow.SetBands(singleViewBand);
        }

        private async void UpdateAttenuatorBand(SingleViewBandAWS bandAtt)
        {
            tcpClientDDF.SetAttenuatorSBV(new TransmissionPackage.Svb()
            {
                NumberSvb = bandAtt.Number,
                Att1 = bandAtt.Att1,
                Att2 = bandAtt.Att2
            });
        }
        

        private void DisconnectDDF()
        {
            try
            {
                if (tcpClientDDF == null) return;

                tcpClientTDF?.Disconnect();

                tcpClientDDF.OnConnect -= TcpClientDDF_OnConnect;
                tcpClientDDF.OnDisconnect -= TcpClientDDF_OnDisconnect;
                tcpClientDDF.OnGetScan -= TcpClientDDF_OnGetScan;
                tcpClientDDF.OnGetBandBearing -= TcpClientDDF_OnGetBandBearing;

                //tcpClientDDF?.Dispose();
                tcpClientDDF = null;

                mainWindowViewModel.StateConnectionTDF = ConnectionStates.Disconnected;
            }
            catch 
            {
                mainWindowViewModel.StateConnectionTDF = ConnectionStates.Disconnected;
            }
        }
    }
}
