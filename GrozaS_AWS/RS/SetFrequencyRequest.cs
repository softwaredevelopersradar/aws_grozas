﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaS_AWS.RS
{
    public class SetFrequencyRequest
    {
        public short ID { get; set; }
        public double FrequencyMHz { get; set; }
        public double ReceiverFrequencyMHz { get; set; }
        public float BandwidthMHz { get; set; }

    }
}
