﻿using GrozaS_AWS.Models;
using GrozaS_AWS.RS;
using GrozaSModelsDBLib;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using TableEvents;

namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        
        private void RSConnection_Click(object sender, RoutedEventArgs e)
        {
            

            if (clientRS == null)
                ConnectRS();
            else
            {
                DisconnectRS();
                UploadRS = 0;

                mainWindowViewModel.ChannelTDF_RS.IDGetRS = 0;
                mainWindowViewModel.ChannelTDF_RS.GetRS = false;

                mainWindowViewModel.ChannelTDF_RS.RecordRS = false;


            }
                
        }

        
        private async void ClientRS_IsConnected(bool isConnected)
        {
            if (isConnected)
            {
                mainWindowViewModel.StateConnectionRS = WPFControlConnection.ConnectionStates.Connected;
                await Task.Run(() => SendGainUSRP(UIRsDetecting.GainUSRP));

                

                //try
                //{
                //    GetActiveListFriendlyUAVsStringResponse answer = await clientRS.GetActiveListFriendlyUAVsString();

                //    //var answer = ImitDataRS();

                //    UpdateListOwnUAV(answer);
                //}
                //catch
                //{ }
                
            }
            else
                mainWindowViewModel.StateConnectionRS = WPFControlConnection.ConnectionStates.Disconnected;

            if (channelsRecuestCode != 255)
            { CheckRS_ProtocolsRecognition(); }

        }



        private GetActiveListFriendlyUAVsStringResponse ImitDataRS()
        {
            GetActiveListFriendlyUAVsStringResponse answer = new GetActiveListFriendlyUAVsStringResponse();


            Random rnd = new Random();

            int countUAV = rnd.Next(1, 4);

            string[] strSerial = new string[countUAV];

            for (int j = 0; j < countUAV; j++)
            {
                strSerial[j] = "qqqqqqqqqqqqq" + j.ToString();
            }

                int count = rnd.Next(2, 6);

            //    answer.IsFileSignalActiveUAVs = new IsFileSignalActiveUAV[count* countUAV];

            //    for (int i = 0; i < count * countUAV; i++)
            //    {
            //        answer.IsFileSignalActiveUAVs[i] = new IsFileSignalActiveUAV();
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV = new UAV();
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV.ID = new byte[14];
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV.ID = Encoding.ASCII.GetBytes(strSerial[i/count]);
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV.TableID = (byte)(i + 1);
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV.FreqMinMHz = rnd.Next(1200, 5800);
            //        answer.IsFileSignalActiveUAVs[i].FileSignalActiveUAV.BandwidthMHz = rnd.Next(5, 20);
            //        answer.IsFileSignalActiveUAVs[i].isFileSignal = (byte)rnd.Next(0, 2);
            //        answer.IsFileSignalActiveUAVs[i].isActive = (byte)rnd.Next(0, 2);
            //}
                
            return answer;
        }


      

        private async Task<AffiliationUAVResponse> SendRequestIFF(SetFrequencyRequest sourceRequest, byte gainAntenna, byte gainPreselector)
        {
            try
            {
                var answer = await clientRS.AffiliationUAV(sourceRequest.ID,sourceRequest.ReceiverFrequencyMHz, sourceRequest.BandwidthMHz, gainPreselector, gainAntenna);

                return answer;
            }
            catch
            {
                return null;
            }

        }

        private async Task<DefaultMessage> SendRequestIFF(int Id)
        {
            try
            {
                var answer = await clientRS.AffiliationUAV(Id);
                return answer;
            }
            catch
            {
                return null;
            }

        }

        private async Task<GetActiveListFriendlyUAVsStringResponse> SendRecordSignalOWnUAV(byte[] serial, TableOwnUAVFreq tableOwnUAVFreq, byte gainAntenna, byte gainPreselector)
        {
            try
            {
                isGainUAV uavs = new isGainUAV()
                {
                    ValueAntGain = gainAntenna,
                    ValuePreselGain = gainPreselector,
                    GainUAV = new UAV()
                    {
                        ID = serial,
                        TableID = Convert.ToByte(tableOwnUAVFreq.IdSR),
                        FreqMinMHz = tableOwnUAVFreq.FrequencyMHz,
                        BandwidthMHz = tableOwnUAVFreq.BandMHz
                    }
                };
               
                var answer = await clientRS.RecordSignalUAVString(uavs);

                return answer;

            }
            catch
            {
                return null;
            }

        }


        private async Task<DefaultMessage> SendRecordSignalOWnUAV(int id)
        {
            try
            {
                var answer = await clientRS.RecordSignalUAV(id);

                return answer;

            }
            catch
            {
                return null;
            }

        }

        private async Task<DefaultMessage> SendGainUSRP(byte gainUSRP)
        {
            try
            {
                var answer = await clientRS.SetUSRPn310Gain(gainUSRP);

                return answer;
            }
            catch
            {
                return null;
            }

        }

        private void ResetResultDRS()
        {
            try
            {
                UIRsDetecting.ClearAll();
            }
            catch
            {
                
            }

        }



        private async Task<GetActiveListFriendlyUAVsStringResponse> SendAddOwnUAV(TableOwnUAV tableOwnUAV)
        {
            try
            {
                List<Active> freqUAV = new List<Active>();

                foreach (var s in tableOwnUAV.Frequencies)
                {

                    s.IdSR = (byte)(tableOwnUAV.Frequencies.IndexOf(s) + 1);
                    freqUAV.Add(new Active()
                    {
                        isActive = Convert.ToByte(s.IsActive),
                        FreqMinMHz = s.FrequencyMHz,
                        BandwidthMHz = s.BandMHz,
                        TableID = Convert.ToByte(s.IdSR)

                    });
                }

                UAVandString uavOne = new UAVandString()
                {
                    ID = tableOwnUAV.SerialNumber,
                    Name = tableOwnUAV.Name,
                    Note = tableOwnUAV.Note,

                };

                var answer = await clientRS.AddFriendlyUAVs(uavOne, freqUAV);

                return answer;

            }
            catch
            {
                return null;
            }

        }

        private async Task<GetActiveListFriendlyUAVsStringResponse> SendChangeOwnUAV(TableOwnUAV tableOwnUAV)
        {

            try
            {
                List<Active> uavs = new List<Active>();
                byte _lastIdSR = 0;
                foreach (var s in tableOwnUAV.Frequencies)
                {


                    if (s.IdSR == 0)
                        s.IdSR = (byte)(_lastIdSR + 1);
                   
                        _lastIdSR = s.IdSR;

                    if (s.IdSR > 255)
                        s.IdSR = 1;

                    uavs.Add(new Active()
                    {
                        isActive = Convert.ToByte(s.IsActive),
                        FreqMinMHz = s.FrequencyMHz,
                        BandwidthMHz = s.BandMHz,
                        TableID = Convert.ToByte(s.IdSR)

                    });
                }
                
                UAVandString uav = new UAVandString()
                {
                    ID = tableOwnUAV.SerialNumber,
                    Name = tableOwnUAV.Name,
                    Note = tableOwnUAV.Note
                };

                var answer = await clientRS.SetFriendlyUAVs(uav, uavs);

                return answer;
            }
            catch
            {
                return null;
            }

        }

        private async Task<GetActiveListFriendlyUAVsStringResponse> SendDeleteOwnUAV(TableOwnUAV tableOwnUAV)
        {
            try
            {
                var answer = await clientRS.DeleteFriendlyUAV(tableOwnUAV.SerialNumber);

                return answer;
            }
            catch
            {
                return null;
            }

        }

        private async Task<GetActiveListFriendlyUAVsStringResponse> SendClearOwnUAV()
        {
            try
            {
                var answer = await clientRS.DeleteFriendlyUAVsString();

                return answer;

            }
            catch
            {
                return null;
            }

        }



        private void UpdateListOwnUAV(GetActiveListFriendlyUAVsStringResponse listRS)
        {
            if (mainWindowViewModel.clientDB != null)
            {

                mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Clear();

                if (listRS != null && listRS.CountBPLA > 0)
                {
                    List<TableOwnUAV> listDB = ConvertToListUAV(listRS);

                    mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].AddRange(listDB);
                }
                
            }
        }

        //private void UpdateListOwnUAV(TableOwnUAV rec)
        //{
        //    if (mainWindowViewModel.clientDB != null && rec != null)
        //    {
        //        mainWindowViewModel.clientDB.Tables[NameTable.TableOwnUAV].Add(rec);
        //    }
        //}

        private List<TableOwnUAV> ConvertToListUAV(GetActiveListFriendlyUAVsStringResponse listRS)
        {
            if (listRS == null)
                return null;

            List<TableOwnUAV> listDB = new List<TableOwnUAV>();


            for (int i = 0; i < listRS.BPLAs.Length; i++)
            {
                
                string SerialNumber = listRS.BPLAs[i].uav.ID;

                if (SerialNumber != "")
                {
                    DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    

                    listDB.Add(new TableOwnUAV()
                    {
                        SerialNumber = SerialNumber,
                        Note = listRS.BPLAs[i].uav.Note,
                        Name = listRS.BPLAs[i].uav.Name
                    });

                    for (int j = 0; j < listRS.BPLAs[i].ActiveAndTimePackage.Length; j++)
                    {

                        if (listRS.BPLAs[i].ActiveAndTimePackage[j].Time > 0)
                            start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        else
                            start = new DateTime();

                        listDB.Last().Frequencies.Add(new TableOwnUAVFreq()
                        {
                            FrequencyMHz = listRS.BPLAs[i].ActiveAndTimePackage[j].Freq.FreqMinMHz,
                            BandMHz = (float)listRS.BPLAs[i].ActiveAndTimePackage[j].Freq.BandwidthMHz,
                            IsActive = Convert.ToBoolean(listRS.BPLAs[i].ActiveAndTimePackage[j].Freq.isActive),
                            SR = Convert.ToBoolean(listRS.BPLAs[i].ActiveAndTimePackage[j].isFileSignal),
                            IdSR = listRS.BPLAs[i].ActiveAndTimePackage[j].Freq.TableID,
                            //Time = start.AddMilliseconds(listRS.BPLAs[i].ActiveAndTimePackage[j].Time).ToLocalTime()
                            Time = start.AddSeconds(listRS.BPLAs[i].ActiveAndTimePackage[j].Time).ToLocalTime()
                        }) ;

                            
                    }
                }
            }

            ResultMessageRS(listRS.Header.ErrorCode);

            return listDB;
        }



        private void ResultMessageRS(byte error)
        {
            object objKey = "ErrorRS";
            var dictionaryList = this.Resources.MergedDictionaries.ToList();
            int i = 0;
            int ind = 0;
            while (i < dictionaryList.Count)
            {
                if (dictionaryList[i].Contains(objKey))
                {
                    ind = i;
                    i = dictionaryList.Count;
                }
                else
                    i++;
            }

            if (error != 0)
            {
                objKey = "Error_" + error.ToString();

                string _resultMessage = dictionaryList[ind].Contains(objKey) ? dictionaryList[ind][objKey] as string : null;
                //MessageBox.Show(_resultMessage);
                mainWindowViewModel.ErrorRS = _resultMessage;
            }
            else { mainWindowViewModel.ErrorRS = string.Empty; }


        }


        private void GetTypeRS(int ID)
        {
            try
            {
                //if (clientRS != null && !mainWindowViewModel.ChannelTDF_RS.GetRS) 
                //{
                //    mainWindowViewModel.ChannelTDF_RS.IDGetRS = ID;
                //    mainWindowViewModel.ChannelTDF_RS.GetRS = true;

                //    if (mainWindowViewModel.ChannelTDF_RS.StateRS == MODE_RS.NOTACTIVE)
                //        SendModeRS(MODE_RS.ACTIVE);
                //    else
                //        CheckSignRS();
                //}
                if (clientRS != null)
                {
                    mainWindowViewModel.ChannelTDF_RS.IDGetRS = ID;

                    CheckSignRS();
                }
            }
            catch
            { }
        }

        private async void CheckSignRS()
        {

            try
            {
                if (clientRS != null)
                {

                    SourceDF requestSource = CurrentSourcesDF.Where(x => x.ID == mainWindowViewModel.ChannelTDF_RS.IDGetRS).FirstOrDefault();

                    //SetFrequencyRequest sourceRequest = new SetFrequencyRequest()
                    //{
                    //    ID = (short)requestSource.ID,
                    //    FrequencyMHz = requestSource.Track.Last().Frequency,
                    //    ReceiverFrequencyMHz = requestSource.Track.Last().FrequencyRX,
                    //    BandwidthMHz = requestSource.Track.Last().Band,

                    //};


                    CurrentSourcesDF.Where(x => x.ID == mainWindowViewModel.ChannelTDF_RS.IDGetRS).FirstOrDefault().TypeRSM = TypeRSMart.PROCESS;

                    RefreshDependenceSource();

                    ////await SendGainUSRP(gainWindow.GainUSRP);

                    //var res = await SendRequestIFF(sourceRequest, gainWindow.GainAntenna, gainWindow.GainPreselector);
                    var res = await SendRequestIFF(requestSource.ID);

                    if (res != null)
                    {
                        if (res.Header.ErrorCode != 0)
                            //CurrentSourcesDF.Where(x => x.ID == mainWindowViewModel.ChannelTDF_RS.IDGetRS).FirstOrDefault().TypeRSM = (TypeRSMart)Convert.ToByte(res.Type);
                        //else
                        {
                            CurrentSourcesDF.Where(x => x.ID == mainWindowViewModel.ChannelTDF_RS.IDGetRS).FirstOrDefault().TypeRSM = TypeRSMart.NOSIGNAL;
                        }
                        ResultMessageRS(res.Header.ErrorCode);
                    }
                    else
                        CurrentSourcesDF.Where(x => x.ID == mainWindowViewModel.ChannelTDF_RS.IDGetRS).FirstOrDefault().TypeRSM = TypeRSMart.NOSIGNAL;


                    //SaveSourceTDF_DB();
                }
            }
            catch
            { }

            mainWindowViewModel.ChannelTDF_RS.GetRS = false;
            mainWindowViewModel.ChannelTDF_RS.IDGetRS = 0;
            mainWindowViewModel.ChannelTDF_RS.SenToRS = false;

            //SendModeRS(MODE_RS.NOTACTIVE);

        }

        private async void RecordRS()
        {

            TableOwnUAVFreq uav = new TableOwnUAVFreq();

            //byte[] serial = Encoding.ASCII.GetBytes(mainWindowViewModel.ChannelTDF_RS.ItemRecordRS.SerialNumber);

            //uav.BandMHz = mainWindowViewModel.ChannelTDF_RS.ItemRecordRS.Frequencies[0].BandMHz;
            //uav.FrequencyMHz = mainWindowViewModel.ChannelTDF_RS.ItemRecordRS.Frequencies[0].FrequencyMHz;
            //uav.IdSR = mainWindowViewModel.ChannelTDF_RS.ItemRecordRS.Frequencies[0].IdSR;


            UploadRS = 1;
            tmrUploadRS.Enabled = true;

            ////await SendGainUSRP(gainWindow.GainUSRP);

            //var answer = await SendRecordSignalOWnUAV(serial, uav, gainWindow.GainAntenna, gainWindow.GainPreselector);
            var answer = await SendRecordSignalOWnUAV(mainWindowViewModel.ChannelTDF_RS.ItemRecordRS.Frequencies[0].Id); //почему 0?
            UploadRS = 0;

            if (answer != null)
            {
                ResultMessageRS(answer.Header.ErrorCode);


                //if (answer.Header.ErrorCode != 0)
                //    answer = await clientRS.GetActiveListFriendlyUAVsString(); //МБ НАДО???


                //UpdateListOwnUAV(answer);

            }

            mainWindowViewModel.ChannelTDF_RS.RecordSetRS = false;          
            mainWindowViewModel.ChannelTDF_RS.RecordSetRS = false;

         
            //SendModeRS(MODE_RS.NOTACTIVE);
        }


        private void UIRsDetection_ChangedChannelsState(object sender, UIRsDetecting.ChannelsState e)
        {
            var oldValue = channelsRecuestCode;

            var both = e.State24 && e.State58;
            var noone = !e.State24 && !e.State58;
            if (both)
            {
                channelsRecuestCode = 0;
            }
            else if (noone)
            {
                channelsRecuestCode = 255;
                UIRsDetecting.UpdateIndicators(0,0);
            }
            else
            {
                channelsRecuestCode = e.State24 == true ? (byte)1 : (byte)2;
            }

            if (oldValue == 255 && channelsRecuestCode != 255)
            { CheckRS_ProtocolsRecognition(); }
            if (oldValue != 255 && channelsRecuestCode == 255)
            { cts.Cancel(); }
        }


        private CancellationTokenSource cts = new CancellationTokenSource();
        //private void RS_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "ProtocolsRecognition")
        //    {
        //        //if (!basicProperties.Local.RS.ProtocolsRecognition)
        //        //{
        //        //    SaveSourceTDF_DB();
        //        //}

        //        CheckRS_ProtocolsRecognition();
        //    }
        //}

        //private void CheckRS_ProtocolsRecognition()
        //{
        //    try
        //    {
        //        cts.Cancel(); 

        //        if (basicProperties.Local.RS.ProtocolsRecognition && clientRS != null && clientRS.IsConnected)
        //        {
        //            cts = new CancellationTokenSource();
        //            Task.Run(() => CheckChannels(cts.Token));
        //        }
        //    }
        //    catch (Exception ex)
        //    { }
        //}

        private void CheckRS_ProtocolsRecognition()
        {
            try
            {
                cts.Cancel();

                if (clientRS != null && clientRS.IsConnected)
                {
                    cts = new CancellationTokenSource();
                    Task.Run(() => CheckDetailChannels(cts.Token));
                }
            }
            catch (Exception ex)
            { }
        }

        private async Task CheckChannels(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                Thread.Sleep(basicProperties.Local.RS.Channel1Interval * 1000);

                var code = channelsRecuestCode;
                if (code != 255)
                {
                    //await SendGainUSRP(gainWindow.GainUSRP);
                    var objects = await clientRS?.RequestNewSignals((RecognitionSystemClient2.FreqCode)code);

                    if (objects!= null)
                        ResultMessageRS(objects.Header.ErrorCode);

                    UIRsDetecting.UpdateIndicators(objects.Signal1, objects.Signal2);
                }
            }
        }

        private async Task CheckDetailChannels(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                Thread.Sleep(basicProperties.Local.RS.Channel1Interval * 1000);

                var code = channelsRecuestCode;
                if (code != 255)
                {
                    //await SendGainUSRP(gainWindow.GainUSRP);
                    var objects = await clientRS?.RequestNewSignalsDetail((RecognitionSystemClient2.FreqCode)code);

                    if (objects != null)
                        ResultMessageRS(objects.Header.ErrorCode);

                   

                    UIRsDetecting.UpdateIndicators(objects.Signal1, objects.Signal2,
                                                   objects.Frequency_2_4, objects.Frequency_5_8,
                                                   objects.Bandwidth_2_4, objects.Bandwidth_5_8);
                }
            }
        }


        private async Task<DefaultMessage> SendStartRebooting()
        {
            try
            {
                var answer = clientRS?.RebootUsrp().Result;

                return answer;
            }
            catch
            {
                return null;
            }

        }
        //private async Task<GetNewObjectsListResponse> RequestNewObjectsOnRange2_4()
        //{
        //    try
        //    {
        //        var answer = await clientRS.RequestNewObjectsOnRange(RecognitionSystemClient2.FreqCode.f2400);
        //        return answer;

        //    }
        //    catch
        //    {
        //        return null;
        //    }

        //}

        //private async Task<GetNewObjectsListResponse> RequestNewObjectsOnRange5_8()
        //{
        //    try
        //    {
        //        var answer = await clientRS.RequestNewObjectsOnRange(RecognitionSystemClient2.FreqCode.f5800);
        //        return answer;

        //    }
        //    catch
        //    {
        //        return null;
        //    }

        //}




        //private async Task CheckChannels(CancellationToken token)
        //{
        //    while (!token.IsCancellationRequested)
        //    {

        //        //if (basicProperties.Local.RS.Channel1Request)
        //        //{
        //        Thread.Sleep(basicProperties.Local.RS.Channel1Interval * 1000);
        //        await SendGainUSRP(gainWindow.GainUSRP);
        //        var objects = await RequestNewSignal(channelsRecuestCode);

        //        if (!token.IsCancellationRequested)
        //        {
        //            WriteToTableSource_RS(objects.NewObjects, token);
        //        }
        //        //}

        //        //if (basicProperties.Local.RS.Channel2Request)
        //        //{
        //        //    Thread.Sleep(basicProperties.Local.RS.Channel2Interval * 1000);
        //        //    await SendGainUSRP(gainWindow.GainUSRP);
        //        //    var objects = await RequestNewObjectsOnRange5_8();

        //        //    if (!token.IsCancellationRequested)
        //        //    {
        //        //        WriteToTableSource_RS(objects.NewObjects, token); 
        //        //    }
        //        //}

        //        Thread.Sleep(3000);
        //    }
        //}


        //private void WriteToTableSource_RS(NewObject[] objects, CancellationToken token)
        //{
        //    if (objects == null)
        //    { return; }

        //    var listRec = objects.ToList();
        //    var listtemp = new List<TableSource>();

        //    foreach (var item in listRec)
        //    {
        //        if (item.Signal == 0)
        //        {
        //            continue;
        //        }

        //        var ownStation = lJammerStation.Find(x => x.Role == StationRole.Own); //проверять на наличие станции

        //        var source = new TableSource()
        //        {

        //            Type = 3,
        //            TypeRSM = (byte)TypeRSMart.EMPTY,
        //            Track = new ObservableCollection<TableTrack>()
        //            {
        //                new TableTrack()
        //                {
        //                    Time = DateTime.Now,
        //                    BandMHz = item.BandwidthMHz,
        //                    FrequencyMHz = item.FreqMinMHz,
        //                    FrequencyRX = item.FreqMinMHz,
        //                    Bearing = new ObservableCollection<TableJamBearing>(){ new TableJamBearing() { NumJammer = ownStation.Id }  } 

        //                }
        //            }
        //        };

        //        //AddSourceTDF_DB(source);

        //        listtemp.Add(source);

        //    }

        //    if (!token.IsCancellationRequested)
        //    {
        //        InitCurrentSourcesDF2(new ObservableCollection<TableSource>(listtemp), new ObservableCollection<TableJammerStation>(lJammerStation));
        //        RefreshDependenceSource();
        //    }

        //    //SelectToDeleteByRange( , listtemp);
        //    //RefreshDependenceSource();
        //    //SaveSourceTDF_DB();
        //}


        //private void SelectToDeleteByRange(byte rangeCode, List<TableSource> tableSource)
        //{

        //    ObservableCollection<SourceDF> sourcesDF = new ObservableCollection<SourceDF>();
        //    foreach (var source in tableSource)
        //    {
        //        ObservableCollection<TrackPoint> tempTrack = new ObservableCollection<TrackPoint>();
        //        foreach (var track in source.Track)
        //        {
        //            ObservableCollection<BearingItem> tempBearing = new ObservableCollection<BearingItem>();

        //            tempBearing.Add(new BearingItem()
        //            {
        //            });

        //            tempTrack.Add(new TrackPoint()
        //            {
        //                Time = track.Time,
        //                Frequency = track.FrequencyMHz,
        //                Band = track.BandMHz,
        //                Bearings = tempBearing,

        //            }); ; ;
        //        }

        //        sourcesDF.Add(new SourceDF()
        //        {
        //            Track = tempTrack,
        //            TypeRSM = (TypeRSMart)(source.TypeRSM)
        //        });
        //    }

        //    foreach (var s in sourcesDF)
        //        CurrentSourcesDF.Add(s);

        //    if (rangeCode == 2)
        //    {
        //        var listToDelete = CurrentSourcesDF.Where(x => (x.Track[0].Frequency < 2500)).ToList();
        //        foreach (var item in listToDelete)
        //        {
        //            CurrentSourcesDF.Remove(item);
        //        }
        //    }

        //    if (rangeCode == 1)
        //    {
        //        var listToDelete = CurrentSourcesDF.Where(x => (x.Track[0].Frequency > 5700)).ToList();
        //        foreach (var item in listToDelete)
        //        {
        //            CurrentSourcesDF.Remove(item);
        //        }
        //    }

        //}



    }
} 