﻿
using RecognitionSystemProtocol;
using RecognitionSystemProtocol2;
using System.Windows;
using UIRsDetecting;


namespace GrozaS_AWS
{
    public partial class MainWindow : Window
    {
        RecognitionSystemClient2 clientRS;
        private byte channelsRecuestCode = 255;

        private void ConnectRS()
        {
            try
            {
                clientRS = new RecognitionSystemClient2();
                clientRS.OnConnected += ClientRS_IsConnected;
                
                clientRS.IQConnectToServer(mainWindowViewModel.LocalPropertiesVM.RS.IpAddress, mainWindowViewModel.LocalPropertiesVM.RS.Port);
            }
            
            catch { }
        }

        

        private void DisconnectRS()
        {
            try
            {
                clientRS.DisconnectFromServer();

                clientRS.OnConnected -= ClientRS_IsConnected;
                
                clientRS = null;

                mainWindowViewModel.ErrorRS = string.Empty;
            }
            
            catch{ }
        }

    }
}
